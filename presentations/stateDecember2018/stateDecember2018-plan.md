# Cahier des charges: Application monitoring via SSH

- Contexte : Petit lot de serveurs tels que ceux de l'université de Liège
  - Exemple : Vérifier le week-end si telle ou telle tâche fonctionne toujours
  - L'use case se porte sur des serveurs qui sont déjà gérés par SSH
    - L'utilisateur utilise SSH par choix (en considération de la sécurité et/ou des risques)
- Alerte lors d'événements particuliers

# Travail réalisé

## Implémentation

- Connexion de l'app
- Détection version python la plus haute
  - Utilise cette version pour créer un venv
- Envoie les scripts python
  - Appelle les scripts lorsque nécessaire pour avoir l'état de la machine
- Notifie lorsque certains paramètres dépassent un certain pourcentage
- Interface en cours de finition
  - Bug en cours de résolution

### À propos des dernières considérations

- Polling régulier = activité du résumé au premier plan
- Arrière-plan = polling plus basse fréquence pour les alertes

## À réaliser

- Tests de performances
- Proxying/Hopping

## Rédaction

### En partie réalisé

- Introduction
- Contexte / Établissement du problème
- État de l'art
- Solution

### Points à rajouter

- Détail du fonctionnement et de la justification des choix effectués
- Section auto-critique
- Section difficultés rencontrées


## Difficultés rencontrées

- Binding python dépendants de la distribution
  - Tri des versions pour utiliser la plus grande version.
    - 3.3 nécessaire pour les venv
- Python = nouveau langage
  - Certains idiomes sont bien différents de java
- Bugs développement android