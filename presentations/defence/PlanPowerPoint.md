# Sshatellite

## Introduction

- Besoins [de monitoring diverses]
- [On se limite aux] Petites Organisations
	- ULiège
		- Serveurs de laboratoire
			[- Tâches de longue durée]
		- Serveurs de production
	- ~~SNMP~~
	- SSH [Déjà présent]
	
## Buts

- Monitoring
	- Alertes
- Sécurité
	[Bien qu'SSH déjà bien sécurisé en lui-même]
	- Failles d'SSH
- Stabilité
- Extensibilité
- Prototype

- Backend
	- Scripts `shell` ?
	- Scripts Python !
		- Suffisamment répandu
		- Utilisation de `venv`
			- => Librairies installables
			- `psutil`

- Frontend
	- Modèle flexible
		- Prêt pour le futur
	- Implémentations abstraites du coeur de l'application
		- Ne voit tout en amont
	- UI
		- Groupes
			- Résumé serveur
				- Résumé des groupes de paramètres

- Android
	- Mécanismes d'optimisation de la batterie
		- Mode Doze
		- App Standby
			- App Standby Buckets

- Communications
	- Chargement des paramètres d'identification
		- Clef privée
		- Clef publique
	- Initialisation de la connexion
		- gzip
	- Installation du backend [Si nécessaire]
		- Détection de l'exécutable python[TM]
			[Pas toujours le même nom, pas toujours à la même place]
			[Exemple développeur Python]
	- Communications frontend-backend
		[Diagrammes]
		- Avant-plan
			- Appel avec fréquence élevée
		- Arrière-plan
			- Appels regroupés à fréquence basse
			- Limite d'un nombre de connexions maximums
			- Durée maximum
			
- Sécurité
	- Préoccupations de sécurité
		- Bruteforce
		- Attaques par déni de service
		- Analyses du trafic
		- Négligence
		- failles de l'implémentation
	- Interdire / Éviter les connexions par mot de passe
  		- Rotation des paires de clefs
  - Port knocking
  - SSH Hopping
	- Modèle de menace
		[Importance de l'information à protéger, étudier les potentielles portes ouvertes]
	- Veille sécuritaire / Audits / Formations
		[Faille des libs/implémentations]
	- Établir un nombre maximum d'essais de connexions
		- -> Fail2Ban
		- IDS/IPS
	- Restrictions / isolations
	- Gestionnaire de clefs publiques
	- HSM / KMS

(Travail futur)
	- Agents externe SSH (+ distribué)
	- Pré-filtrage logs
	- Solution de rechange de Python
	- Étendre 
			
# Plus tard

- Bit SUID - psutil

# Possibles questions

- Menace des "canaux secrets", pourquoi pas suffisamment dangereux ?
- `venv` avant py3.4?
- Quoi de réflexif/master ?
- ReactiveX? D:
- Temps d'une fenêtre d'action ou autre ?
- HSM ?
- VPN
- Comparaison avec les autres gros logiciels