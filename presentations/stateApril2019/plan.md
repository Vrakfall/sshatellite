# Scenario

- Chercheur ULiège effectue des expérimentations fonctionnant sur les serveurs de l'université
  - Longueur des expérimentations -> Gestion hors-heures avec gsm
  - Plusieurs tâches rapides à effectuer: stop, kill, pause, resume, etc..

# Buts / Fonctionnalités principales

- Facilité d'utilisation
  - Pour une application utile pour les chercheurs
  - Focus sur la gestion des expérimentations plus que sur celle des serveurs
- Gestion des processus
  - Possibilité d'ajouts en favoris
    - Gardés en mémoire même s'ils s'arrêtent
  - Gestion de groupe de processus enfants/parents
  - Gestion des fichiers input/output/logs des processus
    - Consultation + alerte regex du contenu
  - Manipulation du/des processus (redémarrer, etc)
    - Actions de base
    - Actions personnalisables configurées par l'utilisateur
- Différentes vues résumant plusieurs catégories de paramètres
  - Vue des processus favoris
    - Liste des processus favoris
      - Ceux en erreur d'abord
      - Les autres
    - Triable par paramètre désiré
    - Recherche de processus (nom, pid, etc)
    - Ajout aux favoris
    - Clic -> Menu rapide apparaît sous le processus
      - Boutons d'actions rapides (stop, kill, pause/resume, warnings, more)
      - More -> Détails du processus
  - Vue des groupes de serveurs
    - Liste les différents groupe et leurs alertes les plus graves + 1 paramètre rapide (automatique ou choisi par 
    l'utilisateur)
    - S'il n'y a pas d'alerte, affiche quelques paramètres rapides (nombre de serveurs, de processus favoris en 
    fonctionnement, etc)
  - Vue d'un seul groupe
    - Pareil que la liste des groupes mais liste les serveurs
  - Vue d'un serveur
    - Vue résumée des différentes catégories de paramètres
      - Première catégorie -> Processus favoris, en fonctionnement, en erreur
        - Clic -> Vue des processus favoris
      - Idem pour les autres erreurs
      - Autres catégories de paramètres du serveur (CPU, Ram, logs, etc)
        - Clic -> Vue avec détails complets pour cette catégorie
          - Vue détaillée comprend liste quelques processus triée par utilisation de cette catégorie
    - Liste complète de processus
      - Mêmes actions possibles que liste processus favoris

# Gestion des clefs et mot de passe

- Différents scenarii de connexion
  - Import de clefs existantes
    - Pas besoin de rajouter sa clef sur plusieurs serveurs
    - Moins secure
  - Génération de nouvelles clefs
    - Clef privée directement encryptée
    - Possibilité d'ajouter la clef sur le serveur s'il accepte la connexion par mot de passe
  - Ajout mot de passe
- Stockage des clefs/mot de passe
  - AndroidKeyStore
    - HSM dans le téléphone
      - La clef privée ne peut pas être extraite
      - L'app peut demander des opérations avec la clef avec accord de l'utilisateur
        - Lié au système de blocage du téléphone
        - Demande d'accès sur une période limitée
    - Génération de la clef via AndroidKeyStore
      - Problème librairie
      - Import possible depuis derniers niveau api
    - Création d'une clef pour l'appli
      - Encryption de la pair SSH avec cette clef puis stockage dans SharedPreferences ou stockage privé de 
      l'application (inaccessible par les autres applications si non-compris et pas d'accès root)
      - Même processus utilisable pour les mots de passe

# Améliorations possibles

- Shell
  - Deviner les commandes que l'utilisateur cherche à écrire
    - termux + mosh + fish-shell
- Make it possible that one server sends the backend to other servers and gathers the data from other servers and 
sends it all at once to the application
- Combiner les conditions d'alertes