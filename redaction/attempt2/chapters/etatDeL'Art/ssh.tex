% !TeX spellcheck = fr_FR

\glsreset{SSH}%
\section{\gls{SSH}}%
\label{ssh.etat-de-l'art}

\glsreset{SSH}
<<~\gls{SSH} est un protocole permettant une connexion sécurisée à distance et d'autres services réseaux sécurisés par le biais d'un réseau non-sécurisé~>>\cite[3]{ylonen_secure_2006}. Il permet ainsi d'établir une connexion cryptée à une machine distante via différents algorithmes de cryptage et par le biais de différents mécanismes d'authentification tels que l'authentification par mot de passe ou l'authentification par clef publique.

\subsection{Implémentations \gls{SSH}}%
\label{implémentations-ssh.ssh.etat-de-l'art}

% The following parameter is for what is actually shown in the toc
\subsubsection[OpenSSH]{OpenSSH\footnote{%
		Site officiel d'OpenSSH~: \url{https://www.openssh.com}%
}}%
\label{subsubsec:openssh}

<<~OpenSSH est le principal outil de connexion pour les login à distance avec le protocole SSH~>>\cite{openbsd_openssh_nodate}.
Il s'agit probablement de l'implémentation la plus répandue\footnote{%
	\url{https://ssh-comparison.quendi.de/impls/openssh.html}
}.
Durant tout ce mémoire, il est sous-entendu que cette implémentation est celle installée sur le ou les serveurs à monitorer.
Néanmoins, toutes ces informations devraient être correctes pour toute autre implémentation \gls{SSH} au niveau du serveur car elles doivent répondre aux mêmes standards établis pour le protocole \gls{SSH}.

\subsubsection[SSHJ]{SSHJ\footnote{%
	Site officiel d'SSHJ~: \url{https://github.com/hierynomus/sshj} (Dépôt du code source)%
}}%
\label{subsubsec:sshj}

\emph{SSHJ} est une librairie en \emph{Java}, une \gls{API}, implémentant le protocole \gls{SSH}.
Elle est utilisée dans ce mémoire comme implémentation principale pour la partie client (\gls{frontend}) de l'application.

Elle implémente une majeure partie des algorithmes disponibles pour le protocole \gls{SSH} et est une des seules librairies \emph{Java} actuellement encore maintenues.

\subsection{Préoccupations de sécurité \textemdash{} Attaques et failles potentielles}%
\label{préoccupations-de-sécurité.ssh.etat-de-l'art}

Pour établir un système de prévention contre les attaques éventuelles et failles inhérentes au protocole, une liste non-exhaustive\footnote{%
	\hyperref[veille.securisation.serveur:subsubsec]{Car un nouveau type d'attaque peut toujours être trouvé à tout moment.}
}
de celles-ci est dressée.

\citeauthor{barrett_ssh_2001} ont établi une liste de <<~menaces qu'\gls{SSH} peut contrer~>>\cite[Section 3.10, p.90-92]{barrett_ssh_2001} ainsi qu'une liste des <<~menaces qu'[il] ne prévient pas~>>\cite[Section 3.11, p.92-95]{barrett_ssh_2001}.
Celle-ci est reprise ci-dessous, à l'exception de la menace des <<~canaux secrets~>>\cite[Section 3.11.4, p.82-83]{barrett_ssh_2001} car elle ne représente pas de danger pour le cas d'utilisation de l'application dans le cadre de l'université de Liège.
%
En effet, bloquer les communications entre possesseurs de clefs privées n'a pas d'intérêt~: Si un attaquant entre en possession d'une telle clef et est dans la capacité de s'en servir\footnote{
	Si, par exemple, cette clef privée ne possède pas de mot de passe de protection et qu'elle est directement fonctionnelle.
},
le système d'information est de toutes façons déjà compromis à un degré plus élevé. %Dois-je expliquer ceci plus en détails ou cela suffit ?
Certaines failles supplémentaires à cette dernière liste sont aussi considérées.

\subsubsection{Attaques par `Bruteforce'}
\label{bruteforce.attaquesFaillesSSH:subsubsec}

Aussi appelé <<~Password Cracking~>>\cite[Section 3.11.1, p.80]{barrett_ssh_2001}.

\begin{quotationbar}
	Les attaques de type brute force sont utilisées afin de connaître la paire du nom d'utilisateur et du mot de passe d'un compte sur un serveur SSH. L'attaquant tentera alors différents noms d'utilisateur avec différentes combinaisons de mot de passe, les devinant de façon manuelle ou à l'aide d'outils automatisés. Ces derniers contiennent une base de données de noms et mots de passes couramment utilisés.
	
	Lors de ces attaques, sous réserve que la paire du nom d'utilisateur et du mot de passe soit correcte, l'attaquant obtient un shell avec accès au système de fichiers du serveur. Dès lors, l'attaquant aurait la possibilité d'entreprendre des activités malveillantes \cite[Section 1, p.1]{sadasivam_classification_2016}. %TODO gls
\end{quotationbar}

Ce type d'attaque est la plus répandue parmi celles tentées par les attaquants~\cite[Table 1, p
.3]{sadasivam_classification_2016}.
Si cette attaque est accomplie avec succès, l'attaquant obtient tous les accès que possède l'utilisateur auquel il réussit à se connecter.
En fonction de ces accès, le système est alors plus ou moins sévèrement compromis.

\subsubsection{Attaques par déni de service}
\label{DoS.attaquesFaillesSSH:subsubsec}
%Seule protection: Layer 2 encryption

\begin{quotationbar}
	\gls{SSH}, opèrant par-dessus le protocole \gls{TCP}, est vulnérable aux attaques visant les faiblesses existantes au sein du protocole \gls{TCP} et \gls{IP}. La confidentialité, l'intégrité et les garanties d'authentification limitent cette vulnérabilité aux attaques par déni de service \cite[Section 3.11.2, p.81]{barrett_ssh_2001}.
\end{quotationbar}

Une attaque par déni de service comprend tout type d'attaque tentant de rendre un service indisponible.
Plusieurs techniques sont possibles pour parvenir à ce résultat.
L'une d'entre elles est d'inonder le serveur ciblé de paquet \gls{TCP}
de type \emph{SYN} afin que la pile \gls{TCP} de celui-ci soit pleine et qu'il ne puisse plus accepter de connexion légitime \cite[Section 3.11.2, p.81]{barrett_ssh_2001}.
Certaines manipulations permettent aussi de déconnecter des connexions en cours \cite[Section 3.11.2, p.81]{barrett_ssh_2001}.

Cette attaque ne compromet pas, en elle-même, la sécurité du système d'information de l'organisation ou du particulier employant l'application.
Néanmoins, elle peut entraîner une indisponibilité du service \gls{SSH}, rendant tout monitoring impossible ou déclenchant une fausse alarme décrivant le serveur à monitorer comme étant indisponible alors qu'il n'est pas impossible que seul le protocole \gls{SSH} soit impacté.

<<~Parce que ces menaces se concentrent sur des problèmes liés au protocole TCP/IP, elles ne peuvent être contrées que par des techniques réseau de plus bas niveau, telles que le chiffrement des liaisons matériels ou \acrshort{IPSEC}~>> \cite[Section 3.11.2, p.82]{barrett_ssh_2001}.

\subsubsection{Analyses du trafic} % Dois-je citer pour si peu de mots ? D:
\label{analyseTrafic.attaquesFaillesSSH:subsubsec}

L'analyse du trafic ne constitue par vraiment une faille en soi mais~:

\begin{quotationbar}
	Même lorsque l'attaquant ne parvient pas à lire votre trafic réseau, il peut glaner un grand nombre d'informations utiles simplement en les observant - prenant note de la quantité de données, des adresses source et destination, ainsi que de la synchronisation. Une augmentation soudaine du trafic avec une autre entreprise pourrait l'avertir qu'un imminent contrat d'affaire est sur le point d'être conclu. Les patterns de trafic peuvent également indiqués des horaires de backup ou des moments de la journée plus vulnérables aux attaques par déni de service. Des silences prolongés sur une connexion \glsunset{SSH}\gls{SSH} depuis le poste de travail d'un administrateur système suggère que cette même connexion est dépassée et que le moment est idéal pour s'introduire, que ce soit de manière électronique ou physique \cite[Section 3.11.23, p.82]{barrett_ssh_2001}.
\end{quotationbar}

Cela reste donc un outil permettant à l'attaquant de mieux comprendre le système d'information qu'il cherche à attaquer.

\subsubsection{Négligence}
\label{negligence.attaquesFaillesSSH:subsubsec}

%Vol, perte de clef, 

\begin{quotationbar}
	À ma grande surprise, j'ai découvert que les points faibles n'avaient rien à voir avec les mathématiques. Ils se situaient au niveau du matériel, du logiciel, des réseaux, et des gens. De magnifiques mathématiques ont perdu toute pertinence dû à de la mauvaise programmation, un système d'exploitation mauvais, ou un mauvais choix de mot de passe [\dots]
	``La sécurité est une chaîne ; elle [l'application] n'est autant sécurisée que son maillon le plus faible'' \cite[Préface, p.xxiv]{schneier_secrets_2015}.
\end{quotationbar}

Le point le plus faible de la sécurité de l'application est donc très probablement l'être humain et donc toutes les personnes touchant de près ou de loin à la réalisation et à l'utilisation de l'application.
Cette chaîne comprend donc, entre autres, l'auteur de ce mémoire, les développeurs de l'implémentation de l'application, le ou les installateurs du ou des serveurs monitorés ainsi que de l'application sur le téléphone mobile\footnote{Cela comprend sa configuration.}, les développeurs des différentes \gls{API} utilisées, les développeur de l'implémentation du serveur \gls{SSH} utilisé, les développeurs des différents systèmes d'exploitation utilisés,\dots

Cette liste est longue, ce qui génère un nombre important d'éléments différents pouvant avoir fait une erreur, augmentant donc drastiquement la probabilité qu'une faille soit présente.
Il est donc nécessaire d'imaginer un maximum de façons dont le facteur humain peut intervenir dans les faiblesses du programme et de concevoir un système tentant de contrecarrer celles-ci.

\subsubsection{Failles potentielles de l'implémentation utilisée}
\label{implementation.attaquesFaillesSSH:par}

Les implémentations utilisées ne sont pas à l'abri de bugs ou d'erreurs introduites lors de leur conception, de leur maintenance ou de leur mise à jour. OpenSSH par exemple, l'implémentation serveur d'\gls{SSH} la plus commune sur système \emph{Unix},
a déjà eu des failles de sécurité dans le passé et une nouvelle vient tout juste d'être découverte~:~\cite{noauthor_cve_2018}.
Elle a, cependant, été résolue depuis la version 7.7.\cite{noauthor_nvd_2018}

Il ne s'agit là que des failles ayant déjà été découvertes par des personnes dont les intentions sont de prévenir ce genre de brèche; il n'est cependant pas exclu que d'autres personnes\footnote{Comme un groupement de malfaiteurs agissant sur \emph{Internet}, par exemple.} aient découvert d'autres failles et en profitent sans les divulguer.
Ce genre de scénario n'est toutefois pas forcément voué à rester dans le secret.
En effet, une organisation subissant une attaque inconnue pourrait se rendre compte d'une intrusion, si elle s'en est préalablement donné les moyens, et enquêter sur la raison de cette infraction\footnote{Avec l'aide d'experts de sécurité, par exemple.}.
























