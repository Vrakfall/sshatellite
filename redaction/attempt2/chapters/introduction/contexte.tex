% !TeX spellcheck = fr_FR

\section{Contexte}
\label{sec:contexte}

Un responsable d'infrastructure informatique % Formulation correcte ?
est une personne ayant pour fonction le maintien des installations informatiques, que ce soit pour son propre compte ou pour celui d'une entreprise.
Il est parfois amené à concevoir et installer une infrastructure, que ce soit partiellement ou entièrement, mais aussi à s'assurer de la bonne évolution de cette dernière en fonction des besoins du métier~\footnote{
	Le métier correspond aux besoins de l'utilisateur final.
},
et ce généralement sur le long terme.

Outre la conception, l'installation et l'évolution de l'infrastructure, une tâche parmi les plus nécessaires est le monitoring des serveurs, autrement dit s'assurer de leur bon fonctionnement.
Il est en effet important que leur taux de disponibilité tende vers 100\% lorsque les services qu'ils proposent le nécessitent, ce qui est presque toujours le cas, et plus particulièrement lorsqu'il s'agit de services proposés à des utilisateurs extérieurs à l'organisation qui les offre.
Cependant, un mode de fonctionnement continu implique inévitablement des conséquences concernant les pannes potentielles~: La principale étant qu'elles peuvent survenir à tout moment du jour et de la nuit, que ce soit durant les heures de bureau ou non.

Le nombre de ces serveurs d'applications, qu'ils soient \gls{software} ou \gls{hardware}\footnote{
	Un serveur est sous-entendu software lorsqu'il s'agit d'une instance de programme fonctionnant sur une machine physique, partageant généralement les ressources de cette machine avec d'autres instances.
	Les appellations `serveur software', `application-serveur' et `serveur logiques' seront utilisées pour définir ce type de serveur.
	
	Il est sous-entendu hardware (physique) lorsqu'il s'agit de la machine physique en elle-même, faisant fonctionner les instances de serveur software.
	Les appellations `serveur hardware', `machine physique' et `serveur physique' seront utilisées pour définir ce type de serveur.
},
ne cesse de croître. % Citation ?

En effet, cela est en partie dû au fait qu'un bon nombre d'architectures se transforme et passe d'un design dit \gls{monolithique} à un design dit de \glspl{microservice}.
%phrase principale manquante
Plusieurs couches d'une application peuvent ainsi communiquer entre elles.
La logique métier se retrouve donc souvent séparée des données qui sont généralement contenues dans une application base de données et souvent même précédée d'un serveur \gls{frontend} tel qu'\emph{nginx}\footnotemark{} qui sert,
\footnotetext
{
	\url{https://nginx.org} - Site \emph{Internet} d'`nginx'.
}
par exemple, à protéger les serveurs logiques en \gls{backend} ou à faire du \gls{loadbalancing}.
Cela a pour effet d'accroître le nombre d'applications-serveur nécessaires à une architecture.

La quantité grandissante de serveurs à monitorer comporte aussi d'autres conséquences néfastes au bon fonctionnement de l'architecture système~:
Une probabilité nettement plus élevée qu'une ou plusieurs failles surviennent d'une part, et un nombre multiplié de façons différentes pour elles de se produire d'autre part.
Cette quantité est d'autant plus importante que, souvent, un informaticien est amené à gérer différents groupes de serveurs appartenant à différentes organisations.
Il peut très bien devoir gérer des serveurs pour l'entreprise de son emploi principal ainsi que d'autres pour une activité d'indépendant complémentaire, tout en ayant des serveurs personnels qu'il utilise uniquement à des fins privées, possiblement pour proposer des services informatiques au sein de sa famille.

% Pour la question de recherche: C'est pourquoi les gestionnaires de ces serveurs ont particulièrement besoin de pouvoir surveiller ces serveurs à tout moment et de pouvoir être alerté

\subsection{Le cas ULiège}
\label{subsec:le-cas-uliège}

Dans le monde de l'infrastructure informatique, les besoins en monitoring et les exigences encadrant son processus diffèrent énormément suivant l'importance du système et du modèle de menace de la personne ou de l'organisation à qui appartient l'infrastructure.
Considérant que les cas d'utilisation des serveurs sont quasiment aussi variés que les organisations ayant besoin de ces serveurs, les modèles de menace et les exigences en matière de sécurité qui leur sont associés peuvent être profusément différents, voire même antinomiques.

Il est évident qu'il n'est pas possible de répondre à toutes ces possibilités de la même manière, tant elles sont diverses.
C'est pour cette raison que ce mémoire se concentre sur le cas d'utilisation de l'université de Liège\footnote{%
	Site officiel~: \url{https://www.uliege.be}
}
ainsi que toutes les infrastructures similaires.

Ce cas d'utilisation est illustré par l'exemple suivant~:
L'institut du Montéfiore, la section de l'\emph{ULiège} comprenant les sciences informatiques, dispose de plusieurs serveurs de laboratoire que peuvent utiliser chercheurs et étudiants à des fins diverses et variées.
Leurs utilisations les plus fréquentes sont soit d'exécuter des tâches nécessitant une longue période de temps avant de se terminer, soit d'héberger une application-serveur en cours de développement afin de faciliter ce dernier.
Un même serveur peut donc ainsi héberger plusieurs de ces projets simultanément.

La personne désirant avoir accès à ces serveurs doit d'abord effectuer la demande au service informatique de l'université qui le lui octroiera une fois son identité et son autorisation à cet accès vérifiée.
Chaque personne autorisée est ainsi liée à un nom d'utilisateur, ayant ainsi parfois accès à plusieurs de ces serveurs.
Ce mécanisme permet de s'assurer que les personnes accédant légalement à ces serveurs fassent bien partie du cadre de l'université et que leur utilisation de l'infrastructure soit principalement bien intentionnée.

Dans le cas où un chercheur désire exécuter une ou plusieurs tâches nécessitant une longue période de temps, il est intéressant pour lui de pouvoir suivre la progression de cette exécution.
Il n'est en effet pas rare que celles-ci durent plusieurs jours, voire semaines.
Dans un tel cas et dans l'éventualité qu'une anomalie se produise durant le processus, il est attrayant pour le chercheur d'en être alerté, et ce, où qu'il soit.
Cet anomalie peut en effet survenir durant un weekend ou un congé d'une ou plusieurs semaines, ce qui engendrerait un retard conséquent pour l'expérience menée.
Au plus tôt le chercheur est prévenu du problème, au plus tôt il peut agir pour tenter de le régler et relancer l'exécution qui prendra alors un retard moins conséquent.

De plus, cette infrastructure ne dispose pas des ressources suffisantes, que ce soit financières, matérielles ou humaines, pour mettre en place une application-serveur de monitoring habituelle.
Ce genre d'application utilise généralement le protocole \gls{SNMP},
qui n'est initialement pas prévu pour prendre en compte des éléments de sécurité,
et nécessite généralement d'être installé sur un serveur physique différent de ceux à monitorer.
Cela permet ainsi la collecte d'informations sur le long terme, ce qui a pour intérêt de les compiler par la suite pour calculer des statistiques sur ces serveurs, de générer des graphiques résumant leur situation sur une période de temps, etc.

Ces serveurs sont, depuis longtemps déjà, administrés à distance via le protocole \gls{SSH} et leurs utilisateurs peuvent aussi y accéder de la sorte.
De fait, le protocole constitue même, dans certains cas, l'unique accès pour les chercheurs car ces serveurs sont généralement dépourvus d'interface graphique et sont accédés à distance, que ce soit au sein des bâtiments de l'université ou en itinérance.

% Le niveau relatif de sécurité ainsi que les risques ainsi apportés par l'utilisation principale de ce protocole ont donc déjà été considérés => Pour la justification.
