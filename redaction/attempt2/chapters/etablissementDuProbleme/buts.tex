% !TeX spellcheck = fr_FR

\section{Buts}
\label{sec:buts}

L'objectif de ce mémoire est de concevoir le design d'une application dont les buts sont déclinés en cinq axes distincts~:
Le monitoring, la visibilité, la sécurité, la stabilité et l'extensibilité.
Cette application a pour public cible celui cité précédemment, c'est-à-dire celui des administrateurs systèmes désireux de disposer d'une solution de monitoring portable et légère, mais aussi celui des utilisateurs de serveurs voulant s'assurer du bon fonctionnement de tâches de longue durée.

\subsection{Monitoring}
\label{subsec:monitoring}

Au vu du nombre grandissant de serveurs à monitorer, il est de plus en plus intéressant pour un responsable d'architecture de pouvoir être informé de l'état des serveurs et de l'architecture dont il est garant, et ce, à n'importe quel moment.
Il est encore plus intéressant de pouvoir être alerté directement lors d'incidents.

L'application doit donc répondre à ce besoin de plus en plus oppressant de s'assurer qu'un système d'information, dont les possibilités de pannes différentes ne font que grandir, soit continuellement disponible.
Pour ce faire, elle devra permettre de monitorer l'état de serveurs \emph{Unix}.
%Elle se doit d'être flexible afin que ses fonctionnalités puissent être étendues dans le futur en fonction des besoins changeants des organisations ou des particuliers qui seront amenés à l'utiliser.

Ce monitoring devra être effectué à l'aide du protocole \gls{SSH}.
En effet, les protocoles de monitoring habituels tels que \gls{SNMP} ne peuvent être utilisés car le but recherché ici est d'atteindre un nombre minimal d'éléments à installer sur les serveurs à monitorer.

L'application doit aussi alerter l'utilisateur de l'apparition d'anomalies qui pourraient apparaître dans le système d'information, et ce, dès qu'elles sont détectées.
Elle doit ensuite permettre à l'utilisateur de réparer son serveur dans la limite des moyens disponibles avec la plate-forme utilisée.

L'application se veut Open Source, permettant à toute personne le souhaitant de réviser le code, de l'analyser et d'alerter les auteurs en cas de découverte d'une faille ou d'un bug qui pourrait compromettre sa bonne utilisation.
Cela a aussi comme intérêt d'autoriser la venue de nouveaux développeurs qui peuvent ainsi participer activement à l'implémentation, l'amélioration et la sécurisation du projet.

Ce prototype est appelé <<~Sshatellite~>>, comme le mélange entre \gls{SSH} et \emph{satellite} car elle n'est, au final, qu'un élément externe aux serveurs qui les sonde par intermittence.
Elle sera disponible sur le dépôt \url{https://gitlab.com/Vrakfall/sshatellite} une fois l'autorisation obtenue pour la rendre publique, après la défense de ce mémoire si celle-ci est réussie.

\subsection{Visibilité et synthèse des informations}
\label{visibilité-et-synthèse-des-informations.buts}

Un seul serveur physique peut comporter une multitude de paramètres différents.
En plus des paramètres habituels (utilisation du processeur, de la mémoire vive, des disques durs, de la couche réseau, \dots), beaucoup d'autres paramètres sont utiles à monitorer, entre autres~: L'état de fonctionnement de la base de données, l'état des machines virtuelles ou des conteneurs, etc.
Parmi ceux-ci, les journaux d'événements\footnote{Aussi appelés `historiques', `registres' ou même `logs' (en \emph{Anglais})} ont beaucoup d'intérêt pour l'administrateur d'un serveur.
En effet, ces journaux contiennent généralement une description faite par le programme de son déroulement comme l'ensemble des événements qui ont affecté ce dernier, les actions effectuées mais plus particulièrement des informations permettant de comprendre pourquoi un incident est survenu.
Il est donc essentiel de pouvoir utiliser ce dernier type d'information afin d'en alerter efficacement l'utilisateur de l'application.

Cependant, ces journaux peuvent être très longs et contenir beaucoup d'informations.
En plus de cela, tous les autres paramètres doivent être disponibles afin de constituer un rapport complet.
L'application doit subséquemment tenter de les synthétiser du mieux possible afin que l'information la plus pertinente possible soit rapidement visible par l'utilisateur et qu'il ait une bonne vue d'ensemble de l'état actuel du fonctionnement du serveur.

De surcroît, la quantité d'information disponible sera multipliée par le nombre de serveurs que l'utilisateur désire gérer~\footnote{La multiplication n'est pas exacte car chaque serveur peut comporter différents services, générant un nombre différent de journaux d'événements en plus fait qu'ils ont une taille proportionnelle à la durée du fonctionnement du service.}.
Cela implique qu'une gestion pratique d'une quantité préalablement indéfinie de serveurs doit faire partie des caractéristiques de l'application.

\subsection{Sécurité}
\label{subsec:sécurité}

Monitorer des serveurs via une application mobile soulève un bon nombre de problèmes de sécurité.
Quid en cas de perte ou de vol du téléphone ?
Si un tel événement se produisait, la ou les clefs \gls{SSH} pourraient de cette façon se retrouver dans des mains mal intentionnées.

Une autre faille souvent oubliée mais qui reste l'une des plus répandues et des plus dangereuses est l'ingénierie sociale. % Trouver une citation adéquate.
Même sans perte ou vol de l'appareil, les clefs \gls{SSH} restent vulnérables.
En effet, une personne mal intentionnée pourrait avoir préalablement identifié l'utilisateur de l'application comme un responsable de l'architecture système d'une entreprise dont la valeur des informations qu'elle stocke est importante, par exemple, et tenter d'hypocritement sympathiser avec l'utilisateur afin de profiter d'un moment d'inattention ou de pratiquer une supercherie lui permettant d'accéder aux précieuses clefs privées. % Glossaire pour clef privée ?

Le protocole \gls{SSH} en lui-même possède quelques failles connues et certaines attaques sont encore possibles sur des installations mal réalisées~\footnote{
	Ne considérant les bonnes pratiques en la matière~: Par exemple, en gardant les paramètres par défaut d'\gls{SSH} qui permettent de se connecter sans pair de clef privée-publique, uniquement à l'aide du mot de passe de l'utilisateur de la machine hôte.
}
ou mal défendues~\cites{owens_study_2008}{gasser_deeper_2014}[Section 1 \textendash Introduction, p.1]{sadasivam_classification_2016}{venafi_inc._ponemon_2014}{song_timing_2001}{kent_unsecured_2010}.
La \fullref{préoccupations-de-sécurité.ssh.etat-de-l'art} détaillera ces attaques potentielles et les moyens pensés pour s'en défendre seront décrits dans la \fullref{sécurité.solution:sec}.

En outre, l'utilisateur de l'application n'est pas toujours un administrateur ayant tous les droits sur un serveur.
Une entreprise, ou n'importe quelle autre organisation, pourrait très bien vouloir séparer les pouvoirs entre ses employés dans le but d'améliorer la sécurité de son système.
Par exemple, dans une stratégie de sécurité élevée, il est possible qu'un responsable du bon fonctionnement du serveur\footnote{
	Serveur logique, dans cet exemple.
}
d'une certaine application\footnote{
	Suffisamment complexe que pour nécessiter l'attention spécifique de certains employés.
}
ne puisse pas avoir la capacité de modifier les informations des comptes des employés de l'entreprise, ceux-ci se trouvant sur une base de données fonctionnant sur le même serveur physique.
L'utilisation d'\gls{SSH} implique que l'utilisateur de l'application cliente est, par défaut, limité par les droits d'accès de l'utilisateur auquel il se connecte sur la machine hôte\footnote{
	En réalité, certaines techniques existent afin de dissocier certains utilisateurs clients se connectant au même compte sur la machine hôte.
}.
Une attention particulière doit donc être apportée à ce que l'utilisateur de l'application mobile de monitoring soit bel et bien limité aux droits d'accès qui lui sont nécessaires (cfr. \autoref{la-librairie-psutil}).

Autrement dit, l'application doit être pensée en fonction de ces considérations de sécurité afin d'éviter le plus possible qu'elle ne devienne un nouveau point d'entrée potentiel pour les hackers. % TODO: gls?

\subsection{Stabilité}
\label{subsec:stabilité}

La stabilité de l'application est également une caractéristique majeure de l'application.
Monitorer plusieurs serveurs simultanément peut facilement générer une certaine charge sur l'appareil.
D'autant plus que, de par sa nature\footnotemark, un téléphone mobile moyen est généralement moins puissant qu'un ordinateur moyen.

\footnotetext{
	Les téléphones mobiles sont communément conçus de façon à épargner la batterie le plus possible, la plupart avec un processeur de type \gls{ARM}
	utilisé afin de troquer de la puissance de calcul contre une consommation électrique plus basse.
}

Au demeurant, une application s'interrompant de façon inattendue serait contre-productive.
L'utilisateur pourrait s'habituer aux alertes systématiques de l'application en cas de problème et procéder ensuite à une vérification manuelle, autrement moins régulière que s'il n'utilisait pas l'application.
Une discontinuité du fonctionnement de l'application pourrait, dans ce cas, occasionner un déficit ou une défaillance au niveau des alertes reçues par l'utilisateur.
Ce dernier ne serait alors alerté qu'en cas de vérification manuelle régulière, et ce, avec un retard possible~: Cela serait contre-productif et réduirait considérablement l'intérêt de l'application.

\subsection{Extensibilité}
\label{subsec:extensibilité}

Concevoir l'application en prévoyant une possible extension paraît primordial. Cela permettrait, par exemple, de pouvoir facilement l'adapter par la suite à d'autres plate-formes.
Elle pourrait ainsi, éventuellement, être adaptée pour monitorer des serveurs Windows ou pour fonctionner sur un système d'exploitation d'un téléphone différent.

Un autre intérêt est de pouvoir facilement changer les librairies utilisées, particulièrement celle permettant la communication via le protocole \gls{SSH}.
Dans l'éventualité où celle-ci n'est plus maintenue à jour ou que de failles de sécurité critiques lui sont découvertes, il serait ainsi aisé de l'échanger avec une autre ne posant pas ces problèmes.
Dans un tel cas, il suffirait donc de ré-écrire les parties du programme qui implémente les fonctions génériques utilisées dans d'autres parties.
Le reste de la logique du programme peut ainsi aisément être gardée.

Cette extensibilité est aussi intéressante car elle permet, après l'achèvement de ce mémoire, de pouvoir continuer l'application et lui apporter de nouvelles fonctionnalités afin qu'elle soit toujours plus complète.