% !TeX spellcheck = fr_FR

\section{Sécurité}
\label{sécurité.solution:sec}

La sécurisation du système d'information est une des préoccupations les plus importantes dès qu'il s'agit d'établir des communications entre plusieurs parties de celui-ci.
En effet, la plupart de ces communications s'effectuent au moyen d'un médium qui peut généralement être utilisé par une tierce partie dans le but de récupérer l'information à laquelle il n'aurait normalement pas accès ou pour causer des ravages sur le système d'information, que ce soit dans le but de menacer, de faire chanter, de perturber ou autre.

Dans le but de sécuriser le mieux possible ces communications, dans les limites des restrictions techniques, une série de contre-mesures préventives est listée ci-dessous.
Cette liste n'est pas exhaustive quant à la totalité des mesures possibles mais elle tente néanmoins de s'approcher au mieux de ce qui peut être réalisé.
La plupart de ces mesures ne sont pas directement intégrables dans l'implémentation de l'application mais font néanmoins partie d'éléments du design de l'architecture du système d'informations qui doivent absolument être pris en considération par l'administrateur dudit système, en fonction du système de menace correspondant à celui-ci.
Ne pas considérer et ne pas mettre en \oe{}uvre un de ces éléments constitue une faille potentielle de sécurité.

\subsection{Modèle de menace}
\label{modeleDeMenace}

La quantité et la sévérité des mesures à prendre afin de sécuriser l'utilisation dépend entièrement du modèle de menace établi par l'entreprise désirant monitorer ses serveurs ou par tout autre particulier ou organisation désirant atteindre ce but.

Ce modèle de menace est important à établir car il << [\dots] est la clef d'une défense concentrée.
Sans modèles de menace, on jouera continuellement au jeu de la taupe >> \cite[Introduction, p.xxiii]{shostack_threat_2014}.
Il permet de << trouver rapidement les bugs de sécurité >>, << comprendre les exigences de sécurité >>, << réaliser et délivrer de meilleurs produits >> et de << traiter les questions qui ne le sont pas par d'autres techniques >>.\cite[Introduction, p.xxiii-xxiv]{shostack_threat_2014}

Comme il dépend principalement de l'organisation ou du particulier qui utilisera l'application, les différents moyens
de protection décrits dans cette section sont accompagnés d'une évaluation rapide du modèle de menace pour lequel ils sont pertinents.

\subsection{Sécurisation du serveur}
\label{securisation.serveur:subsec}

La sécurisation du serveur est un des éléments permettant de renforcer cette sécurité.
Bien qu'il ne s'agisse pas directement de la conception de l'application, son intérêt est évident~:
L'application n'étant <<~autant sécurisée que son maillon le plus faible~>> \cite[Preface, p.xxiv]{schneier_secrets_2015}, la sécurisation du serveur \gls{SSH} renforce la sécurité de l'ensemble du système d'information.

\subsubsection{Veille sécuritaire}
\label{veille.securisation.serveur:subsubsec}

La veille sécuritaire fait partie des éléments les plus importants de tout le processus de sécurisation d'un système d'information~:
Il s'agit de rester au courant de l'évolution de la situation quant à la sécurité des technologies utilisées.
Il est donc impératif de réaliser des vérifications régulièrement, voire de s'abonner aux différents services répertoriant les failles découvertes dans un logiciel ou dans un matériel employé.
L'un des services plus connus est celui des \gls{cve}\footnote{
	Site officiel des \gls{cve}~: \url{https://cve.mitre.org/index.html}
}.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \nameref{negligence.attaquesFaillesSSH:subsubsec} (\nameref{implementation.attaquesFaillesSSH:par})
	\item [Modèle de menace] Tous
\end{labeling}

\subsubsection{Interdire les connexions par mot de passe}
\label{subsubsec:mdp.securisation.serveur}

L'authentification par mot de passe simple est la méthode de connexion la plus faible
et la plus attaquée~\cite[Table 1, p.3]{sadasivam_classification_2016}
du protocole \gls{SSH}.
Il est alors conseillé soit de refuser cette méthode de connexion afin de favoriser la méthode par association de clef privée et clef publique\footnote{
	Dont la sécurisation sera détaillée en \fullref{pubkeyManagers.securisation.serveur:subsubsec},
	 \fullref{privkeyPassword.securisation.client:subsubsec}, \fullref{rotation.securisation.client:subsubsec} et \fullref{hsm-kms.securisation.client:subsubsec}.
}
ou de limiter son utilisation à des mots de passe forts, c'est-à-dire de minimum 16 caractères~\footnote{%
	Ce nombre est estimé. La valeur réelle évolue proportionnellement à la puissance de calcul disponible au grand publique.
}, comprenant des minuscules, majuscules, chiffres et caractères spéciaux.

Néanmoins, dans un soucis de compatibilité, l'application n'interdit pas se mode de connexion.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \nameref{bruteforce.attaquesFaillesSSH:subsubsec} et \nameref{negligence.attaquesFaillesSSH:subsubsec}
	\item [Modèle de menace] Mot de passe fort~: Tous. Authentification par mot de passe simple interdite~: Moyen et +
\end{labeling}

\subsubsection{Établir un nombre maximum d'essais de connexions}
\label{nombreEssais.securisation.serveur:subsubsec}

Au vu de la nature extrêmement répétitive des tentatives de connexion via une attaque de type \hyperref[bruteforce.attaquesFaillesSSH:subsubsec]{\emph{Bruteforce}}, il est intéressant de limiter le nombre de tentatives de connexion que peut établir une personne tentant de se connecter.

L'option \emph{MaxAuthTries}\footnote{\url{https://jlk.fjfi.cvut.cz/arch/manpages/man/core/openssh/sshd_config.5.en\#MaxAuthTries}}
des options de configuration du serveur \gls{SSH} permet de contrôler le nombre de tentatives erronées pouvant être effectuées avant que le serveur ne coupe la connexion.
Cependant, cette option est insuffisante car, une fois la connexion coupée, l'attaquant peut de nouveau tenter de se connecter afin, par exemple, d'essayer d'autres mots de passe.

Afin de rendre cet effet plus définitif ou en vigueur au minimum pendant une durée définie, il est conseillé de combiner ce mécanisme avec un logiciel de type \gls{ips} tel que \emph{Fail2ban}\cite{jaquier_fail2ban:_2018}.
Il <<[\dots] scanne les journaux du système comme \path{/var/log/auth.log} et bannit les adresses \gls{IP} conduisant à de trop nombreux échecs de tentatives de connexion.
Pour ce faire, il met à jour les règles du pare-feu du système dans le but de rejeter les nouvelles connexions par ces adresses \gls{IP}, pour un laps de temps configurable >> \cite{jaquier_fail2ban:_2018}.

Si le serveur est configuré pour ne pas autoriser les connexions via mot de passe simple, cette méthode sert tout de même à alléger le serveur puisqu'il bannira les hôtes qui n'avaient d'autre but que de compromettre le système d'information.
Il est préférable que ceux-ci soient bannis même s'ils ne tentent pas de connexion en utilisant la bonne méthode car il n'est pas impossible qu'ils tentent une autre méthode par la suite;
il a déjà été établi que leurs intentions sont néfastes.

Il est aussi recommandé de définir un temps de bannissement ainsi que certaines adresses \gls{IP}
ne pouvant être bannies afin d'éviter une situation de lockdown %TODO: Glossaire
du serveur.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \nameref{bruteforce.attaquesFaillesSSH:subsubsec} et \nameref{negligence.attaquesFaillesSSH:subsubsec}
	\item [Modèle de menace] Si authentification par mot de passe simple ou pour modèle de menace `Moyen et +`
\end{labeling}

\subsubsection{Restrictions}
\label{restrictions.securisation.serveur:subsubsec}

Il peut être utile de limiter les commandes auxquelles le responsable du monitoring aura accès via l'utilisateur de la machine hôte.
Le choix de ces limitations est laissé à l'organisation ou au particulier qui se destine à utiliser l'application car celles-ci dépendent des droits d'accès définis, par exemple, aux employés au sein d'une organisation.
Néanmoins, la \fullref{la-librairie-psutil} liste les restrictions nécessaires au bon fonctionnement de cette librairie.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \nameref{negligence.attaquesFaillesSSH:subsubsec} (\nameref{implementation.attaquesFaillesSSH:par})
	\item [Permet] Séparation des pouvoirs dans une organisation.
	\item [Modèle de menace] En fonction de l'installation
\end{labeling}

\subsubsection{Isolation}
\label{isolation.securisation.serveur:subsubsec}

L'isolation permet de confiner l'utilisateur avec un mécanisme de conteneur tel que \emph{Chroot} ou \emph{LXC} de \emph{Linux} ou les \emph{jails} de \emph{BSD}.
Tout comme \hyperref[restrictions.securisation.serveur:subsubsec]{la méthode précédente}, cette technique permet de confiner l'utilisateur mais apporte un niveau de sécurité supplémentaire.

Cette technique peut être utilisée afin de contrôler un utilisateur qui ne peut que monitorer un serveur.
Elle se révèle, par contre, peu pratique si l'utilisateur est amené à réparer le système en cas de dysfonctionnement.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \nameref{negligence.attaquesFaillesSSH:subsubsec} (\nameref{implementation.attaquesFaillesSSH:par})
	\item [Permet] Séparation des pouvoirs dans une organisation.
	\item [Modèle de menace] En fonction de l'installation
\end{labeling}

\subsubsection{Gestionnaire de clefs publiques}
\label{pubkeyManagers.securisation.serveur:subsubsec}

Un gestionnaire de clefs publiques, tel que Keybox~\footnote{
	Site officiel~: \url{https://www.sshkeybox.com}
}
ou KeyManagerPlus~\footnote{%
	Site officiel~: \url{https://www.keymanagerplus.com}
},
peut être utilisé afin de contrôler plus efficacement quel \emph{employé} peut accéder à quel serveur.
Ce type de logiciel permet de prendre en compte les considérations de \citeauthor{venafi_inc._ponemon_2014}\cite{venafi_inc._ponemon_2014} et \citeauthor{kent_unsecured_2010}\cite{kent_unsecured_2010}.

Une meilleure gestion des clefs publiques permet de diminuer le nombre de paires de clefs qui pourraient potentiellement un jour se retrouver dans les mains d'une personne mal intentionnée.
Dès lors qu'un employé ne fait plus partie de l'organisation, grâce à l'utilisation de ce type de logiciel, il est facile de révoquer la ou les clefs publiques que celui-ci utilisait pour se connecter aux serveurs.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \nameref{negligence.attaquesFaillesSSH:subsubsec}
	\item [Permet] Séparation des pouvoirs dans une organisation.
	\item [Modèle de menace] Moyen et +
\end{labeling}

\subsubsection{Systèmes de détection et de protection d'intrusion}
\label{ids.securisation.serveur:subsubsec}

Les \gls{ids} et les \gls{ips} permettent, dans certains cas, de détecter et de protéger un système d'information en cas d'intrusion. Une utilisation concrète est décrite en \hyperref[nombreEssais.securisation.serveur:subsubsec]{\autoref*{nombreEssais.securisation.serveur:subsubsec} \textendash \nameref*{nombreEssais.securisation.serveur:subsubsec}}.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \nameref{bruteforce.attaquesFaillesSSH:subsubsec} et \nameref{negligence.attaquesFaillesSSH:subsubsec} (\nameref{implementation.attaquesFaillesSSH:par})
	\item [Modèle de menace] Si authentification par mot de passe simple ou pour modèle de menace `Moyen et +`
\end{labeling}

\subsubsection{Port knocking}
\label{portKnocking.securisation.serveur:subsubsec}

Le port knocking permet d'ajouter une couche supplémentaire au serveur \gls{SSH}, rendant la tâche de découverte du serveur plus ardue \cites{krzywinski_port_2005}{krzywinski_portknocking_2018}.
L'application devra alors, afin de se connecter correctement, envoyer un message à certains ports de l'interface réseau publique du serveur\footnote{
	Il peut s'agir ici de l'interface \gls{WAN}
	du pare-feu protégeant le serveur, celui-ci n'étant pas forcément la même machine physique que le serveur.
}
dans un ordre bien précis. Ce n'est qu'une fois cette séquence correctement effectuée que l'application accède au serveur désiré.

Cette technique ne protège cependant pas directement le serveur car ce \emph{port knocking} de la machine est visible en clair par toute personne interceptant les paquets réseaux.
Nonobstant cela, elle permet d'obscurcir le processus correct de connexion et permet de ralentir les intrusions et d'empêcher celles commises par ceux ne possédant pas les moyens ou compétences pour l'identifier et la contourner.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \hyperref[analyseTrafic.attaquesFaillesSSH:subsubsec]{\nameref*{analyseTrafic.attaquesFaillesSSH:subsubsec}}
	\item [Modèle de menace] Moyen et +
\end{labeling}

\subsubsection{\gls{SSH} hopping}
\label{portForwarding}

Le \gls{SSH} hopping est un technique qui consiste à se connecter à un serveur en \gls{SSH} via une ou plusieurs autres passerelles.
Il s'agit techniquement d'un \emph{proxying} via ce protocole.
L'avantage réside dans le fait que le serveur ne doit pas être directement en contact avec les réseaux extérieurs et qu'il est ainsi plus facile de concentrer toutes les mesures de sécurité sur le premier serveur.
Cette technique est même parfois nécessaire lorsqu'il s'agit d'une architecture dont certaines des machines à
monitorer ne peuvent pas être directement connectées à \emph{Internet} soit pour des raisons techniques,
sécuritaires, physiques, ou autres.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \hyperref[DoS.attaquesFaillesSSH:subsubsec]{\nameref*{DoS.attaquesFaillesSSH:subsubsec}} et certaines \hyperref[negligence.attaquesFaillesSSH:subsubsec]{Négligences}
	\item [Modèle de menace] Moyen et +
\end{labeling}

\subsubsection{Audits}
\label{audits.securisation.serveur:subsubsec}

Toute entreprise se doit de faire régulièrement un audit de la sécurité de son système d'information par des experts en sécurité informatique ou par une organisation agréée afin d'avoir un regard critique et extérieur de ses installations.
Cette méthode est coûteuse mais permet souvent de mettre en évidence certaines négligences qu'il n'est pas évident de notifier en faisant partie intégrante du système d'information.

Une autre possibilité est d'engager des \emph{pentesters}, %TODO: Glossaire
sous couvert d'un contrat protégeant l'organisation, qui tenteront de s'introduire dans le système de façon imprévue, et ce, afin de pouvoir rapporter d'éventuelles failles découvertes lors de cette expérience.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \nameref{negligence.attaquesFaillesSSH:subsubsec} (\nameref{implementation.attaquesFaillesSSH:par})
	\item [Modèle de menace] Élevé
\end{labeling}

\subsection{Sécurisation du client}
\label{securisation.client:subsec}

La sécurisation de l'application cliente est aussi un élément important à consolider.
En effet, il s'agit d'un des éléments les plus faibles de la << chaîne de sécurité >> car il constitue une porte ouverte vers un système d'information potentiellement sensible.
Les points suivants participent donc à tenter de fermer cette porte le plus possible afin que seules les personnes concernées puissent s'en servir.

\subsubsection{Veille sécuritaire}
\label{veille.securisation.client:subsubsec}

%SSHJ, SpongyCastle,...

Comme dans la \hyperref[veille.securisation.serveur:subsubsec]{section \emph{\nameref*{veille.securisation.serveur:subsubsec}} pour le serveur},
la veille sécuritaire est aussi importante pour l'implémentation de l'application.
Les développeurs de celle-ci doivent se tenir informés des potentielles vulnérabilités de leur propre implémentation ainsi que celles des \gls{API} utilisées.
Ils devront d'autant plus surveiller celle qui implémente le protocole \gls{SSH} car il s'agit là de l'élément sécurisant la connexion.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \nameref{negligence.attaquesFaillesSSH:subsubsec} (dont \nameref{implementation.attaquesFaillesSSH:par})
	\item [Modèle de menace] Tous
\end{labeling}

\subsubsection{Encryptage de la clef privée avec un mot de passe fort}
\label{privkeyPassword.securisation.client:subsubsec}

En cas de perte ou de vol de la clef privée, dans le cas où elle serait stockée sur le téléphone mobile de l'utilisateur, ou tout simplement si un attaquant parvient à obtenir la clef privée de n'importe quelle façon, l'application, et donc tout le système d'information monitoré, peut être compromis.
Afin d'essayer d'endiguer ce problème, une méthode simple est applicable~:
L'utilisateur doit crypter sa clef privée à l'aide d'un mot de passe fort.\footnote{
	Cette méthode est facilitée à la création de la pair de clefs privée\textendash{}publique~:
	Il y est proposé à l'utilisateur d'entrer un mot de passe.
	Il lui est toujours possible de le changer par la suite ou d'en ajouter un s'il ne l'avait pas déjà fait.
}

Celle-ci permet de ralentir la progression du voleur dans sa tentative d'intrusion dans le système.
Plus le mot de passe est fort\footnote{
	Plus un mot de passe comprend un grand nombre de caractères différents ainsi que plusieurs types de caractères différents plus il est dit fort.
	Un type de caractère peut être une lettre minuscule, une lettre majuscule, un chiffre, un caractère spécial tel que `\#', un caractère \gls{ASCII}
	étendu comme `†', \dots
},
plus il prendra du temps à être trouvé via une attaque de type \hyperref[bruteforce.attaquesFaillesSSH:subsubsec]{\emph{Bruteforce}},
à moins qu'il ne fasse partie d'un dictionnaire de mots couramment utilisés~\cite{owens_study_2008}.
Cela laisse donc le temps à l'utilisateur ou à l'organisation possédant le système d'information de s'apercevoir que la clef a disparu ou que quelqu'un ait pu s'en emparer et de la supprimer du fichier des paires de clefs autorisées, le tout sans que ledit système ne soit compromis.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \nameref{negligence.attaquesFaillesSSH:subsubsec}
	\item [Modèle de menace] Tous
\end{labeling}

\subsubsection{Rotation des paires de clefs}
\label{rotation.securisation.client:subsubsec}

Si l'on omet le fait qu'il est possible d'outrepasser la sécurité d'une paire de clefs privée\textendash{}publique, bien que nécessitant temps et puissance de calcul\footnote{
	Le temps nécessaire pour cela dépend de la taille des de clefs utilisées.
},
le système reste sensible à un potentiel vol de clef non-identifié.
En effet, un attaquant pourrait être en possession d'une clef privée protégée par un mot de passe et pourrait continuellement tenter de s'introduire dans le système\footnote{En tentant une attaque par `\hyperref[bruteforce.attaquesFaillesSSH:subsubsec]{Bruteforce}', par exemple.}, même si cela lui prendra des années.

La technique de rotation des clefs permet de contrer cela.
Elle consiste au simple fait de changer les paires de clefs privée\textendash{}publique de tous les utilisateurs à une fréquence définie par l'organisation possédant le système d'information.
Par exemple, cette technique fait partie intégrante du fonctionnement des certificats \gls{SSL}
utilisés dans le fonctionnement du protocole \gls{HTTPS}.
Ceux-ci périment au bout d'une période définie par l'émetteur du certificat.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \nameref{bruteforce.attaquesFaillesSSH:subsubsec} et \nameref{negligence.attaquesFaillesSSH:subsubsec} (dont \nameref{implementation.attaquesFaillesSSH:par})
	\item [Modèle de menace] Tous
\end{labeling}

\subsubsection{Intégration avec un \emph{HSM} ou un \emph{KMS}}
\label{hsm-kms.securisation.client:subsubsec}

Une technique permet de ne pas devoir stocker de clef privée sur le téléphone mobile faisant fonctionner l'application ou de la stocker de façon cryptée.
Celle-ci consiste en l'utilisation d'une \gls{KEK}
extérieure à l'application, cryptant la clef privée de l'utilisateur via un service aussi extérieur à l'application.
Les conceptions de deux mécanismes utilisant cette \gls{KEK} sont décrits dans la \fullref{intégration-d'un-ou-plusieurs-agents-ssh.travailFutur}.

\begin{labeling}{Modèle de menace}
	\item [Failles contrées] \nameref{bruteforce.attaquesFaillesSSH:subsubsec} et \nameref{negligence.attaquesFaillesSSH:subsubsec} (dont \nameref{implementation.attaquesFaillesSSH:par})
	\item [Modèle de menace] Élevé
\end{labeling}

\subsection{Formation du responsable de monitoring}
\label{formation.responsable:subsec}

Étant donné que \hyperref[negligence.attaquesFaillesSSH:subsubsec]{la plupart des brèches de sécurité proviennent des utilisateurs},
il est aussi important de les former en profondeur afin qu'ils considèrent l'ampleur des responsabilités de la possession de clefs privées les reliant directement à leur(s) serveur(s) ou à ceux de l'organisation dont ils font partie.