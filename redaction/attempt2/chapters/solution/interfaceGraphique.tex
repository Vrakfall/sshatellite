% !TeX spellcheck = fr_FR

\section{Interface graphique}
\label{interface-graphique:sec}

Comme indiqué précédemment dans la \fullref{visibilité-et-synthèse-des-informations.buts}, il est important que ces informations soient présentées de façon visible et concise afin que l'utilisateur puisse naviguer instinctivement à travers l'application. Pour ce faire, une série de lignes directrices est indiquée ci-dessous, dressant le portrait de cette navigation.

Les différentes entités à monitorer sont organisées de façon hiérarchique~:
\begin{itemize}
	% Est-ce que ça fait sens de ne pas imbriquer ces trois prochains éléments les uns dans les autres ? Car hiérarchiquement, ils le sont.
	\item L'utilisateur peut créer plusieurs groupes qui représentent le haut de cette hiérarchie.
	\item Chacun de ces groupes peut contenir plusieurs serveurs à monitorer.
	\item Chacun de ces serveurs dispose de plusieurs groupes de paramètres, regroupés selon l'élément du système qu'ils affectent. Ces groupes de paramètres sont~:
	\begin{itemize}
		\item Le processeur
		\begin{itemize}
			\item Les processus (sous-groupe)
		\end{itemize}
		\item Le processeur graphique (si le serveur en possède un)
		\item La mémoire vive
		\item Les disque dur
		\item La couche réseau
		\item Les journaux système
	\end{itemize}
\end{itemize}

\subsection{Menu de navigation latéral}
\label{menu-de-navigation-latéral.ui}

\begin{figure}
	\centering
	\includegraphics[width=10cm]{./graphics/menu.jpg}
	\caption{Disposition du menu latéral}
	\label{menu:fig}
\end{figure}

Ces différentes entités sont représentées avec la même hiérarchie dans le menu de navigation latéral\footnote{%
	Celui-ci s'affiche à habituellement à gauche pour les langues dont la lecture s'effectue de gauche à droite et à droite pour les langues dont la lecture s'effectue de droite à gauche.
}
Ce menu est ainsi composé de différents menus déroulants imbriqués, comme illustré sur la \fullref{menu:fig}.
Un appui sur une lignée précédée d'une flèche vers la droite ($\blacktriangleright$) déploie le menu correspondant, faisant apparaître ses sous-objets.

\subsection{Activités principales}
\label{activités-principales.ui}

L'application dispose de plusieurs activités résumant différentes informations.
Elles apparaîtront lorsque l'utilisateur appuiera sur l'élément correspondant sur le menu de navigation.
Lorsque l'application est démarrée, l'\nameref{activité-des-groupes} est affichée comme activité d'accueil.

\subsubsection{Activité des groupes}
\label{activité-des-groupes}

Cette activité comprend un résumé des différents groupes de serveurs à monitorer.
Elle comprend aussi un bouton <<~+~>>, dans le coin inférieur opposé au menu de navigation, permettant à l'utilisateur d'ajouter un élément à monitorer.

Si un groupe comprend un serveur disposant d'au moins une alerte qui lui est associée, celui-ci est affiché en premier avec un message indiquant le nombre d'alertes associées au groupe ou, s'il ne dispose que d'une alerte unique, avec un court message résumant le problème si l'espace disponible le permet.
Si plusieurs groupes disposent ainsi d'alertes associées, il seront affichés par ordre décroissant de nombre d'alertes totales associées.

Un appui long sur un des groupes permet de créer une règle d'alerte qui s'appliquera à tous les serveurs du groupe.

Si l'application ne dispose que d'un seul groupe de serveurs à monitorer, l'\hyperref[activité-d'un-groupe]{activité de ce groupe} sera affichée à la place de celle-ci.

\subsubsection{Activité d'un groupe}
\label{activité-d'un-groupe}

Cette activité comprend un résumé des différents serveur compris dans un même groupe.
Elle comprend aussi un bouton <<~+~>>, dans le coin inférieur opposé au menu de navigation, permettant à l'utilisateur d'ajouter un élément à monitorer.

Si un serveur dispose d'au moins une alerte qui lui est associée, celui-ci est affiché en premier avec un message indiquant le nombre d'alertes ou, s'il ne dispose que d'une alerte unique, avec un court message résumant le problème si l'espace disponible le permet.
Si plusieurs serveurs disposent ainsi d'alertes, ils seront affichés par ordre décroissant de nombre d'alertes.

Un appui long sur un des serveurs permet de lui créer une règle d'alerte.

Si l'application ne dispose que d'un seul serveur à monitorer, l'\hyperref[activité-d'un-serveur]{activité de ce groupe} sera affichée à la place de celle-ci.

\subsubsection{Activité d'un serveur}
\label{activité-d'un-serveur}

\begin{figure}
	\centering
	\includegraphics[width=10cm]{./graphics/serverSummaryActivity.jpg}
	\caption{Disposition de l'activité résumant l'état d'un serveur}
	\label{serverSummaryActivity:fig}
\end{figure}

Cette activité comprend un résumé des différents groupes de paramètres s'afférant au serveur lié à celle-ci.
Comme illustré sur la \fullref{serverSummaryActivity:fig}, chacun de ces groupes comprend un résumé des paramètres principaux qui les compose ainsi qu'un nombre d'alertes dont au moins une des conditions de déclenchement de celles-ci est vraie (de la même façon que pour les activités précédentes).

Un appui sur un de ces groupes de paramètres fait apparaître l'\hyperref[activité-d'un-groupe-de-paramètres]{activité de ce groupe de paramètres}.

\subsubsection{Activité d'un groupe de paramètres}
\label{activité-d'un-groupe-de-paramètres}

Cette activité comprend tous les paramètres liés à un même groupe de paramètres.
Lors d'un appui sur un de ces paramètres, plusieurs actions sont proposées à l'utilisateur en fonction de la nature du paramètre.
Dans tous les cas, sera proposé dans cette liste la possibilité de créer une alerte liée au dit paramètre.
Comme exemple pour ces actions proposées, il est possible, dans le cas des processus, de stopper un processus, le redémarrer, le tuer, etc.
Dans le cas d'un fichier journal, il peut créer une expression régulière (Regex) qui sert de condition de déclenchement de l'alerte.

Avant que l'alerte ne soit créée, l'utilisateur pourra encore lui ajouter d'autres conditions.
Il lui sera aussi possible de spécifier qu'il désire que l'application se souvienne du processus si celui-ci se termine ou non avec le code de statut (de fin d'exécution) qu'il spécifie.
Si un tel cas se présente, en plus de l'alerte qui sera reportée à l'utilisateur, l'application présentera ce processus en haut de la liste en tant que processus <<~terminé~>> et il sera alors possible à l'utilisateur d'effectuer des actions, comme citées précédemment, telles que le redémarrer, consulter ses journaux d'exécution\footnote{
	Si l'emplacement de ceux-ci est connu ou si l'utilisateur l'a préalablement indiqué.
}, etc.


























