% !TeX spellcheck = fr_FR

\section{Communications}
\label{sec:communications}

Lors du monitoring d'un serveur par l'application, les communications s'effectuent en plusieurs phases, comme représentées sur la \autoref{activeUpdateSequence:fig} et la \autoref{slowUpdateSequence:fig}.
Certaines phases peuvent se répéter selon certaines conditions.
Chacune de ces phases et les liens entre elles sont décrits ci-dessous.

\subsection{Initialisation de la connexion}
\label{subsec:initialisation}

Lorsque l'utilisateur ajoute un nouveau serveur à monitorer, l'application doit d'abord initialiser la communication avec le serveur à monitorer.

\subsubsection{Paramètres d'identification}
\label{subsubsec:paramètres-d'identification}

Avant toute communication avec le serveur, l'application doit d'abord demander à l'utilisateur des informations d'identification afin de se connecter.
L'utilisateur peut choisir entre deux modes de connexion et ce de façon non-exclusive.
Ces deux modes ainsi que les informations nécessaires pour la connexion sont les suivants:

%\begin{savenotes}
	\begin{itemize}
		\item Identification par clef publique. Informations nécessaires~:
		
		\begin{itemize}
			\item Nom de l'utilisateur distant
			\item Emplacements des fichiers suivants~:
			\begin{itemize}
				\item Clef privée
				\item Clef publique
				\item Fichier \emph{known\textunderscore hosts}\footnote{
					\label{known-hosts:fn}
					Le fichier \emph{known\textunderscore hosts} contient la liste des clefs publiques connues et autorisées du client \gls{SSH} liée au nom de domaine ou à l'adresse \gls{IP} de la machine distante.
				}
			\end{itemize}
		\end{itemize}
	
		\item Identification par mot de passe
		
		\begin{itemize}
			\item Nom de l'utilisateur distant
			\item Mot de passe de l'utilisateur distant
			\item Emplacement du fichier \emph{known\textunderscore hosts}\footref{known-hosts:fn}
		\end{itemize}
	\end{itemize}
%\end{savenotes}

Suivant que les informations que l'utilisateur entre sont suffisantes pour l'un ou l'autre type d'identification, l'application utilisera la méthode adéquate pour se connecter.
S'il entre toutes les informations, l'application tentera de se connecter d'abord via l'identification par clef publique, ensuite par mot de passe si la première échoue.

\subsubsection{Connexion \gls{SSH}}
\label{subsubsec:connexion-ssh}

Si l'utilisateur parvient à fournir les informations nécessaires pour un serveur donné à la précédente étape, l'application doit ensuite tenter d'initier une connexion \gls{SSH} avec celui-ci.
Cette connexion est compressée via une compression de type \emph{gzip} afin de réduire la quantité de données transmises entre le téléphone et le serveur.
L'option de désactivation de cette compression est laissée à l'utilisateur.
Néanmoins, vu que cette communication se fait souvent à l'aide de données cellulaires souvent limitées sur une période de temps donnée, il est de fait préférable d'échanger une petite quantité de charge de la batterie, dont une légère sur-utilisation est générée par les calculs dûs à l'algorithme de compression, contre moins de données transmises sur le réseau.

Suivant le processus de connexion du protocole, l'application tente d'abord de se connecter par le biais d'un des algorithmes déjà autorisés par l'utilisateur dans son fichier \emph{known\textunderscore hosts} pour le nom de domaine ou l'adresse \gls{IP} associé au serveur.
Si aucune de ces méthodes n'aboutit à une connexion en bonne et due forme ou si aucune autorisation n'est présente pour ledit serveur dans le fichier \emph{known\textunderscore hosts}, l'application doit ensuite afficher l'utilisateur l'empreinte de la clef publique d'une autre méthode d'authentification\footnote{
	Par ordre de préférence de connexion, établie par le protocole \gls{SSH}.
}.
Il peut ensuite l'accepter ou la refuser en fonction du fait qu'elle corresponde ou non à l'empreinte réelle et correcte du serveur pour l'algorithme donné.
S'il accepte cette dernière, une nouvelle entrée est ajoutée au fichier \emph{known\textunderscore hosts} comprenant le nom de domaine ou l'adresse \gls{IP} du serveur, l'algorithme de communication autorisé ainsi que la clef publique qui y est associée, le tout selon le format établi pour ce type d'entrée.

Si l'utilisateur a fourni une ou des informations de connexion incorrectes et donc si aucune méthode d'authentification ne permet d'aboutir à une connexion valide au serveur, l'application ne tente plus de s'y connecter, informe l'utilisateur qu'au moins une des données fournies est probablement incorrecte et lui propose ensuite de les corriger afin de potentiellement tenter une nouvelle connexion.

\subsection{Installation du \gls{backend}}
\label{subsec:installation-du-backend}

Si la précédente étape s'est déroulée correctement et qu'une connexion \gls{SSH} a pu être établie avec le serveur, l'application passe ensuite à l'installation de la partie \gls{backend} s'il s'agit d'un serveur qui vient d'être ajouté par l'utilisateur à la liste des serveurs à monitorer.
L'application garde ainsi en mémoire sur quel serveur elle a déjà effectué ce processus afin de ne pas le répéter inutilement.
Elle ne reproduira cette étape qu'en cas de problème à l'exécution des scripts, auquel cas elle vérifiera que la partie \gls{backend} soit bien dans l'état qu'elle a gardé en mémoire.

L'application doit envoyer des fichiers de script \emph{Shell} et \emph{Python} à plusieurs reprises~:
Ceux-ci seront envoyés à l'emplacement désigné par l'utilisateur.
Un emplacement par défaut lui sera aussi proposé~: \path{~/sshatellite}\footnote{
	\path{~} représente le répertoire \emph{home} de l'utilisateur par le biais duquel la connexion a été effectuée.
}

\subsubsection{Détection de l'exécutable \emph{Python} de la plus récente version}
\label{subsubsec:détection-de-l'exécutable-python-de-la-plus-récente-version}

L'application doit ensuite vérifier la présence d'exécutables \emph{Python}\texttrademark{} et trier ceux-ci afin de trouver celui ayant la plus grande version afin de pouvoir profiter de la version la plus stable et la plus optimisée de \emph{Python}\texttrademark{} disposant du plus grand nombre de fonctionnalités et de corrections de bugs.

Pour ce faire, un premier script de type \emph{Shell} doit d'abord être envoyé.
Celui-ci est d'abord copié sur le serveur puis exécuté.
Il parcourt tous les dossiers compris dans la variable système \emph{PATH}\footnote{
	Cette variable système comprend une liste de répertoires. <<~Quand vous exécutez une commande, le shell lance des recherches dans chaque répertoire, l'un après l'autre, jusqu'à ce qu'il trouve un répertoire où l'exécutable existe.~>>\cite{bhargava_path_2008}
}
et cherche toutes les instances de \emph{Python}\texttrademark{} disponibles dans ses répertoires.
Il renvoie ainsi en sortie le nom des commandes avec leur emplacement associés.

Cette recherche d'instances est en effet indispensable car, suivant les systèmes, les versions de \emph{Python} disponibles ainsi que les commandes par défaut pour l'exécuter ne sont pas uniforme.
En effet, certaines distributions telles que \emph{FreeBSD} ne dispose pas de la variante simple de la commande \emph{`python'} mais uniquement celles avec la version incluse telles que~: \emph{python3}, \emph{python3.6}, \emph{python2.7}.
Sur d'autres distributions, comme \emph{Debian 3.16}, la commande \emph{python} est un lien menant vers la version 2.7 alors que la version 3.4 est disponible.
De plus, certaines fonctionnalités de \emph{Python} utilisées pour l'application nécessite une version plus grande ou égale à 3.4.

Chacune de ces commandes est ensuite appelée avec l'argument \emph{--version} dans le but de filtrer et faire abstraction des commandes récupérées qui ne correspondent pas à un exécutable d'environnement de \emph{Python}\texttrademark{} telles que \emph{python3.4-config} qui est une commande de configuration qui est parfois disponible et qui peut être présent dans la sortie du script \emph{shell}.
Si cette dernière exécution génère, en sortie, une version de Python valide au format \emph{PEP440}~\cite{coghlan_pep_2014},
alors l'exécutable correspondant est temporairement retenu comme étant un environnement d'exécution valide.

Une fois tous les environnements valides recensés, ceux-ci sont triés par ordre décroissant.
Si plusieurs de ces environnements disposent d'une version identique, la seconde priorité de tri devient alors l'ordre d'apparition de l'environnement dans la variable système \emph{PATH}.

L'exécutable associé à la première version de ce tri, et donc la plus grande, sera donc ainsi retenu en cache pendant une longue période.
C'est cet exécutable qui sera utilisé pour l'exécution des scripts \emph{Python}\texttrademark{}.
Ce n'est qu'en cas d'erreur liée à l'exécutable, si par exemple il devient soudainement introuvable en tentant de l'appeler, ou une fois cette période de temps écoulée que cette étape sera répétée afin de re-calculer la plus grande version disponible.
Cela permettra donc que l'application détecte, au bout d'un moment, que les paquets \emph{Python}\texttrademark{} installés sur le serveur ont été mis à jour.

\subsubsection{Installation de l'environnement virtuel}
\label{installation-de-l'environnement-virtuel}

Une fois qu'une version suffisante d'un exécutable \emph{Python}\texttrademark{} a été trouvé grâce à l'étape précédente, un environnement d'exécution virtuel (\emph{venv}) est installé sur le serveur.
\emph{pip}, le gestionnaire de librairies du framework \emph{Python}\texttrademark{}, y est ensuite mis à jour puis les librairies nécessaires, \emph{psutil} et \emph{pyyaml}\footnote{%
	\emph{pyyaml} sert à transformer les \emph{dict} Python\texttrademark{} en \emph{Yaml} qui est ensuite utilisé pour communiquer l'état de la machine au \gls{frontend}.
}
, y sont installées.

\subsubsection{Envoi des fichiers du \gls{backend}}
\label{envoir-des-fichiers-du-backend}

Parallèlement à l'étape précédente, les autres fichiers du \gls{backend} sont envoyés en même temps sur le serveur.
Ceux-ci sont intégrés dans l'archive \gls{APK} de l'application.

%TODO: Parler de la vérification par checksum

\subsection{Communication \gls{frontend} - \gls{backend}}
\label{subsec:communication-frontend-backend}

Une fois toutes les précédentes étapes réussie avec succès, la partie \gls{frontend} de l'application peut enfin communiquer avec la partie \gls{backend}.
Cette communication se distingue en deux processus différents, selon que l'utilisateur soit en train de visionner une des activités
%TODO: gls
résumant l'état du serveur.

\subsubsection{Communications lorsque l'activité résumant l'état du serveur est en avant-plan}
\label{communications-lorsque-l'activité-résumant-les-paramètres-du-serveur-est-en-avant-plan}

\begin{figure}
	\thisfloatpagestyle{plainlower}
	\centering
	\vspace*{-1.5cm}%
	\hspace*{-2.3cm}%
	\input{./diagrams/sequence/activeUpdateSequence}
	\caption{Schéma UML de séquence de la communication à fréquence élevée entre le \gls{frontend} et le \gls{backend}}
	\label{activeUpdateSequence:fig}
\end{figure}

Lorsque l'utilisateur est en train de visionner une activité résumant l'entièreté ou une partie des paramètres du serveur, la fréquence de mise à jour des informations est plus élevée.
Une valeur par défaut correcte serait d'une mise à jour toutes les 5 à 10 secondes afin que l'utilisateur puisse avoir un visuel relativement direct.
Cette valeur peut-être modifiée par l'utilisateur.

Lors de l'apparition de ladite activité, l'application \gls{frontend} fait ainsi appel à la partie \gls{backend} en \emph{Python}\texttrademark{} en lui indiquant qu'il faut envoyer des mises à jour de façon répétitive, et ce jusqu'à la fermeture de l'exécution, ainsi que la fréquence à laquelle elle désire des mises à jour.
La connexion reste ainsi ouverte entre les deux parties et le \gls{backend} envoie les données en sortie au format \gls{YAML} dans un stream.
%TODO: gls
L'application interprète ainsi les données envoyées et met à jour l'activité à chaque nouvelle mise à jour.

Une fois que l'utilisateur quitte l'activité liée au serveur et que la suivante n'est pas liée à celui-ci, par exemple s'il retourne à l'écran d'accueil de l'application ou qu'il quitte l'application qui fonctionne alors en arrière-plan, l'application ferme le stream de la communication avec le \gls{backend} qui stoppe son exécution jusqu'au prochain appel.

Le schéma \emph{UML} de la \fullref{activeUpdateSequence:fig} illustre cette communication.

\subsubsection{Communications d'arrière-plan}
\label{communications-d'arrière-plan}

\begin{figure}
	\thisfloatpagestyle{plainlower}
	\centering
	\vspace*{-3cm}
	\hspace*{-2.5cm}
	\begin{turn}{90}
		\input{./diagrams/sequence/slowUpdateSequence}
	\end{turn}
	\caption{Schéma UML de séquence de la communication à long terme entre le \gls{frontend} et le \gls{backend}}
	\label{slowUpdateSequence:fig}
\end{figure}

Lorsque l'utilisateur n'est pas en train de visionner une activité résumant l'entièreté ou une partie de l'état du serveur, la fréquence de mise à jour des informations est plus faible.
Une valeur par défaut correcte dans ce cas serait celle d'une vérification toutes les heures.
Cette valeur est arbitraire, l'intérêt étant de ne pas trop utiliser la batterie mais de quand même pouvoir être au courant d'une anomalie relativement rapidement.
Étant donné que le cas d'utilisation principale est de monitorer le déroulement de longues tâches, il est moins important d'être notifié d'un problème dans la minute que s'il s'agissait d'un monitoring concernant la sécurité du serveur et des intrusions potentielles.
Elle peut aussi évidemment être changée par l'utilisateur afin de mieux correspondre à ses besoins.

\subsubsection{Regroupement des sondages des serveurs}
\label{par:regroupement-des-sondages-des-serveurs}

Afin de profiter du mécanisme de mise-en-sommeil du système Android\texttrademark{} et autres mécanismes d'optimisation de la batterie, comme décrits dans la \fullref{subsec:mécanisme-d'optimisation-de-la-batterie}, il est intéressant de regrouper par lots les communications lorsqu'elles se font en arrière-plan.
Cela est d'autant plus important que \hyperref[par:app-standby-buckets]{la prochaine version majeure d'Android\texttrademark{} impose des restrictions plus sévères pour les tâches d'une application n'étant pas actuellement affichée à l'écran (\autoref*{par:app-standby-buckets})}.

Quand le téléphone n'est pas en cours de recharge de sa batterie, l'application doit donc utiliser les fenêtres de mise à jour établies par le \nameparref{subsubsec:mode-doze} pour vérifier l'état de tous les serveurs à monitorer et ce tant qu'elles ne sont pas plus espacées que la durée désirée par l'utilisateur.
L'application doit aussi, à chaque vérification de l'état des serveurs, interroger le système Android\texttrademark{} afin de connaître la durée avant la prochaine fenêtre du \nameparref{subsubsec:mode-doze}.
Au vu du fait que cet intervalle augmente au cours du temps, il est primordial que l'application alerte l'utilisateur quand elle ne sera bientôt plus capable d'honorer la durée désirée entre les vérifications des serveurs.

De même, l'application doit vérifier fréquemment dans quel \hyperref[par:app-standby-buckets]{\emph{App Standby Bucket} (\autoref*{par:app-standby-buckets})} elle se trouve et alerter l'utilisateur dès qu'elle le peut lorsque qu'elle se trouve soit~:
\begin{itemize}
	\item déjà dans un \emph{bucket} qui possède un délai pour les tâches de fond plus grand que la période désirée entre deux vérifications des serveurs.
	\item dans le \emph{bucket} le plus proche de ceux correspondant à la catégorie précédente.
	En effet, bien que ce \emph{bucket} ne soit pas lui-même problématique, si l'application se retrouve par la suite à être déplacée dans un \emph{bucket} de la précédente catégorie, il est fort probable qu'elle ne soit pas en mesure d'alerter l'utilisateur de ce fait dans le temps imparti avant la prochaine vérification de l'état des serveurs.
\end{itemize}
Cette alerte peut se faire à l'aide d'une notification.

Si cela arrive, il est alors opportun de proposer à l'utilisateur d'inclure l'application à la liste des applications partiellement exemptes des restrictions liées à l'utilisation de la batterie tout en le prévenant qu'elle continuera à effectuer les vérifications d'état des serveur par lots de manière à épargner la batterie au maximum \cite[Section Support for other use cases]{google_optimize_2018}.
Si l'utilisateur effectue cette opération, l'application ne sera alors plus soumise aux restrictions décrites dans la \fullref{subsec:mécanisme-d'optimisation-de-la-batterie}.
Ce cas d'utilisation fait partie de ceux où une telle demande est considérée légitime selon la politique de Google\texttrademark{} en la matière \cite[Section Acceptable use cases for whitelisting]{google_optimize_2018}.

De plus, afin que ces lots de vérification s'effectuent de manière saine pour les capacités de téléphone, l'application limite le nombre de connexions \gls{SSH} qu'elle effectue en même temps afin d'interroger les serveurs.
Cette quantité limitée peut évidemment être modifiée par l'utilisateur.
Pour ce faire, si le nombre de serveurs à vérifier est plus grand que le nombre maximum de connexions autorisées, ces vérifications seront placées dans une file d'attente.
Elles s'enchaîneront ainsi lorsque celles qui étaient actives se termine.

Afin que ce système de file d'attente ne puisse pas se bloquer lui-même, il est important qu'une durée maximum soit attribuée à chaque vérification.
Cette durée maximum peut être égale au double de la durée habituelle moyenne des précédentes vérifications pour le même serveur, le tout plafonné à une valeur saine.
L'application notifie ainsi l'utilisateur si elle n'a pas pu vérifier l'état d'un serveur dans la durée maximum impartie car cela peut signifier que celui-ci est actuellement indisponible, ce que l'utilisateur ne désire pas forcément.

Le schéma \emph{UML} de la \fullref{slowUpdateSequence:fig} illustre cette communication.








































