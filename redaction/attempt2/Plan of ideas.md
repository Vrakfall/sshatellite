# Introduction

## Contexte

- Un responsable informatique design et entretien des infrastructures informatiques
- Ces infrastructures doivent être le plus disponible possible.
	- Des pannes peuvent arriver à tout moment.
- Le nombre de serveurs ne cesse de croître.
	- En partie du à la translation des architectures d'un paradigme monolithique vers des microservices.
	- La quantité de pannes potentielles augmente avec la quantité de serveurs.
		- Un sysadmin peut être ainsi amené à gérer plusieurs infrastructures.

### Le cas ULiège

- Dans le monde de l'infrastructure informatique, les besoins en monitoring et exigences encadrant son processus 
diffèrent énormément suivant la criticité du système et de son modèle de menace.
- Il est évident qu'il n'est pas possible de répondre à toutes ces possibilités de la même manière, tant elles sont 
diverses et parfois antinomiques.
- L'use case principal de ce mémoire sera donc représenté par celui des serveurs de laboratoire de 
l'université de Liège.
	- Il s'agit de serveurs utilisés par plusieurs personnes, des chercheurs ou des étudiants.
		- Utilisation principale:
			- Soit pour faire fonctionner des tâches de longue durée.
			- Soit pour aider au développement d'applications ou autres.
		- Les chercheurs sont donc intéressés de pouvoir vérifier le bon fonctionnement de leurs tâches.
			- D'être alerté en cas de problème, d'arrêt anormal...
		- Cette infrastructure ne dispose pas de ressources suffisantes pour faire fonctionner une application de
		monitoring utilisant, par exemple, le protocole SNMP prévu à cet effet.
		- Ils sont déjà administrés par SSH.
			- La sécurité et les risques du protocoles ont donc déjà été considérés. => Pour la justification.
				- Un cadre de sécurité minimum/préféré sera néanmoins défini. => Pour la justification.

**\[Récupérable de l'ancienne rédaction: À retravailler pour donner l'impression de moins viser de grosses entreprises
et re-contextualiser l'utilisation de l'application pour l'uLiège et autres sysadmin administrant leurs serveurs par
ssh.\]**

## Les contraintes du cahier des charges et leurs raisons (Android, Java)

\+ Rapide paragraphe sur les différentes possibilités de développement sous Android.

# Établissement du problème

TODO: Vérifier plus tard si tout ou partie de ce chapitre ne devrait pas plutôt intégrer l'introduction.

- Anciens éléments pouvant peut-être repris
	- Buts
		- Répondre au besoin de s'assurer du bon fonctionnement du SI en concevant une application pour le monitorer. 
    Flexible et extensible dans le futur. **[R]**
    - Utiliser SSH et pas SNMP car recherche d'une installation minimale **[R]**
    - Alerter l'utilisateur d'anomalies et  permettre de réparer dans la limite des moyens disponibles. **[R]**
    - Open source pour autoriser les apports et analyses extérieures. **[R]**
	- Établissement du problème
		- Monitoring
			- Au vu du nombre grandissant de serveurs, il est intéressant de pouvoir les monitorer et d'être alerté en cas de 
			problème. **[R]**
			- Cette multiplicité de failles -> nécessité de pouvoir réagir (pour résoudre) rapidement. Téléphone si possible, 
			sinon ordinateur -> tethering si nécessaire. **[PER]**
			- But: Sysadmin alerté le plus tôt possible -> disponibilité d'outils de debugging moins prioritaire, néanmoins un 
			maximum d'outils doit être disponible.
			Outils de réparation limités à la plate-forme du téléphone mobile. Exemple: clavier limité, manque de caractères 
			spéciaux, de raccourcis, etc. **[PER]**
		- Visibilité et synthèse des informations
			- Un serveur physique = beaucoup de paramètres à monitorer.
			Parmi, les logs sont très intéressants car description du déroulement du programme = bon pour le debugging. **[R]**
			- Logs = beaucoup d'info en plus des autres paramètres à afficher -> Bien les synthétiser. **[R]**
			- Quantité d'info multipliée par le nombre de serveurs à monitorer -> doit être pris en compte. **[R]**
		- Sécurité
			- Monitorer via téléphone -> problèmes de sécurité.
			Vol du device = clef privée dans des mains mal intentionnées. **[R]**
			- L'ingénierie sociale peut permettre de voler les clefs **[R]**
			- SSH possède quelques failles connues. -> Lien vers section le détaillant **[R]**
			- L'utilisateur n'a pas toujours tous les droits sur le serveur <- Séparation des pouvoirs.
			Exemple d'une grosse entreprise.
			SSH -> application limitée aux droits d'utilisateurs donnés à l'utilisateur auquel l'application se connecte.
			Il faut vérifier que cet utilisateur ait les autorisations nécessaires à l'application. **[R]**
			- Éviter que l'application ne devienne une nouvelle porte d'entrée pour les hackers. **[R]**
		- Stabilité
			- L'app doit être stable. Monitorer plusieurs serveurs peut facilement générer une charge sur le téléphone. **[R]**
			- Une instabilité serait contre-productive: confiance envers des alertes soudainement non-présentes -> erreur sur 
			l'état supposé du serveur. **[R]**

## Buts

- Déclinés en X axes: Monitoring, Visibilité, Sécurité, Stabilité, extensibilité.

### Monitoring

- Au vu du nombre grandissant de serveurs, il est intéressant de pouvoir les monitorer et d'être alerté en cas de
problème. **[A]**
- Répondre au besoin de s'assurer du bon fonctionnement du SI en concevant une application pour le monitorer. 
Flexible et extensible dans le futur. **[A]**
- Utiliser SSH et pas SNMP car recherche d'une installation minimale **[A]**
- Alerter l'utilisateur d'anomalies et  permettre de réparer dans la limite des moyens disponibles. **[A]**
- Open source pour autoriser les apports et analyses extérieures. **[A]**

### Visibilité

- Un serveur physique = beaucoup de paramètres à monitorer.
Parmi, les logs sont très intéressants car description du déroulement du programme = bon pour le debugging. **[A]**
- Logs = beaucoup d'info en plus des autres paramètres à afficher -> Bien les synthétiser. **[A]**
- Quantité d'info multipliée par le nombre de serveurs à monitorer -> doit être pris en compte. **[A]**

### Sécurité

- Monitorer via téléphone -> problèmes de sécurité. **[A]**
Vol du device = clef privée dans des mains mal intentionnées.
- L'ingénierie sociale peut permettre de voler les clefs **[A]**
- SSH possède quelques failles connues. -> Lien vers section le détaillant **[A]**
- L'utilisateur n'a pas toujours tous les droits sur le serveur <- Séparation des pouvoirs.
Exemple d'une ~~grosse~~ entreprise.
SSH -> application limitée aux droits d'utilisateurs donnés à l'utilisateur auquel l'application se connecte.
Il faut vérifier que cet utilisateur ait les autorisations nécessaires à l'application. **[A AC]**
- Éviter que l'application ne devienne une nouvelle porte d'entrée pour les hackers. **[A]**

### Stabilité

- L'app doit être stable. Monitorer plusieurs serveurs peut facilement générer une charge sur le téléphone. **[A]**
- Une instabilité serait contre-productive: confiance envers des alertes soudainement non-présentes -> erreur sur 
l'état supposé du serveur. **[A]**

### Extensibilité

- L'app doit être conçue de façon à pouvoir être étendue.
Cela permettrait de l'adapter à d'autres plate-formes, autres librairies.
- Libs qui peuvent soudainement ne plus être maintenues, avoir des problèmes de sécurité, etc...
Il est intéressant de pouvoir changer facilement, tout en gardant la logique de l'application.
- Permet aussi d'ajouter des fonctionnalités par la suite.

## Proof of concept

**\[Peut être repris tel quel, je pense (de l'intro)\]**

# État de l'Art

**\[Est-ce que je dois y consacrer un sous-chapitre concernant le développement pour mobile/android ? -- 
Probablement pas ?\]**

**\[Pareil pour le protocole SNMP et les applications de monitoring plus "traditionnelles" ? -- Oui\]**

**\[Pareil pour le développement python ?  -- Probablement pas ?\]**

## Applications existantes

**\[À quel point dois-je/ne dois-je pas parler des applications existantes et des fonctionnalités qu'elles offrent ?\]**

Fonctionnalités et sécurités des applications suivantes brièvement décrites dans la rédaction précédente: JuiceSSH, Admin Hands, Monitor my server (AppSshForm), ZFS Monitor.

**\[Une bonne partie peut être reprise de l'ancienne rédaction tout en modifiant.\]**

**\[/!\ Attention, d'autres nouvelles applications sont peut-être apparues depuis.\]**

## Environnement Android

### Mécanismes d'optimisation de la batterie

#### Doze

#### App Standby

##### App Standby buckets

\>= Android P

## SSH

- Qu'est-ce qu'SSH
- Comment et pourquoi est-ce une bonne solution pour le management et le monitoring d'un serveur
  - Exemples d'autres systèmes de monitoring, par exemple utilisant snmp
  - Expliquer pourquoi ces solutions ne sont pas efficientes pour notre use case
- Librairies disponibles (JSCH et SSHJ) et pourquoi le choix d'SSHJ **\[Cela n'a probablement pas sa place dans l'état de l'art ?\]**
- Sécurité
  - Failles connues
  - Sa sécurité "moyenne" n'est plus vraiment à prouver.
  - Potentiels éléments de l'implémentation dont la sécurité finale incombe à l'utilisateur de l'application et/ou du sysadmin responsable du/des serveur(s) à monitorer.

## RxJava

- Ce qu'est ReactiveX/RxJava
  - Programmation réactive et fonctionnelle basée sur le pattern observer/observable permettant de contrôler le flux d'objets émis via le scheduler adéquat pour la tâche.

## psutil

- Librairie python permettant d'obtenir les paramètres principaux de la machine à monitorer.

# Solution / Design de l'application

**\[À rajouter: Prendre en compte les permissions et accès de psutil.\]**

## Vue d'ensemble

Vue d'ensemble de la suite et diagramme décrivant la mise en place.

## Choix

- Pourquoi l'utilisation de python
  - Présent "de base" sur la plupart des distributions
  - Suffisamment léger pour notre use case
  - Permet l'installation des dépendances dans un `venv` lié au projet, pas d'interférence avec les reste du système.
  - Librairie `psutil` très connue et répandue pour accéder aux paramètres de la machine.
  - Formatage et transmission facile des données.
- TODO: Évoquer pourquoi l'utilisation de YAML


## Modèle

- Diagrammes de classes et description de la conception du modèles
  - Celui-ci est conçu de façon à abstraire un maximum les librairies utilisées ainsi que les éléments inhérents à la plate-forme.
- Diagramme de communication avec l'interface
  - MVC particulier avec RxBinding pour une modification réactive de l'interface graphique.

## UI

- Screenshots & Mocks de l'interface
  - Description des interactions
  - Regroupement des serveurs par groupes

## Communications

- Ajouter des diagrammes de séquence pour ce qui suit

- L'application mobile se connecte via SSH au serveur à monitorer et envoie le backend si celui-ci n'est pas déjà présent ou que la version n'est pas la bonne.
  - Mobile app <==== SSH ====> Server
  - Cette connexion est compressée pour optimiser la quantité de données transférées (opt-out possible).
- L'application doit ensuite détecter la présence de python et tenter de trouver la version de python la plus grande (// certaines librairies requièrent python 3.4 minimum)
  - Appeler la commande python (`/usr/bin/python` ou autre) ne fonctionne pas forcément sur certains systèmes tels que FreeBSD sur lequel chaque commande python comprend le numéro de version (ex.: `python36`).
  - Filtrage des exécutables (omission des exec de config). **\[+ exécutables compilés en C/C++\]**
- Installation d'un `venv` afin d'installer les librairies nécessaires manquantes.
  - Mise à jour de `pip` et installation des librairies requises et manquantes sur le système hôte.
- Téléchargement des fichiers du backend sur le serveur à monitorer
  - Peut se faire soit via HTTPS via `wget` (depuis un serveur hôte pour les fichiers du backend).
    - Cela permet d'économiser des données à envoyer par l'application (mais pas tant que ça non plus).
  - Si le serveur hôte ne dispose pas d'accès pour télécharger des fichiers via HTTPS, l'application envoie les fichiers du backend via la connexion SSH.
  - Pourrait, à terme, être un package publique sur PyPi afin de pouvoir installer le backend avec les autres librairies via `pip`.
- TODO: Vérification de l'intégrité des fichiers du backend via checksums.
- Le backend envoie ensuite les paramètres de la machine sous forme de YAML
  - Le choix du YAML n'est pas définitif et est principalement dans un but de pouvoir facilement débugger les communications en cas de problème. Vu que la communication peut être compressée pour un infime overhead **\[trouver une citation\]**, l'overhead de la quantité de transmission des données n'est pas énorme comparer au coût temporel que prendrait le design et l'implémentation d'un protocole.
  - Il envoie ses données de façon régulière, selon une fréquence déterminée par l'application suivant son état actuel.
    - Si l'application se trouve être en arrière-plan (pas l'`Activity` principale), il serait suffisant d'établir comme fréquence par défaut celle d'une mise à jour toutes les 10 minutes.
      - Cela doit pouvoir être modifié par l'utilisateur afin qu'il adapte cette valeur de façon à ce qu'elle corresponde à la vitesse à laquelle il désire être informé de problèmes potentiels sur son serveur. Une fréquence plus élevée peut évidemment drainer plus rapidement la batterie du téléphone faisant fonctionner l'application.
      - La connexion SSH peut temporairement être coupée entre 2 envois d'état de mise à jour par le serveur, afin d'économiser sur le coût d'une connexion restant active. **\[Ce coût est-il bien significatif ? Trouver une étude là-dessus et faire des tests.\]** **\[Ré-ouvrir une nouvelle connexion ne coûterait-il pas tout autant, voir plus ?\]**
        - Dans ce cas, il faut que ce soit l'application qui fasse une nouvelle requête auprès du backend.
        - L'application peut ainsi "batcher" les requêtes (au backend afin de connaître l'état de la machine) afin de les effectuer toutes les unes à la suite de l'autre, entre 2 "ticks du counter", permettant au téléphone de rester en veille/endormi plus longtemps ce qui a pour effet de diminuer la consommation de la batterie. Cela peut s'avérer particulièrement utile lorsque l'application est utilisée pour monitorer plusieurs serveurs à la fois.
    - Si l'application se trouve actuellement sur une `Activity` dont le but est de visualiser l'état actuel du serveur ou de fournir plus de détail sur un groupe de paramètres particuliers (par exemple: les processus actifs), il est intéressant d'augmenter la fréquence à laquelle le backend envoie les mises à jour d'état de la machine (à monitorer).
      - Une mise à jours toutes les quelques secondes (ajustable par l'utilisateur) peut alors être considérée car il ne s'agit généralement que d'une période limitée durant laquelle l'utilisateur navigue dans l'application et il ne s'agit alors que d'un seul serveur.
        - Dans ce cas, l'application n'a besoin de n'effectuer qu'une seule requête au backend, qui envoie ensuite un état de mise à jours toutes les périodes spécifiées.
        - Ce comportement n'est pas complètement nécessaire et peut être désactivé par l'utilisateur.
      - Aussitôt l'`Activity` quittée, aussitôt une fréquence de mise à jour plus lente peut être remise en place.
        - L'application envoie alors une nouvelle requête afin que stoppe la précédente émission des mises à jour d'état de la machine.
- L'application vérifie l'état de machine et envoie des alertes, si nécessaires, à l'utilisateur sous forme de notification détaillée, d'alarme, etc..., selon ses paramètres.
  - Les critères déclenchant ces alertes sont à définir par l'utilisateur. L'application peut néanmoins proposer une série de conditions d'alertes par défaut (par exemple: la RAM est utilisée à plus de 90% depuis plus d'une heure).

### Cas particuliers

- Possibilité de se connecter via un serveur proxy
  - **Pas encore implémenté**
- Possibilité de faire du port-knocking
  - **Pas encore implémenté**

# Conclusion

Une partie est à reprendre de l'ancienne rédaction

## Auto-critique

- Qu'est-ce qui marche bien, pas bien dans l'application
- Difficultés rencontrées
- Qu'est-ce que j'aurais fait différemment si je devais le refaire
- Ce que je ferais en plus si j'avais plus de temps **\[Merge future work?\]**
	- \+ pour l'app: ajouter un terminal

## Future work

- Intégrer l'authentification via un agent SSH
  - Intégrer des facteurs externes d'authentifications telles que les clefs USB SSH/PGP, YubiKey, Nitrokey, etc...
- Intégrer un pré-filtrage potentiel des logs via des listes existantes telles que `logcheck`. **\[todo: Vérifier la présence ou non de listes/techniques plus récentes\]**
- Intégrer d'autres mécanismes de sécurisation des clefs (k-out-of-n threshold,etc...).
- Intégrer une possibilité d'utiliser des scripts shell au lieu de python dans le cas où celui-ci n'est vraiment pas disponible, quitte à disposer de moins de paramètres.
- Porter l'application à d'autres systèmes (Windows (serveur), iOs (phone), etc...)

# Gros TODO:

- ~~Graphique des communications~~
- ~~Graphique du design avec RxJava et Android **\[+-\]**~~
- ~~/!\ Permissions nécessaires au backend (psutil)~~
- ~~Protocoles (marche à suivre) d'autorisation d'accès~~ -> Pas le temps
- ~~Parler du modèle de menace dans l'état de l'art ET dans la solution (// paragraphes du chapitre attempt1 4.5)~~
- ~~Déplacer tous les liens en footnote vers la bibliographie~~ -> En fait non
- Should be ok: ~~/!\ Réparer tous les liens -> Fait pour la plupart -> +- 2-3 restants pour les permissions de psutil~~
- ~~Décrire le design de l'UI~~
- ~~Intégrer les traductions de Marie~~
- ~~Intégrer les corrections de Marie~~
- ~~/!\ Remercier les créateurs de symboles~~
- ~~Évoquer le port forwarding~~
- ~~Gls + accros~~