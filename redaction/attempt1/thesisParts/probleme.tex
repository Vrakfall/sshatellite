% !TeX spellcheck = fr_FR
\chapter{Établissement du problème et question de recherche}

\section{Monitoring}

Au vu du nombre grandissant de serveurs à monitorer, il est de plus en plus intéressant pour un responsable d'architecture de pouvoir être informé de l'état des serveurs et de l'architecture dont il est garant, et ce, à n'importe quel moment. Il est encore plus intéressant de pouvoir être alerté directement lors d'incidents.

La multiplicité croissante de failles potentielles entraîne la nécessité de pouvoir réagir rapidement à celles-ci et d'avoir la capacité de tenter de les résoudre sur le moment même, à condition que le problème soit suffisamment simple ou commun que pour être solutionné par le biais d'un téléphone mobile, souvent moins pratique qu'un ordinateur de travail.
Toutefois, si le problème se révèle être plus complexe après quelques investigations, il est toujours possible pour l'administrateur du serveur d'ensuite changer de machine de travail~: Il peut ainsi, par exemple, utiliser un ordinateur portable, s'il en dispose d'un à proximité, pour régler le problème avec des outils plus puissants mais surtout plus pratiques.
Si, à ce moment-là, il ne dispose pas d'une connexion régulière à \emph{Internet}, il lui est toujours possible d'utiliser son téléphone mobile afin de se connecter via \gls{tethering}.

Le but est donc que le responsable d'architecture soit informé le plus rapidement possible d'un problème ou d'une situation alarmante.
Par conséquent, la disponibilité d'outils de debugging % TODO: glossaire
sur l'application mobile, bien que crucial, ne doit pas être considérée comme une priorité ni constituer le gros du travail des développeurs.
Bien évidemment, un maximum d'outils doit rester disponible afin de ne pas limiter l'utilisateur qui doit essayer de réparer un serveur à distance et qui ne dispose pas, sur le moment, d'autre appareil qu'un téléphone mobile.
Cependant, il ne sera pas possible de faciliter l'utilisation de l'application au-delà des limites de la plate-forme\footnote{Le téléphone mobile}.
L'élément le plus souvent gênant se trouve être le clavier virtuel sur l'écran tactile.
Celui-ce ne permet pas d'écrire aussi rapidement qu'avec un clavier matériel, ne dispose pas (ou de façon peu aisée) de tous les caractères nécessaires pour une utilisation dans un terminal de commandes et ne permet en principe pas d'effectuer les raccourcis couramment utilisés dans un terminal car des touches comme \gls{ctrl} ne sont pas présentes.

\section{Visibilité et synthèse des informations}

Un seul serveur physique peut comporter une multitude de paramètres différents. En plus des paramètres habituels (utilisation du processeur, de la mémoire vive, des disques durs, de la couche réseau, \dots), beaucoup d'autres paramètres sont utiles à monitorer, entre autres~: L'état de fonctionnement de la base de données, l'état des machines virtuelles ou des conteneurs, etc. % TODO: glossaire
Parmi ceux-ci, les journaux d'événements\footnote{Aussi appelés `historiques', `registres' ou même `logs' (en \emph{Anglais})} ont beaucoup d'intérêt pour l'administrateur d'un serveur.
En effet, ces journaux contiennent généralement une description faite par le programme de son déroulement comme l'ensemble des événements qui ont affecté ce dernier, les actions effectuées mais plus particulièrement des informations permettant de comprendre pourquoi un incident est survenu.
Il est donc essentiel de pouvoir utiliser ce dernier type d'information afin d'en alerter efficacement l'utilisateur de l'application.

Cependant, ces journaux peuvent être très longs et contenir beaucoup d'informations.
En plus de cela, tous les autres paramètres doivent être disponibles afin de constituer un rapport complet.
L'application doit subséquemment tenter de les synthétiser du mieux possible afin que l'information la plus pertinente possible soit rapidement visible par l'utilisateur et qu'il ait une bonne vue d'ensemble de l'état actuel du fonctionnement du serveur.

De surcroît, la quantité d'information disponible sera multipliée par le nombre de serveurs que l'utilisateur désire gérer~\footnote{La multiplication n'est pas exacte car chaque serveur peut comporter différents services, générant un nombre différent de journaux d'événements en plus fait qu'ils ont une taille proportionnelle à la durée du fonctionnement du service.}.
Cela implique qu'une gestion pratique d'une quantité préalablement indéfinie de serveurs doit faire partie des caractéristiques de l'application.

\section{Sécurité}
\label{sec:securite.probleme}

Monitorer des serveurs via une application mobile soulève un bon nombre de problèmes de sécurité.
Quid en cas de perte ou de vol du téléphone ?
Si un tel événement se produisait, la ou les clefs \gls{SSH} pourraient de cette façon se retrouver dans des mains mal intentionnées.

Une autre faille souvent oubliée mais qui reste l'une des plus répandues et des plus dangereuses est l'ingénierie sociale. % Trouver une citation adéquate.
Même sans perte ou vol de l'appareil, les clefs \gls{SSH} restent vulnérables. En effet, une personne mal intentionnée pourrait avoir préalablement identifié l'utilisateur de l'application comme un responsable de l'architecture système d'une entreprise dont la valeur des informations qu'elle stocke est importante, par exemple, et tenter d'hypocritement sympathiser avec l'utilisateur afin de profiter d'un moment d'inattention ou de pratiquer une supercherie lui permettant d'accéder aux précieuses clefs privées. % Glossaire pour clef privée ?

Le protocole \gls{SSH} en lui-même possède quelques failles connues et certaines attaques sont encore possibles sur des installations mal réalisées~\footnote{
	Ne considérant les bonnes pratiques en la matière~: Par exemple, en gardant les paramètres par défaut d'\gls{SSH} qui permettent de se connecter sans pair de clef privée-publique, uniquement à l'aide du mot de passe de l'utilisateur de la machine hôte.
}
ou mal défendues~\cites{owens_study_2008}{gasser_deeper_2014}[Section 1 \textendash Introduction, p.1]{sadasivam_classification_2016}{venafi_inc._ponemon_2014}{song_timing_2001}{kent_unsecured_2010}.
La \autoref{attaquesFaillesSSH:subsec} détaillera ces attaques potentielles et les moyens conçus dans l'application pour s'en défendre.

En outre, l'utilisateur de l'application n'est pas toujours un administrateur ayant tous les droits sur un serveur. Une entreprise, ou n'importe quelle autre organisation, pourrait très bien vouloir séparer les pouvoirs entre ses employés dans le but d'améliorer la sécurité de son système.
Par exemple, dans la stratégie de sécurité d'une grande entreprise, il est possible qu'un responsable du bon fonctionnement du serveur\footnote{Serveur logique, dans cet exemple} d'une certaine application\footnote{Suffisamment complexe que pour nécessiter l'attention spécifique de certains employés} ne puisse pas avoir la capacité de modifier les informations des comptes des employés de l'entreprise, ceux-ci se trouvant sur une base de données fonctionnant sur le même serveur physique.
L'utilisation d'\gls{SSH} implique que l'utilisateur de l'application cliente est, par défaut, limité par les droits d'accès de l'utilisateur auquel il se connecte sur la machine hôte\footnotemark.
Une attention particulière doit donc être apportée à ce que l'utilisateur de l'application mobile de monitoring soit bel et bien limité aux droits d'accès qui lui sont définis.

\footnotetext{
	En réalité, certaines techniques existent afin de dissocier certains utilisateurs clients se connectant au même compte sur la machine hôte.
} % Partie floue à éclaircir et citer une source dans l'idéal.

Autrement dit, l'application doit être pensée en fonction de ces considérations de sécurité afin d'éviter le plus possible qu'elle ne devienne un nouveau point d'entrée potentiel pour les hackers.

\section{Stabilité}

La stabilité de l'application est également une caractéristique majeure de l'application.
Monitorer plusieurs serveurs simultanément peut facilement générer une certaine charge sur l'appareil.
D'autant plus que, de par sa nature\footnotemark, un téléphone mobile moyen est généralement moins puissant qu'un ordinateur moyen.

\footnotetext{
	Les téléphones mobiles sont communément conçus de façon à épargner la batterie le plus possible, la plupart avec un processeur de type ARM %TODO: Glossaire
	utilisé afin de troquer de la puissance de calcul contre une consommation électrique plus basse.
}

Au demeurant, une application s'interrompant de façon inattendue serait contre-productive.
L'utilisateur pourrait s'habituer aux alertes systématiques de l'application en cas de problème et procéder ensuite à une vérification manuelle, autrement moins régulière que s'il n'utilisait pas l'application.
Une discontinuité du fonctionnement de l'application pourrait, dans ce cas, occasionner un déficit ou un défaillance au niveau des alertes reçues par l'utilisateur. Ce dernier ne serait alors alerté qu'en cas de vérification manuelle régulière, et ce, avec un retard possible~: Cela réduirait considérablement l'intérêt de l'application.