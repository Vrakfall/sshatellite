# Native on Unix & Linux with some small differences (to be checked).

- top
  - Details on top processes
- vmstat
  - Some details on the virtual memory.
- lsof
  - List open files.
- uptime
  - Actually the first line of top
- w
  - uptime + users and their open processes.
- /proc files have many useful infos.
  - (cat) /proc/cpuinfo: complete info on the cpus.

#Native on FreeBSD (to be checked and check whereelse it is available).

# To be installed

- netstat
  - Network statistics and details (and most details sockets).
- sysstat
  - Unknown
- iotop
  - Unknown
- iostat
  - Unknown
- htop
  - top with more details
- glances
  - htop using more resources but with the mind of quickly seeing what's wrong on a machine.
- nmmon
  - An alternative to glances/htop but seems to have more generalized info on IO.
- PAPI (Performance Application Programming Interface)
  - On ne peut pas du tout utiliser cela car c'est una API qui nécessite intallation avec patch du kernel (ou installation de drivers, suivant les cas) pour qu'elle fonctionne ainsi que d'être appelée par du code C ou Fortran.
  - Pour d'autres langages, cela dépend des librairies disponibles. Exemple pour java: []https://github.com/vhotspur/papi-java]
  - Perfmon2 semble basé là-dessus.
- Parmon (vieux ?)

# Graphical | Pour l'existant.

- Netdata
  - Monitoring réseau.
