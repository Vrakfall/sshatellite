# Introduction

- \[Le but de ce mémoire\]
  - Application mobile permettant de monitorer plusieurs serveurs Unix / Linux.
  - Doit permettre de monitorer les paramètres de base d'un serveur: utilisation du CPU, utilisation de la mémoire vive, utilisation et état de la couche réseau, espace disponible/utilisé sur le matériel de stockage, etc...
  - Doit utiliser le protocole SSH pour éviter de devoir installer quoi que ce soit sur le serveur.
    - Ceci permet potentiellement de pouvoir monitorer toute machine gérant le protocole SSH.
  - Doit permettre de recevoir des alertes lors d'événements alarments que l'utilisateur peut définir. Exemple: serveur indisponible, disques durs pleins, etc...
- "Proof of Concept" accompagnant le mémoire.
- Parler de pourquoi SSH
	- Avantages et inconvénients
		- A. Surtout la sécurité
		- I. Limitations: Pas de possiblité d'installer de programme sur la machine hôte.
			- Mais c'est aussi un avantage: plus léger
		- Devoir utiliser des commandes systèmes et des languages comme `awk` pour des petits scripts si nécessaire
			- Le moins possible de scripts pour éviter un overhead inutile sur le serveur.

# Contexte

- Augmentation accrue du nombre de serveurs qu'un informaticien doit gérer. \[Trouver une citation à ce sujet.\]
  - Les applications réseaux nécessitent de plus en plus différents programmes dits "serveurs" afin de fonctionner dans des designs variés et parfois plus complexes "qu'un simple client-serveur".
  - L'augmentation de ce nombre de serveur augmente aussi les probabilités que des problèmes surgissent et qu'une intervention soit nécessaire et ce à toute heure du jour ou de la nuit.
- Un même informaticien est d'ailleurs souvent amené à gérer différents groupes de serveurs appartenant à différentes organisations. Il peut très bien devoir gérer des serveurs pour l'entreprise de son emploi principal ainsi que d'autres pour une activité d'indépendant complémentaire qu'il exerce en plus de son emploi principal tout en ayant des serveurs personnels qu'il utilise uniquement à des fins privées, principalement pour proposer des services informatiques au sein de sa famille.

# État de l'Art

# Software existant

\[Besoin d'une analyse rapide des logiciels SNMP et autres ?\]

## Pourquoi ils ne sont pas ce qui est recherché

- JuiceSSH
  - 

# Question de recherche / établissement du problème

## Monitoring

- Au vu du nombre grandissant de serveurs à monitorer, il est de plus en plus intéressant pour un responsable d'architecture de pouvoir être informé n'importe quand de l'état des serveurs et de l'architecture (dont il est responsable \[trouver une autre formulation\]) et encore plus intéressant de pouvoir être alerté lors d'incidents.
- Il est aussi intérressant de pouvoir réagir aux incidents \[Ceux grandissant(s?) dont on parle dans le contexte\] et d'avoir la capacité de tenter de les résoudre sur le moment-même, que l'utilisateur soit dans le contexte de son travail qui consiste, entre autre, à maintenir le serveur ayant eu l'incident ou bien que ce soit à un tout autre moment (par exemple: pendant la nuit) ou dans un lieu différent (par exemple: lors dans voyage d'affaire).

## Synthèse des informations et visibilité (Facilités d'utilisation merged in)

- Beaucoup de paramètres à représenter.
- Il serait donc intéressant pour l'utilisateur de pouvoir dissocier ces différents contextes.
    - Un intérêt serait de pouvoir apporter une priorité plus importante à certains groupes de serveurs, pour ceux de son emploi principal par exemple.
    - Un autre intérêt serait donc d'avoir une vue plus globale de ces serveurs et d'en faire ressortir principalement les informations les plus intéressantes et les plus pertinentes.

## Sécurité

- Monitorer des serveurs via une application mobile soulève un bon nombre de problèmes de sécurité.
  - Quid en cas de perte/vol du téléphone ?
    - La clef ssh pourrait maintenant être dans les mains d'une personne mal intentionnée.
  - Principal faille de sécurité : Ingénierie sociale. \[Trouver une citation quant au fait que c'est la plus dangereuse, la plus répandue ou quelque chose du genre.\]
    - Même sans perte/vol, l'application pourrait être accédée momentanément lors d'un moment d'inatention.
- Monitorer via `SSH` peut aussi apporter d'autres failles de sécurité.
  - `SSH` possède lui-même quelques failles et est sensible à certaines attaques. \[Citer des attaques connues.\]
  - Le responsable du monitoring n'est pas forcément sensé, au sein d'une entreprise par exemple, censé avoir tous les droits sur un serveur (séparation des pouvoirs) \[Citer un conseil sur la séparation des pouvoirs.\]
    - L'utilisateur, sur la machine hôte, apporte un nouvel élément devant être limité, suivant les entreprises et leur modèle de menace. \[Trouver une citation sur les modèles de menace.\]

## Stabilité

Les plantages pourraient occasioner le fait que l'utilisateur ne soit pas alerté d'un incident.

# Description de la solution

## Choix \[Peut-être plus loin ?\]

- SSHJ. Pourquoi pas JSCH et les autres ? Pourquoi/quand/dans quelles conditions il pourrait quand même être remplacé ?
	-Impl/Comp
- Le remplacement de BouncyCastle par SpongyCastle pour faire fonctionner l'ECDSA.
  -Impl/Comp
- RxJava: Gestion différente des flux d'une application.Gestion des threads et des messages.
  -Stabilité
- Utilisation/non-utilisation de `awk`.
  - Pas si utilisé que ça
  - Pour plus tard, peut-être ne même pas évoquer.
- Envoi de `strings` plus intéressant qu'une transformation sur le serveur qui créerait un léger overhead.
  - \[`awk` est-il un language interprété ? J'imagine que oui mais il vaut mieux vérifier.\]
	  - [Oui](https://earthsci.stanford.edu/computing/unix/programming/interpreted/)

## Monitoring

- Tous les paramètres des machines
- Tous les logs
- État des NICs / de la couche réseau

# Stabilité

- Diagramme de séquence/état pour les connexions suivant l'état du réseau.

### Synthèse des informations et visibilité

- Mode permettant de ne voir que les paramètres sortant de la normale.

## Synchronisation comptes
### Gestion accès

- (Grosses entreprises)
  - Synchro LDAP?
  - Utilisation enterprise IDM?
- \[Rechercher les différents KMS (Key Management System) et les potentielles intégrations avec l'application.\]

### Facilités d'utilisation

- Aussi pour les plus petits utilisateurs.
- Possibilité de grouper les serveurs.

## Personalisation

- Possibilité de changer les commandes exécutées.
  - Bouton pour revenir à la commande par défaut.
  	- Peut-être plus tard.

## Sécurité

- It all depends on the treat model
- Attaques sur SSH même
	- Implémentation
		- OpenSSH already had flaws.
	- Bruteforce
	- DoS
	- ~~MITD (Hence why securing known_hosts is important)~~
		- Apparently, it's a nope!
	- (There were other ones. To list!)
- Sécurisation du serveur
	- Ne pas autoriser les connexions par mot de passe.
	- Nombre maximum d'essais de connexions.
	- (Pam ?)
	- Se tenir au courant de CVE et mettre à jour régulièrement. (Vaut aussi pour le client.)
	- Chroot
		- Limits the fixing part (not the monitoring part as long as the needed commands are allowed).
	- Limitations de l'utilisateur host (Nécessite de délimiter les commandes utilisées.)
	- Management des clefs autorisées
	- IDS
	- Port knocking (Cite the long lasting admin use case)
	- Hire pentesters and IT security specialists, do audits! $
- Sécurisation du client
	- (Secure the phone !)
	- Vol/perte de clef
		- Mot de passe fort sur les clefs
		- Key rotation
		- Implémentations avec KMS
	- Sécurité des librairies
		- SSHJ (JSCH?)
		- SpongyCastle
- Educate/train the user!
  - Many security breaches originiate from the user
  - Les formations aident à une utilisation plus responsable.

### Old ideas

- Limiter l'utilisateur auquel l'application se connecte sur la machine hôte. Une fois connectée, l'application aura les mêmes droits d'accès que cet utilisateur.
  - Cela pourrait par contre empêcher certaines manipulations lors de tentatives de réparation.
  - Cela doit être planifié par l'utilisateur suivant les menaces auxquelles il s'expose et la sécurité recherchée.
  - \[Chercher après ce qui est courrament fait pour protéger des utilisateurs et des connexions ssh, aussi bien sur le serveur que sur le client ou sur le réseau en chemin. > Agent SSH\]
  - Si l'utilisateur est une entreprise et qu'elle souhaite que plusieurs de ses employés accèdent à certains serveurs, il vaut mieux qu'ils soient formés.
- \[Penser aux différents algorithmes utilisés, les différentes méthodes de connexion: mot de passe, clefs publiques, etc...\]
  - SpongyCastle est-il "safe" ?
  - SSHJ est-il "safe" ?
  - Sécurité des librairies en général.

## Implémentation

- Interchangeabilité des librairies et des "type" d'OS hôte.
	- Diagramme de classe

# Travail futur

- Compression (De JuiceSSH)
- Test des services hébergeur par l'organisation cliente.

# Conclusion

# Pas encore classés

- Utilisation et parsing de la commande top.
- \[Chercher la définition d'un monad pour la citer ?\]
- Utiliser nice sur les commandes pour ne pas surchager le serveur.
- Clavier avec touches difficiles à trouver (De JuiceSSH)
- Bon support des déconnexions.
- Export des logs (De JuiceSSH)
- Possibilité de grouper les serveurs. (Des consignes et de JuiceSSH).
- Connexions en arrière-plan.
- 2FA (peut-être en travail future...)
- Gestion identité (De JuiceSSH)
- Port knocking
- Commandes pré-enregistrées
- Proposer des actions de base pour solutionner des problèmes de base.


- Graphiques présentation des informations.

# TODO
- Precise RFC page in cites
- Check all cites

# Principal restant:
- ~~gestion logs~~
- ~~Conception/uml~~
	- ~~Regex interprétation des commandes~~
	- Example ReactiveX +-
- ~~UX~~
  - ~~Présentation et navigation~~
  - ~~Création de regex~~
- ~~Comparer avec JuiceSSH & autres~~
- ~~Intro~~
- ~~PoC~~ +-
- ~~Conclusion~~
- Abstract

- ~~Conclusion~~
- Améliorer Intro +-
- ~~Vérifier les todos non gls~~
- Ajouter les sources manquantes +-
- Abstract
- Ajouter les gls + todos restants
- Page de garde avec logo + page vide + corriger numéro pages
- Relire
- Restorer modifications biblio
