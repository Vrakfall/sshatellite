# RxJava | ReactiveX

- GOTO 2013 • Functional Reactive Programming with RxJava • Ben Christensen
  - https://www.youtube.com/watch?v=_t06LRX0DV0
- Rx Design Guidelines
  - http://go.microsoft.com/fwlink/?LinkID=205219

# IDM (Vraiment intéressant ?)

- Shibboleth
  - https://www.shibboleth.net
  - https://web.archive.org/web/20070701112923/http://shibboleth.internet2.edu/latest.html
- Authentication and Authorization User Management within a Collaborative Community
  - http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.416.4349&rep=rep1&type=pdf

# User Management

- Towards Unified User Management for Achieving Cloud Adoption
  - http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.476.469&rep=rep1&type=pdf
- Federated identity management - AD FS for single sign-on and federated identity management
  - http://miun.diva-portal.org/smash/record.jsf?pid=diva2%3A556053&dswid=2349
  - http://miun.diva-portal.org/smash/get/diva2:556053/FULLTEXT01.pdf
- User Management in a Context-Aware Environment
  - http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.96.5762&rep=rep1&type=pdf

## Brevets

- Account synchronization for common identity in an unmanaged network
  - Brevet de microsoft de 2011
  - https://patentimages.storage.googleapis.com/5a/4c/2d/55502649903989/US7958543.pdf
- Account synchronization
  - Brevet de Bank of America Corporation de 2011.
  - http://patentimages.storage.googleapis.com/pdfs/US8001045.pdf
- User account establishment and synchronization in heterogeneous networks
  - https://patentimages.storage.googleapis.com/b7/09/c5/088ce3ab3a1151/US6269405.pdf

## Key management

- Separating key management from file system security
  - SFS, network file system that avoids internal key management.
  - http://www.news.cs.nyu.edu/~jinyang/sp07/papers/sfs.pdf
- Simplifying public key management
  - https://ieeexplore.ieee.org/abstract/document/1266303/
- Fast Authentication Public Key Infrastructure for Mobile Ad Hoc Networks Based on Trusted Computing
  - https://ieeexplore.ieee.org/document/4149404/
- Cryptographic Key Management Issues & Challenges in Cloud Services
  - https://ws680.nist.gov/publication/get_pdf.cfm?pub_id=914304

### Brevets

- User key management for the secure shell (SSH)
  - Brevet de SSH communications security corp, Helsinki, de 2013
    - https://patentimages.storage.googleapis.com/ca/b8/57/0a724a1196261d/US20130117554A1.pdf
  - Brevet de 2018, même corp.
    - https://patentimages.storage.googleapis.com/b3/8b/45/4cc1ddf47bcc80/US10003458.pdf
- Automated access, key, certificate, and credential management
  - https://patentimages.storage.googleapis.com/df/79/41/c1e40ae6ca0d95/US9515999.pdf

# Monitoring

## Software monitoring

- An Approach for Estimation of Software Aging in a Web Server
  - https://s3.amazonaws.com/academia.edu.documents/44558548/An_approach_for_estimation_of_software_a20160408-20493-j3ghv1.pdf?AWSAccessKeyId=AKIAIWOWYYGZ2Y53UL3A&Expires=1532876182&Signature=Uxxx2mxPRH8aKT5wBCqR6v8Sa7o%3D&response-content-disposition=inline%3B%20filename%3DAn_Approach_for_Estimation_of_Software_A.pdf
  - To read

## Server Monitoring

### Slightly useful

- User-level scheduling on NUMA multicore systems under Linux
  - \[In\] Proceedings of the Linux Symposium (2011)
  - https://s3.amazonaws.com/academia.edu.documents/30814388/ols2011.pdf?AWSAccessKeyId=AKIAIWOWYYGZ2Y53UL3A&Expires=1532949345&Signature=sDXqzmzLoTFklorgS1z4c2BgF6g%3D&response-content-disposition=inline%3B%20filename%3DTowards_co-existing_of_Linux_and_real-ti.pdf#page=81
- Self-monitoring Overhead of the Linux perf event Performance Counter Interface
  - Proposition d'amélioration pour l'implémentation de perf_event dans Linux.
  - http://web.eece.maine.edu/~vweaver/projects/perf_events/overhead/2015_ispass_overhead.pdf

## Network Monitoring

### Slightly useful

- Method and system for network fault monitoring with Linux
  - Brevet américain, il pourrait être intéressant de citer quelque chose sur la méthodologie.
  - http://patentimages.storage.googleapis.com/pdfs/US7296069.pdf

# Security

- Security Challenges in Cloud Computing
  - https://www.researchgate.net/profile/Levent_Ertaul/publication/267697749_Security_Challenges_in_Cloud_Computing/links/54984b260cf2519f5a1dddb4/Security-Challenges-in-Cloud-Computing.pdf

## SELinux

- Implementing SELinux as a Linux Security Module
  - http://www.cs.unibo.it/~sacerdot/doc/so/slm/selinux-module.pdf
- Linux Security Modules: General Security Support for the Linux Kernel
  - https://www.usenix.org/legacy/event/sec02/full_papers/wright/wright.pdf
- Integrating Flexible Support for Security Policies into the Linux Operating System
  - https://www.usenix.org/legacy/event/usenix01/freenix01/full_papers/loscocco/loscocco.pdf

## SSH

- Protecting SSH Servers with Single Packet Authorization
  - https://www.ee.ryerson.ca/~courses/coe518/LinuxJournal/elj2007-157-protectSSH-singlepkt.pdf
- A deeper understanding of SSH: Results from Internet-wide scans
  - https://ralphholz.science/publications/ADeeperUnderstandingOfSshResultsFromInternetwideScans.pdf
- SSH, The Secure Shell: The Definitive Guide
  - O'Reily book on SSH
  - https://cdn.preterhuman.net/texts/computing/security/SSH,%20The%20Secure%20Shell%20-%20The%20Definitive%20Guide%202001.pdf

## Key Checking

- Mining Your Ps and Qs: Detection of Widespread Weak Keys in Network Devices
  - Analysis of how many unsecure servers are online due to poor random number generation.
  - https://www.usenix.org/system/files/conference/usenixsecurity12/sec12-final228.pdf
- DoubleCheck: Multi-path Verification Against Man-in-the-Middle Attacks
  - http://web3.cs.columbia.edu/~angelos/Papers/2009/doublecheck.pdf
- Perspectives: Improving SSH-style Host Authentication with Multi-Path Probing
  - https://www.usenix.org/legacy/event/usenix08/tech/full_papers/wendlandt/wendlandt_html/

# Probably completely useless

- Adaptation to TV Delays Based on the User Behaviour
towards a Cheating-Free Second Screen Entertainment
  - https://hal.inria.fr/hal-01758439
  - https://hal.inria.fr/hal-01758439/document
- Improving Simulations of MPI Applications Using A
Hybrid Network Model with Topology and Contention
Support
  - https://hal.inria.fr/hal-00821446
  - https://hal.inria.fr/hal-00821446/document
- Using PAPI for hardware performance monitoring on Linux
systems
  - https://www.researchgate.net/profile/Dan_Terpstra/publication/241410487_Using_PAPI_for_Hardware_Performance_Monitoring_on_Linux_Systems/links/0c960529fa70adbfe3000000.pdf
- Perfmon2: a flexible performance monitoring interface for Linux
  - \[In\] Proceedings of the Linux Symposium - 2006 - Volume One
  - https://pdfs.semanticscholar.org/355a/c65470c38333b26d55b7c8493d93c419cd2e.pdf#page=269
- PARMON: a portable and scalable monitoring system for clusters
  - http://cloudbus.org/papers/parmon.pdf
- Method and apparatus for a key-management scheme for internet protocols
  - https://patentimages.storage.googleapis.com/7e/86/e8/2777ccc32b5820/US5588060.pdf
