# Ce qui marche

- Connexion de l'app
- Détection version python la plus haute
  - Utilise cette version pour créer un venv
- Envoie les scripts python
  - Appelle les scripts lorsque nécessaire pour avoir l'état de la machine
- Notifie lorsque certains paramètres dépassent un certain pourcentage

# Ce qui reste à faire (dans l'implémentation, mais prévu dans la rédaction)

- Améliorer l'interface pour pouvoir prendre en charge plusieurs serveurs
- Intégrer plus de paramètres
- Permettre à l'utilisateur de choisir quels paramètres déclenche les notifications
- Proxying/Hopping
- Faire des tests pour calculer l'overhead
- Faire un service pour alerter l'utilisateur lorsque l'activité n'est pas active

rrdtools