# Backend installation

- Having python as a requirement on the host machine seems to be the sanest choice.

    - It allows to generalize the target machine while avoiding most of the problems raised by the fact we don't know in advance what is the specific machine it will installed on.

    - C++ and most of other languages often require to build for a specific target. Some source code made for an old version of gcc, for example, might become obsolete in a future version due to specification changes.

    - It is also very easy to maintain and it could be understood by a larger portion of developers \[numbers?\] than with things like shell scripts. (OOP, easier to read, libraries, etc...)

    - It allows to use a virtual environment to install specific dependencies. All it requires is an `HTTP(S)` connection and a python version that meets the requirements of our code.

    - It is then more future-proof than most other possible choices.

    - It's still possible to use shell scripts as a fallback but then the output needs parsing and we lose many features.

- It's sane to try to download the install script and python dependencies through `HTTPS` as long as there's a server redundancy (easy to implement).

    - An interesting fallback could be to download them from the phone and then copy them to the target machine, in case it doesn't have access to `HTTP(S)` connections or it doesn't have a suitable program to download it (`wget`, `curl`, etc...).

    - With could say that an user with more very specific machines could adapt his environment by himself so it meets our requirements.

    - TODO: Other details?

- Compression

    - `gzip` or another tool should be present. \[Even more chance than python?\]

    - TODO: Other details?

- Flutter, cordova and other multi-platform environments?

    - Problems with libs like SSH?