package be.vrakfall.sshatellite;

import be.vrakfall.sshatellite.system.commands.top.BSDCPUUsage;
import be.vrakfall.sshatellite.system.commands.top.BSDTop;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will synchronousExecute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
	@Test
	public void addition_isCorrect() {
		assertEquals(4, 2 + 2);
	}

	@Test
	public void topCPURegexHasRightGroupAmount() {
		final Pattern cpuPattern = Pattern.compile(BSDTop.CPU_REGEX);
		final Matcher cpuMatcher = cpuPattern.matcher("");
		assertEquals(cpuMatcher.groupCount(), BSDTop.CPU_OUTPUT_FORMATS.length);
	}

	@Test
	public void topCPUPatternJustCompilesRegex() {
		assertEquals(BSDTop.CPU_PATTERN.pattern(), BSDTop.CPU_REGEX);
	}

	@Test
	public void topCPUOutputFormatsExtendNumber() {
		for (int i = 0; i < BSDTop.CPU_OUTPUT_FORMATS.length; i++) {
			Class formatClass = BSDTop.CPU_OUTPUT_FORMATS[i];
			assertTrue(String.format(Locale.US, "BSDTop.CPU_OUTPUT_FORMATS isn't filled with number only classes. The format at index %d is of type %s.", i, formatClass.getSimpleName()), Number.class.isAssignableFrom(formatClass));
		}
	}

	@Test
	public void topCPUOutputFormatConformsSample() {
		final Pattern cpuPattern = Pattern.compile(BSDTop.CPU_REGEX);
		final Matcher cpuMatcher = cpuPattern.matcher(BSDTop.TEST_SAMPLE);

		while (cpuMatcher.find()) {
			for (int i = 1; i <= cpuMatcher.groupCount(); i++) {
				Class<?> formatClass = BSDTop.CPU_OUTPUT_FORMATS[i - 1];

				try {
					formatClass.getMethod("valueOf", String.class).invoke(null, cpuMatcher.group(i));
				}
				catch (NoSuchMethodException e) {
					fail("The BSDTop.CPU_OUTPUT_FORMATS variable should be from a number class that has a method `valueOf(String s).");
				}
				catch (IllegalAccessException e) {
					fail(String.format(Locale.US, "Tried to access to a private instance of %s.valueOf(String s).\n%s", formatClass.getSimpleName(), e));
				}
				catch (InvocationTargetException e) {
					if (e.getCause() instanceof NumberFormatException) {
						fail(String.format(Locale.US, "Format at index %d defined in BSDTop.CPU_OUTPUT_FORMATS doesn't fit the sample. Format is %s when the `group` found is \"%s\".\n" +
							"The entire line was:\n%s", i - 1, formatClass.getSimpleName(), cpuMatcher.group(i), cpuMatcher.group(0)));
					}
					else {
						fail(String.format(Locale.US, "%s.valueOf(String s) threw an unknown exception:\n%s", formatClass.getSimpleName(), e.getCause()));
					}
				}
			}
		}
	}

	@Test
	public void bsdCPUUsageHasRightConstructorForOutputFormats() {
		for (Constructor constructor : BSDCPUUsage.class.getConstructors()) {
			if (constructor.getParameterCount() == BSDTop.CPU_OUTPUT_FORMATS.length) {
				boolean typesAreGood = true;
				Class<?>[] parameterTypes = constructor.getParameterTypes();

				for (int i = 0; i < parameterTypes.length; i++) {
					try {
						if (!parameterTypes[i].equals(BSDTop.CPU_OUTPUT_FORMATS[i].getField("TYPE").get(null))) {
							typesAreGood = false;
							break;
						}
					}
					catch (NoSuchFieldException | IllegalAccessException e) {
						fail("One of the classes within BSDTop.CPU_OUTPUT_FORMATS is probably not a subclass of `Number`. It either has no static field called `TYPE` or it is not accessible.");
					}
				}

				if (typesAreGood) {
					return;
				}
			}
		}

		fail("BSDCPUUsage doesn't have a constructor that the BSDTop.CPU_OUTPUT_FORMATS conform. Keep in mind that this is looking for primitive types (like `int`) within the constructors' parameters when the formats array is made of classes like `Integer`.");
	}

	@Test
	public void nawak() {
		// Put your sudden urge of a test over here
	}
}