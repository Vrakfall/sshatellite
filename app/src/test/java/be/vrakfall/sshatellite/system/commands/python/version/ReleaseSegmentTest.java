package be.vrakfall.sshatellite.system.commands.python.version;

import be.vrakfall.sshatellite.system.commands.python.version.errors.ReleaseSegmentParsingException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

//import static org.junit.jupiter.api.Assertions.assertEquals;

class ReleaseSegmentTest {
	private static ReleaseSegment releaseSegment2_1_3 = new ReleaseSegment(2L, 1L, 3L);
	private static String releaseSegmentString2_1_3 = "2.1.3";

	private static ReleaseSegment releaseSegment2_4_3 = new ReleaseSegment(2L, 4L, 3L);
	private static String releaseSegmentString2_4_3 = "2.4.3";

	private static ReleaseSegment releaseSegment2_4_1 = new ReleaseSegment(2L, 4L, 1L);
	private static String releaseSegmentString2_4_1 = "2.4.1";

	private static ReleaseSegment releaseSegment1_4_3 = new ReleaseSegment(1L, 4L, 3L);
	private static String releaseSegmentString1_4_3 = "1.4.3";

	private static ReleaseSegment releaseSegment1 = new ReleaseSegment(1L);
	private static String releaseSegmentString1 = "1";

	private static ReleaseSegment releaseSegment1_bis = new ReleaseSegment(1L);

	private static ReleaseSegment releaseSegment2 = new ReleaseSegment(2L);
	private static String releaseSegmentString2 = "2";

	private static ReleaseSegment releaseSegment2_5 = new ReleaseSegment(2L, 5L);
	private static String releaseSegmentString2_5 = "2.5";

	private static ReleaseSegment releaseSegment2_5_bis = new ReleaseSegment(2L, 5L);

	private static ReleaseSegment releaseSegment2_4_3_1 = new ReleaseSegment(2L, 4L, 3L, 1L);
	private static String releaseSegmentString2_4_3_1 = "2.4.3.1";

	private static ReleaseSegment releaseSegment2_5_3 = new ReleaseSegment(2L, 5L, 3L);
	private static String releaseSegmentString2_5_3 = "2.5.3";

	private static ReleaseSegment releaseSegment2_5_3_bis = new ReleaseSegment(2L, 5L, 3L);

	@Test
	void throwsIllegalArgumentExceptionIfWrongAmountOfSubVersions() {
		assertThrows(IllegalArgumentException.class, ReleaseSegment::new);
	}

	@Test
	void getRightSubVersion() {
		ReleaseSegment releaseSegment1_2_3 = new ReleaseSegment(1L, 2L, 3L);

		assertEquals(1, releaseSegment1_2_3.getSubVersion(0));
		assertEquals(2, releaseSegment1_2_3.getSubVersion(1));
		assertEquals(3, releaseSegment1_2_3.getSubVersion(2));

		assertEquals(1, releaseSegment1_2_3.getMajorVersion());
		assertEquals(2, releaseSegment1_2_3.getMinorVersion());
		assertEquals(3, releaseSegment1_2_3.getPatchVersion());
	}

	@Test
	void comparisonIsCorrect() {
		// Should be lower
		// | Same size

		assertEquals(-1, releaseSegment2_1_3.compareTo(releaseSegment2_4_3));
		assertEquals(-1, releaseSegment2_4_1.compareTo(releaseSegment2_4_3));
		assertEquals(-1, releaseSegment1_4_3.compareTo(releaseSegment2_4_3));
		assertEquals(-1, releaseSegment1.compareTo(releaseSegment2));

		// | Different size

		assertEquals(-1, releaseSegment1.compareTo(releaseSegment2_5));
		assertEquals(-1, releaseSegment2.compareTo(releaseSegment2_5));
		assertEquals(-1, releaseSegment1.compareTo(releaseSegment2_4_3_1));
		assertEquals(-1, releaseSegment2.compareTo(releaseSegment2_4_3_1));
		assertEquals(-1, releaseSegment2_4_3_1.compareTo(releaseSegment2_5));
		assertEquals(-1, releaseSegment2_4_3.compareTo(releaseSegment2_4_3_1));
		assertEquals(-1, releaseSegment2_4_3_1.compareTo(releaseSegment2_5_3));

		// Should be equal

		assertEquals(0, releaseSegment1.compareTo(releaseSegment1_bis));
		assertEquals(0, releaseSegment2_5.compareTo(releaseSegment2_5_bis));
		assertEquals(0, releaseSegment2_5_3.compareTo(releaseSegment2_5_3_bis));

		// Should be higher
		// | Same size

		assertEquals(1, releaseSegment2_4_3.compareTo(releaseSegment2_1_3));
		assertEquals(1, releaseSegment2_4_3.compareTo(releaseSegment2_4_1));
		assertEquals(1, releaseSegment2_4_3.compareTo(releaseSegment1_4_3));
		assertEquals(1, releaseSegment2.compareTo(releaseSegment1));

		// | Different size

		assertEquals(1, releaseSegment2_5.compareTo(releaseSegment1));
		assertEquals(1, releaseSegment2_5.compareTo(releaseSegment2));
		assertEquals(1, releaseSegment2_4_3_1.compareTo(releaseSegment1));
		assertEquals(1, releaseSegment2_4_3_1.compareTo(releaseSegment2));
		assertEquals(1, releaseSegment2_5.compareTo(releaseSegment2_4_3_1));
		assertEquals(1, releaseSegment2_4_3_1.compareTo(releaseSegment2_4_3));
		assertEquals(1, releaseSegment2_5_3.compareTo(releaseSegment2_4_3_1));
	}

	@Test
	void fromString() {
		try {
			assertEquals(releaseSegment2_1_3, ReleaseSegment.fromString(releaseSegmentString2_1_3));
			assertEquals(releaseSegment2_4_3, ReleaseSegment.fromString(releaseSegmentString2_4_3));
			assertEquals(releaseSegment2_4_1, ReleaseSegment.fromString(releaseSegmentString2_4_1));
			assertEquals(releaseSegment1_4_3, ReleaseSegment.fromString(releaseSegmentString1_4_3));
			assertEquals(releaseSegment1, ReleaseSegment.fromString(releaseSegmentString1));
			assertEquals(releaseSegment2, ReleaseSegment.fromString(releaseSegmentString2));
			assertEquals(releaseSegment2_5, ReleaseSegment.fromString(releaseSegmentString2_5));
			assertEquals(releaseSegment2_4_3_1, ReleaseSegment.fromString(releaseSegmentString2_4_3_1));
			assertEquals(releaseSegment2_5_3, ReleaseSegment.fromString(releaseSegmentString2_5_3));
		}
		catch (ReleaseSegmentParsingException e) {
			fail(e);
		}
	}
}
