package be.vrakfall.sshatellite.system.commands.python.version;

import be.vrakfall.sshatellite.utils.io.errors.NoMatchFoundException;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static be.vrakfall.sshatellite.system.commands.python.version.PEP440Version.NO_EPOCH_VALUE;
import static be.vrakfall.sshatellite.system.commands.python.version.ReleaseState.*;
import static org.junit.jupiter.api.Assertions.*;

public class PEP440VersionTest {
	private static PEP440Version pep440VersionExample0 = new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(456L));
	private static PEP440Version pep440VersionExample0_identical = new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(456L));

	// TODO: 4/10/18 Add the local version segment.
	private static PEP440Version[] expectedOrder = {
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, ALPHA), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, BETA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(457L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(346L), null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, new ComparableVersionSegment(456)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456L), new ComparableVersionSegment(34L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456), null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 1L), null, null, new ComparableVersionSegment(1L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 1L), null, null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, ALPHA), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, BETA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(457L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(346L), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, new ComparableVersionSegment(456)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456L), new ComparableVersionSegment(34L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 1L), null, null, new ComparableVersionSegment(1L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 1L), null, null, null),
		new PEP440Version(2L, new ReleaseSegment(1L, 1L), null, null, null)
	};

	// TODO: 6/10/18 Add the different possibilities to write the same version.
	private static String[] expectedOrderAsStrings = {
		"1.0.dev456",
		"1.0a1",
		"1.0a2.dev456",
		"1.0a12.dev456",
		"1.0a12",
		"1.0b1.dev456",
		"1.0b2",
		"Python 1.0b2.post345.dev456",
		"1.0b2.post345.dev457",
		"1.0b2.post345",
		"v1.0b2.post346",
		"1.0rc1.dev456",
		"SomeProgram v1.0rc1",
		"1.0rc2",
		"1.0",
		"1.0+abc.5",
		"1.0+abc.7",
		"1.0+5",
		"1.0.post456.dev34",
		"1.0.post456",
		"1.1.dev1",
		"1.1",
		"1!1.0.dev456",
		"1!1.0a1",
		"1!1.0a2.dev456",
		"1!1.0a12.dev456",
		"v1!1.0a12",
		"1!1.0b1.dev456",
		"v1!1.0b2",
		"1!1.0b2.post345.dev456",
		"1!1.0b2.post345.dev457",
		"1!1.0b2.post345",
		"1!1.0b2.post346",
		"1!1.0rc1.dev456",
		"1!1.0rc1",
		"1!1.0rc2",
		"1!1.0",
		"1!1.0.post456.dev34+abc.5",
		"SomeOtherProgram 1!1.0.post456+abc.7",
		"Python v1!1.1.dev1+5",
		"1!1.1",
		"2!1.1"
	};

	private static PEP440Version[] randomizedVersions0 = {
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(457L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, null, new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 1L), null, null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456L), new ComparableVersionSegment(34L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, BETA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, ALPHA), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(346L), null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 1L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456L), new ComparableVersionSegment(34L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, ALPHA), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(2L, new ReleaseSegment(1L, 1L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, BETA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, new ComparableVersionSegment(456)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, new ComparableVersionSegment(456)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456), null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(457L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(346L), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 1L), null, null, new ComparableVersionSegment(1L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 1L), null, null, new ComparableVersionSegment(1L))
	};

	private static PEP440Version[] randomizedVersions1 = {
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 1L), null, null, new ComparableVersionSegment(1L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(346L), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, null, new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 1L), null, null, new ComparableVersionSegment(1L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, BETA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 1L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, new ComparableVersionSegment(456)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(2L, new ReleaseSegment(1L, 1L), null, null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, ALPHA), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(457L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456L), new ComparableVersionSegment(34L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456L), new ComparableVersionSegment(34L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, new ComparableVersionSegment(456)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 1L), null, null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, BETA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(457L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, ALPHA), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(346L), null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, new ComparableVersionSegment(456L))
	};

	private static PEP440Version[] randomizedVersions2 = {
		new PEP440Version(2L, new ReleaseSegment(1L, 1L), null, null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456L), new ComparableVersionSegment(34L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, BETA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, new ComparableVersionSegment(456)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, ALPHA), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 1L), null, null, new ComparableVersionSegment(1L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, null, new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 1L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(346L), null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456L), new ComparableVersionSegment(34L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(457L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(457L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, new ComparableVersionSegment(456), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 1L), null, null, new ComparableVersionSegment(1L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 1L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, BETA), null, new ComparableVersionSegment(456L)),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, ALPHA), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(346L), null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(12L, ALPHA), null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, BETA), new ComparableVersionSegment(345L), null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), null, null, null),
		new PEP440Version(1L, new ReleaseSegment(1L, 0L), new PreReleaseSegment(1L, RELEASE_CANDIDATE), null, new ComparableVersionSegment(456)),
		new PEP440Version(NO_EPOCH_VALUE, new ReleaseSegment(1L, 0L), new PreReleaseSegment(2L, RELEASE_CANDIDATE), null, null)
	};

	/**
	 * This only tests if the examples given in the arrays have the same size as the expected result.
	 */
	@Test
	void testExampleArraysHaveTheRightSize() {
		assertEquals(expectedOrder.length, expectedOrderAsStrings.length);
		assertEquals(expectedOrder.length, randomizedVersions0.length);
		assertEquals(expectedOrder.length, randomizedVersions1.length);
		assertEquals(expectedOrder.length, randomizedVersions2.length);
	}

	@Test
	void equals() {
		assertEquals(pep440VersionExample0, pep440VersionExample0_identical);
		for (int i = 0; i < expectedOrder.length; i++) {
			for (int j = i + 1; j < expectedOrder.length; j++) {
				if (i >= 14 && i <= 17 && j >= 14 && j <= 17) {
					assertEquals(expectedOrder[i], expectedOrder[j]);
				}
				else {
					assertNotEquals(expectedOrder[i], expectedOrder[j]);
				}
			}
		}
	}

	@Test
	void comparisonIsCorrect() {
		PEP440Version[] result = randomizedVersions0.clone();
		Arrays.sort(result);
		assertArrayEquals(expectedOrder, result);

		result = randomizedVersions1.clone();
		Arrays.sort(result);
		assertArrayEquals(expectedOrder, result);

		result = randomizedVersions2.clone();
		Arrays.sort(result);
		assertArrayEquals(expectedOrder, result);
	}

	@Test
	void fromVersionString() {
		for (int i = 0; i < expectedOrder.length; i++) {
			try {
				assertEquals(expectedOrder[i], PEP440Version.fromVersionString(expectedOrderAsStrings[i]));
			}
			catch (NoMatchFoundException e) {
				fail(e);
			}
		}
	}
}
