package be.vrakfall.sshatellite.system.commands.python.version;

import be.vrakfall.sshatellite.system.commands.python.version.errors.ReleaseStateParsingException;
import org.junit.jupiter.api.Test;

import static be.vrakfall.sshatellite.system.commands.python.version.ReleaseState.*;
import static org.junit.jupiter.api.Assertions.*;

class ReleaseStateTest {
	@SuppressWarnings("SpellCheckingInspection")
	private static String obviouslyWrongLabel = "dr3t57hnf38u14,5fuyj4n468drn642dn";

	@Test
	void fromLabelTest() {
		try {
			for (String alphaLabel : ALPHA_LABELS) {
				assertEquals(ALPHA, fromLabel(alphaLabel));
			}

			for (String betaLabel : BETA_LABELS) {
				assertEquals(BETA, fromLabel(betaLabel));
			}

			for (String releaseCandidateLabel : RELEASE_CANDIDATE_LABELS) {
				assertEquals(RELEASE_CANDIDATE, fromLabel(releaseCandidateLabel));
			}

			for (String finalLabel : FINAL_LABELS) {
				assertEquals(FINAL, fromLabel(finalLabel));
			}
		}
		catch (ReleaseStateParsingException e) {
			fail(e);
		}

		assertThrows(ReleaseStateParsingException.class, () -> fromLabel(obviouslyWrongLabel));
	}
}