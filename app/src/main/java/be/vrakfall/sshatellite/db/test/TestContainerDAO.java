package be.vrakfall.sshatellite.db.test;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface TestContainerDAO {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
  @Query("SELECT * FROM container")
  List<TestContainer> getAll();

  @Insert
  long[] insert(TestContainer... containers);

  @Delete
  void delete(TestContainer container);
}
