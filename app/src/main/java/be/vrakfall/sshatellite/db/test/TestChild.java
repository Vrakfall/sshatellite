package be.vrakfall.sshatellite.db.test;

import androidx.room.Entity;
import lombok.*;

@Value
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity(tableName = "children")
public class TestChild extends TestParent {
  //==== Static variables ====//

  public static final TestChild IRRELEVANT = new TestChild("", "");

  //==== > Error messages ====//

  //==== Attributes ====//

  private String childName;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  public TestChild(String parentName, String childName) {
    super(parentName);
    this.childName = childName;
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
