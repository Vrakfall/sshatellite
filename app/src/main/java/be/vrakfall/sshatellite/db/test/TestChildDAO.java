package be.vrakfall.sshatellite.db.test;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface TestChildDAO {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @Query("SELECT * FROM children")
  List<TestChild> getAll();

  @Query("SELECT * FROM children WHERE id = :id")
  TestChild get(long id);

  @Insert
  void insert(TestChild... children);

  @Delete
  void delete(TestChild child);
}
