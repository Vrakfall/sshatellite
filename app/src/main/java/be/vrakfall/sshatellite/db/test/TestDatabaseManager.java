package be.vrakfall.sshatellite.db.test;

import be.vrakfall.sshatellite.utils.annotations.NotNull;
import java9.util.stream.Collectors;
import java9.util.stream.StreamSupport;
import lombok.Value;

import java.util.List;

@Value
public class TestDatabaseManager {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  private TestChildDAO testChildDAO;

  private TestContainerDAO testContainerDAO;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  public TestDatabaseManager(@NotNull TestDatabase testDatabase) {
    testChildDAO = testDatabase.testChildDAO();
    testContainerDAO = testDatabase.testContainerDAO();
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  public List<TestContainer> getAllContainers() {
    return
        // Next lines equals: testContainerDAO.getAll().stream()
        StreamSupport.parallelStream(testContainerDAO.getAll())
        .map(testContainer ->
            testContainer.withChild(
                testContainer,
                testChildDAO.get(testContainer.getChildId())
            )
        )
        .collect(Collectors.toList());
  }
}
