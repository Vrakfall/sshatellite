package be.vrakfall.sshatellite.db.test;

import android.content.Context;
import android.util.Log;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import lombok.val;

import java.util.Arrays;
import java.util.Locale;

@Database(entities = {TestChild.class, TestContainer.class}, version = 1)
public abstract class TestDatabase extends RoomDatabase {
  //==== Static variables ====//

  public static final String TAG = TestDatabase.class.getSimpleName();

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  /**
   * Test a few features of Room to see how it works with SQLite.
   * @param context The context of the application.
   */
  public static void testTheDatabase(@NotNull Context context) {
    val testDatabase = Room
        .databaseBuilder(context, TestDatabase.class, "test_database")
        .allowMainThreadQueries().build();
    val testDatabaseManager = new TestDatabaseManager(testDatabase);
    val testChild = new TestChild("The name of the parent", "the name of the child");
    testDatabase.testChildDAO().insert(testChild);
    val testContainer = new TestContainer(testChild, "container name");
    val newIds = testDatabase.testContainerDAO().insert(testContainer);
    Log.d(TAG, String.format(Locale.US, "testTheDatabase: new ids: %s", Arrays.toString(newIds)));
    val requestedChildren = testDatabase.testChildDAO().getAll();
    Log.d(TAG, String.format(Locale.US, "Requested children: %s", requestedChildren.toString()));
    val requestedContainers = testDatabaseManager.getAllContainers();
    Log.d(TAG, String.format(Locale.US, "onStart: Containers: %s", requestedContainers));
  }

  //==== Object Methods ====//

  public abstract TestChildDAO testChildDAO();

  public abstract TestContainerDAO testContainerDAO();
}
