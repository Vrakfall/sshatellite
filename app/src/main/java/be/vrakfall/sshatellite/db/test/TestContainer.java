package be.vrakfall.sshatellite.db.test;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import lombok.Data;
import lombok.Value;
import org.jetbrains.annotations.Contract;

@Data
@Entity(tableName = "container")
public class TestContainer {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  @PrimaryKey(autoGenerate = true)
  private long id;

  @ForeignKey(entity = TestChild.class, parentColumns = "id", childColumns = "childId")
  private long childId;

  @Ignore
  @NotNull
  private TestChild child;

  @NotNull
  private String name;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  public TestContainer(long id, long childId, @NotNull String name) {
    this.id = id;
    this.childId = childId;
    this.name = name;
    child = TestChild.IRRELEVANT;
  }

  public TestContainer(long id, @NotNull TestChild child, @NotNull String name) {
    this.id = id;
    this.child = child;
    this.childId = child.getId();
    this.name = name;
  }

  public TestContainer(@NotNull TestChild child, @NotNull String name) {
    this.child = child;
    this.childId = child.getId();
    this.name = name;
  }

  @Ignore
  private TestContainer(long id, long childId, @NotNull TestChild child, @NotNull String name) {
    this.id = id;
    this.childId = childId;
    this.child = child;
    this.name = name;
  }

  //==== Getters and Setters ====//

  @NotNull
  @Contract("_, _ -> new")
  public TestContainer withChild(@NotNull TestContainer testContainer, TestChild testChild) {
    return new TestContainer(testContainer.getId(), testContainer.getChildId(), testChild, testContainer.name);
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
