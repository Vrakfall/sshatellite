package be.vrakfall.sshatellite.db.test;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.concurrent.atomic.AtomicLong;

@ToString
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter(AccessLevel.PUBLIC)
public abstract class TestParent {
  //==== Static variables ====//

  public static final AtomicLong nextId = new AtomicLong(0);

  //==== > Error messages ====//

  //==== Attributes ====//

  @PrimaryKey
  private long id;

  private String parentName;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  public TestParent(String parentName) {
    initializeId();
    this.parentName = parentName;
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  private void initializeId() {
    this.id = nextId.getAndIncrement();
  }
}
