package be.vrakfall.sshatellite.system.errors;

public class WrongLoadAverageAmountException extends Exception {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public WrongLoadAverageAmountException() {
  }

  public WrongLoadAverageAmountException(String message) {
    super(message);
  }

  public WrongLoadAverageAmountException(String message, Throwable cause) {
    super(message, cause);
  }

  public WrongLoadAverageAmountException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
