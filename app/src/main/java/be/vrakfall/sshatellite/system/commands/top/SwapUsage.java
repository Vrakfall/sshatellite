package be.vrakfall.sshatellite.system.commands.top;

// TODO: 18/09/18 When a better representation of byteSize/Amount is done, make it so we can estimate used/free part of the swap.

import be.vrakfall.sshatellite.utils.annotations.NotNull;

public class SwapUsage {
  //==== Static variables ====//

  //==== Attributes ====//

  @NotNull
  private ByteAmount total;
  @NotNull
  private ByteAmount used;
  @NotNull
  private ByteAmount free;

  //==== Getters and Setters ====//

  public SwapUsage(@NotNull ByteAmount total, @NotNull ByteAmount free) {
    setTotal(total);
    setFree(free);
    setUsed(new ByteAmount());
  }

  public SwapUsage(@NotNull ByteAmount total, @NotNull ByteAmount used, @NotNull ByteAmount free) {
    setTotal(total);
    setUsed(used);
    setFree(free);
  }

  @NotNull
  public ByteAmount getTotal() {
    return total;
  }

  public void setTotal(@NotNull ByteAmount total) {
    if (total.getQuantity() < 0) {
      this.total = new ByteAmount();
    }
    else {
      this.total = total;
    }
  }

  @NotNull
  public ByteAmount getUsed() {
    return used;
  }

  public void setUsed(@NotNull ByteAmount used) {
    if (used.getQuantity() < 0) {
      this.used = new ByteAmount();
    }
    else {
      this.used = used;
    }
  }

  //==== Constructors ====//

  @NotNull
  public ByteAmount getFree() {
    return free;
  }

  public void setFree(@NotNull ByteAmount free) {
    if (free.getQuantity() < 0) {
      this.free = new ByteAmount();
    }
    else {
      this.free = free;
    }
  }
//==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    SwapUsage swapUsage = (SwapUsage) o;

    if (!total.equals(swapUsage.total)) return false;
    return free.equals(swapUsage.free);
  }

  @Override
  public int hashCode() {
    int result = total.hashCode();
    result = 31 * result + free.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "SwapUsage{" +
        "total=" + total +
        ", free=" + free +
        '}';
  }
}
