package be.vrakfall.sshatellite.system.commands.python.version;

import be.vrakfall.sshatellite.system.commands.python.version.errors.NonExistingSegmentException;
import be.vrakfall.sshatellite.utils.annotations.NotNull;

import java.util.Locale;

public abstract class VersionSegment {
  //==== Static variables ====//

  public static final long VERSION_MIN = 0;
  public static final long VERSION_MAX = Long.MAX_VALUE; // Not used yet.

  //==== > Error messages ====//

  public static String NO_VERSION_SEGMENT_FOUND = "No correct version segment found in the string: %s";

  //==== Attributes ====//

  private long version = VERSION_MIN;

  //==== Getters and Setters ====//

  public VersionSegment(long version) {
    setVersion(version);
  }

  /**
   * Copy constructor.
   *
   * @param other The object to copy.
   */
  public VersionSegment(@NotNull VersionSegment other) {
    this.version = other.version;
  }

  //==== Constructors ====//

  public long getVersion() {
    return version;
  }

  private void setVersion(long version) {
    if (version >= VERSION_MIN) {
      this.version = version;
    }
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  public static long parseVersionNumber(@NotNull String string) throws NonExistingSegmentException {
    try {
      return Long.parseLong(string);
    }
    catch (NumberFormatException e) {
      throw new NonExistingSegmentException(String.format(Locale.US, NO_VERSION_SEGMENT_FOUND, string), e);
    }
  }

  //====== Object Methods ======//

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    VersionSegment that = (VersionSegment) o;

    return getVersion() == that.getVersion();
  }

  @Override
  public int hashCode() {
    return (int) (getVersion() ^ (getVersion() >>> 32));
  }

  @Override
  public String toString() {
    return "VersionSegment{" +
        "version=" + version +
        '}';
  }
}
