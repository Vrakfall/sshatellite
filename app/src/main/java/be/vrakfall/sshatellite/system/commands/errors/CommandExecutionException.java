package be.vrakfall.sshatellite.system.commands.errors;

import lombok.AllArgsConstructor;

public class CommandExecutionException extends Exception {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  public CommandExecutionException() {
    super();
  }

  public CommandExecutionException(String message) {
    super(message);
  }

  public CommandExecutionException(String message, Throwable cause) {
    super(message, cause);
  }

  public CommandExecutionException(Throwable cause) {
    super(cause);
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
