package be.vrakfall.sshatellite.system.errors;

public abstract class MachineException extends Exception {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public MachineException() {
  }

  public MachineException(String message) {
    super(message);
  }

  public MachineException(String message, Throwable cause) {
    super(message, cause);
  }

  public MachineException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
