package be.vrakfall.sshatellite.system.commands.top;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class LinuxCPUUsage extends CPUUsage {
  //==== Static variables ====//

  //==== Attributes ====//

  /**
   * Represents the percentage of time spent by this CPU waiting for I/O completion.
   */
  public final float IO_WAIT;

  /**
   * Represents the percentage of time spent by this CPU servicing hardware interrupts.
   */
  public final float HARDWARE_INTERRUPTS;

  /**
   * Represents the percentage of time spent by this CPU servicing software interrupts.
   */
  public final float SOFTWARE_INTERRUPTS;

  /**
   * Represents the percentage of time stolen from a virtual machine by the hypervisor on this CPU.
   */
  // TODO: 10/07/18 Check the validity of the previous comment (in the javadoc).
  public final float STOLEN;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public LinuxCPUUsage(int NUMBER, float USER, float NICE, float SYSTEM, float IDLE, float IO_WAIT, float HARDWARE_INTERRUPTS, float SOFTWARE_INTERRUPTS, float STOLEN) {
    super(NUMBER, USER, NICE, SYSTEM, IDLE);
    this.IO_WAIT = IO_WAIT;
    this.HARDWARE_INTERRUPTS = HARDWARE_INTERRUPTS;
    this.SOFTWARE_INTERRUPTS = SOFTWARE_INTERRUPTS;
    this.STOLEN = STOLEN;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
