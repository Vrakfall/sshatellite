package be.vrakfall.sshatellite.system.commands;

import be.vrakfall.sshatellite.network.errors.ActionOnNotAuthenticatedConnectionException;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import net.schmizz.sshj.connection.ConnectionException;
import net.schmizz.sshj.transport.TransportException;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

/**
 * Means that we can synchronousExecute system commands on the object, like `cd` or `ls`.
 */
public interface Commandable {
  // TODO: 22/10/18 Handle the different types of exceptions better
  ExecutionHandler execute(String command) throws IOException;

  InputStream getInputStreamTest(String command) throws ActionOnNotAuthenticatedConnectionException,
      ConnectionException, TransportException;

  void stopExecution(@Nullable Closeable execution) throws ConnectionException, TransportException;
}
