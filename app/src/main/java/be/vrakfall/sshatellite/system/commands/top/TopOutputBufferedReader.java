package be.vrakfall.sshatellite.system.commands.top;

import be.vrakfall.sshatellite.system.monitoring.state.ProcessInfo;
import be.vrakfall.sshatellite.system.monitoring.state.MachineState;
import be.vrakfall.sshatellite.system.monitoring.state.MemoryUsage;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.io.ExtendedBufferedReader;
import be.vrakfall.sshatellite.utils.io.errors.NoMatchFoundException;

import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Map;

public abstract class TopOutputBufferedReader extends ExtendedBufferedReader {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public TopOutputBufferedReader(@NotNull final Reader in, final int sz) {
    super(in, sz);
  }

  public TopOutputBufferedReader(@NotNull final Reader in) {
    super(in);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  @NotNull
  public abstract LoadAveragesAndTimeInfo readAndParseIfTopFirstLine(int goodElementsFound) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract LoadAveragesAndTimeInfo readAndParseIfTopFirstLine(int goodElementsFound, boolean verboseException) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract LoadAveragesAndTimeInfo readAndParseNextTopFirstLine() throws IOException, NoMatchFoundException;

  @NotNull
  public abstract LoadAveragesAndTimeInfo readAndParseNextTopFirstLine(boolean verboseException) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract ProcessRecap readAndParseIfProcessRecapLine() throws IOException, NoMatchFoundException;

  @NotNull
  public abstract ProcessRecap readAndParseIfProcessRecapLine(boolean verboseException) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract CPUUsage readAndParseIfCPUUsageLine() throws IOException, NoMatchFoundException;

  @NotNull
  public abstract CPUUsage readAndParseIfCPUUsageLine(boolean verboseException) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract List<CPUUsage> readAndParseAllCPUUsageLines() throws IOException, NoMatchFoundException;

  @NotNull
  public abstract List<CPUUsage> readAndParseAllCPUUsageLines(boolean verboseException) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract MemoryUsage readAndParseIfMemoryUsageLine() throws IOException, NoMatchFoundException;

  @NotNull
  public abstract MemoryUsage readAndParseIfMemoryUsageLine(boolean verboseException) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract ARCUsage readAndParseIfARCUsageLine() throws IOException, NoMatchFoundException;

  @NotNull
  public abstract ARCUsage readAndParseIfARCUsageLine(boolean verboseException) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract ARCCompression readAndParseIfARCCompressionLine() throws IOException, NoMatchFoundException;

  @NotNull
  public abstract ARCCompression readAndParseIfARCCompressionLine(boolean verboseException) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract SwapUsage readAndParseIfSwapUsageLine() throws IOException, NoMatchFoundException;

  @NotNull
  public abstract SwapUsage readAndParseIfSwapUsageLine(boolean verboseException) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract boolean readAndTrashIfProcessColumnTitles(boolean verboseException) throws IOException;

  @NotNull
  public abstract ProcessInfo readAndParseIfProcessDetailsLine() throws IOException, NoMatchFoundException;

  @NotNull
  public abstract ProcessInfo readAndParseIfProcessDetailsLine(boolean verboseException) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract Map<Integer, ProcessInfo> readAndParseAllProcessDetailsLines(boolean verboseException) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract Map<Integer, ProcessInfo> readAndParseAllProcessDetailsLines(int supposedAmountOfProcesses) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract Map<Integer, ProcessInfo> readAndParseAllProcessDetailsLines() throws IOException, NoMatchFoundException;

  @NotNull
  public abstract Map<Integer, ProcessInfo> readAndParseAllProcessDetailsLines(int supposedAmountOfProcesses, boolean verboseException) throws IOException, NoMatchFoundException;

  @NotNull
  public abstract MachineState readAndParseIfMachineStateLines() throws IOException, NoMatchFoundException;

  @NotNull
  public abstract MachineState readAndParseIfMachineStateLines(boolean verboseException) throws IOException, NoMatchFoundException;
}
