package be.vrakfall.sshatellite.system.commands.python.version.errors;

public class ReleaseSegmentParsingException extends VersionParsingException {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public ReleaseSegmentParsingException() {
    super();
  }

  public ReleaseSegmentParsingException(String message) {
    super(message);
  }

  public ReleaseSegmentParsingException(String message, Throwable cause) {
    super(message, cause);
  }

  public ReleaseSegmentParsingException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
