package be.vrakfall.sshatellite.system.commands;

import net.schmizz.sshj.connection.channel.direct.Signal;

import java.io.InputStream;

public class ExitedCommandResponse {
  //==== Static variables ====//

  public static final String TAG = ExitedCommandResponse.class.getSimpleName();

  //==== Attributes ====//

  public final InputStream stderr;
  private String errorMessage;
  private Signal signal; // TODO: 4/07/18 To be replaced with our own implementation
  private Integer status;
  private Boolean wasCoreDumped;

  //==== Getters and Setters ====//

  public InputStream getStderr() {
    return stderr;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  private void setErrorMessage(final String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public Signal getSignal() {
    return signal;
  }

  private void setSignal(final Signal signal) {
    this.signal = signal;
  }

  public Integer getStatus() {
    return status;
  }

  private void setStatus(final Integer status) {
    this.status = status;
  }

  public Boolean getWasCoreDumped() {
    return wasCoreDumped;
  }

  private void setWasCoreDumped(final Boolean wasCoreDumped) {
    this.wasCoreDumped = wasCoreDumped;
  }

  //==== Constructors ====//

  public ExitedCommandResponse(final InputStream stderr, final String errorMessage, final Signal signal, final Integer status, final Boolean wasCoreDumped) {
    this.stderr = stderr;
    this.errorMessage = errorMessage;
    this.signal = signal;
    this.status = status;
    this.wasCoreDumped = wasCoreDumped;
  }


  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
