package be.vrakfall.sshatellite.system;

public interface UnixConventions {
  //==== Static variables ====//

  /**
   * The number of UNIX load averages.
   * See: <href a="https://en.wikipedia.org/wiki/Load_(computing)">https://en.wikipedia.org/wiki/Load_(computing)</href>
   */
  int LOAD_AVERAGES_AMOUNT = 3;

  int[] LOAD_AVERAGES_PERIODS_IN_MINUTES = {1, 5, 15};

  //====== Static Methods ======//

  //====== Object Methods ======//
}
