package be.vrakfall.sshatellite.system.commands.python;

import android.os.Build;
import android.util.Log;
import androidx.annotation.RequiresApi;
import be.vrakfall.sshatellite.network.errors.ActionOnNotAuthenticatedConnectionException;
import be.vrakfall.sshatellite.system.Machine;
import be.vrakfall.sshatellite.system.commands.ExecutionHandler;
import be.vrakfall.sshatellite.system.commands.ShellEnvironment;
import be.vrakfall.sshatellite.system.commands.python.version.PythonVersion;
import be.vrakfall.sshatellite.system.monitoring.state.variables.ExecutablePathEntry;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.ordering.ExtendedComparable;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.val;
import net.schmizz.sshj.connection.ConnectionException;
import net.schmizz.sshj.transport.TransportException;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@Value
@EqualsAndHashCode(callSuper = true)
//@SuperBuilder
@RequiresApi(api = Build.VERSION_CODES.O)
public class StaticShellPythonEnvironment extends ShellPythonEnvironment
    implements ExtendedComparable<StaticShellPythonEnvironment> {
  //==== Static variables ====//

  public static final String TAG = StaticShellPythonEnvironment.class.getSimpleName();

  private static final LoadingCache<StaticShellPythonEnvironment, PythonVersion> versionCache = Caffeine.newBuilder()
      .refreshAfterWrite(Machine.ARCHITECTURE_CACHE_REFRESH_TIME_IN_MILLISECONDS, TimeUnit.SECONDS)
      .expireAfterWrite(Machine.ARCHITECTURE_CACHE_EXPIRATION_TIME_IN_MILLISECONDS, TimeUnit.SECONDS)
      .build(StaticShellPythonEnvironment::requestVersion);

  //==== Attributes ====//

  @NotNull
  private final ExecutablePathEntry executablePathEntry;

  //==== Constructors ====//

  @Builder
  private StaticShellPythonEnvironment(@NotNull final ShellEnvironment shellEnvironment,
                                       @NotNull final ExecutablePathEntry executablePathEntry) {
    super(shellEnvironment);
    this.executablePathEntry = executablePathEntry;
  }

  //==== Getters and Setters ====//

  @Override
  @Nullable
  @RequiresApi(api = Build.VERSION_CODES.O)
  public PythonVersion getVersion() {
    final val pythonVersion = versionCache.get(this);
    Log.d(TAG, String.format(Locale.US, "getVersion: %s - path: %s", pythonVersion, executablePathEntry));
    return pythonVersion;
    // TODO: 31/10/18 Find and implement a way to handle exceptions nicely. Clues were found: check the notes.
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @RequiresApi(api = Build.VERSION_CODES.O)
  @Override
  public int compareTo(@NotNull final StaticShellPythonEnvironment o) {
    return getVersion().compareTo(o.getVersion());
  }

  @Override
  public ExecutionHandler execute(final String command) {
    return null;
    // TODO: 31/10/18 Implement this
  }

  @Override
  public InputStream getInputStreamTest(final String command) throws ActionOnNotAuthenticatedConnectionException,
      ConnectionException, TransportException {
    return null;
    // TODO: 31/10/18 Implement this
  }

  @Override
  public void stopExecution(@Nullable final Closeable execution) throws ConnectionException, TransportException {
    // TODO: 31/10/18 Implement this
  }

  @Nullable
  @RequiresApi(api = Build.VERSION_CODES.O)
  private PythonVersion requestVersion() {
    if (getExecutablePathEntry() == null) {
      return null;
    }
    try {
      return getVersionFromExecutable(getExecutablePathEntry());
    }
    catch (final IOException e) {
      return null;
    }
  }

  public void refreshVersion() {
    versionCache.refresh(this);
  }
}
