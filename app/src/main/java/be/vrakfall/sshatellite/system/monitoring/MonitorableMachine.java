package be.vrakfall.sshatellite.system.monitoring;

import be.vrakfall.sshatellite.network.ssh.SFTPEnvironment;
import be.vrakfall.sshatellite.system.Machine;
import be.vrakfall.sshatellite.system.commands.ShellEnvironment;
import be.vrakfall.sshatellite.system.commands.python.PythonEnvironment;
import be.vrakfall.sshatellite.system.commands.python.errors.NoPythonInstalledException;
import be.vrakfall.sshatellite.system.commands.python.errors.VenvCannotBeInstalledException;
import be.vrakfall.sshatellite.system.errors.NonExistingCPUCoreException;
import be.vrakfall.sshatellite.system.errors.NonExistingCPUException;
import be.vrakfall.sshatellite.system.errors.NonExistingItemException;
import be.vrakfall.sshatellite.system.monitoring.state.MachineState;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.files.PythonApplicationFilesManager;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.joda.time.Duration;

import java.io.IOException;

@ToString
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter(AccessLevel.PROTECTED)
public abstract class MonitorableMachine extends Machine implements Monitorable {
  //==== Static variables ====//

  public static final String TAG = MonitorableMachine.class.getSimpleName();

  //==== > Error messages ====//

  private static final String NO_PYTHON_EXECUTABLE_FOUND_ERROR_MESSAGE =
      "No python executable has been found in the path.";

  //==== Attributes ====//

  @NotNull
  private ShellEnvironment commandProxy;

  // TODO: 17/12/18 Move this to the remote class
  @NotNull
  private PythonApplicationFilesManager backendFilesManager;

  //==== Constructors ====//

  protected MonitorableMachine(
      @Nullable final Long id, @NotNull final ShellEnvironment commandProxy,
      @NotNull final String backendDirectoryPath
  ) {
    super(id);
    this.commandProxy = commandProxy;
    // TODO: 26/11/18 Make some tests on the following
    final val backendFilesManagerBuilder = PythonApplicationFilesManager.builder()
        .root(backendDirectoryPath)
        .mainExecutable("sshatellite_backend/main/main.py");
    if (commandProxy instanceof SFTPEnvironment) {
      // TODO: 18/12/18 This should probably lower in the inheritance
      backendFilesManagerBuilder.sftpProxy((SFTPEnvironment) commandProxy);
    }
    this.backendFilesManager = backendFilesManagerBuilder.build();
  }

  public MonitorableMachine(final MonitorableMachine other) {
    super(other);
    this.commandProxy = other.commandProxy;
    this.backendFilesManager = other.backendFilesManager;
  }

  //==== Getters and Setters ====//

  protected PythonEnvironment getPythonEnvironment() throws IOException {
    return commandProxy.getHighestVersionPythonEnvironment();
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @Override
  public float getCPUCoreUsage(final int coreNumber) throws NonExistingCPUCoreException {
    return 0;
  }

  @Override
  public float getCPUCoreUsage(final int cpuNumber, final int coreNumber) throws NonExistingItemException {
    return 0;
  }

  @Override
  public float getCPUUsage() {
    return 0;
  }

  @Override
  public float getCPUUsage(final int cpuNumer) throws NonExistingCPUException {
    return 0;
  }

  @Override
  public float getAllCPUsUsage() {
    return 0;
  }

  @Override
  public Observable<MachineState> pulseMachineState(final Duration interval) {
    return Observable.<MachineState>create((ObservableEmitter<MachineState> emitter) -> {
      final val pythonEnvironment = getCommandProxy().getHighestVersionPythonEnvironment();
      if (pythonEnvironment == null) {
        // TODO: 29/12/18 Check if the following shouldn't be rewritten
        emitter.tryOnError(new NoPythonInstalledException("There's apparently no python environment installed on the " +
            "machine as no potential instance is found in the execution path."));
      }
      else {
        try {
          pythonEnvironment.installVenvUpdatePackagesAndInstallRequiredOnes(
              getBackendFilesManager().getAbsoluteVenvPath()
          );
        }
        catch (final Exception e) {
          // TODO: 5/12/18 Make a better way to know why it doesn't install
          emitter.tryOnError(new VenvCannotBeInstalledException(e));
        }
        // TODO: 6/12/18 The rest of commands to call once the venv is installed
      }
    });
  }
}
