package be.vrakfall.sshatellite.system.commands.top;

import android.annotation.SuppressLint;
import android.os.Build;
import android.util.Log;
import be.vrakfall.sshatellite.system.monitoring.state.MachineState;
import be.vrakfall.sshatellite.system.monitoring.state.MemoryUsage;
import be.vrakfall.sshatellite.system.monitoring.state.ProcessInfo;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.io.errors.NoMatchFoundException;
import io.reactivex.Observable;
import lombok.val;

import java.io.IOException;
import java.io.Reader;
import java.util.*;

public class BSDTopOutputBufferedReader extends TopOutputBufferedReader {
  //==== Static variables ====//

  //==== Attributes ====//

  private int supposedAmountOfCPUs = -1;
  private int goodElementsFound = 0;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public BSDTopOutputBufferedReader(final int supposedAmountOfCPUs, @NotNull final Reader in, final int sz) {
    super(in, sz);
    this.supposedAmountOfCPUs = supposedAmountOfCPUs;
  }

  public BSDTopOutputBufferedReader(final int supposedAmountOfCPUs, @NotNull final Reader in) {
    super(in);
    this.supposedAmountOfCPUs = supposedAmountOfCPUs;
  }

  public BSDTopOutputBufferedReader(@NotNull final Reader in, final int sz) {
    super(in, sz);
  }

  public BSDTopOutputBufferedReader(@NotNull final Reader in) {
    super(in);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  private static void dispose() {
    Log.d(TAG, "dispose: Disposing.");
  }

  @Override
  @NotNull
  public LoadAveragesAndTimeInfo readAndParseIfTopFirstLine(final int goodElementsFound) throws IOException, NoMatchFoundException {
    return readAndParseIfTopFirstLine(goodElementsFound, true);
  }

  @Override
  @NotNull
  public LoadAveragesAndTimeInfo readAndParseNextTopFirstLine() throws IOException, NoMatchFoundException {
    return readAndParseNextTopFirstLine(true);
  }

  @Override
  @NotNull
  public LoadAveragesAndTimeInfo readAndParseIfTopFirstLine(int goodElementsFound, final boolean verboseException) throws IOException, NoMatchFoundException {
    final LoadAveragesAndTimeInfo loadAveragesAndTimeInfo;
    mark(BSDTop.MAX_LINE_LENGTH);

    try {
      loadAveragesAndTimeInfo = BSDTop.parseFirstLine(readNextNonEmptyLineEOFAsException(verboseException), verboseException);
      ++goodElementsFound;
      return loadAveragesAndTimeInfo;
    }
    catch (final NoMatchFoundException e) {
      reset();
      throw e;
    }
  }

  @Override
  @NotNull
  public ProcessRecap readAndParseIfProcessRecapLine() throws IOException, NoMatchFoundException {
    return readAndParseIfProcessRecapLine(true);
  }

  @Override
  @NotNull
  public LoadAveragesAndTimeInfo readAndParseNextTopFirstLine(final boolean verboseException) throws IOException, NoMatchFoundException {
    LoadAveragesAndTimeInfo loadAveragesAndTimeInfo;
    mark(BSDTop.MAX_LINE_LENGTH * READ_LOOP_LIMIT);

    for (int i = 0; i < READ_LOOP_LIMIT; i++) {
      try {
        loadAveragesAndTimeInfo = BSDTop.parseFirstLine(readNextNonEmptyLineEOFAsException(false), false);
        ++goodElementsFound;
        return loadAveragesAndTimeInfo;
      }
      catch (final NoMatchFoundException ignored) {
        // If it fails, just go to the next loop iteration.
      }
    }

    // Reset back to where we started if we found no first line.
    reset();

    if (verboseException) {
      throw new NoMatchFoundException(String.format(Locale.US, "Too many non-empty lines not containing the expected first line for an output of the `top` command on a BSD system have been read, aborting the reading loop. Limit is %d. The buffer has been reset to its state before this method was called.", READ_LOOP_LIMIT));
    }
    else {
      throw new NoMatchFoundException();
    }
  }

  @Override
  @NotNull
  public CPUUsage readAndParseIfCPUUsageLine() throws IOException, NoMatchFoundException {
    return readAndParseIfCPUUsageLine(true);
  }

  @Override
  @NotNull
  public ProcessRecap readAndParseIfProcessRecapLine(final boolean verboseException) throws IOException, NoMatchFoundException {
    final ProcessRecap processRecap;
    mark(BSDTop.MAX_LINE_LENGTH);

    try {
      processRecap = BSDTop.parseProcessRecap(readNextNonEmptyLineEOFAsException(verboseException), verboseException);
      ++goodElementsFound;
      return processRecap;
    }
    catch (final NoMatchFoundException e) {
      reset();
      throw e;
    }
  }

  @NotNull
  public BSDCPUUsage readAndParseIfBSDCPUUsageLine() throws IOException, NoMatchFoundException {
    return readAndParseIfBSDCPUUsageLine(true);
  }

  @Override
  @NotNull
  public CPUUsage readAndParseIfCPUUsageLine(final boolean verboseException) throws IOException, NoMatchFoundException {
    return readAndParseIfBSDCPUUsageLine(verboseException);
  }

  @Override
  @NotNull
  public List<CPUUsage> readAndParseAllCPUUsageLines() throws IOException, NoMatchFoundException {
    return readAndParseAllCPUUsageLines(true);
  }

  @NotNull
  public BSDCPUUsage readAndParseIfBSDCPUUsageLine(final boolean verboseException) throws IOException, NoMatchFoundException {
    mark(BSDTop.MAX_LINE_LENGTH);
    try {
      return BSDTop.parseCPUUsage(readNextNonEmptyLineEOFAsException(verboseException), verboseException);
    }
    catch (final NoMatchFoundException e) {
      reset();
      throw e;
    }
  }

  @Override
  @NotNull
  public MemoryUsage readAndParseIfMemoryUsageLine() throws IOException, NoMatchFoundException {
    return readAndParseIfMemoryUsageLine(true);
  }

  @Override
  @NotNull
  public List<CPUUsage> readAndParseAllCPUUsageLines(final boolean verboseException) throws IOException, NoMatchFoundException {
    final List<BSDCPUUsage> bsdCPUUsages;
    if (supposedAmountOfCPUs <= 0) {
      bsdCPUUsages = new ArrayList<>();
    }
    else {
      bsdCPUUsages = new ArrayList<>(supposedAmountOfCPUs);
    }

    mark(BSDTop.MAX_LINE_LENGTH);
    String currentLine = readNextNonEmptyLineEOFAsException(verboseException);

    try {
      //noinspection InfiniteLoopStatement
      while (true) { // TODO: 19/09/18 Put a limit?
        bsdCPUUsages.add(BSDTop.parseCPUUsage(currentLine, verboseException));

        mark(BSDTop.MAX_LINE_LENGTH);
        currentLine = readNextNonEmptyLineEOFAsException(verboseException);
      }
    }
    catch (final NoMatchFoundException e) {
      reset();

      if (bsdCPUUsages.isEmpty()) {
        if (verboseException) {
          throw new NoMatchFoundException(String.format(Locale.US, "No match corresponding to a cpu line found in the reader. Last line was: %s", currentLine), e);
        }
        else {
          throw new NoMatchFoundException(e);
        }
      }

      // At least one CPU usage line has been found.
      ++goodElementsFound;
      supposedAmountOfCPUs = bsdCPUUsages.size(); // We don't want to change the supposed amount of CPUs if we found none: It was probably the very first iteration of `top`.
      return new ArrayList<>(bsdCPUUsages);
    }
  }

  @Override
  @NotNull
  public ARCUsage readAndParseIfARCUsageLine() throws IOException, NoMatchFoundException {
    return readAndParseIfARCUsageLine(true);
  }

  @Override
  @NotNull
  public MemoryUsage readAndParseIfMemoryUsageLine(final boolean verboseException) throws IOException, NoMatchFoundException {
    final MemoryUsage memoryUsage;
    mark(BSDTop.MAX_LINE_LENGTH);

    try {
      memoryUsage = BSDTop.parseMemoryUsage(readNextNonEmptyLineEOFAsException(verboseException), verboseException);
      ++goodElementsFound;
      return memoryUsage;
    }
    catch (final NoMatchFoundException e) {
      reset();
      throw e;
    }
  }

  @Override
  @NotNull
  public ARCCompression readAndParseIfARCCompressionLine() throws IOException, NoMatchFoundException {
    return readAndParseIfARCCompressionLine(true);
  }

  @Override
  @NotNull
  public ARCUsage readAndParseIfARCUsageLine(final boolean verboseException) throws IOException, NoMatchFoundException {
    final ARCUsage arcUsage;
    mark(BSDTop.MAX_LINE_LENGTH);

    try {
      arcUsage = BSDTop.parseARCUsage(readNextNonEmptyLineEOFAsException(verboseException), verboseException);
      ++goodElementsFound;
      return arcUsage;
    }
    catch (final NoMatchFoundException e) {
      reset();
      throw e;
    }
  }

  @Override
  @NotNull
  public SwapUsage readAndParseIfSwapUsageLine() throws IOException, NoMatchFoundException {
    return readAndParseIfSwapUsageLine(true);
  }

  @Override
  @NotNull
  public ARCCompression readAndParseIfARCCompressionLine(final boolean verboseException) throws IOException, NoMatchFoundException {
    final ARCCompression arcCompression;
    mark(BSDTop.MAX_LINE_LENGTH);

    try {
      arcCompression = BSDTop.parseARCCompression(readNextNonEmptyLineEOFAsException(verboseException), verboseException);
      ++goodElementsFound;
      return arcCompression;
    }
    catch (final NoMatchFoundException e) {
      reset();
      throw e;
    }
  }

  @Override
  @NotNull
  public SwapUsage readAndParseIfSwapUsageLine(final boolean verboseException) throws IOException, NoMatchFoundException {
    final SwapUsage swapUsage;
    mark(BSDTop.MAX_LINE_LENGTH);

    try {
      swapUsage = BSDTop.parseSwapUsage(readNextNonEmptyLineEOFAsException(verboseException), verboseException);
      ++goodElementsFound;
      return swapUsage;
    }
    catch (final NoMatchFoundException e) {
      reset();
      throw e;
    }
  }

  @Override
  @NotNull
  public ProcessInfo readAndParseIfProcessDetailsLine() throws IOException, NoMatchFoundException {
    return readAndParseIfProcessDetailsLine(true);
  }

  @Override
  @NotNull
  public boolean readAndTrashIfProcessColumnTitles(final boolean verboseException) throws IOException {
    mark(BSDTop.MAX_LINE_LENGTH);
    if (!BSDTop.checkHasProcessColumnTitles(readNextNonEmptyLineEOFAsException(verboseException))) {
      reset();
      return false;
    }
    return true;
  }

  @Override
  @NotNull
  public ProcessInfo readAndParseIfProcessDetailsLine(final boolean verboseException) throws IOException, NoMatchFoundException {
    mark(BSDTop.MAX_LINE_LENGTH);
    try {
      return BSDTop.parseProcessDetails(readNextNonEmptyLineEOFAsException(verboseException), verboseException);
    }
    catch (final NoMatchFoundException e) {
      reset();
      throw e;
    }
  }

  @Override
  @NotNull
  public Map<Integer, ProcessInfo> readAndParseAllProcessDetailsLines(final boolean verboseException) throws IOException, NoMatchFoundException {
    return readAndParseAllProcessDetailsLines(-1, verboseException);
  }

  @Override
  @NotNull
  public Map<Integer, ProcessInfo> readAndParseAllProcessDetailsLines() throws IOException, NoMatchFoundException {
    return readAndParseAllProcessDetailsLines(-1, true);
  }

  @Override
  @NotNull
  public Map<Integer, ProcessInfo> readAndParseAllProcessDetailsLines(final int supposedAmountOfProcesses) throws IOException, NoMatchFoundException {
    return readAndParseAllProcessDetailsLines(supposedAmountOfProcesses, true);
  }

  @Override
  @NotNull
  public MachineState readAndParseIfMachineStateLines() throws IOException, NoMatchFoundException {
    return readAndParseIfMachineStateLines(true);
  }

  @SuppressLint("UseSparseArrays")
  @Override
  @NotNull
  public Map<Integer, ProcessInfo> readAndParseAllProcessDetailsLines(final int supposedAmountOfProcesses, final boolean verboseException) throws IOException, NoMatchFoundException {
    final Map<Integer, ProcessInfo> processesDetails; // TODO: 19/07/18 Use HashMap back again because this implementation looks horrible for this use case... Done?
    if (supposedAmountOfProcesses <= 0 || Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
      processesDetails = new HashMap<>();
    }
    else {
      processesDetails = new HashMap<>(supposedAmountOfProcesses);
    }

    mark(BSDTop.MAX_LINE_LENGTH);
    String currentLine = readNextNonEmptyLineEOFAsException(verboseException);

    try {
      //noinspection InfiniteLoopStatement
      while (true) { // TODO: 19/09/18 Put a limit?
        final ProcessInfo processInfo = BSDTop.parseProcessDetails(currentLine, verboseException);
        processesDetails.put(processInfo.getId(), processInfo);

        mark(BSDTop.MAX_LINE_LENGTH);
        currentLine = readNextNonEmptyLineEOFAsException(verboseException);
      }
    }
    catch (final NoMatchFoundException e) {
      reset();

      if (processesDetails.size() <= 0) {
        if (verboseException) {
          throw new NoMatchFoundException(String.format(Locale.US, "No match corresponding to a process details line found in the reader. Last line was: %s", currentLine), e);
        }
        else {
          throw new NoMatchFoundException(e);
        }
      }

      return processesDetails;
    }
  }

  @Override
  @NotNull
  public MachineState readAndParseIfMachineStateLines(final boolean verboseException) throws IOException, NoMatchFoundException {
    // TODO: 19/09/18 Check if this method shouldn't be removed.
    final int goodElementsFound = 0;

    LoadAveragesAndTimeInfo loadAveragesAndTimeInfo = null;
    try {
      loadAveragesAndTimeInfo = readAndParseIfTopFirstLine(goodElementsFound, false);
    }
    catch (final NoMatchFoundException e) {
    }

    Log.i(TAG, "readAndParseIfMachineStateLines: goodElementsFound: " + goodElementsFound);

    return readAndParseIfRestMachineStateLines(loadAveragesAndTimeInfo, verboseException);
  }

  /**
   * Reads the rest of potential output lines from the command {@code top} on a BSD system.
   *
   * @param loadAveragesAndTimeInfo
   * @param verboseException
   * @return
   * @throws IOException
   * @throws NoMatchFoundException
   */
  @NotNull
  private MachineState readAndParseIfRestMachineStateLines(final LoadAveragesAndTimeInfo loadAveragesAndTimeInfo, final boolean verboseException) throws IOException, NoMatchFoundException {

    ProcessRecap processRecap = null;
    try {
      processRecap = readAndParseIfProcessRecapLine(false);
    }
    catch (final NoMatchFoundException e) {
    }

    List<CPUUsage> cpuUsages = null;
    try {
      cpuUsages = readAndParseAllCPUUsageLines(false);
    }
    catch (final NoMatchFoundException e) {
    }

    MemoryUsage memoryUsage = null;
    try {
      memoryUsage = readAndParseIfMemoryUsageLine(false);
    }
    catch (final NoMatchFoundException e) {
    }

    ARCUsage arcUsage = null;
    try {
      arcUsage = readAndParseIfARCUsageLine(false);
    }
    catch (final NoMatchFoundException e) {
    }

    ARCCompression arcCompression = null;
    try {
      arcCompression = readAndParseIfARCCompressionLine(false);
    }
    catch (final NoMatchFoundException e) {
    }

    SwapUsage swapUsage = null;
    try {
      swapUsage = readAndParseIfSwapUsageLine(false);
    }
    catch (final NoMatchFoundException ignored) {
    }

    readAndTrashIfProcessColumnTitles(false);

    mark(BSDTop.MAX_LINE_LENGTH);
    Map<Integer, ProcessInfo> processesDetails = null;
    try {
      if (processRecap != null) {
        processesDetails = readAndParseAllProcessDetailsLines(processRecap.processesTotal, false);
      }
      else {
        processesDetails = readAndParseAllProcessDetailsLines(false);
      }
      ++goodElementsFound;
    }
    catch (final NoMatchFoundException e) {
      reset();
    }

    if (goodElementsFound == 0) {
      if (verboseException) {
        throw new NoMatchFoundException("The first line (empty lines excluded) doesn't appear to correspond to any output line from the command `top` on a BSD system.");
      }
      else {
        throw new NoMatchFoundException();
      }
    }

    final val sortedProcessesDetails = new LinkedHashMap<>(processesDetails);
    // TODO: 16/11/18 Hardcoded cpuAmount for temporary test with remote yaml call, change this at some point.
    return new MachineState(loadAveragesAndTimeInfo.loadAverages, loadAveragesAndTimeInfo.upTime,
        loadAveragesAndTimeInfo.localTime, processRecap.processesTotal, processRecap.runningProcessesAmount,
        processRecap.sleepingProcessesAmount, 0, cpuUsages, memoryUsage, arcUsage, arcCompression, swapUsage,
        sortedProcessesDetails);
  }

  public MachineState readAndParseNextMachineStateLines(final boolean verboseException) throws IOException, NoMatchFoundException {
    goodElementsFound = 0;

    final LoadAveragesAndTimeInfo loadAveragesAndTimeInfo = readAndParseNextTopFirstLine(verboseException);

    return readAndParseIfRestMachineStateLines(loadAveragesAndTimeInfo, verboseException);
  }

  public Observable<MachineState> readEveryMachineStateOutput(final boolean verboseException) {
    return Observable.create(emitter -> {
      while (true) {
        try {
//          emitter.setCancellable(this::close);
          emitter.setCancellable(BSDTopOutputBufferedReader::dispose);
          // TODO: 24/07/18 Find out why it stops after the first emitted item.
          emitter.onNext(readAndParseNextMachineStateLines(verboseException));
        }
        catch (final Exception e) {
          System.out.println("smth weird happened");
          emitter.onError(e); // TODO: 18/09/18 Replace with the following line when the debugging is done.
//          emitter.tryOnError(e);
          break;
        }
      }
    });
  }
}
