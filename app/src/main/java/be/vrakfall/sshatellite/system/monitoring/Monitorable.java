package be.vrakfall.sshatellite.system.monitoring;

import be.vrakfall.sshatellite.system.errors.NonExistingCPUCoreException;
import be.vrakfall.sshatellite.system.errors.NonExistingCPUException;
import be.vrakfall.sshatellite.system.errors.NonExistingItemException;
import be.vrakfall.sshatellite.system.monitoring.state.MachineState;
import io.reactivex.Observable;
import org.joda.time.Duration;

public interface Monitorable {
  //==== Static variables ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
  float getCPUCoreUsage(int coreNumber) throws NonExistingCPUCoreException;

  float getCPUCoreUsage(int cpuNumber, int coreNumber) throws NonExistingItemException;

  float getCPUUsage();

  float getCPUUsage(int cpuNumber) throws NonExistingCPUException;

  float getAllCPUsUsage();

  Observable<MachineState> pulseMachineState(Duration interval);
}
