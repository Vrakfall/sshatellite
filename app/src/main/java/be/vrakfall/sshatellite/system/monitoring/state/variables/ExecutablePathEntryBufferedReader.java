package be.vrakfall.sshatellite.system.monitoring.state.variables;

import be.vrakfall.sshatellite.system.commands.VersionedCommand;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.io.ExtendedBufferedReader;
import be.vrakfall.sshatellite.utils.io.Parser;
import lombok.NonNull;
import lombok.var;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

public class ExecutablePathEntryBufferedReader extends ExtendedBufferedReader implements Parser<ExecutablePathEntry> {
  //==== Static variables ====//

  //==== > Error messages ====//

  private static final String NO_EXECUTABLE_PATH_ENTRY_FOR_THIS_PROGRAM_ERROR_MESSAGE = "No executable path entry has been found for the program with the command name: %s";

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public ExecutablePathEntryBufferedReader(@NonNull final Reader in, final int sz) {
    super(in, sz);
  }

  public ExecutablePathEntryBufferedReader(@NonNull final Reader in) {
    super(in);
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @Override
  @Nullable
  public ExecutablePathEntry readNext() throws IOException {
    var line = readNextNonEmptyLine();
    ExecutablePathEntry executablePathEntry;

    // Return the next ExecutablePathEntry found, null ONLY if the end of the stream has been reached.
    for (int i = 0; line != null && i < READ_LOOP_LIMIT; i++) {
      executablePathEntry = ExecutablePathEntry.fromString(line);
      if (executablePathEntry != null) {
        return executablePathEntry;
      }

      // End of loop
      line = readNextNonEmptyLine();
    }
    return null;
  }

  @NotNull
  @NonNull
  public List<ExecutablePathEntry> readAllAsList(final String commandName) throws IOException {
    final List<ExecutablePathEntry> entries = new ArrayList<>();
    ExecutablePathEntry entry;
    while ((entry = readNext()) != null) {
      if (entry.isSameProgram(commandName)) {
        entries.add(entry);
      }
    }
    return entries;
  }

  @NotNull
  @NonNull
  public ExecutablePathEntry[] readAllAsArray(final String commandName) throws IOException {
    return readAllAsList(commandName).toArray(new ExecutablePathEntry[0]);
  }

  @NotNull
  @NonNull
  public LinkedHashSet<ExecutablePathEntry> readAllAsLinkedHashSet(final String commandName) throws IOException {
    final LinkedHashSet<ExecutablePathEntry> entries = new LinkedHashSet<>();
    ExecutablePathEntry entry;
    while ((entry = readNext()) != null) {
      if (entry.isSameProgram(commandName)) {
        entries.add(entry);
      }
    }
    return entries;
  }

  @NotNull
  @NonNull
  public LinkedHashMap<VersionedCommand, ExecutablePathEntry>
  readAllWithoutDuplicatedCommandAsLinkedHashMap(final String commandName) throws IOException {
    final LinkedHashMap<VersionedCommand, ExecutablePathEntry> entries = new LinkedHashMap<>();
    ExecutablePathEntry entry;
    while ((entry = readNext()) != null) {
      if (entry.isSameProgram(commandName) && !entries.containsKey(entry.command)) {
        entries.put(entry.command, entry);
      }
    }
    return entries;
  }
}
