package be.vrakfall.sshatellite.system.commands.python.version;

import static be.vrakfall.sshatellite.system.commands.python.version.PEP440Version.EPOCH_MIN;

public interface ModuleRequirements {
  //==== Static variables ====//

  /**
   * Minimum Python version to be able to use the module `venv`.
   * Source: <a href="https://docs.python.org/3.7/library/venv.html#module-venv">the Python documentation</a>.
   */
  PEP440Version VENV_MINIMUM_PYTHON_VERSION = new PEP440Version(EPOCH_MIN, new ReleaseSegment(3L, 3L), null, null, null);

  //==== > Error messages ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
