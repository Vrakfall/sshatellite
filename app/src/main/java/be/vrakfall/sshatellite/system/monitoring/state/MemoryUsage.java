package be.vrakfall.sshatellite.system.monitoring.state;

import be.vrakfall.sshatellite.system.commands.top.ByteAmount;
import be.vrakfall.sshatellite.utils.annotations.NotNull;

public class MemoryUsage {
  //==== Static variables ====//

  //==== Attributes ====//

  @NotNull
  public final ByteAmount active;
  @NotNull
  public final ByteAmount inactive;
  @NotNull
  public final ByteAmount laundry;
  @NotNull
  public final ByteAmount wired;
  @NotNull
  public final ByteAmount free;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public MemoryUsage(@NotNull final ByteAmount active, @NotNull final ByteAmount inactive, @NotNull final ByteAmount laundry, @NotNull final ByteAmount wired, @NotNull final ByteAmount free) {
    this.active = active;
    this.inactive = inactive;
    this.laundry = laundry;
    this.wired = wired;
    this.free = free;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  public long getTotal() { // TODO: 20/09/18 Handle this much better!
    return active.getQuantity() + inactive.getQuantity() + laundry.getQuantity() + wired.getQuantity() + free.getQuantity();
  }

  @Override
  public String toString() {
    return "MemoryUsage{" +
        "active=" + active +
        ", inactive=" + inactive +
        ", laundry=" + laundry +
        ", wired=" + wired +
        ", free=" + free +
        '}';
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final MemoryUsage that = (MemoryUsage) o;

    if (!active.equals(that.active)) {
      return false;
    }
    if (!inactive.equals(that.inactive)) {
      return false;
    }
    if (!laundry.equals(that.laundry)) {
      return false;
    }
    if (!wired.equals(that.wired)) {
      return false;
    }
    return free.equals(that.free);
  }

  @Override
  public int hashCode() {
    int result = active.hashCode();
    result = 31 * result + inactive.hashCode();
    result = 31 * result + laundry.hashCode();
    result = 31 * result + wired.hashCode();
    result = 31 * result + free.hashCode();
    return result;
  }
}
