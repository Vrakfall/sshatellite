package be.vrakfall.sshatellite.system.commands.python;

import be.vrakfall.sshatellite.system.commands.Commandable;
import be.vrakfall.sshatellite.system.commands.ExecutionHandler;
import be.vrakfall.sshatellite.system.commands.errors.NoExecutablePathException;
import be.vrakfall.sshatellite.system.commands.python.errors.PipCannotInstallException;
import be.vrakfall.sshatellite.system.commands.python.errors.VenvCannotBeInstalledException;
import be.vrakfall.sshatellite.system.commands.python.version.PEP440Version;
import be.vrakfall.sshatellite.utils.annotations.NotNull;

import java.io.IOException;

public interface PythonEnvironment extends Commandable {
  //==== Static variables ====//

  String TAG = PythonEnvironment.class.getSimpleName();

  String SHORT_COMMAND_NAME = "python";

  String INSTALL_VENV_ARGUMENT = "-m venv";

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  PEP440Version getVersion();

  // TODO: 22/11/18 Do this more idiomatically
  void installVenvUpdatePackagesAndInstallRequiredOnes(String destinationPath) throws IOException,
      NoExecutablePathException, VenvCannotBeInstalledException, PipCannotInstallException;

  // TODO: 15/11/18 Make this better with a better venv handling
  ExecutionHandler executeScriptInVenv(@NotNull final String venvPath, @NotNull final String scriptPath,
                                       @NotNull final String scriptHome) throws IOException;
}
