package be.vrakfall.sshatellite.system.commands.python.version.errors;

public class NonExistingSegmentException extends VersionParsingException {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public NonExistingSegmentException() {
    super();
  }

  public NonExistingSegmentException(String message) {
    super(message);
  }

  public NonExistingSegmentException(String message, Throwable cause) {
    super(message, cause);
  }

  public NonExistingSegmentException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
