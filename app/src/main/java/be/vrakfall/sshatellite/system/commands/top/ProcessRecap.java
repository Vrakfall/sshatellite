package be.vrakfall.sshatellite.system.commands.top;

import be.vrakfall.sshatellite.utils.annotations.NotNull;

public class ProcessRecap {
  //==== Static variables ====//

  //==== Attributes ====//

  /**
   * Total amount of processes on a system. In the case of the output of the command `top`, it corresponds to the total amount of processes the command calculates. It is useful, for example, to check if enough processes are seen later.
   */
  @NotNull
  public final int processesTotal;

  /**
   * Amount of running processes on a system.
   */
  @NotNull
  public final int runningProcessesAmount;

  /**
   * Amount of sleeping processes on a system.
   */
  @NotNull
  public final int sleepingProcessesAmount;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public ProcessRecap(@NotNull int processesTotal, @NotNull int runningProcessesAmount, @NotNull int sleepingProcessesAmount) {
    this.processesTotal = processesTotal;
    this.runningProcessesAmount = runningProcessesAmount;
    this.sleepingProcessesAmount = sleepingProcessesAmount;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ProcessRecap that = (ProcessRecap) o;

    if (processesTotal != that.processesTotal) return false;
    if (runningProcessesAmount != that.runningProcessesAmount) return false;
    return sleepingProcessesAmount == that.sleepingProcessesAmount;
  }

  @Override
  public int hashCode() {
    int result = processesTotal;
    result = 31 * result + runningProcessesAmount;
    result = 31 * result + sleepingProcessesAmount;
    return result;
  }

  @Override
  public String toString() {
    return "ProcessRecap{" +
        "processesTotal=" + processesTotal +
        ", runningProcessesAmount=" + runningProcessesAmount +
        ", sleepingProcessesAmount=" + sleepingProcessesAmount +
        '}';
  }
}
