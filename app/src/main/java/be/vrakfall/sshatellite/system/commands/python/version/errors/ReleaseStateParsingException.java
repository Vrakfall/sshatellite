package be.vrakfall.sshatellite.system.commands.python.version.errors;

public class ReleaseStateParsingException extends VersionParsingException {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public ReleaseStateParsingException() {
    super();
  }

  public ReleaseStateParsingException(String message) {
    super(message);
  }

  public ReleaseStateParsingException(String message, Throwable cause) {
    super(message, cause);
  }

  public ReleaseStateParsingException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
