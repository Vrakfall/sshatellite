package be.vrakfall.sshatellite.system.commands.top;

import android.util.Log;
import be.vrakfall.sshatellite.BuildConfig;
import be.vrakfall.sshatellite.system.monitoring.state.ProcessInfo;
import be.vrakfall.sshatellite.system.monitoring.state.MemoryUsage;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.io.errors.NoMatchFoundException;
import org.joda.time.Duration;
import org.joda.time.LocalTime;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface BSDTop {
  //==== Static variables ====//

  String TAG = BSDTop.class.getSimpleName();

  String SINGLE_TOP = "top -bPd 2 -s %d all | awk \'/^last pid/ {\n" +
      "  if (nextBlock == 1) {\n" +
      "    buffer = $0;\n" +
      "    nextBlock++;\n" +
      "    next;\n" +
      "  }\n" +
      "  nextBlock++;\n" +
      "}\n" +
      "nextBlock == 2 {\n" +
      "  buffer = buffer \"\\n\" $0;\n" +
      "}\n" +
      "END {\n" +
      "  printf \"%%s\", buffer;\n" +
      "  exit;\n" +
      "}\'";

  String INFINITE_TOP = "top -bPd all -s %d all";

  /**
   * Maximum interval value of time in seconds between two updates.
   */
  int MAX_INTERVAL_VALUE = Integer.MAX_VALUE;

  /**
   * Max line length for a {@code top} output line. This should be a way bigger value than what is observed (e.g. the double) in order to handle edge cases.
   * This is mainly in order to prevent from reading an infinite error line.
   */
  int MAX_LINE_LENGTH = 160;

  String FIRST_LINE_REGEX = "last pid: +\\d+; +load averages: +(\\d+(?:\\.\\d+)?), +(\\d+(?:\\.\\d+)?), +(\\d+(?:\\.\\d+)?) +up (?:(\\d+)\\+)?(?:(\\d{1,2})\\:)?(?:(\\d{1,2})\\:)?(\\d{1,2}) +(?:(\\d{1,2})\\:)?(?:(\\d{1,2})\\:)?(\\d{1,2})";
  Pattern FIRST_LINE_PATTERN = Pattern.compile(FIRST_LINE_REGEX);

  Class<?>[] FIRST_LINE_OUTPUT_FORMATS = new Class[]{
      Float.class,
      Float.class,
      Float.class,
      Integer.class,
      Integer.class,
      Integer.class,
      Integer.class,
      Integer.class,
      Integer.class,
      Integer.class
  };

  String PROCESS_RECAP_REGEX = "(\\d+) process(?:es)?\\: +(\\d+) running, (\\d+) sleeping"; // TODO: 16/07/18 Find if there are edge cases with other things summed up in here, like interrupts.
  Pattern PROCESS_RECAP_PATTERN = Pattern.compile(PROCESS_RECAP_REGEX);

  Class<?>[] PROCESS_RECAP_OUTPUT_FORMATS = new Class[]{
      Integer.class,
      Integer.class,
      Integer.class
  };

  String CPU_REGEX = "CPU(?: (\\d+)): +(\\d{1,3}(?:\\.\\d)?)% user, +(\\d{1,3}(?:\\.\\d)?)% nice, +(\\d{1,3}(?:\\.\\d)?)% system, +(\\d{1,3}(?:\\.\\d)?)% interrupt, +(\\d{1,3}(?:\\.\\d)?)% idle"; // TODO: 5/07/18 Check what happens when there's no per cpu usage, which we shouldn't use anyway
  Pattern CPU_PATTERN = Pattern.compile(CPU_REGEX);

  /**
   * Array that describes the formats that should be found in the groups matched by the regex. Used only for unit tests to check they conform to the sample.
   */
  Class<?>[] CPU_OUTPUT_FORMATS = new Class[]{
      Integer.class,
      Float.class,
      Float.class,
      Float.class,
      Float.class,
      Float.class
  };

  String MEMORY_REGEX = "Mem: (\\d+)(\\w)? Active, (\\d+)(\\w)? Inact, (\\d+)(\\w)? Laundry, (\\d+)(\\w)? Wired, (\\d+)(\\w)? Free";
  Pattern MEMORY_PATTERN = Pattern.compile(MEMORY_REGEX);

  Class<?>[] MEMORY_OUTPUT_FORMATS = new Class[]{
      Long.class,
      Character.class,
      Long.class,
      Character.class,
      Long.class,
      Character.class,
      Long.class,
      Character.class,
      Long.class,
      Character.class
  };

  String ARC_REGEX = "ARC: (\\d+)(\\w)? Total, (\\d+)(\\w)? MFU, (\\d+)(\\w)? MRU, (\\d+)(\\w)? Anon, (\\d+)(\\w)? Header, (\\d+)(\\w)? Other";
  Pattern ARC_PATTERN = Pattern.compile(ARC_REGEX);

  Class<?>[] ARC_OUTPUT_FORMATS = new Class[]{
      Long.class,
      Character.class,
      Long.class,
      Character.class,
      Long.class,
      Character.class,
      Long.class,
      Character.class,
      Long.class,
      Character.class,
      Long.class,
      Character.class
  };

  String ARC_COMPRESSION_RECAP_REGEX = "(\\d+)(\\w)? Compressed, (\\d+)(\\w)? Uncompressed"; // We don't really care about what follows in the line as we can calculate it from the preceding number we get.
  Pattern ARC_COMPRESSION_RECAP_PATTERN = Pattern.compile(ARC_COMPRESSION_RECAP_REGEX);

  Class<?>[] ARC_COMPRESSION_RECAP_OUTPUT_FORMATS = new Class[]{
      Long.class,
      Character.class,
      Long.class,
      Character.class
  };

  String SWAP_REGEX = "Swap: (\\d+)(\\w)? Total,(?: (\\d+)(\\w)? Used,)? (\\d+)(\\w)? Free";
  Pattern SWAP_PATTERN = Pattern.compile(SWAP_REGEX);

  Class<?>[] SWAP_OUTPUT_FORMATS = new Class[]{
      Long.class,
      Character.class,
      Long.class,
      Character.class,
      Long.class,
      Character.class
  };

  String PROCESS_COLUMN_TITLES_REGEX = "PID +USERNAME +THR +PRI +NICE +SIZE +RES +STATE +C +TIME +WCPU +COMMAND";
  Pattern PROCESS_COLUMN_TITLES_PATTERN = Pattern.compile(PROCESS_COLUMN_TITLES_REGEX);

  String PROCESS_REGEX = "(\\d+) +(\\S+) +(\\d+) +(-?\\d+) +r?(\\d+) +(\\d+)(\\w)? +(\\d+)(\\w)? (\\S+) +(\\d+) +(?:(\\d+):(\\d{2})|(?:(\\d+)(?:\\.(\\d+))?(\\w))) +(\\d{1,3}(?:\\.\\d{1,2})?)% +(.+)";
  // TODO: 16/07/18 Might need to be updated to take hours and days into account.
  // TODO: 20/09/18 Handle the case where the nice value has a 'r' (for real time?).
  Pattern PROCESS_PATTERN = Pattern.compile(PROCESS_REGEX);

  Class<?>[] PROCESS_OUTPUT_FORMATS = new Class[]{
      Integer.class,
      String.class,
      Integer.class,
      Integer.class,
      Integer.class,
      Long.class,
      Character.class,
      Long.class,
      Character.class,
      String.class,
      Integer.class,
      Long.class,
      Integer.class,
      Float.class,
      String.class
  };

  /**
   * Sample output for the command {@code top -P} on a FreeBSD operating system. This should stay closer as possible to reality in order for tests to check the matching regexes are correct.
   * Last update made on FreeBSD ** (2018-07)
   */
  // TODO: 11/07/18 Check the last FreeBSD version used.
  String TEST_SAMPLE = "last pid: 63434;  load averages:  0.16,  0.05,  0.01  up 122+13:59:48    17:23:08\n" +
      "34 processes:  1 running, 33 sleeping\n" +
      "CPU 0:   0.0% user,  0.0% nice,  0.0% system,  0.0% interrupt,  100% idle\n" +
      "CPU 1:   0.8% user,  0.0% nice,  0.0% system,  0.0% interrupt, 99.2% idle\n" +
      "CPU 2:   0.0% user,  0.0% nice,  0.0% system,  0.0% interrupt,  100% idle\n" +
      "CPU 3:   0.8% user,  0.0% nice,  1.5% system,  0.0% interrupt, 97.7% idle\n" +
      "CPU 4:   0.0% user,  0.0% nice,  0.0% system,  0.0% interrupt,  100% idle\n" +
      "CPU 5:   0.0% user,  0.0% nice,  0.0% system,  0.0% interrupt,  100% idle\n" +
      "CPU 6:   0.0% user,  0.0% nice,  0.0% system,  0.0% interrupt,  100% idle\n" +
      "CPU 7:   0.0% user,  0.0% nice,  0.0% system,  0.0% interrupt,  100% idle\n" +
      "CPU 8:   0.0% user,  0.0% nice,  0.0% system,  0.0% interrupt,  100% idle\n" +
      "CPU 9:   0.8% user,  0.0% nice,  0.0% system,  0.0% interrupt, 99.2% idle\n" +
      "CPU 10:  0.0% user,  0.0% nice,  0.8% system,  0.0% interrupt, 99.2% idle\n" +
      "CPU 11:  0.0% user,  0.0% nice,  0.0% system,  0.0% interrupt,  100% idle\n" +
      "CPU 12:  0.0% user,  0.0% nice,  0.0% system,  0.0% interrupt,  100% idle\n" +
      "CPU 13:  0.0% user,  0.0% nice,  0.0% system,  0.0% interrupt,  100% idle\n" +
      "CPU 14:  0.0% user,  0.0% nice,  0.0% system,  0.0% interrupt,  100% idle\n" +
      "CPU 15:  0.8% user,  0.0% nice,  0.8% system,  0.0% interrupt, 98.5% idle\n" +
      "Mem: 53M Active, 1406M Inact, 78M Laundry, 8686M Wired, 5699M Free\n" +
      "ARC: 6305M Total, 1692M MFU, 4028M MRU, 1479K Anon, 85M Header, 500M Other\n" +
      "     5413M Compressed, 8826M Uncompressed, 1.63:1 Ratio\n" +
      "Swap: 2048M Total, 2048M Free\n" +
      "\n" +
      "  PID USERNAME    THR PRI NICE   SIZE    RES STATE   C   TIME    WCPU COMMAND\n" +
      "94308 vrakfall     23  20    0 13027M  1077M nanslp  7 386:22   0.24% factorio\n" +
      "55862 vrakfall      1  20    0 20160K  3464K CPU7    7   0:00   0.18% top\n" +
      "54526 vrakfall      1  21    0 62480K  7360K select  8   0:00   0.10% sshd\n" +
      "55989 vrakfall      1  22    0 10752K  2628K piperd  2   0:00   0.01% awk\n" +
      "53597 root          1  21    0 62480K  7356K select  0   0:00   0.00% sshd";

  //====== Static Methods ======//

  /**
   * Returns the corresponding `top` command depending on the wanted arguments.
   *
   * @param infinite          True if the command should be run indefinitely.
   * @param IntervalInSeconds The delay in seconds between each refresh of the command (between each new output).
   * @return The command to be run.
   */
  static String getCommand(final boolean infinite, int IntervalInSeconds) {
    if (IntervalInSeconds < 1) {
      IntervalInSeconds = 1;
      if (BuildConfig.DEBUG) {
        Log.d(TAG, "WARNING: An interval of less than a second between 2 updates as been asked for the command `top`, switching to 1s."); // TODO: 23/07/18 Check if we can do less than tenths of seconds.
      }
    }

    if (infinite) {
      return String.format(Locale.US, INFINITE_TOP, IntervalInSeconds);
    }
    return String.format(Locale.US, SINGLE_TOP, IntervalInSeconds);
  }

  /**
   * Parses what should be the first line of the command `top` on a BSD system. It should contain the load averages, the uptime of the server and its time (without the date).
   * The Exception will be verbose by default. Use {@link #parseFirstLine(String, boolean)} to specify
   * the Exception's
   * verbosity.
   *
   * @param topLine The line to be parsed.
   * @return The Response object with the parsed values.
   * @throws NoMatchFoundException Thrown if no match has been found.
   */
  @NotNull
  static LoadAveragesAndTimeInfo parseFirstLine(@NotNull final String topLine) throws NoMatchFoundException {
    return parseFirstLine(topLine, true);
  }

  /**
   * Parses what should be the first line of the command `top` on a BSD system. It should contain the load averages, the uptime of the server and its time (without the date).
   *
   * @param topLine          The line to be parsed.
   * @param verboseException Whether a verbose exception containing an explanation message is wanted or not.
   * @return The Response object with the parsed values.
   * @throws NoMatchFoundException Thrown if no match has been found.
   */
  @NotNull
  static LoadAveragesAndTimeInfo parseFirstLine(@NotNull final String topLine, final boolean verboseException) throws NoMatchFoundException {
    final Matcher matcher = FIRST_LINE_PATTERN.matcher(topLine);

    if (matcher.find()) {
      final Duration upTime = Duration
          .standardDays(Long.valueOf(matcher.group(4)))
          .plus(Duration.standardHours(Long.valueOf(matcher.group(5))))
          .plus(Duration.standardMinutes(Long.valueOf(matcher.group(6))))
          .plus(Duration.standardSeconds(Long.valueOf(matcher.group(7))));

      final LocalTime serverTime = new LocalTime(Integer.valueOf(matcher.group(8)), Integer.valueOf(matcher.group(9)));

      return new LoadAveragesAndTimeInfo(
          Float.valueOf(matcher.group(1)),
          Float.valueOf(matcher.group(2)),
          Float.valueOf(matcher.group(3)),
          upTime,
          serverTime);
    }

    if (verboseException) {
      throw new NoMatchFoundException(String.format(Locale.US, "No match corresponding to the first line of the top command on BSD found in the line:\n%s", topLine));
    }
    else {
      throw new NoMatchFoundException();
    }
  }

  /**
   * Parse the line of the command `top` of a BSD system that contains the recap of the processes.
   * The Exception will be verbose by default. Use {@link #parseProcessRecap(String, boolean)} to specify the Exception's verbosity.
   *
   * @param topLine The line to be parsed.
   * @return The Response object with the parsed values.
   * @throws NoMatchFoundException Thrown if no match has been found.
   */
  static ProcessRecap parseProcessRecap(final String topLine) throws NoMatchFoundException {
    return parseProcessRecap(topLine, true);
  }

  /**
   * Parse the line of the command `top` of a BSD system that contains the recap of the processes.
   *
   * @param topLine          The line to be parsed.
   * @param verboseException Whether a verbose exception containing an explanation message is wanted or not.
   * @return The Response object with the parsed values.
   * @throws NoMatchFoundException Thrown if no match has been found.
   */
  static ProcessRecap parseProcessRecap(final String topLine, final boolean verboseException) throws NoMatchFoundException {
    final Matcher matcher = PROCESS_RECAP_PATTERN.matcher(topLine);

    if (matcher.find()) {
      return new ProcessRecap(
          Integer.valueOf(matcher.group(1)),
          Integer.valueOf(matcher.group(2)),
          Integer.valueOf(matcher.group(3))
      );
    }

    if (verboseException) {
      throw new NoMatchFoundException(String.format(Locale.US, "No match corresponding to the process recap line of the top command on BSD found in the line:\n%s", topLine));
    }
    else {
      throw new NoMatchFoundException();
    }
  }

  /**
   * Parse the usage percentages of one CPU from a single output line from the command `top`.
   * The Exception will be verbose by default. Use {@link #parseCPUUsage(String, boolean)} to specify the Exception's verbosity.
   *
   * @param topLine The line to be parsed
   * @return The corresponding CPU usage object
   * @throws NoMatchFoundException Thrown if no match has been found.
   */
  static @NotNull
  BSDCPUUsage parseCPUUsage(final String topLine) throws NoMatchFoundException {
    return parseCPUUsage(topLine, true);
  }

  /**
   * Parse the usage percentages of one CPU from a single output line from the command `top`.
   *
   * @param topLine          The line to be parsed
   * @param verboseException Whether a verbose exception containing an explanation message is wanted or not.
   * @return The corresponding CPU usage object
   * @throws NoMatchFoundException Thrown if no match has been found.
   */
  static @NotNull
  BSDCPUUsage parseCPUUsage(final String topLine, final boolean verboseException) throws NoMatchFoundException {
    final Matcher matcher = CPU_PATTERN.matcher(topLine);

    if (matcher.find()) {
      return new BSDCPUUsage(
          Integer.valueOf(matcher.group(1)),
          Float.valueOf(matcher.group(2)),
          Float.valueOf(matcher.group(3)),
          Float.valueOf(matcher.group(4)),
          Float.valueOf(matcher.group(5)),
          Float.valueOf(matcher.group(6))
      );
    }

    if (verboseException) {
      throw new NoMatchFoundException(String.format(Locale.US, "No match corresponding to a cpu line found in the line: %s", topLine));
    }
    else {
      throw new NoMatchFoundException();
    }
  }

  static @NotNull
  MemoryUsage parseMemoryUsage(final String topLine) throws NoMatchFoundException {
    return parseMemoryUsage(topLine, true);
  }

  static @NotNull
  MemoryUsage parseMemoryUsage(final String topLine, final boolean verboseException) throws NoMatchFoundException {
    final Matcher matcher = MEMORY_PATTERN.matcher(topLine);

    if (matcher.find()) {
      return new MemoryUsage(
          new ByteAmount(Long.valueOf(matcher.group(1)), matcher.group(2).charAt(0)),
          new ByteAmount(Long.valueOf(matcher.group(3)), matcher.group(4).charAt(0)),
          new ByteAmount(Long.valueOf(matcher.group(5)), matcher.group(6).charAt(0)),
          new ByteAmount(Long.valueOf(matcher.group(7)), matcher.group(8).charAt(0)),
          new ByteAmount(Long.valueOf(matcher.group(9)), matcher.group(10).charAt(0))
      );
    }

    if (verboseException) {
      throw new NoMatchFoundException(String.format(Locale.US, "No match corresponding to a memory line recap found in the line: %s", topLine));
    }
    else {
      throw new NoMatchFoundException();
    }
  }

  static @NotNull
  ARCUsage parseARCUsage(final String topLine) throws NoMatchFoundException {
    return parseARCUsage(topLine, true);
  }

  static @NotNull
  ARCUsage parseARCUsage(final String topLine, final boolean verboseException) throws NoMatchFoundException {
    final Matcher matcher = ARC_PATTERN.matcher(topLine);

    if (matcher.find()) {
      return new ARCUsage(
          new ByteAmount(Long.valueOf(matcher.group(1)), matcher.group(2).charAt(0)),
          new ByteAmount(Long.valueOf(matcher.group(1)), matcher.group(2).charAt(0)),
          new ByteAmount(Long.valueOf(matcher.group(1)), matcher.group(2).charAt(0)),
          new ByteAmount(Long.valueOf(matcher.group(1)), matcher.group(2).charAt(0)),
          new ByteAmount(Long.valueOf(matcher.group(1)), matcher.group(2).charAt(0)),
          new ByteAmount(Long.valueOf(matcher.group(1)), matcher.group(2).charAt(0))
      );
    }

    if (verboseException) {
      throw new NoMatchFoundException(String.format(Locale.US, "No match corresponding to an ARC usage line found in the line: %s", topLine));
    }
    else {
      throw new NoMatchFoundException();
    }
  }

  static @NotNull
  ARCCompression parseARCCompression(final String topLine) throws NoMatchFoundException {
    return parseARCCompression(topLine, true);
  }

  static @NotNull
  ARCCompression parseARCCompression(final String topLine, final boolean verboseException) throws NoMatchFoundException {
    final Matcher matcher = ARC_COMPRESSION_RECAP_PATTERN.matcher(topLine);

    if (matcher.find()) {
      return new ARCCompression(
          new ByteAmount(Long.valueOf(matcher.group(1)), matcher.group(2).charAt(0)),
          new ByteAmount(Long.valueOf(matcher.group(3)), matcher.group(4).charAt(0))
      );
    }

    if (verboseException) {
      throw new NoMatchFoundException(String.format(Locale.US, "No match corresponding to an ARC compression recap line found in the line: %s", topLine));
    }
    else {
      throw new NoMatchFoundException();
    }
  }

  static @NotNull
  SwapUsage parseSwapUsage(final String topLine) throws NoMatchFoundException {
    return parseSwapUsage(topLine, true);
  }

  static @NotNull
  SwapUsage parseSwapUsage(final String topLine, final boolean verboseException) throws NoMatchFoundException {
    final Matcher matcher = SWAP_PATTERN.matcher(topLine);

    if (matcher.find()) {
      return new SwapUsage(
          new ByteAmount(Long.valueOf(matcher.group(1)), matcher.group(2).charAt(0)),
          new ByteAmount(Long.valueOf(matcher.group(3)), matcher.group(4).charAt(0)),
          new ByteAmount(Long.valueOf(matcher.group(5)), matcher.group(6).charAt(0))
      );
    }

    if (verboseException) {
      throw new NoMatchFoundException(String.format(Locale.US, "No match corresponding to a Swap usage line found in the line: %s", topLine));
    }
    else {
      throw new NoMatchFoundException();
    }
  }

  static boolean checkHasProcessColumnTitles(final String topLine) {
    final Matcher matcher = PROCESS_COLUMN_TITLES_PATTERN.matcher(topLine);

    if (matcher.find()) {
      return true;
    }

    return false;
  }

  static @NotNull
  ProcessInfo parseProcessDetails(final String topLine) throws NoMatchFoundException {
    return parseProcessDetails(topLine, true);
  }

  static @NotNull
  ProcessInfo parseProcessDetails(final String topLine, final boolean verboseException) throws NoMatchFoundException {
    final Matcher matcher = PROCESS_PATTERN.matcher(topLine);

    if (matcher.find()) {
      final Duration duration;
      final String minutes = matcher.group(12);

      if (minutes != null) {
        duration = Duration.standardMinutes(Long.valueOf(minutes)).plus(Duration.standardSeconds(Integer.valueOf(matcher.group(13))));
      }
      else {
        if (matcher.group(16).equals("H")) {
          duration = Duration.standardHours(Long.valueOf(matcher.group(14))).plus(Duration.standardMinutes(Integer.valueOf(matcher.group(15)) * 6));
          // TODO: 19/09/18 Do this better and for other possible outcomes in a better way.
        }
        else {
          duration = Duration.standardSeconds(0);
        }
      }

      return new ProcessInfo(
          Integer.valueOf(matcher.group(1)),
          null,
          matcher.group(2),
          null,
          null,
          Integer.valueOf(matcher.group(3)),
          Integer.valueOf(matcher.group(4)),
          Integer.valueOf(matcher.group(5)),
          new ByteAmount(Long.valueOf(matcher.group(6)), matcher.group(7).charAt(0)),
          new ByteAmount(Long.valueOf(matcher.group(8)), matcher.group(9).charAt(0)),
          matcher.group(10),
          Integer.valueOf(matcher.group(11)),
          duration,
          Float.valueOf(matcher.group(17)),
          matcher.group(18)
      );
    }

    if (verboseException) {
      throw new NoMatchFoundException(String.format(Locale.US, "No match corresponding to a process details line found in the line: %s", topLine));
    }
    else {
      throw new NoMatchFoundException();
    }
  }

  //====== Object Methods ======//
}
