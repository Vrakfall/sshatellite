package be.vrakfall.sshatellite.system.commands.python.errors;

public class NoPythonInstalledException extends Exception {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public NoPythonInstalledException() {
  }

  public NoPythonInstalledException(String message) {
    super(message);
  }

  public NoPythonInstalledException(String message, Throwable cause) {
    super(message, cause);
  }

  public NoPythonInstalledException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
