package be.vrakfall.sshatellite.system.commands.python.errors;

import be.vrakfall.sshatellite.utils.io.errors.ParsingException;

public class NoVersionedCommandException extends ParsingException {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public NoVersionedCommandException() {
    super();
  }

  public NoVersionedCommandException(String message) {
    super(message);
  }

  public NoVersionedCommandException(String message, Throwable cause) {
    super(message, cause);
  }

  public NoVersionedCommandException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
