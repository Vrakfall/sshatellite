package be.vrakfall.sshatellite.system.errors;

public class AttributeNotSetException extends MachineException {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public AttributeNotSetException() {
  }

  public AttributeNotSetException(String message) {
    super(message);
  }

  public AttributeNotSetException(String message, Throwable cause) {
    super(message, cause);
  }

  public AttributeNotSetException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
