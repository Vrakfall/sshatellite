package be.vrakfall.sshatellite.system.commands.top;

import be.vrakfall.sshatellite.Parameters;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CPUUsage implements Comparable<CPUUsage> {
  //==== Static variables ====//

  public static final String TAG = CPUUsage.class.getSimpleName();

  //==== Attributes ====//

  private int number;
  private float user;
  private float nice;
  private float system;
  // TODO: 5/12/18 Make sure idle and others can't be outside their range.
  private float idle;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  @Builder
  public CPUUsage(final int number, final float user, final float nice, final float system, final float idle) {
    this.number = number;
    this.user = user;
    this.nice = nice;
    this.system = system;
    this.idle = idle;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  public float getTotal() {
//    Log.d(TAG, String.format(Locale.US, "getTotal: %s", toString()));
    return Parameters.PERCENTAGE_MAX - getIdle();
  }

  @Override
  public int compareTo(@NotNull final CPUUsage o) {
    return Integer.compare(getNumber(), o.getNumber());
  }
}
