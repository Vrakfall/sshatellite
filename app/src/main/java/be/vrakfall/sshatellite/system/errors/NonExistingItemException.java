package be.vrakfall.sshatellite.system.errors;

public class NonExistingItemException extends Exception {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public NonExistingItemException() {
    super();
  }

  public NonExistingItemException(String message) {
    super(message);
  }

  public NonExistingItemException(String message, Throwable cause) {
    super(message, cause);
  }

  public NonExistingItemException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
