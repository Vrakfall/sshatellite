package be.vrakfall.sshatellite.system.commands;

import be.vrakfall.sshatellite.system.commands.python.errors.NoVersionedCommandException;
import be.vrakfall.sshatellite.system.commands.python.version.ReleaseSegment;
import be.vrakfall.sshatellite.system.commands.python.version.errors.ReleaseSegmentParsingException;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;

import java.text.Collator;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VersionedCommand implements Comparable<VersionedCommand> {
  //==== Static variables ====//

  public static final String COMMAND_REGEX_GROUP = "command";
  public static final String VERSION_REGEX_GROUP = "version";
  public static final String SUFFIX_REGEX_GROUP = "suffix";

  public static final String PARSING_REGEX = "(?<" + COMMAND_REGEX_GROUP + ">[^\\d\\s]+)(?<" + VERSION_REGEX_GROUP + ">\\d+(?:\\.\\d+)*)?(?<" + SUFFIX_REGEX_GROUP + ">\\S+)?";

  public static final Pattern PARSING_PATTERN = Pattern.compile(PARSING_REGEX);

  //==== > Error messages ====//

  private static final String NO_VERSIONED_COMMAND_ERROR_MESSAGE = "No correct versioned command has been found in the string: %s";

  //==== Attributes ====//

  @NotNull
  public final String name;

  @Nullable
  public final ReleaseSegment versionNumber;

  @Nullable
  final public String suffix;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public VersionedCommand(@NotNull String name, @Nullable ReleaseSegment versionNumber, @Nullable String suffix) {
    this.name = name;
    this.versionNumber = versionNumber;
    this.suffix = suffix;
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  public static VersionedCommand fromString(String string) throws NoVersionedCommandException {
    Matcher matcher = PARSING_PATTERN.matcher(string);

    if (matcher.find()) {
      if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
        String name = matcher.group(COMMAND_REGEX_GROUP);
        if (name == null || name.isEmpty()) {
          // This should actually not happen because of the regex's matching group.
          throw new NoVersionedCommandException(String.format(Locale.US, NO_VERSIONED_COMMAND_ERROR_MESSAGE, string));
        }

        String version = matcher.group(VERSION_REGEX_GROUP);
        if (version == null || version.isEmpty()) {
          return new VersionedCommand(name, null, null);
        }

        String suffix = matcher.group(SUFFIX_REGEX_GROUP);
        if (suffix == null || suffix.isEmpty()) {
          try {
            return new VersionedCommand(name, ReleaseSegment.fromString(version), null);
          }
          catch (ReleaseSegmentParsingException e) {
            throw new NoVersionedCommandException(String.format(Locale.US, NO_VERSIONED_COMMAND_ERROR_MESSAGE, string), e);
          }
        }
        else {
          try {
            return new VersionedCommand(name, ReleaseSegment.fromString(version), suffix);
          }
          catch (ReleaseSegmentParsingException e) {
            throw new NoVersionedCommandException(String.format(Locale.US, NO_VERSIONED_COMMAND_ERROR_MESSAGE, string), e);
          }
        }
      }
      else {
        // TODO: 17/10/18 Parse another way for older SDKs.
      }
    }
    throw new NoVersionedCommandException(String.format(Locale.US, NO_VERSIONED_COMMAND_ERROR_MESSAGE, string));
  }

  public static VersionedCommand parseVersionedCommand(String string) throws NoVersionedCommandException {
    return fromString(string);
  }

  public static int compare(@NotNull VersionedCommand versionedCommand0, @NotNull VersionedCommand versionedCommand1) {
    Collator collator = Collator.getInstance(Locale.US);
    collator.setStrength(Collator.TERTIARY);

    int result = collator.compare(versionedCommand0.name, versionedCommand1.name);
    if (result != 0) {
      return result;
    }

    if (versionedCommand0.versionNumber == null) {
      if (versionedCommand1.versionNumber == null) {
        return 0;
      }
      return -1;
    }
    if (versionedCommand1.versionNumber == null) {
      return 1;
    }

    result = versionedCommand0.versionNumber.compareTo(versionedCommand1.versionNumber);
    if (result != 0) {
      return result;
    }

    if (versionedCommand0.suffix == null) {
      if (versionedCommand1.suffix == null) {
        return 0;
      }
      return -1;
    }
    if (versionedCommand1.suffix == null) {
      return 1;
    }

    return collator.compare(versionedCommand0.suffix, versionedCommand1.suffix);
  }

  //==== Object Methods ====//

  public boolean isSameName(VersionedCommand other) {
    return name.equals(other.name);
  }

  public boolean isSameName(String otherName) {
    return name.equals(otherName);
  }

  public String toSimpleString(String levelSeparator) {
    return name + (versionNumber == null ? null : versionNumber.toSimpleString(levelSeparator)) + suffix;
  }

  public String toSimpleString() {
    return name + (versionNumber == null ? null : versionNumber.toSimpleString()) + suffix;
  }

  @Override
  public int compareTo(@NotNull VersionedCommand o) {
    return compare(this, o);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    VersionedCommand that = (VersionedCommand) o;

    if (!name.equals(that.name)) return false;
    if (versionNumber != null ? !versionNumber.equals(that.versionNumber) : that.versionNumber != null) return false;
    return suffix != null ? suffix.equals(that.suffix) : that.suffix == null;
  }

  @Override
  public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + (versionNumber != null ? versionNumber.hashCode() : 0);
    result = 31 * result + (suffix != null ? suffix.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "VersionedCommand{" +
        "name='" + name + '\'' +
        ", versionNumber=" + versionNumber +
        ", suffix='" + suffix + '\'' +
        '}';
  }
}
