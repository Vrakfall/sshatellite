package be.vrakfall.sshatellite.system.commands.top;

public interface LinuxTop {
  //==== Static variables ====//

  /**
   * Maximum interval value of time in seconds between two updates.
   */
  int MAX_INTERVAL_VALUE = 2147483583;

  //====== Static Methods ======//

  //====== Object Methods ======//
}
