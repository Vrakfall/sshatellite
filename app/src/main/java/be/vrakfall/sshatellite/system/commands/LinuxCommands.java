package be.vrakfall.sshatellite.system.commands;

public interface LinuxCommands {
  //==== Static variables ====//

  String top = "top -b1n 2 -d 1 | awk \\'/^top -/ {\n" +
      "  if (nextBlock == 1) {\n" +
      "    buffer = $0;\n" +
      "    nextBlock++;\n" +
      "    next;\n" +
      "  }\n" +
      "  nextBlock++;\n" +
      "}\n" +
      "nextBlock == 2 {\n" +
      "  buffer = buffer \"\\n\" $0;\n" +
      "}\n" +
      "END {\n" +
      "  printf buffer;\n" +
      "  exit;\n" +
      "}\\'";

  String infiniteTop = "top -b1 -d 1";

  //====== Static Methods ======//

  //====== Object Methods ======//
}
