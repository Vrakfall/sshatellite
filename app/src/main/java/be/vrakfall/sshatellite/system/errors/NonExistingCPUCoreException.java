package be.vrakfall.sshatellite.system.errors;

public class NonExistingCPUCoreException extends NonExistingItemException {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public NonExistingCPUCoreException() {
    super();
  }

  public NonExistingCPUCoreException(String message) {
    super(message);
  }

  public NonExistingCPUCoreException(String message, Throwable cause) {
    super(message, cause);
  }

  public NonExistingCPUCoreException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
