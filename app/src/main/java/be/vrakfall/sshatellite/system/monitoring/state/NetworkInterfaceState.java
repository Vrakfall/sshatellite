package be.vrakfall.sshatellite.system.monitoring.state;

import android.annotation.TargetApi;
import android.os.Build;
import androidx.annotation.RequiresApi;
import lombok.Data;
import lombok.var;

import java.time.Duration;

@Data
public class NetworkInterfaceState {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  private String name;
  private long sentByteAmount;
  private long receivedByteAmount;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  /**
   * Calculates the upload speed.
   *
   * @param previousSentBytes The previous amount of sent bytes from the previous state, should be less than the
   *                          current state.
   * @param duration          The duration between the previous state and this one.
   * @return The upload speed in bytes.
   */
  @TargetApi(Build.VERSION_CODES.O)
  @RequiresApi(Build.VERSION_CODES.O)
  public long uploadSpeed(final long previousSentBytes, final Duration duration) {
    final var deltaByteAmount = getSentByteAmount() - previousSentBytes;
    if (deltaByteAmount < 0 || duration.isZero()) {
      // TODO: 23/01/19 Change this workaround for potential edge cases not even encountered (+ think about how it can
      //  fail)
      return 0L;
    }
    return deltaByteAmount / duration.getSeconds();
  }
}
