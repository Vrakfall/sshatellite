package be.vrakfall.sshatellite.system.monitoring.state;

import android.util.SparseArray;
import be.vrakfall.sshatellite.system.commands.top.CPUUsage;

public class MachineStateSparseArray {
  //==== Static variables ====//

  //==== Attributes ====//

  public final SparseArray<CPUUsage> cpuUsages;
  public final float memoryUsage;

  //==== Getters and Setters ====//

  public MachineStateSparseArray(final SparseArray<CPUUsage> cpuUsages, final float memoryUsage) {
    this.cpuUsages = cpuUsages;
    this.memoryUsage = memoryUsage;
  }

  //==== Constructors ====//

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  @Override
  public String toString() {
    return "MachineState{" +
        "cpuUsages=" + cpuUsages +
        ", memoryUsage=" + memoryUsage +
        '}';
  }
}
