package be.vrakfall.sshatellite.system.monitoring.state.variables;

import android.annotation.TargetApi;
import androidx.annotation.RequiresApi;
import be.vrakfall.sshatellite.system.commands.VersionedCommand;
import be.vrakfall.sshatellite.system.commands.python.errors.NoVersionedCommandException;
import be.vrakfall.sshatellite.system.monitoring.state.variables.errors.NoExecutablePathEntryException;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION_CODES;

public class ExecutablePathEntry {
  //==== Static variables ====//

  public static final String COMMAND_REGEX_GROUP = "command";
  public static final String PATH_REGEX_GROUP = "path";

  public static final String PARSING_REGEX = "(?<" + COMMAND_REGEX_GROUP + ">\\S+) = (?<" + PATH_REGEX_GROUP + ">\\S+)";

  public static final Pattern PARSING_PATTERN = Pattern.compile(PARSING_REGEX);

  /**
   * Shell command to list all (or most of) the commands a shell knows with the full path to its executable file.
   * Taken from <a href="https://unix.stackexchange.com/questions/94775/list-all-commands-that-a-shell-knows">https://unix.stackexchange.com/questions/94775/list-all-commands-that-a-shell-knows</a>
   */
  public static final String LIST_ALL_SHELL_COMMAND = "sh -c 'case \"$PATH\" in\n" +
      "\t(*[!:]:) PATH=\"$PATH:\" ;;\n" +
      "esac\n" +
      "\n" +
      "set -f; IFS=:\n" +
      "for dir in $PATH; do\n" +
      "\tset +f\n" +
      "\t[ -z \"$dir\" ] && dir=\".\"\n" +
      "\tfor file in \"$dir\"/*; do\n" +
      "\t\tif [ -x \"$file\" ] && ! [ -d \"$file\" ]; then\n" +
      "\t\t\tprintf \"%s = %s\\n\" \"${file##*/}\" \"$file\"\n" +
      "\t\tfi\n" +
      "\tdone\n" +
      "done'"; // TODO: 17/10/18 Put this in a file to copy on the server and then execute?

  //==== > Error messages ====//

  private static final String NO_EXECUTABLE_PATH_ENTRY_ERROR_MESSAGE = "No correct executable path entry has been found in the string: %s";

  //==== Attributes ====//

  @NotNull
  public final VersionedCommand command;

  @NotNull
  public final String path;

  //==== Getters and Setters ====//

  public ExecutablePathEntry(@NotNull final VersionedCommand command, @NotNull final String path) {
    this.command = command;
    this.path = path;
  }

  @NotNull
  public String getPath() {
    return path;
  }

  //==== Constructors ====//

  @NotNull
  public VersionedCommand getCommand() {
    return command;
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  @Nullable
  public static ExecutablePathEntry fromString(final String string) {
    if (string == null) {
      return null;
    }
    final Matcher matcher = PARSING_PATTERN.matcher(string);

    if (matcher.find()) {
      if (VERSION.SDK_INT >= VERSION_CODES.O) {
        return buildExecutablePathEntryFromMatcher(matcher);
      }
      else {
        // TODO: 17/10/18 Parse another way for older SDKs.
      }
    }
    return null;
  }

  @TargetApi(VERSION_CODES.O)
  @RequiresApi(api = VERSION_CODES.O)
  @Nullable
  private static ExecutablePathEntry buildExecutablePathEntryFromMatcher(final Matcher matcher) {
    final String command = matcher.group(COMMAND_REGEX_GROUP);
    if (command == null || command.isEmpty()) {
      // This should actually not happen because of the regex's matching group.
      return null;
    }

    final String path = matcher.group(PATH_REGEX_GROUP);
    if (path == null || path.isEmpty()) {
      // This should actually not happen because of the regex's matching group.
      return null;
    }

    try {
      return new ExecutablePathEntry(VersionedCommand.fromString(command), path);
    }
    catch (final NoVersionedCommandException e) {
      return null;
    }
  }

  public static ExecutablePathEntry parseExecutablePathEntry(final String string) throws NoExecutablePathEntryException {
    return fromString(string);
  }

  //==== Object Methods ====//

  public boolean isSameProgram(final ExecutablePathEntry other) {
    return command.isSameName(other.command);
  }

  public boolean isSameProgram(final String otherProgramName) {
    return command.isSameName(otherProgramName);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final ExecutablePathEntry that = (ExecutablePathEntry) o;

    if (!command.equals(that.command)) {
      return false;
    }
    return path.equals(that.path);
  }

  @Override
  public int hashCode() {
    int result = command.hashCode();
    result = 31 * result + path.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "ExecutablePathEntry{" +
        "command=" + command +
        ", path='" + path + '\'' +
        '}';
  }
}
