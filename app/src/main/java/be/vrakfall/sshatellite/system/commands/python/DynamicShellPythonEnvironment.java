package be.vrakfall.sshatellite.system.commands.python;

import be.vrakfall.sshatellite.system.commands.ShellEnvironment;

public abstract class DynamicShellPythonEnvironment extends ShellPythonEnvironment {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Constructors ====//

  public DynamicShellPythonEnvironment(final ShellEnvironment shellEnvironment) {
    super(shellEnvironment);
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
