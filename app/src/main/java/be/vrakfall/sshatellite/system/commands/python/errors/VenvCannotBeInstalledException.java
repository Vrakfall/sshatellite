package be.vrakfall.sshatellite.system.commands.python.errors;

import android.annotation.TargetApi;
import android.os.Build;
import androidx.annotation.RequiresApi;

public class VenvCannotBeInstalledException extends Exception {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Constructors ====//

  public VenvCannotBeInstalledException() {
  }

  public VenvCannotBeInstalledException(String message) {
    super(message);
  }

  public VenvCannotBeInstalledException(String message, Throwable cause) {
    super(message, cause);
  }

  public VenvCannotBeInstalledException(Throwable cause) {
    super(cause);
  }

  @TargetApi(Build.VERSION_CODES.N)
  @RequiresApi(Build.VERSION_CODES.N)
  public VenvCannotBeInstalledException(String message, Throwable cause, boolean enableSuppression,
                                        boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
