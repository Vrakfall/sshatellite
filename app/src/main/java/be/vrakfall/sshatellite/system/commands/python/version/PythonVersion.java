package be.vrakfall.sshatellite.system.commands.python.version;

import android.os.Build;
import androidx.annotation.RequiresApi;
import be.vrakfall.sshatellite.utils.io.errors.NoMatchFoundException;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.regex.Pattern;

@Value
@EqualsAndHashCode(callSuper = true)
public class PythonVersion extends PEP440Version {
  //==== Static variables ====//

  public static final String TAG = PythonVersion.class.getSimpleName();

  public static final String PARSING_REGEX = "^[Pp]ython " + PEP440Version.PARSING_REGEX + "$";

  public static final Pattern PARSING_PATTERN = Pattern.compile(PARSING_REGEX);

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Constructors ====//

  public PythonVersion(final PEP440Version other) {
    super(other);
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  @Nullable
  @RequiresApi(api = Build.VERSION_CODES.O)
  public static PythonVersion fromVersionString(@NotNull @NonNull final String versionString) {
    try {
      return new PythonVersion(parsePEP440VersionWithPattern(versionString, PARSING_PATTERN));
    }
    catch (final NoMatchFoundException e) {
      return null;
    }
  }

  @Nullable
  @RequiresApi(api = Build.VERSION_CODES.O)
  public static PythonVersion fromString(@NotNull @NonNull final String string) {
    return fromVersionString(string);
  }

  @Nullable
  @RequiresApi(api = Build.VERSION_CODES.O)
  public static PythonVersion parsePEP440Version(@NotNull @NonNull final String string) {
    return fromVersionString(string);
  }

  //==== Object Methods ====//
}
