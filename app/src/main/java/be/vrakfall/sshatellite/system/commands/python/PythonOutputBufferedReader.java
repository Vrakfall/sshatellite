package be.vrakfall.sshatellite.system.commands.python;

import android.os.Build;
import androidx.annotation.RequiresApi;
import be.vrakfall.sshatellite.system.commands.python.version.PEP440Version;
import be.vrakfall.sshatellite.system.commands.python.version.PythonVersion;
import be.vrakfall.sshatellite.utils.io.ExtendedBufferedReader;
import be.vrakfall.sshatellite.utils.io.errors.NoMatchFoundException;
import lombok.NonNull;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.Reader;

public class PythonOutputBufferedReader extends ExtendedBufferedReader {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public PythonOutputBufferedReader(@NotNull @NonNull final Reader in, final int sz) {
    super(in, sz);
  }

  public PythonOutputBufferedReader(@NotNull @NonNull final Reader in) {
    super(in);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  @Nullable
  @RequiresApi(api = Build.VERSION_CODES.O)
  public PEP440Version readNextPEP440Version() throws IOException {
    final val line = readNextNonEmptyLine();
    if (line == null) {
      return null;
    }
    try {
      return PEP440Version.fromString(line);
    }
    catch (final NoMatchFoundException e) {
      return null;
    }
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  @Nullable
  public PythonVersion readNextPythonVersion() throws IOException {
    final var line = readNextNonEmptyLine();
    if (line == null) {
      return null;
    }
    return PythonVersion.fromString(line);
  }
}
