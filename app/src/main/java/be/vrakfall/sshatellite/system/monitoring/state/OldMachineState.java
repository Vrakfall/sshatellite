package be.vrakfall.sshatellite.system.monitoring.state;

import be.vrakfall.sshatellite.system.commands.top.CPUUsage;

import java.util.Map;

public class OldMachineState {
  //==== Static variables ====//

  //==== Attributes ====//

  public final Map<Integer, CPUUsage> cpuUsages;
  public final float memoryUsage;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public OldMachineState(final Map<Integer, CPUUsage> cpuUsages, final float memoryUsage) {
    this.cpuUsages = cpuUsages;
    this.memoryUsage = memoryUsage;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  @Override
  public String toString() {
    return "OldMachineState{" +
        "cpuUsages=" + cpuUsages +
        ", memoryUsage=" + memoryUsage +
        '}';
  }
}
