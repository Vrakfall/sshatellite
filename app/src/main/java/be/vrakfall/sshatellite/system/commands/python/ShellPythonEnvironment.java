package be.vrakfall.sshatellite.system.commands.python;

import android.os.Build;
import android.util.Log;
import androidx.annotation.RequiresApi;
import be.vrakfall.sshatellite.Parameters;
import be.vrakfall.sshatellite.system.commands.ExecutionHandler;
import be.vrakfall.sshatellite.system.commands.ShellEnvironment;
import be.vrakfall.sshatellite.system.commands.VersionedCommand;
import be.vrakfall.sshatellite.system.commands.errors.NoExecutablePathException;
import be.vrakfall.sshatellite.system.commands.python.errors.PipCannotInstallException;
import be.vrakfall.sshatellite.system.commands.python.errors.VenvCannotBeInstalledException;
import be.vrakfall.sshatellite.system.commands.python.version.PEP440Version;
import be.vrakfall.sshatellite.system.commands.python.version.PythonVersion;
import be.vrakfall.sshatellite.system.monitoring.state.variables.ExecutablePathEntry;
import be.vrakfall.sshatellite.system.monitoring.state.variables.ExecutablePathEntryBufferedReader;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.io.errors.NoMatchFoundException;
import be.vrakfall.sshatellite.utils.ordering.NumberedObject;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.schedulers.Schedulers;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Locale;

// The following is equivalent to @Value (immutable classes) but without `final` so we can inherit from it.
@ToString
@AllArgsConstructor
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
//@SuperBuilder
public abstract class ShellPythonEnvironment implements PythonEnvironment {
  //==== Static variables ====//

  public static final String TAG = ShellPythonEnvironment.class.getSimpleName();

  //==== > Error messages ====//

  private static final String NO_EXECUTABLE_PATH_ENTRY_ERROR_MESSAGE =
      "No executable path entry has been found for this %s"; // %s = class name

  private static final String VENV_INSTALLATION_ERROR_MESSAGE = "The venv installation did not end successfully. " +
      "Status code: %s";

  private static final String PIP_INSTALLATION_ERROR_MESSAGE = "pip couldn't install the required packages. Status " +
      "cade: %s";

  //==== Attributes ====//

  @NotNull
  private final ShellEnvironment shellEnvironment;

  //==== Getters and Setters ====//

  //==== > Abstract ====//

  // TODO: 24/10/18 For dynamically accessed ExecutablePathEntry, handle the case when the python executable disappear.
  @Nullable
  protected abstract ExecutablePathEntry getExecutablePathEntry();

  //==== Constructors ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  /**
   * Returns all the executable path entries that can potentially be used to run python ordered by their priority
   * (which is defined by the System's `PATH` variable).
   * This can potentially make one network request depending on the {@link ShellEnvironment} implementation an can
   * thus be a costly call.
   *
   * @return The executable entries, as an array, ordered by the `PATH` priority.
   * @throws NoMatchFoundException In case there's no executable path entry found.
   * @throws IOException           The exception from the underlying protocol used for executing commands on the shell environment.
   */
  @NotNull
  protected ExecutablePathEntry[] getExecutablePathEntries() throws IOException {
    final ExecutionHandler executionHandler =
        getShellEnvironment().execute(ExecutablePathEntry.LIST_ALL_SHELL_COMMAND + " | grep " + SHORT_COMMAND_NAME);
    final InputStream output = executionHandler.getOutput();
    final ExecutablePathEntryBufferedReader bufferedReader =
        new ExecutablePathEntryBufferedReader(new InputStreamReader(output));
    try {
      return bufferedReader.readAllAsArray(SHORT_COMMAND_NAME);
    }
    finally {
      bufferedReader.close();
      executionHandler.close();
    }
  }

  @NotNull
  protected Observable<ExecutablePathEntry> getExecutablePathEntriesRx() {
    return Observable.<ExecutablePathEntry>create(emitter -> {
      @Cleanup final val executionHandler =
          getShellEnvironment().execute(ExecutablePathEntry.LIST_ALL_SHELL_COMMAND + " | grep " + SHORT_COMMAND_NAME);
      @Cleanup final val output = executionHandler.getOutput();
      @Cleanup final val bufferedReader = new ExecutablePathEntryBufferedReader(new InputStreamReader(output));

      ExecutablePathEntry executablePathEntry;
      while ((executablePathEntry = bufferedReader.readNext()) != null) {
        emitter.onNext(executablePathEntry);
      }
    })
        .subscribeOn(Schedulers.io());
  }

  /**
   * Returns the executable path entries that can potentially be used to run python ordered by their priority (which
   * is defined by the System's `PATH` variable), ignoring the subsequent duplicate command names.
   * This can potentially make one network request depending on the {@link ShellEnvironment} implementation an can
   * thus be a costly call.
   *
   * @return The list of the executable entries, ordered by the `PATH` priority.
   * @throws NoMatchFoundException In case there's no executable path entry found.
   * @throws IOException           The exception from the underlying protocol used for executing commands on the
   *                               shell environment.
   */
  protected LinkedHashMap<VersionedCommand, ExecutablePathEntry> getExecutablePathEntriesWithoutDuplicate() throws IOException {
    final ExecutionHandler executionHandler =
        getShellEnvironment().execute(ExecutablePathEntry.LIST_ALL_SHELL_COMMAND + " | grep " + SHORT_COMMAND_NAME);
    final InputStream inputStream = executionHandler.getOutput();
    final ExecutablePathEntryBufferedReader bufferedReader = new ExecutablePathEntryBufferedReader(new InputStreamReader(inputStream));
    try {
      return bufferedReader.readAllWithoutDuplicatedCommandAsLinkedHashMap(SHORT_COMMAND_NAME);
    }
    finally {
      bufferedReader.close();
      executionHandler.close();
    }
  }

  @NotNull
  protected Observable<NumberedObject<ExecutablePathEntry>> getNumberedPathEntriesRx() {
    return Observable.create((ObservableEmitter<NumberedObject<ExecutablePathEntry>> emitter) -> {
      @Cleanup final val executionHandler =
          getShellEnvironment().execute(ExecutablePathEntry.LIST_ALL_SHELL_COMMAND + " | grep " + SHORT_COMMAND_NAME);
      @Cleanup final val output = executionHandler.getOutput();
      @Cleanup final val bufferedReader = new ExecutablePathEntryBufferedReader(new InputStreamReader(output));

      ExecutablePathEntry executablePathEntry;
      for (int i = 0; (executablePathEntry = bufferedReader.readNext()) != null; i++) {
        if (executablePathEntry.isSameProgram(SHORT_COMMAND_NAME)) {
          emitter.onNext(
              NumberedObject.<ExecutablePathEntry>builder()
                  .number(i)
                  .object(executablePathEntry).build()
          );
        }
      }
      Log.d(TAG, "getNumberedPathEntriesRx: Finished.");
      emitter.onComplete();
    })
        .subscribeOn(Schedulers.io());
  }

  @Nullable
  protected ExecutablePathEntry getPriorityExecutablePathEntry() throws IOException {
    @Cleanup final val executionHandler =
        getShellEnvironment().execute(ExecutablePathEntry.LIST_ALL_SHELL_COMMAND + " | grep " + SHORT_COMMAND_NAME);
    @Cleanup final val bufferedReader =
        new ExecutablePathEntryBufferedReader(new InputStreamReader(executionHandler.getOutput()));
    return bufferedReader.readNext();
  }

  @Nullable
  @RequiresApi(api = Build.VERSION_CODES.O)
  protected PythonVersion getVersionFromExecutable(@NotNull final ExecutablePathEntry executablePathEntry)
      throws IOException {
    Log.d(TAG, String.format(Locale.US, "getVersionFromExecutable: path: %s", executablePathEntry.getPath()));
    @Cleanup final val executionHandler =
        getShellEnvironment().execute(executablePathEntry.getPath() + " " + PEP440Version.ARGUMENT);
    @Cleanup final val bufferedReader =
        new PythonOutputBufferedReader(new InputStreamReader(executionHandler.getOutput()));

    final val version = bufferedReader.readNextPythonVersion();
    if (version != null) {
      return version;
    }
    @Cleanup final val errorBufferedReader =
        new PythonOutputBufferedReader(new InputStreamReader(executionHandler.getErrorOutput()));
    return errorBufferedReader.readNextPythonVersion();
  }

  /**
   * Synchronously install a `venv` for the application, update its packages and install the ones required by the app.
   * (TODO: This should at some point get generalized and placed somewhere else.)
   *
   * @param destinationPath The desination directory for the `venv`.
   * @return true if the process was successful, false otherwise.
   * @throws IOException In case an unexpected IO error occurs during the process (this can potentially be executed
   *                     over a network connection).
   */
  @Override
  public void installVenvUpdatePackagesAndInstallRequiredOnes(final String destinationPath) throws
      IOException, NoExecutablePathException, VenvCannotBeInstalledException, PipCannotInstallException {
//    Log.d(TAG, String.format(
//        Locale.US, "installVenvUpdatePackagesAndInstallRequiredOnes: Path is %s",
//        destinationPath
//    ));
    // TODO: 15/11/18 Check the ending of the path
    final val executablePath = getExecutablePathEntry();
    if (executablePath == null) {
      throw new NoExecutablePathException(String.format(Locale.US, NO_EXECUTABLE_PATH_ENTRY_ERROR_MESSAGE,
          this.getClass().getSimpleName()));
    }

    var executionHandler =
        shellEnvironment.execute(getExecutablePathEntry().getPath() + " " + INSTALL_VENV_ARGUMENT + " " +
            destinationPath);
    final val installationStatusCode = executionHandler.waitExecutionAndGetExitStatus();
    if (installationStatusCode != 0) {
      throw new VenvCannotBeInstalledException(
          String.format(Locale.US, VENV_INSTALLATION_ERROR_MESSAGE, installationStatusCode)
      );
    }

    final val executedCommand = destinationPath + "/" + Parameters.PIP_BIN_PATH_IN_VENV + " install " +
        "--upgrade pip setuptools psutil pyyaml";
    executionHandler =
        shellEnvironment.execute(executedCommand);
    final val pipStatusCode = executionHandler.waitExecutionAndGetExitStatus();
    Log.d(TAG, String.format(Locale.US, "installVenvUpdatePackagesAndInstallRequiredOnes: Backend installation should" +
        " have been successful with status: %d", pipStatusCode));
    if (pipStatusCode != 0) {
      throw new PipCannotInstallException(String.format(Locale.US, PIP_INSTALLATION_ERROR_MESSAGE, pipStatusCode));
    }
  }

  @Override
  public ExecutionHandler executeScriptInVenv(@NotNull final String venvPath, @NotNull final String scriptPath,
                                              @NotNull final String scriptHome) throws IOException {
    // TODO: 18/12/18 Handle the way to make commands way better than this
    return shellEnvironment.execute("sh -c 'PYTHONPATH=\"" + scriptHome + "\" " + venvPath + "/" +
        Parameters.PYTHON_BIN_PATH_IN_VENV + " " + scriptPath + "'");
  }
}
