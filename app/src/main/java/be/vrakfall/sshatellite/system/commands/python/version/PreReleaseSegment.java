package be.vrakfall.sshatellite.system.commands.python.version;

import be.vrakfall.sshatellite.system.commands.python.version.errors.NonExistingSegmentException;
import be.vrakfall.sshatellite.system.commands.python.version.errors.ReleaseStateParsingException;
import be.vrakfall.sshatellite.utils.annotations.NotNull;

import static be.vrakfall.sshatellite.system.commands.python.version.ReleaseState.ALPHA;
import static be.vrakfall.sshatellite.system.commands.python.version.ReleaseState.RELEASE_CANDIDATE;

public class PreReleaseSegment extends VersionSegment implements Comparable<PreReleaseSegment> {
  //==== Static variables ====//

  public static final int STAGE_MIN = ALPHA;
  public static final int STAGE_MAX = RELEASE_CANDIDATE;

  //==== Attributes ====//

  private int stage = STAGE_MIN;

  //==== Getters and Setters ====//

  public PreReleaseSegment(long version, int stage) {
    super(version);
    setStage(stage);
  }

  /**
   * Copy constructor.
   *
   * @param other The object to copy.
   */
  public PreReleaseSegment(@NotNull PreReleaseSegment other) {
    super(other);
    this.stage = other.stage;
  }

  //==== Constructors ====//

  public int getStage() {
    return stage;
  }

  private void setStage(int stage) {
    if (stage > 0 && stage <= 2) {
      this.stage = stage;
    }
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  public static PreReleaseSegment fromString(@NotNull String prefix, @NotNull String string) throws NonExistingSegmentException, ReleaseStateParsingException {
    return new PreReleaseSegment(parseVersionNumber(string), ReleaseState.fromLabel(prefix));
  }

  //==== Object Methods ====//

  @Override
  public int compareTo(@NotNull PreReleaseSegment o) {
    if (this.getStage() != o.getStage()) {
      return Integer.compare(this.getStage(), o.getStage());
    }
    return Long.compare(this.getVersion(), o.getVersion());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;

    PreReleaseSegment that = (PreReleaseSegment) o;

    return getStage() == that.getStage();
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + getStage();
    return result;
  }

  @Override
  public String toString() {
    return "PreReleaseSegment{" +
        "stage=" + stage +
        "} " + super.toString();
  }
}
