package be.vrakfall.sshatellite.system.monitoring.state;

import android.annotation.TargetApi;
import android.os.Build;
import androidx.annotation.RequiresApi;
import be.vrakfall.sshatellite.system.commands.top.ARCCompression;
import be.vrakfall.sshatellite.system.commands.top.ARCUsage;
import be.vrakfall.sshatellite.system.commands.top.CPUUsage;
import be.vrakfall.sshatellite.system.commands.top.SwapUsage;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import lombok.*;
import org.joda.time.Duration;
import org.joda.time.LocalTime;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static be.vrakfall.sshatellite.utils.io.ObjectMapperDispenser.YAML_OBJECT_MAPPER;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MachineState {
  //==== Static variables ====//

  public static final String TAG = MachineState.class.getSimpleName();

  //==== Attributes ====//

  private long associatedMachineId;

  // TODO: 22/07/18 Unit test to check the amount in the array.
  @NotNull
  private float[] loadAverages = {-1.F, -1.F, -1.F};
  // TODO: 22/07/18 Unit test to check the amount in the array.
  @NotNull
  private boolean[] loadAverageSetLastUpdate = {false, false, false};

  @Nullable
  private Duration upTime;
  private boolean upTimeSetLastUpdate;

  @Nullable
  private java.time.LocalDateTime localDateTime;
  private boolean localTimeSetLastUpdate;

  private long processesTotal = -1L;
  private boolean processesTotalSetLastUpdate;
  private long runningProcessesAmount = -1L;
  private boolean runningProcessesAmountSetLastUpdate;
  private long sleepingProcessesAmount = -1L;
  private boolean sleepingProcessesAmountSetLastUpdate;

  @Getter
  private int cpuAmount;

  @Nullable
  private List<CPUUsage> cpuUsages;
  private boolean cpuUsagesSetLastUpdate;
  @Nullable
  private MemoryUsage memoryUsage;
  private boolean memoryUsageSetLastUpdate;
  @Nullable
  private ARCUsage arcUsage;
  private boolean arcUsageSetLastUpdate;
  @Nullable
  private ARCCompression arcCompression;
  private boolean arcCompressionSetLastUpdate;
  @Nullable
  private SwapUsage swapUsage;
  private boolean swapUsageSetLastUpdate;

  @Nullable
  private LinkedHashMap<Integer, ProcessInfo> processesDetails = new LinkedHashMap<>();
  private boolean processesDetailsSetLastUpdate;

  private float memoryUsagePercentage;
  private float swapUsagePercentage;

  @Nullable
  private List<DiskPartition> diskPartitions;

  @Nullable
  @Getter(AccessLevel.NONE)
  private Map<String, NetworkInterfaceState> networkInterfaces;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  @Builder
  @Deprecated
  public MachineState(
      @NotNull final float[] loadAverages, @Nullable final Duration upTime,
      @Nullable final LocalTime localDateTime,
      final long processesTotal,
      final long runningProcessesAmount,
      final long sleepingProcessesAmount,
      final int cpuAmount,
      @Nullable final List<CPUUsage> cpuUsages,
      @Nullable final MemoryUsage memoryUsage,
      @Nullable final ARCUsage arcUsage,
      @Nullable final ARCCompression arcCompression,
      @Nullable final SwapUsage swapUsage,
      @Nullable final LinkedHashMap<Integer, ProcessInfo> processesDetails
  ) {
    update(loadAverages, upTime, localDateTime, processesTotal, runningProcessesAmount, sleepingProcessesAmount,
        cpuAmount, cpuUsages, memoryUsage, arcUsage, arcCompression, swapUsage, processesDetails);
  }

  //==== Getters and Setters ====//

  public float getLoadAverage(final int index) {
    return loadAverages[index];
  }

  public void setProcessesDetails(@Nullable final LinkedHashMap<Integer, ProcessInfo> processesDetails) {
    if (processesDetails != null && !processesDetails.isEmpty()) {
      this.processesDetails = processesDetails;
      processesDetailsSetLastUpdate = true;
    }
  }

  //==== Lists' CRUDs ====//

  public NetworkInterfaceState getNetworkInterfaceState(final String name) {
    // TODO: 23/01/19 Handle the potential null case
    return this.networkInterfaces.get(name);
  }

  //====== Static Methods ======//

  public static MachineState parseFromYaml(final InputStream yamlStream) throws IOException {
    return YAML_OBJECT_MAPPER.readValue(yamlStream, MachineState.class);
  }

  //====== Object Methods ======//

  @TargetApi(Build.VERSION_CODES.N)
  @RequiresApi(Build.VERSION_CODES.N)
  public double cpuUsagePercentage() {
    return getCpuUsages().stream().mapToDouble(CPUUsage::getTotal).average().orElse(Double.NaN);
  }

  @TargetApi(Build.VERSION_CODES.N)
  @RequiresApi(Build.VERSION_CODES.N)
  public double diskPartitionUsageAveragePercentage() {
    return diskPartitions.stream().mapToDouble(DiskPartition::userUsedPercentage).average().orElse(0D);
  }

  @TargetApi(Build.VERSION_CODES.N)
  @RequiresApi(Build.VERSION_CODES.N)
  public double diskPartitionUsageMaxPercentage() {
    return diskPartitions.stream().map(DiskPartition::userUsedPercentage).max(Double::compareTo).orElse(0D);
  }

  @TargetApi(Build.VERSION_CODES.O)
  @RequiresApi(Build.VERSION_CODES.O)
  public long uploadSpeedTotal(final MachineState previousState) {
    final val sentBytesTotal = new ArrayList<>(networkInterfaces.entrySet()).stream().mapToLong(entry ->
        entry.getValue().getSentByteAmount() -
            previousState.getNetworkInterfaceState(entry.getKey()).getSentByteAmount())
        .sum();
    return sentBytesTotal /
        java.time.Duration.between(previousState.getLocalDateTime(), getLocalDateTime()).getSeconds();
  }

  @TargetApi(Build.VERSION_CODES.O)
  @RequiresApi(Build.VERSION_CODES.O)
  public long downloadSpeedTotal(final MachineState previousState) {
    final val receivedBytesTotal = new ArrayList<>(networkInterfaces.entrySet()).stream().mapToLong(entry ->
        entry.getValue().getReceivedByteAmount() -
            previousState.getNetworkInterfaceState(entry.getKey()).getReceivedByteAmount())
        .sum();
    return receivedBytesTotal /
        java.time.Duration.between(previousState.getLocalDateTime(), getLocalDateTime()).getSeconds();
  }

  @Deprecated
  public void resetUpdateCheckers() {
    for (int i = 0; i < loadAverageSetLastUpdate.length; i++) {
      loadAverageSetLastUpdate[i] = false;
    }
    upTimeSetLastUpdate = false;
    localTimeSetLastUpdate = false;
    processesTotalSetLastUpdate = false;
    runningProcessesAmountSetLastUpdate = false;
    sleepingProcessesAmountSetLastUpdate = false;
    cpuUsagesSetLastUpdate = false;
    memoryUsageSetLastUpdate = false;
    arcUsageSetLastUpdate = false;
    arcCompressionSetLastUpdate = false;
    swapUsageSetLastUpdate = false;
    processesDetailsSetLastUpdate = false;
  }

  @Deprecated
  public void update(@NotNull final float[] loadAverages, @Nullable final Duration upTime,
                     @Nullable final LocalTime localTime, final long processesTotal,
                     final long runningProcessesAmount, final long sleepingProcessesAmount,
                     final int cpuAmount, @Nullable final List<CPUUsage> cpuUsages,
                     @Nullable final MemoryUsage memoryUsage,
                     @Nullable final ARCUsage arcUsage, @Nullable final ARCCompression arcCompression,
                     @Nullable final SwapUsage swapUsage,
                     @Nullable final LinkedHashMap<Integer, ProcessInfo> processesDetails) {
    resetUpdateCheckers();

    setLoadAverages(loadAverages);
    // The should fix this legacy code but it wasn't tested!
    setUpTime(upTime);
//    setLocalTime(localTime);
    setProcessesTotal(processesTotal);
    setRunningProcessesAmount(runningProcessesAmount);
    setSleepingProcessesAmount(sleepingProcessesAmount);
    this.cpuAmount = cpuAmount;
    setCpuUsages(cpuUsages);
    setMemoryUsage(memoryUsage);
    setArcUsage(arcUsage);
    setArcCompression(arcCompression);
    setSwapUsage(swapUsage);
    setProcessesDetails(processesDetails);
  }
}
