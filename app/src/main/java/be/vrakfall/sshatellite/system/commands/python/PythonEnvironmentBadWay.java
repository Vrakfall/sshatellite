//package be.vrakfall.sshatellite.system.commands.python;
//
//import android.os.Build;
//import be.vrakfall.sshatellite.system.commands.VersionedCommand;
//import be.vrakfall.sshatellite.system.commands.python.errors.NoPythonInstalledException;
//import be.vrakfall.sshatellite.system.commands.python.version.PEP440Version;
//import be.vrakfall.sshatellite.system.monitoring.state.variables.ExecutablePathEntry;
//import be.vrakfall.sshatellite.system.monitoring.state.variables.ExecutablePathEntryBufferedReader;
//import be.vrakfall.sshatellite.utils.io.ExtendedBufferedReader;
//import be.vrakfall.sshatellite.utils.io.errors.StreamEndException;
//import io.reactivex.Single;
//import io.reactivex.schedulers.Schedulers;
//
//import java.io.InputStreamReader;
//import java.util.LinkedHashMap;
//import java.util.Optional;
//
//public class PythonEnvironmentBadWay {
//  //==== Static variables ====//
//
//  public static final String TAG = PythonEnvironmentBadWay.class.getSimpleName();
//
////  private static final LoadingCache<PythonEnvironmentBadWay, ExecutablePathEntry> EXECUTABLE_PATH_ENTRY_CACHE = Caffeine.newBuilder()
////    .refreshAfterWrite(1L, TimeUnit.HOURS)
////    .build(key -> key.commandable)
//
//  //==== > Error messages ====//
//
//  //==== Attributes ====//
//
////  @NonNull
////  private final Commandable commandable;
//
//  //==== Getters and Setters ====//
//
//  //==== Constructors ====//
//
//  //==== Lists' CRUDs ====//
//
//  //==== Static Methods ====//
//
//  //==== Object Methods ====//
//
//  public Single<PEP440Version> getPythonVersion() {
//    // TODO: 12/10/18 Clean this and all the other Rx calls cause this is generation too many events.
//    commandable.execute("python --version")
//        .map(inputStream -> {
//          try {
//            return new ExtendedBufferedReader(new InputStreamReader(inputStream)).readNextNonEmptyLineEOFAsException(false);
//          }
//          catch (StreamEndException e) {
//            throw new NoPythonInstalledException("Python isn't installed.", e);
//          }
//        })
//        .subscribeOn(Schedulers.io());
//      )
//      .map(PEP440Version::fromString);
//
////    .flatMap(commandable -> commandable.execute("python --version")
////      .map(inputStream -> {
////        String line = new BufferedReader(new InputStreamReader(inputStream)).readLine();
////
////        if (line == null) {
////          return "";
////        }
////        return line;
////      }));
//
////      .flatMap(commandable -> {
////        SynchronousCommandResponse synchronousCommandResponse = commandable.synchronousExecute("python --version");
////        return synchronousCommandResponse.stdout;
////      });
//  }
//
//  private Single<LinkedHashMap<VersionedCommand, ExecutablePathEntry>> getPythonExecutablePathEntriesWithoutDuplicateProgram() { // TODO: 17/10/18 Make better class relations
//    return commandProxy
//        .firstOrError()
//        .flatMap(commandable -> commandable.execute(ExecutablePathEntry.LIST_ALL_SHELL_COMMAND + " | grep python")
//            .map(inputStream -> new ExecutablePathEntryBufferedReader(new InputStreamReader(inputStream))
//                .readAllWithoutDuplicatedCommandAsLinkedHashMap("python"))
//        );
//  }
//
//  private Single<ExecutablePathEntry> getHighestPythonExecutablePathEntry() {
//    return getPythonExecutablePathEntriesWithoutDuplicateProgram()
//        .map(executablePathEntries -> {
//          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            Optional<Entry<VersionedCommand, ExecutablePathEntry>> entry = executablePathEntries.entrySet().stream()
//                .max((o1, o2) -> VersionedCommand.compare(o1.getKey(), o2.getKey()));
//            if (entry.isPresent()) {
//              return entry.get().getValue();
//            }
//            throw new NoPythonInstalledException(NO_PYTHON_EXECUTABLE_FOUND_ERROR_MESSAGE);
//          }
//          else {
////          return Stream.of(executablePathEntries.entrySet())
////            .flatMap(entries -> entries.stream().max((o1, o2) -> VersionedCommand.compare(o1.getKey(), o2.getKey())) // TODO: 18/10/18 Does this even work?
//            return null;
//          }
//        })
//        .cache();
//  }
//}
