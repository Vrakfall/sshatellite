package be.vrakfall.sshatellite.system.commands.python.version.errors;

public abstract class VersionException extends Exception {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public VersionException() {
    super();
  }

  public VersionException(String message) {
    super(message);
  }

  public VersionException(String message, Throwable cause) {
    super(message, cause);
  }

  public VersionException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
