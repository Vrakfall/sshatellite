package be.vrakfall.sshatellite.system.commands.python.version;

import android.os.Build;
import androidx.annotation.RequiresApi;
import be.vrakfall.sshatellite.system.commands.python.version.errors.NonExistingSegmentException;
import be.vrakfall.sshatellite.system.commands.python.version.errors.ReleaseSegmentParsingException;
import be.vrakfall.sshatellite.system.commands.python.version.errors.VersionParsingException;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.io.errors.NoMatchFoundException;
import be.vrakfall.sshatellite.utils.ordering.ExtendedComparable;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PEP440Version implements ReleaseState, ExtendedComparable<PEP440Version> {
  // TODO: 28/09/18 Extend to take care of local builds. Only needed if this is exported to make a stand-alone library.
  //==== Static variables ====//

  public static final String ARGUMENT = "--version";

  public static final long EPOCH_MIN = 1;
  public static final long NO_EPOCH_VALUE = 0L;

  public static final String EPOCH_REGEX_GROUP = "epoch";
  public static final String RELEASE_REGEX_GROUP = "release";
  public static final String PRE_RELEASE_REGEX_GROUP = "pre";
  public static final String PRE_RELEASE_LABEL_REGEX_GROUP = "preLabel";
  public static final String PRE_RELEASE_NUMBER_REGEX_GROUP = "preNumber";
  public static final String POST_RELEASE_REGEX_GROUP = "post";
  public static final String[] POST_RELEASE_NUMBER_REGEX_GROUPS = {
      "postNumber0",
      "postNumber1"
  };
  public static final String POST_RELEASE_LABEL_REGEX_GROUP = "postLabel";
  public static final String DEVELOPMENTAL_RELEASE_REGEX_GROUP = "dev";
  public static final String DEVELOPMENTAL_RELEASE_LABEL_REGEX_GROUP = "devLabel";
  public static final String DEVELOPMENTAL_RELEASE_NUMBER_REGEX_GROUP = "devNumber";
  public static final String LOCAL_RELEASE_REGEX_GROUP = "local"; // TODO: 6/10/18 Deal with this kind of version.

  /**
   * Regex to parse a PEP440 version. It comes from <a href=https://www.python.org/dev/peps/pep-0440/#appendix-b-parsing-version-strings-with-regular-expressions>https://www.python.org/dev/peps/pep-0440/#appendix-b-parsing-version-strings-with-regular-expressions</a> and it is adapted to Java's regex dialect.
   */
  public static final String PARSING_REGEX = "v?(?:(?:(?<" + EPOCH_REGEX_GROUP + ">[0-9]+)!)?(?<" + RELEASE_REGEX_GROUP + ">[0-9]+(?:\\.[0-9]+)*)(?<" + PRE_RELEASE_REGEX_GROUP + ">[-_\\.]?(?<" + PRE_RELEASE_LABEL_REGEX_GROUP + ">(a|b|c|rc|alpha|beta|pre|preview))[-_\\.]?(?<" + PRE_RELEASE_NUMBER_REGEX_GROUP + ">[0-9]+)?)?(?<" + POST_RELEASE_REGEX_GROUP + ">(?:-(?<" + POST_RELEASE_NUMBER_REGEX_GROUPS[0] + ">[0-9]+))|(?:[-_\\.]?(?<" + POST_RELEASE_LABEL_REGEX_GROUP + ">post|rev|r)[-_\\.]?(?<" + POST_RELEASE_NUMBER_REGEX_GROUPS[1] + ">[0-9]+)?))?(?<" + DEVELOPMENTAL_RELEASE_REGEX_GROUP + ">[-_\\.]?(?<" + DEVELOPMENTAL_RELEASE_LABEL_REGEX_GROUP + ">dev)[-_\\.]?(?<" + DEVELOPMENTAL_RELEASE_NUMBER_REGEX_GROUP + ">[0-9]+)?)?)(?:\\+(?<" + LOCAL_RELEASE_REGEX_GROUP + ">[a-z0-9]+(?:[-_\\.][a-z0-9]+)*))?";

  public static final Pattern PARSING_PATTERN = Pattern.compile(PARSING_REGEX);

  //==== > Error messages ====//

  private static final String NEGATIVE_EPOCH_ERROR_MESSAGE = "The epoch part of a PEP440 compliant version number cannot be negative";
  private static final String NO_PEP440_VERSION_PARSED_ERROR_MESSAGE = "No correct PEP440 compliant version has been found in the string: %s";

  //==== Attributes ====//

  private boolean hasEpoch = false; // Caches `epoch >= 0`, whether or not this version has an epoch or not.
  private long epoch = NO_EPOCH_VALUE; // Defaults to no epoch.

  @NotNull
  private ReleaseSegment releaseSegment;

  @Nullable
  private PreReleaseSegment preReleaseSegment;
  /**
   * Caches `preReleaseSegment != null`, whether or not this version is a pre-release or not.
   */
  private boolean isPreRelease;

  @Nullable
  private ComparableVersionSegment postReleaseSegment;
  /**
   * Caches `postReleaseSegment != null`, whether or not this version is a post-release or not.
   */
  private boolean isPostRelease;

  @Nullable
  private ComparableVersionSegment developmentalReleaseSegment;
  /**
   * Caches `developmentalReleaseSegment != null`, whether or not this version is a developmental release or not.
   */
  private boolean isDevelopmentalRelease;

  //==== Getters and Setters ====//

  public long getEpoch() {
//    if (!hasEpoch()) {
//      throw new NonExistingSegmentException("No epoch is set for this version.");
//    }
    return epoch;
  }

  // TODO: 4/10/18 Use a factory method for easier creation.
  public PEP440Version(final long epoch, @NotNull final ReleaseSegment releaseSegment, @Nullable final PreReleaseSegment preReleaseSegment, @Nullable final ComparableVersionSegment postReleaseSegment, @Nullable final ComparableVersionSegment developmentalReleaseSegment) throws IllegalArgumentException {
    if (epoch < 0 && epoch != NO_EPOCH_VALUE) {
      throw new IllegalArgumentException(NEGATIVE_EPOCH_ERROR_MESSAGE);
    }

    setEpoch(epoch);
    setReleaseSegment(releaseSegment);
    setPreReleaseSegment(preReleaseSegment);
    setPostReleaseSegment(postReleaseSegment);
    setDevelopmentalReleaseSegment(developmentalReleaseSegment);
  }

  private PEP440Version(final PEP440Version other, final long epoch, @Nullable final ReleaseSegment releaseSegment, final boolean changePreReleaseSegment, @Nullable final PreReleaseSegment preReleaseSegment, final boolean changePostReleaseSegment, @Nullable final ComparableVersionSegment postReleaseSegment, final boolean changeDevelopmentalSegment, @Nullable final ComparableVersionSegment developmentalReleaseSegment) {

    this(other);
    setEpoch(epoch);

    if (releaseSegment != null) {
      setReleaseSegment(releaseSegment);
    }

    if (changePreReleaseSegment) {
      setPreReleaseSegment(preReleaseSegment);
    }

    if (changePostReleaseSegment) {
      setPostReleaseSegment(postReleaseSegment);
    }

    if (changeDevelopmentalSegment) {
      setDevelopmentalReleaseSegment(developmentalReleaseSegment);
    }
  }

  /**
   * Copy constructor.
   *
   * @param other The object to copy.
   */
  public PEP440Version(final PEP440Version other) {
    this.hasEpoch = other.hasEpoch;
    this.epoch = other.epoch;
    this.releaseSegment = other.releaseSegment;
    this.preReleaseSegment = other.preReleaseSegment;
    this.isPreRelease = other.isPreRelease;
    this.postReleaseSegment = other.postReleaseSegment;
    this.isPostRelease = other.isPostRelease;
    this.developmentalReleaseSegment = other.developmentalReleaseSegment;
    this.isDevelopmentalRelease = other.isDevelopmentalRelease;
  }

  private static int compareDevelopmentalSegments(@NotNull final PEP440Version pep440Version0, @NotNull final PEP440Version pep440Version1) {
    if (pep440Version0.isDevelopmentalRelease()) {
      if (pep440Version1.isDevelopmentalRelease()) {
        // Both are developmental versions

        //noinspection ConstantConditions - It cannot be null as we tested isDevelopmentalRelease() on both.
        return pep440Version0.developmentalReleaseSegment.compareTo(pep440Version1.developmentalReleaseSegment);
      }

      // `o` isn't a developmental release.

      return -1;
    }

    // `this` isn't a developmental release.

    if (pep440Version1.isDevelopmentalRelease()) {
      return 1;
    }

    // `o` isn't a developmental release.

    // => They are equal (local versions not tested yet).
    return 0;
  }

  private static int comparePostReleaseAndDevelopmentalSegments(@NotNull final PEP440Version pep440Version0, @NotNull final PEP440Version pep440Version1) {
    final int result;
    if (pep440Version0.isPostRelease()) {
      if (pep440Version1.isPostRelease()) {
        // Both are post-release versions

        //noinspection ConstantConditions - It cannot be null as we tested isPostRelease() on both.
        result = pep440Version0.postReleaseSegment.compareTo(pep440Version1.postReleaseSegment);
        if (result != 0) {
          return result;
        }

        // Both are from the same post-release version.

        return compareDevelopmentalSegments(pep440Version0, pep440Version1);
      }

      // `pep440Version1` isn't a post-release version.
      return 1; // This is true even if any of both is a developmental release.
    }

    // `pep440Version0` isn't a post-release version.

    if (pep440Version1.isPostRelease()) {
      return -1;
    }

    // Both aren't post-release versions.

    return compareDevelopmentalSegments(pep440Version0, pep440Version1);
  }

  //==== Constructors ====//

  public static int compare(@NotNull final PEP440Version pep440Version0, @NotNull final PEP440Version pep440Version1) {
    int result = Long.compare(pep440Version0.getEpoch(), pep440Version1.getEpoch());
    if (result != 0) {
      // The epochs are different, return their comparison.
      return result;
    }

    // Both have the same epoch segment.

    result = pep440Version0.releaseSegment.compareTo(pep440Version1.releaseSegment);
    if (result != 0) {
      // The release segments are different, return their comparison.
      return result;
    }

    // Both have the same release segment.

    if (pep440Version0.isPreRelease()) {
      if (pep440Version1.isPreRelease()) {
        // Both are pre-release versions

        //noinspection ConstantConditions - It cannot be null as we tested isPreRelease() on both.
        result = pep440Version0.preReleaseSegment.compareTo(pep440Version1.preReleaseSegment);
        if (result != 0) {
          // The pre-release segments are different, return the comparison.
          return result;
        }

        // Same pre-release version

        return comparePostReleaseAndDevelopmentalSegments(pep440Version0, pep440Version1);
      }

      // `pep440Version1` isn't a pre-release version.

      if (pep440Version1.isDevelopmentalRelease() && !pep440Version1.isPostRelease()) {
        return 1; // Because non-pre-release developmental versions come before pre-release versions in `PEP440` (if the release segment is the same).
      }

      // `pep440Version1` isn't a developmental version.

      return -1;
    }

    // `pep440Version0` isn't a pre-release version.

    if (pep440Version1.isPreRelease()) {
      if (pep440Version0.isDevelopmentalRelease() && !pep440Version0.isPostRelease()) {
        return -1;
      }

      // `pep440Version0` is neither a developmental version nor a post-release version.

      return 1;
    }

    // Both aren't pre-release versions.

    return comparePostReleaseAndDevelopmentalSegments(pep440Version0, pep440Version1);
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  @NotNull
  protected static PEP440Version parsePEP440VersionWithPattern(@NotNull final String versionString,
                                                               @NotNull final Pattern pattern) throws NoMatchFoundException {
    final Matcher matcher = pattern.matcher(versionString);

    if (matcher.find()) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O || Build.VERSION.SDK_INT == 0) { // The second check is for UnitTests to pass here.
        long epoch = NO_EPOCH_VALUE;
        final ReleaseSegment releaseSegment;
        PreReleaseSegment preReleaseSegment = null;
        ComparableVersionSegment postReleaseSegment = null;
        ComparableVersionSegment developmentalReleaseSegment = null;

        // Release segment (first to throw as soon as possible)
        try {
          releaseSegment = ReleaseSegment.fromString(matcher.group(RELEASE_REGEX_GROUP));
        }
        catch (final ReleaseSegmentParsingException e) {
          throw new NoMatchFoundException(String.format(Locale.US, NO_PEP440_VERSION_PARSED_ERROR_MESSAGE, versionString), e);
        }

        // Epoch
        try {
          epoch = Long.parseLong(matcher.group(EPOCH_REGEX_GROUP)); // TODO: 11/10/18 Check if this needs a try/catch.
        }
        catch (final NumberFormatException ignored) {
        }

        // Pre-release segment
        try {
          preReleaseSegment = PreReleaseSegment.fromString(matcher.group(PRE_RELEASE_LABEL_REGEX_GROUP), matcher.group(PRE_RELEASE_NUMBER_REGEX_GROUP));
        }
        catch (final VersionParsingException ignored) {
          // We don't need to do more, the constructor will handle null segments.
        }

        // Post-release segment
        try {
          postReleaseSegment = ComparableVersionSegment.fromString(matcher.group(POST_RELEASE_NUMBER_REGEX_GROUPS[0]));
        }
        catch (final NonExistingSegmentException e) {
          try {
            postReleaseSegment = ComparableVersionSegment.fromString(matcher.group(POST_RELEASE_NUMBER_REGEX_GROUPS[1]));
          }
          catch (final NonExistingSegmentException ignored) {
          }
        }

        // Developmental release segment
        try {
          developmentalReleaseSegment = ComparableVersionSegment.fromString(matcher.group(DEVELOPMENTAL_RELEASE_NUMBER_REGEX_GROUP));
        }
        catch (final NonExistingSegmentException ignored) {
        }

        return new PEP440Version(epoch,
            releaseSegment,
            preReleaseSegment,
            postReleaseSegment,
            developmentalReleaseSegment
        );
      }
      else {
        // TODO: 6/10/18 Parse the regex without being able to get the groups by their name... Also check how to handle it with unit tests.
      }
    }

    throw new NoMatchFoundException(String.format(Locale.US, NO_PEP440_VERSION_PARSED_ERROR_MESSAGE, versionString));
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  public static PEP440Version fromVersionString(@NotNull final String versionString) throws NoMatchFoundException {
    return parsePEP440VersionWithPattern(versionString, PARSING_PATTERN);
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  public static PEP440Version fromString(@NotNull final String string) throws NoMatchFoundException {
    return fromVersionString(string);
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  @RequiresApi(api = Build.VERSION_CODES.O)
  public static PEP440Version parsePEP440Version(@NotNull final String string) throws NoMatchFoundException {
    return fromVersionString(string);
  }

  /**
   * Set the epoch if there is one.
   * This is not thread-safe and should only be used by a constructor.
   *
   * @param epoch The epoch to set. `NO_EPOCH_VALUE` (-1L) if there's none.
   */
  private void setEpoch(final long epoch) {
    if (epoch == NO_EPOCH_VALUE) {
      this.epoch = NO_EPOCH_VALUE;
      this.hasEpoch = false;
    }
    else if (epoch >= EPOCH_MIN) {
      this.epoch = epoch;
      this.hasEpoch = true;
    }
  }

  /**
   * Set the release version number.
   * This is not thread-safe and should only be used by a constructor.
   *
   * @param releaseSegment The list representing the release segment. Has to contain at least one element.
   */
  private void setReleaseSegment(@NotNull final ReleaseSegment releaseSegment) {
    this.releaseSegment = releaseSegment;
  }

  //==== Object Methods ====//

  public boolean hasEpoch() {
    return hasEpoch;
  }

//  public PEP440Version removeEpoch() {
//    return new PEP440Version(this, NO_EPOCH_VALUE, releaseSegment, false, null, false, null, false, null);
//  }

  @Override
  public int getReleaseState() {
    if (!isPreRelease()) {
      return FINAL;
    }

    return preReleaseSegment.getStage();
  }

  @Override
  public boolean isPreRelease() {
    return isPreRelease;
  }

  @Override
  public boolean isPostRelease() {
    return isPostRelease;
  }

  @Override
  public boolean isDevelopmentalRelease() {
    return isDevelopmentalRelease;
  }

  /**
   * Set the pre-release segment if it is one.
   * This is not thread-safe and should only be used by a constructor.
   *
   * @param preReleaseSegment The pre-release segment to set.
   */
  private void setPreReleaseSegment(@Nullable final PreReleaseSegment preReleaseSegment) {
    if (preReleaseSegment == null) {
      this.preReleaseSegment = null;
      this.isPreRelease = false;
    }
    else {
      this.preReleaseSegment = new PreReleaseSegment(preReleaseSegment);
      this.isPreRelease = true;
    }
  }

  /**
   * Set the post-release version if it is one.
   * This is not thread-safe and should only be used by a constructor.
   *
   * @param postReleaseSegment The post-release segment to set.
   */
  private void setPostReleaseSegment(@Nullable final ComparableVersionSegment postReleaseSegment) {
    if (postReleaseSegment == null) {
      this.postReleaseSegment = null;
      this.isPostRelease = false;
    }
    else {
      this.postReleaseSegment = new ComparableVersionSegment(postReleaseSegment);
      this.isPostRelease = true;
    }
  }

  /**
   * Set the developmental version if it is one.
   * This is not thread-safe and should only be used by a constructor.
   *
   * @param developmentalReleaseSegment The developmental release segment to set.
   */
  private void setDevelopmentalReleaseSegment(@Nullable final ComparableVersionSegment developmentalReleaseSegment) {
    if (developmentalReleaseSegment == null) {
      this.developmentalReleaseSegment = null;
      this.isDevelopmentalRelease = false;
    }
    else {
      this.developmentalReleaseSegment = new ComparableVersionSegment(developmentalReleaseSegment);
      this.isDevelopmentalRelease = true;
    }
  }

  @Override
  public int compareTo(@NotNull final PEP440Version o) {
    return compare(this, o);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final PEP440Version that = (PEP440Version) o;

    if (hasEpoch != that.hasEpoch) {
      return false;
    }
    if (getEpoch() != that.getEpoch()) {
      return false;
    }
    if (isPreRelease() != that.isPreRelease()) {
      return false;
    }
    if (isPostRelease() != that.isPostRelease()) {
      return false;
    }
    if (isDevelopmentalRelease() != that.isDevelopmentalRelease()) {
      return false;
    }
    if (!releaseSegment.equals(that.releaseSegment)) {
      return false;
    }
    if (preReleaseSegment != null ? !preReleaseSegment.equals(that.preReleaseSegment) : that.preReleaseSegment != null) {
      return false;
    }
    if (postReleaseSegment != null ? !postReleaseSegment.equals(that.postReleaseSegment) : that.postReleaseSegment != null) {
      return false;
    }
    return developmentalReleaseSegment != null ? developmentalReleaseSegment.equals(that.developmentalReleaseSegment) : that.developmentalReleaseSegment == null;
  }

  @Override
  public int hashCode() {
    int result = (hasEpoch ? 1 : 0);
    result = 31 * result + (int) (getEpoch() ^ (getEpoch() >>> 32));
    result = 31 * result + releaseSegment.hashCode();
    result = 31 * result + (preReleaseSegment != null ? preReleaseSegment.hashCode() : 0);
    result = 31 * result + (isPreRelease() ? 1 : 0);
    result = 31 * result + (postReleaseSegment != null ? postReleaseSegment.hashCode() : 0);
    result = 31 * result + (isPostRelease() ? 1 : 0);
    result = 31 * result + (developmentalReleaseSegment != null ? developmentalReleaseSegment.hashCode() : 0);
    result = 31 * result + (isDevelopmentalRelease() ? 1 : 0);
    return result;
  }

  @Override
  public String toString() {
    return "PEP440Version{" +
        "hasEpoch=" + hasEpoch +
        ", epoch=" + epoch +
        ", releaseSegment=" + releaseSegment +
        ", preReleaseSegment=" + preReleaseSegment +
        ", isPreRelease=" + isPreRelease +
        ", postReleaseSegment=" + postReleaseSegment +
        ", isPostRelease=" + isPostRelease +
        ", developmentalReleaseSegment=" + developmentalReleaseSegment +
        ", isDevelopmentalRelease=" + isDevelopmentalRelease +
        '}';
  }
}
