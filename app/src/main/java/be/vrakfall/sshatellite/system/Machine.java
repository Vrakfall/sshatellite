package be.vrakfall.sshatellite.system;

import be.vrakfall.sshatellite.Parameters;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import java.util.concurrent.atomic.AtomicLong;

@ToString
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
public class Machine {
  //==== Static variables ====//

  public static final String TAG = Machine.class.getSimpleName();

  public static final long ARCHITECTURE_CACHE_REFRESH_TIME_IN_MILLISECONDS =
      Parameters.SLOWLY_CHANGING_VALUE_CACHE_REFRESH_TIME_IN_MILLISECONDS;

  public static final long ARCHITECTURE_CACHE_EXPIRATION_TIME_IN_MILLISECONDS =
      Parameters.SLOWLY_CHANGING_VALUE_CACHE_EXPIRATION_TIME_IN_MILLISECONDS;

  public static final AtomicLong nextId = new AtomicLong(0);

  //==== Attributes ====//

  private long id;

  //==== Constructors ====//

  protected Machine(@Nullable final Long id) {
    if (id == null) {
      initializeId();
    }
    else {
      this.id = id;
    }
  }

  protected Machine(@NotNull final Machine other) {
    this.id = other.id;
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  private void initializeId() {
    this.id = nextId.getAndIncrement();
  }
}
