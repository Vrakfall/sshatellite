package be.vrakfall.sshatellite.system.commands.python;

import android.os.Build;
import android.util.Log;
import androidx.annotation.RequiresApi;
import be.vrakfall.sshatellite.network.errors.ActionOnNotAuthenticatedConnectionException;
import be.vrakfall.sshatellite.system.Machine;
import be.vrakfall.sshatellite.system.commands.ExecutionHandler;
import be.vrakfall.sshatellite.system.commands.ShellEnvironment;
import be.vrakfall.sshatellite.system.commands.python.version.PEP440Version;
import be.vrakfall.sshatellite.system.monitoring.state.variables.ExecutablePathEntry;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.ordering.ExtendedComparable;
import be.vrakfall.sshatellite.utils.ordering.NumberedComparableObject;
import be.vrakfall.sshatellite.utils.ordering.NumberedComparableObjectComparedByObjectFirst;
import be.vrakfall.sshatellite.utils.ordering.NumberedObject;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Maybe;
import io.reactivex.schedulers.Schedulers;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.val;
import net.schmizz.sshj.connection.ConnectionException;
import net.schmizz.sshj.transport.TransportException;
import org.jetbrains.annotations.Contract;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

@Value
@EqualsAndHashCode(callSuper = true)
public class HighestVersionShellPythonEnvironment extends DynamicShellPythonEnvironment {
  //==== Static variables ====//

  public static final String TAG = HighestVersionShellPythonEnvironment.class.getSimpleName();

  private static final LoadingCache<HighestVersionShellPythonEnvironment, StaticShellPythonEnvironment>
      staticEnvironmentCache = Caffeine.newBuilder()
      .refreshAfterWrite(Machine.ARCHITECTURE_CACHE_REFRESH_TIME_IN_MILLISECONDS, TimeUnit.SECONDS)
      .expireAfterWrite(Machine.ARCHITECTURE_CACHE_EXPIRATION_TIME_IN_MILLISECONDS, TimeUnit.SECONDS)
      .build(HighestVersionShellPythonEnvironment::findStaticShellPythonEnvironmentWithHighestVersion);

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Constructors ====//

  @Builder
  private HighestVersionShellPythonEnvironment(final ShellEnvironment shellEnvironment) {
    super(shellEnvironment);
  }

  //==== Getters and Setters ====//

  @Nullable
  private StaticShellPythonEnvironment getStaticShellPythonEnvironment() {
    return staticEnvironmentCache.get(this);
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  @Contract(pure = true)
  // TODO: 3/11/18 Check the following
  @Nullable
  @Override
  protected ExecutablePathEntry getExecutablePathEntry() {
    final val staticShellPythonEnvironment = getStaticShellPythonEnvironment();
    if (staticShellPythonEnvironment == null) {
      return null;
    }
    return staticShellPythonEnvironment.getExecutablePathEntry();
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  @Contract(pure = true)
  @Nullable
  @Override
  public PEP440Version getVersion() {
    final val staticShellPythonEnvironment = getStaticShellPythonEnvironment();
    if (staticShellPythonEnvironment == null) {
      return null;
    }
    return staticShellPythonEnvironment.getVersion();
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @RequiresApi(api = Build.VERSION_CODES.O)
  private StaticShellPythonEnvironment findStaticShellPythonEnvironmentWithHighestVersion() {
    try {
      return findStaticShellPythonEnvironmentWithHighestVersionRx().blockingGet();
    }
    catch (final Exception e) {
      Log.e(TAG, "findStaticShellPythonEnvironmentWithHighestVersion: ", e);
    }
    return null;
    // TODO: 7/11/18 Urgent: redo this
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  private Maybe<StaticShellPythonEnvironment> findStaticShellPythonEnvironmentWithHighestVersionRx() {
    @NotNull final ShellEnvironment shellEnvironment = getShellEnvironment();
    return getNumberedPathEntriesRx()
        .toFlowable(BackpressureStrategy.BUFFER)
        // TODO: 23/10/18 Make the following linked to the amount of cpus on the server.
        .parallel(32)
        .runOn(Schedulers.io())
        .map((NumberedObject<ExecutablePathEntry> entry) -> {
          val staticShellPythonEnvironment = StaticShellPythonEnvironment.builder()
              .shellEnvironment(shellEnvironment)
              .executablePathEntry(entry.getObject()).build();
          return NumberedComparableObjectComparedByObjectFirst.<StaticShellPythonEnvironment>builder()
              .number(entry.getNumber())
              .object(staticShellPythonEnvironment).ascendingNumberOrder(true).build();
        })
        .filter(environment -> environment.getObject().getVersion() != null)
        .sequential()
        .reduce(ExtendedComparable::max)
        .map(NumberedComparableObject::getObject);
  }

  @Override
  public ExecutionHandler execute(final String command) throws IOException {
    return null;
  }

  @Override
  public InputStream getInputStreamTest(final String command) throws ActionOnNotAuthenticatedConnectionException,
      ConnectionException, TransportException {
    return null;
    // TODO: 31/10/18 Implement this
  }

  @Override
  public void stopExecution(@Nullable final Closeable execution) throws ConnectionException, TransportException {
    // TODO: 31/10/18 Implement this
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  @Override
  @NotNull
  public String toString() {
    return "HighestVersionShellPythonEnvironment{" +
        "executablePathEntry='" + getExecutablePathEntry() + '\'' +
        ", version='" + getVersion() + '\'' +
        '}';
  }
}
