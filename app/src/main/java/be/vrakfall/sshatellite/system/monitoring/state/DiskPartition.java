package be.vrakfall.sshatellite.system.monitoring.state;

import be.vrakfall.sshatellite.Parameters;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DiskPartition {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  private String path;

  private long size;

  private long usedBytes;

  private long freeBytes;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  public double userUsedPercentage() {
    return getUsedBytes() * Parameters.PERCENTAGE_MAX * 1D / size;
  }
}
