package be.vrakfall.sshatellite.system.commands;

import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;

import java.io.InputStream;

public class CommandResponse {
  //==== Static variables ====//

  //==== Attributes ====//

  public final int exitStatus;
  public final InputStream stdout;
  public final InputStream errorStream;
  public final String exitErrorMessage;
  public Boolean exitWasCoreDumped;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public CommandResponse(@NonNull final int exitStatus, @NonNull final InputStream stdout, @NonNull final InputStream errorStream, @NonNull final String exitErrorMessage, @Nullable final Boolean exitWasCoreDumped) {
    this.exitStatus = exitStatus;
    this.stdout = stdout;
    this.errorStream = errorStream;
    this.exitErrorMessage = exitErrorMessage;
    this.exitWasCoreDumped = exitWasCoreDumped;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  /**
   * `equals` auto-generated by IntelliJ.
   *
   * @param o
   * @return
   */
  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final CommandResponse that = (CommandResponse) o;

    if (exitStatus != that.exitStatus) {
      return false;
    }
    if (!stdout.equals(that.stdout)) {
      return false;
    }
    if (!errorStream.equals(that.errorStream)) {
      return false;
    }
    if (!exitErrorMessage.equals(that.exitErrorMessage)) {
      return false;
    }
    return exitWasCoreDumped != null ? exitWasCoreDumped.equals(that.exitWasCoreDumped) : that.exitWasCoreDumped == null;
  }

  /**
   * `hashCode` auto-generated by IntelliJ.
   *
   * @return
   */
  @Override
  public int hashCode() {
    int result = exitStatus;
    result = 31 * result + stdout.hashCode();
    result = 31 * result + errorStream.hashCode();
    result = 31 * result + exitErrorMessage.hashCode();
    result = 31 * result + (exitWasCoreDumped != null ? exitWasCoreDumped.hashCode() : 0);
    return result;
  }
}
