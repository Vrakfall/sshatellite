package be.vrakfall.sshatellite.system.commands;

import java.io.InputStream;

public class SynchronousCommandResponse {
  //==== Static variables ====//

  //==== Attributes ====//

  public final int exitStatus;
  public final String stdout;
  public final InputStream errorStream;
  public final String exitErrorMessage;
  public final Boolean exitWasCoreDumped;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public SynchronousCommandResponse(final int exitStatus, final String stdout, final InputStream errorStream, final String exitErrorMessage, final Boolean exitWasCoreDumped) {
    this.exitStatus = exitStatus;
    this.stdout = stdout;
    this.errorStream = errorStream;
    this.exitErrorMessage = exitErrorMessage;
    this.exitWasCoreDumped = exitWasCoreDumped;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final SynchronousCommandResponse that = (SynchronousCommandResponse) o;

    if (exitStatus != that.exitStatus) {
      return false;
    }
    if (stdout != null ? !stdout.equals(that.stdout) : that.stdout != null) {
      return false;
    }
    if (errorStream != null ? !errorStream.equals(that.errorStream) : that.errorStream != null) {
      return false;
    }
    if (exitErrorMessage != null ? !exitErrorMessage.equals(that.exitErrorMessage) : that.exitErrorMessage != null) {
      return false;
    }
    return exitWasCoreDumped != null ? exitWasCoreDumped.equals(that.exitWasCoreDumped) : that.exitWasCoreDumped == null;
  }

  @Override
  public int hashCode() {
    int result = exitStatus;
    result = 31 * result + (stdout != null ? stdout.hashCode() : 0);
    result = 31 * result + (errorStream != null ? errorStream.hashCode() : 0);
    result = 31 * result + (exitErrorMessage != null ? exitErrorMessage.hashCode() : 0);
    result = 31 * result + (exitWasCoreDumped != null ? exitWasCoreDumped.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "SynchronousCommandResponse{" +
        "exitStatus=" + exitStatus +
        ", stdout='" + stdout + '\'' +
        ", errorStream=" + errorStream +
        ", exitErrorMessage='" + exitErrorMessage + '\'' +
        ", exitWasCoreDumped=" + exitWasCoreDumped +
        '}';
  }
}
