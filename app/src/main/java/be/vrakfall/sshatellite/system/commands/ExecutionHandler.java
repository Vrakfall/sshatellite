package be.vrakfall.sshatellite.system.commands;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import net.schmizz.sshj.connection.ConnectionException;
import net.schmizz.sshj.transport.TransportException;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

@Data
public abstract class ExecutionHandler implements Closeable {
  //==== Static variables ====//

  public static final String TAG = ExecutionHandler.class.getSimpleName();

  //==== Attributes ====//

  @NonNull
  @Getter(AccessLevel.NONE)
  private final Commandable commandable;

  @NonNull
  @Getter(AccessLevel.PROTECTED)
  private final Closeable execution;

  //==== Constructors ====//

  public ExecutionHandler(@NonNull final Commandable commandable, @NonNull final Closeable execution) {
    this.commandable = commandable;
    this.execution = execution;
  }

  //==== Getters and Setters ====//

  //==== > Abstract ====//

  public abstract InputStream getOutput();

  public abstract InputStream getErrorOutput();

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @Override
  public void close() throws ConnectionException, TransportException {
    // Ask the commandable to stop the execution, it has to close it as well. This allows the commandable to handle
    // side-effects like removing the execution from possible lists.
    commandable.stopExecution(execution);
  }

  public abstract int waitExecutionAndGetExitStatus() throws IOException;
}
