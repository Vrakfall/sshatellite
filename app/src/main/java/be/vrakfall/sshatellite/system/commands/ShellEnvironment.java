package be.vrakfall.sshatellite.system.commands;

import be.vrakfall.sshatellite.system.commands.python.PythonEnvironment;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;

import java.io.IOException;

public interface ShellEnvironment extends Commandable {
  //==== Static variables ====//

  String CREATE_DIRECTORY_COMMAND = "mkdir";

  //==== > Error messages ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  void createDirectory(@NotNull final String directory) throws IOException;

  boolean hasAPythonEnvironment() throws IOException;

  // TODO: 5/12/18 Check if this is needed
  @Nullable
  PythonEnvironment getPythonEnvironmentFrom(String path) throws IOException;

  @Nullable
  PythonEnvironment getHighestVersionPythonEnvironment() throws IOException;
}
