package be.vrakfall.sshatellite.system.commands.python.version;

import be.vrakfall.sshatellite.system.commands.python.version.errors.ReleaseSegmentParsingException;
import be.vrakfall.sshatellite.utils.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class ReleaseSegment implements Comparable<ReleaseSegment> {
  //==== Static variables ====//

  public static final int MAJOR_VERSION_LEVEL = 0;
  public static final int MINOR_VERSION_LEVEL = 1;
  public static final int PATCH_VERSION_LEVEL = 2;

  public static final String DEFAULT_LEVEL_SEPARATOR = "\\.";

  //==== > Error messages ======//

  private static final String NO_RELEASE_SEGMENT_FOUND = "No correct release segment found in the string: %s - Separator used: %s";

  //==== Attributes ====//

  @NotNull
  private List<Long> subVersions;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public ReleaseSegment(Long... subVersions) {
    if (subVersions.length < 1) {
      throw new IllegalArgumentException("The release segment of a version needs to have at least one element.");
    }

    this.subVersions = new ArrayList<>(subVersions.length);

    Collections.addAll(this.subVersions, subVersions);
  }

  //==== Lists' CRUDs ====//

  //====== subVersions ======//

  /**
   * Returns the sub-version number at a specific level.
   *
   * @param level The level of the sub-version wanted. E.g.: 0 for the major version number and 1 for the minor version number (#Opinionated).
   * @return The sub-version number.
   */
  public long getSubVersion(int level) {
    return subVersions.get(level);
  }

  /**
   * Alias of `getSubVersion(0)` that helps to get the major version number. #Opinionated
   * E.g.: In `1.33.7`, it corresponds to the number 1.
   *
   * @return The major version number.
   */
  public long getMajorVersion() {
    return subVersions.get(MAJOR_VERSION_LEVEL);
  }

  /**
   * Alias of `getSubVersion(1)` that helps to get the minor version number. #Opinionated
   * E.g.: In `1.33.7`, it corresponds to the number 33.
   *
   * @return The minor version number.
   */
  public long getMinorVersion() {
    return subVersions.get(MINOR_VERSION_LEVEL);
  }

  /**
   * Alias of `getSubVersion(2)` that helps to get the patch version number. #Opinionated
   * E.g.: In `1.33.7`, it corresponds to the number 7.
   *
   * @return The patch version number.
   */
  public long getPatchVersion() {
    return subVersions.get(PATCH_VERSION_LEVEL);
  }

  //==== Static Methods ====//

  public static ReleaseSegment fromString(String string, String levelSeparator) throws ReleaseSegmentParsingException {
    String[] levelStrings = string.split(levelSeparator);
    Long[] levels = new Long[levelStrings.length];

    try {
      for (int i = 0; i < levelStrings.length; i++) {
        levels[i] = Long.parseLong(levelStrings[i]);
      }

      return new ReleaseSegment(levels);
    }
    catch (NumberFormatException e) {
      throw new ReleaseSegmentParsingException(String.format(Locale.US, NO_RELEASE_SEGMENT_FOUND, string, levelSeparator), e);
    }
  }

  public static ReleaseSegment fromString(String string) throws ReleaseSegmentParsingException {
    return fromString(string, DEFAULT_LEVEL_SEPARATOR);
  }

  //==== Object Methods ====//

  public @NotNull
  String toSimpleString(@NotNull String levelSeparator) {
    if (subVersions.size() <= 0) {
      throw new IllegalStateException();
    }

    StringBuilder stringBuilder = new StringBuilder();
    boolean firstElement = true;

    for (Long subVersion :
        subVersions) {
      if (firstElement) {
        stringBuilder.append(subVersion);
        firstElement = false;
      }
      else {
        stringBuilder.append(levelSeparator).append(subVersion);
      }
    }

    return stringBuilder.toString();
  }

  public @NotNull
  String toSimpleString() {
    return toSimpleString(DEFAULT_LEVEL_SEPARATOR);
  }

  @Override
  public int compareTo(@NotNull ReleaseSegment o) {
    int smallerSize = Math.min(subVersions.size(), o.subVersions.size());

    for (int i = 0; i < smallerSize; i++) {
      int comparison = Long.compare(subVersions.get(i), o.subVersions.get(i));

      if (comparison != 0) {
        return comparison;
      }
    }

    return Integer.compare(subVersions.size(), o.subVersions.size());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ReleaseSegment that = (ReleaseSegment) o;

    return subVersions.equals(that.subVersions);
  }

  @Override
  public int hashCode() {
    return subVersions.hashCode();
  }

  @Override
  public String toString() {
    return "ReleaseSegment{" +
        "subVersions=" + subVersions +
        '}';
  }
}
