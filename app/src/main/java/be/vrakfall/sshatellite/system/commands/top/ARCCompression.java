package be.vrakfall.sshatellite.system.commands.top;


import be.vrakfall.sshatellite.utils.annotations.NotNull;

public class ARCCompression {
  //==== Static variables ====//

  //==== Attributes ====//

  @NotNull
  public final ByteAmount compressed;
  @NotNull
  public final ByteAmount uncompressed;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public ARCCompression(@NotNull ByteAmount compressed, @NotNull ByteAmount uncompressed) {
    this.compressed = compressed;
    this.uncompressed = uncompressed;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ARCCompression that = (ARCCompression) o;

    if (!compressed.equals(that.compressed)) return false;
    return uncompressed.equals(that.uncompressed);
  }

  @Override
  public int hashCode() {
    int result = compressed.hashCode();
    result = 31 * result + uncompressed.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "ARCCompression{" +
        "compressed=" + compressed +
        ", uncompressed=" + uncompressed +
        '}';
  }
}
