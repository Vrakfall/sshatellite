package be.vrakfall.sshatellite.system.commands.python.version;

import be.vrakfall.sshatellite.system.commands.python.version.errors.NonExistingSegmentException;
import be.vrakfall.sshatellite.utils.annotations.NotNull;

public class ComparableVersionSegment extends VersionSegment implements Comparable<ComparableVersionSegment> {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public ComparableVersionSegment(long version) {
    super(version);
  }

  public ComparableVersionSegment(@NotNull ComparableVersionSegment other) {
    super(other);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  public static ComparableVersionSegment fromString(@NotNull String string) throws NonExistingSegmentException {
    return new ComparableVersionSegment(parseVersionNumber(string));
  }

  //====== Object Methods ======//

  @Override
  public int compareTo(@NotNull ComparableVersionSegment o) {
    return Long.compare(this.getVersion(), o.getVersion());
  }
}
