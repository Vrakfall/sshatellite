package be.vrakfall.sshatellite.system.commands.python.errors;

import android.annotation.TargetApi;
import android.os.Build;
import androidx.annotation.RequiresApi;

public class PipCannotInstallException extends Exception {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Constructors ====//

  public PipCannotInstallException() {
  }

  public PipCannotInstallException(String message) {
    super(message);
  }

  public PipCannotInstallException(String message, Throwable cause) {
    super(message, cause);
  }

  public PipCannotInstallException(Throwable cause) {
    super(cause);
  }

  @TargetApi(Build.VERSION_CODES.N)
  @RequiresApi(Build.VERSION_CODES.N)
  public PipCannotInstallException(String message, Throwable cause, boolean enableSuppression,
                                   boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
