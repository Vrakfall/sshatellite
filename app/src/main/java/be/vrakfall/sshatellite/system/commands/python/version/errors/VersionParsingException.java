package be.vrakfall.sshatellite.system.commands.python.version.errors;

import be.vrakfall.sshatellite.utils.io.errors.ParsingException;

public abstract class VersionParsingException extends ParsingException {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public VersionParsingException() {
  }

  public VersionParsingException(String message) {
    super(message);
  }

  public VersionParsingException(String message, Throwable cause) {
    super(message, cause);
  }

  public VersionParsingException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
