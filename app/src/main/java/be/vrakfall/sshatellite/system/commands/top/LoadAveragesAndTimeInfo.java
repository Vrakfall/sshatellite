package be.vrakfall.sshatellite.system.commands.top;

import be.vrakfall.sshatellite.system.UnixConventions;
import lombok.Data;
import org.joda.time.Duration;
import org.joda.time.LocalTime;

@Data
public class LoadAveragesAndTimeInfo {
  //==== Static variables ====//

  //==== Attributes ====//

  public final float[] loadAverages = new float[UnixConventions.LOAD_AVERAGES_AMOUNT];
  public final Duration upTime;
  public final LocalTime localTime;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public LoadAveragesAndTimeInfo(final float loadAverage0, final float loadAverage1, final float loadAverage2, final Duration upTime, final LocalTime localTime) {
    this.loadAverages[0] = loadAverage0;
    this.loadAverages[1] = loadAverage1;
    this.loadAverages[2] = loadAverage2;
    this.upTime = upTime;
    this.localTime = localTime;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
