package be.vrakfall.sshatellite.system.commands.top;

// TODO: 18/09/18 Make a much better representation of a byteSize/Amount

/**
 * Represents a byte amount output by the command {@code top} because we don't know how to represent it better (yet).
 */
public class ByteAmount {
  //==== Static variables ====//

  //==== Attributes ====//

  private long quantity;
  private char unity;

  //==== Getters and Setters ====//

  public ByteAmount(long quantity, char unity) {
    setQuantity(quantity);
    setUnity(unity);
  }

  /**
   * Creates a new ByteAmount equals to 0B.
   */
  public ByteAmount() {
    nullify();
  }

  public long getQuantity() {
    return quantity;
  }

  public void setQuantity(long quantity) {
    this.quantity = quantity;
  }

  //==== Constructors ====//

  public char getUnity() {
    return unity;
  }

  public void setUnity(char unity) {
    this.unity = unity;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  public void nullify() {
    quantity = 0L;
    unity = 'B';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ByteAmount that = (ByteAmount) o;

    if (quantity != that.quantity) return false;
    return unity == that.unity;
  }

  @Override
  public int hashCode() {
    int result = (int) (quantity ^ (quantity >>> 32));
    result = 31 * result + (int) unity;
    return result;
  }

  @Override
  public String toString() {
    return "ByteAmount{" +
        "quantity=" + quantity +
        ", unity=" + unity +
        '}';
  }
}
