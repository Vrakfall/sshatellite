package be.vrakfall.sshatellite.system.monitoring.state.variables.errors;

import be.vrakfall.sshatellite.utils.io.errors.ParsingException;

public class NoExecutablePathEntryException extends ParsingException {
  private static final long serialVersionUID = 4677778763147783695L;
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public NoExecutablePathEntryException() {
    super();
  }

  public NoExecutablePathEntryException(final String message) {
    super(message);
  }

  public NoExecutablePathEntryException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public NoExecutablePathEntryException(final Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
