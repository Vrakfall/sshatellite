package be.vrakfall.sshatellite.system.commands.top;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BSDCPUUsage extends CPUUsage {
  //==== Static variables ====//

  //==== Attributes ====//

  public final float INTERRUPTS;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public BSDCPUUsage(int NUMBER, float USER, float NICE, float SYSTEM, float INTERRUPTS, float IDLE) {
    super(NUMBER, USER, NICE, SYSTEM, IDLE);
    this.INTERRUPTS = INTERRUPTS;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
