package be.vrakfall.sshatellite.system.commands.top;

import be.vrakfall.sshatellite.utils.annotations.NotNull;

public class ARCUsage {
  //==== Static variables ====//

  //==== Attributes ====//

  @NotNull
  public final ByteAmount total;
  @NotNull
  public final ByteAmount mfu;
  @NotNull
  public final ByteAmount mru;
  @NotNull
  public final ByteAmount anonymous;
  @NotNull
  public final ByteAmount header;
  @NotNull
  public final ByteAmount other;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public ARCUsage(@NotNull ByteAmount total, @NotNull ByteAmount mfu, @NotNull ByteAmount mru, @NotNull ByteAmount anonymous, @NotNull ByteAmount header, @NotNull ByteAmount other) {
    this.total = total;
    this.mfu = mfu;
    this.mru = mru;
    this.anonymous = anonymous;
    this.header = header;
    this.other = other;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ARCUsage arcUsage = (ARCUsage) o;

    if (!total.equals(arcUsage.total)) return false;
    if (!mfu.equals(arcUsage.mfu)) return false;
    if (!mru.equals(arcUsage.mru)) return false;
    if (!anonymous.equals(arcUsage.anonymous)) return false;
    if (!header.equals(arcUsage.header)) return false;
    return other.equals(arcUsage.other);
  }

  @Override
  public int hashCode() {
    int result = total.hashCode();
    result = 31 * result + mfu.hashCode();
    result = 31 * result + mru.hashCode();
    result = 31 * result + anonymous.hashCode();
    result = 31 * result + header.hashCode();
    result = 31 * result + other.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "ARCUsage{" +
        "total=" + total +
        ", mfu=" + mfu +
        ", mru=" + mru +
        ", anonymous=" + anonymous +
        ", header=" + header +
        ", other=" + other +
        '}';
  }
}
