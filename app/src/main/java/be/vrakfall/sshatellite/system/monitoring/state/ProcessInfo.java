package be.vrakfall.sshatellite.system.monitoring.state;

import be.vrakfall.sshatellite.system.commands.top.ByteAmount;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;
import org.joda.time.Duration;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProcessInfo implements Comparable<ProcessInfo> {
  //==== Static variables ====//

  //==== Attributes ====//

  private int id;
  @Nullable
  private String workingDirectory;
  @Nullable
  private String fullCommand;
  @Nullable
  private String user;
  @Nullable
  private LocalDateTime creationTime;
  private int threadAmount;
  private int priority;
  private int nice;
  private ByteAmount sizeTotal;
  private ByteAmount residentMemory;
  private String state;
  private int cpuNumber;
  private Duration duration;
  private float weightedCPUUsage;
  @Nullable
  private String command;

  //==== Getters and Setters ====//

  @NotNull
  // This fixes the property access in the kotlin script as lombok messes it up
  public String getCommand() {
    return command;
  }

  //==== Constructors ====//

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  @Override
  public int compareTo(@NotNull final ProcessInfo o) {
    final val comparison = -Float.compare(getWeightedCPUUsage(), o.getWeightedCPUUsage());
    if (comparison == 0) {
      return Integer.compare(id, o.id);
    }
    return comparison;
  }
}
