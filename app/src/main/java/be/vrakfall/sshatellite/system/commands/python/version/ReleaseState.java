package be.vrakfall.sshatellite.system.commands.python.version;

import be.vrakfall.sshatellite.system.commands.python.version.errors.ReleaseStateParsingException;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.collections.Arrays;

import java.util.Locale;

public interface ReleaseState {
  //==== Static variables ====//

  String TAG = ReleaseState.class.getSimpleName();

  int ALPHA = 0;
  String[] ALPHA_LABELS = {"a", "alpha"};

  int BETA = 1;
  String[] BETA_LABELS = {"b", "beta"};

  int RELEASE_CANDIDATE = 2;
  String[] RELEASE_CANDIDATE_LABELS = {"c", "rc", "pre", "preview"};

  int FINAL = 3;
  String[] FINAL_LABELS = {""};

  //====== Error messages ======//

  String NO_CORRECT_LABEL_FOUND_ERROR_MESSAGE = "No correct release state label has been found in the string: %s";

  //==== Static Methods ====//

  static int fromLabel(@NotNull String label) throws ReleaseStateParsingException {
    if (Arrays.contains(ALPHA_LABELS, label)) {
      return ALPHA;
    }

    if (Arrays.contains(BETA_LABELS, label)) {
      return BETA;
    }

    if (Arrays.contains(RELEASE_CANDIDATE_LABELS, label)) {
      return RELEASE_CANDIDATE;
    }

    if (Arrays.contains(FINAL_LABELS, label)) {
      return FINAL;
    }

    throw new ReleaseStateParsingException(String.format(Locale.US, NO_CORRECT_LABEL_FOUND_ERROR_MESSAGE, label));
  }

  //==== Object Methods ====//

  int getReleaseState();

  boolean isPreRelease();

  boolean isPostRelease();

  boolean isDevelopmentalRelease();
}
