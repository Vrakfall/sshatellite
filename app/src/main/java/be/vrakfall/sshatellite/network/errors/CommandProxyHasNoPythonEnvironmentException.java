package be.vrakfall.sshatellite.network.errors;

public class CommandProxyHasNoPythonEnvironmentException extends IllegalArgumentException {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Constructors ====//

  public CommandProxyHasNoPythonEnvironmentException() {
    super();
  }

  public CommandProxyHasNoPythonEnvironmentException(String message) {
    super(message);
  }

  public CommandProxyHasNoPythonEnvironmentException(String message, Throwable cause) {
    super(message, cause);
  }

  public CommandProxyHasNoPythonEnvironmentException(Throwable cause) {
    super(cause);
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
