package be.vrakfall.sshatellite.network.ssh.sshj;

import android.util.Log;
import be.vrakfall.sshatellite.BuildConfig;
import be.vrakfall.sshatellite.network.RemoteShellAndSFTPEnvironment;
import be.vrakfall.sshatellite.network.errors.ActionOnNotAuthenticatedConnectionException;
import be.vrakfall.sshatellite.network.ssh.OldSSHAuthenticationParameters;
import be.vrakfall.sshatellite.network.ssh.SSHConnection;
import be.vrakfall.sshatellite.system.commands.ExecutionHandler;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.execution.Threads;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;
import net.schmizz.sshj.AndroidConfig;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.connection.ConnectionException;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.transport.TransportException;
import net.schmizz.sshj.userauth.UserAuthException;
import net.schmizz.sshj.userauth.keyprovider.KeyProvider;
import net.schmizz.sshj.xfer.FileSystemFile;
import org.jetbrains.annotations.Contract;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyPair;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Value
@EqualsAndHashCode(callSuper = true)
public class SSHJConnection extends SSHConnection {
  //==== Static variables ====//

  public static final String TAG = SSHJConnection.class.getSimpleName();

  public static final int SECONDS_BEFORE_EXECUTION_CLOSING_TIMEOUT = 5;

  //==== Attributes ====//

  private final SSHClient sshClient = new SSHClient(new AndroidConfig());
  private final Set<Session> sessions = Collections.synchronizedSet(new HashSet<>());

  @Nullable
  private final ObservableEmitter<RemoteShellAndSFTPEnvironment> emitter;

  @NotNull
  private final SFTPClient sftpClient;

  @NotNull
  private final CompositeDisposable subscriptions = new CompositeDisposable();

  @Nullable
  private KeyPair keyPair;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  /**
   * Synchronously creates a connection (and connects to it).
   *
   * @param authenticationParameters The authentication parameters for the connection to be established.
   * @throws IOException Thrown if something went wrong to establish the connection.
   */
  public SSHJConnection(final OldSSHAuthenticationParameters authenticationParameters) throws IOException {
    super(authenticationParameters);
    this.emitter = null;
    this.keyPair = null;

    if (BuildConfig.DEBUG) {
      Log.d(TAG, String.format(Locale.US, "Initiating connection with server %s.", authenticationParameters.getServer()));
    }

    connect();
    sftpClient = sshClient.newSFTPClient();
  }

  /**
   * Synchronously creates a connection (and connects to it).
   *
   * @param authenticationParameters The authentication parameters for the connection to be established.
   * @param emitter
   * @throws IOException Thrown if something went wrong to establish the connection.
   */
  @Builder
  private SSHJConnection(
      final OldSSHAuthenticationParameters authenticationParameters,
      @Nullable ObservableEmitter<RemoteShellAndSFTPEnvironment> emitter,
      @Nullable KeyPair keyPair
      ) throws IOException {
    super(authenticationParameters);
    this.emitter = emitter;
    this.keyPair = keyPair;

    if (BuildConfig.DEBUG) {
      Log.d(TAG, String.format(Locale.US, "Initiating connection with server %s.", authenticationParameters.getServer()));
    }

    connect();
    sftpClient = sshClient.newSFTPClient();
  }

  private static Observable<RemoteShellAndSFTPEnvironment> createObservableSSHJConnection(
      final Single<OldSSHAuthenticationParameters> authenticationParameters,
      @Nullable KeyPair keyPair
  ) {
    return authenticationParameters
        .flatMapObservable(authenticationParameters1 -> Observable.create((ObservableEmitter<RemoteShellAndSFTPEnvironment> emitter) ->
        {
          final RemoteShellAndSFTPEnvironment connection = new SSHJConnection(
              authenticationParameters1, emitter, keyPair
          );
//        emitter.setCancellable(connection::disconnect); // TODO: Find out why this makes the connection be instantly disconnected.
          emitter.onNext(connection);
//        emitter.onComplete();
          // We do not call onSuccess as we don't want to dispose this observable already.
        }))
        .subscribeOn(Schedulers.io());
  }

  //==== > Builders ====//

  public static Observable<SSHConnection> createObservableSSHConnection(
      final Single<OldSSHAuthenticationParameters> authenticationParameters,
      @Nullable KeyPair keyPair
  ) {
    return SSHJConnection.createObservableSSHJConnection(authenticationParameters, keyPair).cast(SSHConnection.class);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  /**
   * Creates one SSHJ connection and sends it in a Rx Observable when it is done. Temporarily staying as is but
   * should be rewritten soon as this isn't correct for the Rx paradigm!
   *
   * Default scheduler: IO
   * @param authenticationParametersSingle An Single of the authentication parameters
   * @return An Observable that only sends one connection object once it's established.
   */
  public static Observable<RemoteShellAndSFTPEnvironment> createConnectionHandlerAndConnect(
      @NotNull final Single<OldSSHAuthenticationParameters> authenticationParametersSingle,
      @Nullable KeyPair keyPair
  ) {
    final Observable<RemoteShellAndSFTPEnvironment> sshjConnectionObservable =
        createObservableSSHJConnection(authenticationParametersSingle, keyPair);
    final Subject<RemoteShellAndSFTPEnvironment> subject = BehaviorSubject
        .<RemoteShellAndSFTPEnvironment>create()
        .toSerialized();

    sshjConnectionObservable.subscribe(subject);

    return subject
        // TODO: 12/03/19 The following operation should be removed as it blocks errors to flow down the stream
        .doOnError((Throwable throwable) ->
            // TODO: 15/07/18 Handle file location problems and things like that.
            Log.e(TAG, "createConnectionHandler: ", throwable)
        )
        .subscribeOn(Schedulers.io());
  }

  //====== Object Methods ======//

  private void connect() throws IOException {
    sshClient.loadKnownHosts(new File(getAuthenticationParameters().getKnownHostsLocation())); // TODO: 10/07/18 Put this in a reactive
    // stream
    KeyProvider keyProvider;
    if (keyPair != null) {
      keyProvider = sshClient.loadKeys(keyPair);
      Log.d(TAG, "connect: Loaded the keyPair");
    }
    else {
      keyProvider = sshClient.loadKeys(getAuthenticationParameters().getPrivateKeyLocation());
      Log.d(TAG, "connect: Loaded the old keys");
    }

    sshClient.connect(getAuthenticationParameters().getServer(), getAuthenticationParameters().getPort());
    System.out.println(getAuthenticationParameters().getUser());
    System.out.println(keyProvider);
    try {
      sshClient.authPublickey(getAuthenticationParameters().getUser(), keyProvider);
    }
    catch (final UserAuthException e) {
      Log.e(TAG, "connect: Public key authentication didn't work.", e);
      sshClient.authPassword(getAuthenticationParameters().getUser(), getAuthenticationParameters().getPassword());
    }
    catch (final TransportException e) {
      throw new ConnectionException(e);
    }
    // TODO: 12/07/18 Handle both types of connection
  }

  //Dunno if this is actually needed
  public boolean isLoggedIn() {
    return sshClient.isConnected() && sshClient.isAuthenticated();
  }

  private void closeAllSessions() throws ConnectionException, TransportException {
    synchronized (sessions) {
      for (final Session session :
          sessions) {
        session.close();
        sessions.remove(session);
      }
    }
  }

  private void checkState() throws ActionOnNotAuthenticatedConnectionException {
    if (!sshClient.isConnected() || !sshClient.isAuthenticated()) {
      throw new ActionOnNotAuthenticatedConnectionException("Trying to execute an action on a not connected nor authenticated SSH connection!");
    }
  }

  @Override
  public InputStream getInputStreamTest(final String command) throws ActionOnNotAuthenticatedConnectionException, ConnectionException, TransportException {
    checkState();

    final Session session = startSession();
    final Session.Command commandInstance = session.exec(command);

    closeSession(session);
    return session.getInputStream();
  }

  @NotNull
  @Contract("_ -> new")
  @Override
  public ExecutionHandler execute(final String command) throws ActionOnNotAuthenticatedConnectionException,
      ConnectionException, TransportException {
    checkState();
    Session session = null;
    final Session.Command commandInstance;

    try {
      session = startSession();
      commandInstance = session.exec(command);
    }
    catch (final ConnectionException | TransportException e) {
      closeSession(session);
      throw e;
    }

    return new SSHJExecutionHandler(this, commandInstance);
  }

  private Session startSession() throws ConnectionException, TransportException {
    final Session session = sshClient.startSession();
    sessions.add(session);
    return session;
  }

  private void closeSession(@Nullable final Session session) throws ConnectionException, TransportException {
    if (session == null) {
      return;
    }

    try {
      session.join(SECONDS_BEFORE_EXECUTION_CLOSING_TIMEOUT, TimeUnit.SECONDS);
    }
    finally {
      sessions.remove(session);
      session.close();
    }
  }

  @Override
  public void stopExecution(@Nullable final Closeable execution) throws ConnectionException, TransportException {
    if (execution instanceof Session && sessions.contains(execution)) {
      closeSession((Session) execution);
    }
  }

  @Override
  public void disconnect() throws IOException {
    closeAllSessions();
    sshClient.close();
  }

  @Override
  public void createDirectory(@NotNull final String directory, final boolean createMissingParents) throws IOException {
    Log.d(TAG, String.format(Locale.US, "createDirectory: %s", directory));
    Threads.logCurrentThread(TAG, "createDirectory");
    if (createMissingParents) {
      sftpClient.mkdirs(directory);
    }
    else {
      sftpClient.mkdir(directory);
    }
  }

  @Override
  public void createDirectory(@org.jetbrains.annotations.NotNull @NonNull final String directory) throws IOException {
    createDirectory(directory, true);
  }

  @Override
  public boolean sendFile(final String sourceFilePath, final String destinationFilePath) throws IOException {
    Log.d(TAG, String.format(Locale.US, "sendFile: source: %s - destination: %s", sourceFilePath, destinationFilePath));
    sftpClient.put(new FileSystemFile(sourceFilePath), destinationFilePath);
    // TODO: 14/11/18 Implement a verification to be sure the file is created and is not corrupted (with checksums).
    return true;
  }
}
