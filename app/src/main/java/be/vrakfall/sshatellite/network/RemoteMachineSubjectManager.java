package be.vrakfall.sshatellite.network;

import io.reactivex.subjects.Subject;
import lombok.Builder;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
@Builder
public class RemoteMachineSubjectManager {
  //==== Static variables ====//

  private static List<RemoteMachineSubjectManager> subjectManagers = new ArrayList<>();

  //==== > Error messages ====//

  //==== Attributes ====//

  private long machineId;

  private Subject<RemoteMonitorableMachine> remoteMonitorableMachineSubject;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

//  public static void add()

  //==== Static Methods ====//

  //==== Object Methods ====//
}
