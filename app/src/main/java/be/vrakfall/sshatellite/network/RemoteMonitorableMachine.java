package be.vrakfall.sshatellite.network;

import android.annotation.SuppressLint;
import android.util.Log;
import be.vrakfall.sshatellite.Parameters;
import be.vrakfall.sshatellite.network.errors.CommandProxyHasNoPythonEnvironmentException;
import be.vrakfall.sshatellite.network.errors.ErrorWhileSendingBackendException;
import be.vrakfall.sshatellite.system.monitoring.MonitorableMachine;
import be.vrakfall.sshatellite.system.monitoring.state.MachineState;
import be.vrakfall.sshatellite.system.monitoring.state.ProcessInfo;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.execution.Threads;
import be.vrakfall.sshatellite.utils.files.AndroidApplicationFilesManager;
import be.vrakfall.sshatellite.utils.files.AndroidAssetLocalFileLink;
import io.reactivex.Completable;
import io.reactivex.Notification;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import io.reactivex.parallel.ParallelFlowable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.reactivestreams.Subscriber;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static be.vrakfall.sshatellite.utils.files.FileHierarchy.addToFilePath;

@ToString
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter(value = AccessLevel.PROTECTED)
public class RemoteMonitorableMachine extends MonitorableMachine {
  //==== Static variables ====//

  public static final String TAG = RemoteMonitorableMachine.class.getSimpleName();

  public static final RemoteMonitorableMachine IRRELEVANT = RemoteMonitorableMachine.builder().build();

  //==== > Error messages ====//

  public static final String SENDING_BACKEND_ERROR_MESSAGE = "An error occurred while sending the backend. Check the " +
      "cause for more details.";

  //==== Attributes ====//

  @NotNull
  private RemoteShellAndSFTPEnvironment commandProxy;

  @NotNull
  private AndroidApplicationFilesManager localApplicationFilesManager;

  @Getter(AccessLevel.PUBLIC)
  private boolean isBackendInstalled;

  // TODO: 3/04/19 Check if this way to handle it is ok
  @NotNull
  @Getter(AccessLevel.PUBLIC)
  private final Observable<MachineState> machineStateRx =
      Observable.<MachineState>fromPublisher(this::pulsateMachineStates);

  //==== Constructors ====//

  @Builder
  protected RemoteMonitorableMachine(
      @Nullable final Long id, @NotNull final RemoteShellAndSFTPEnvironment commandProxy,
      @NotNull final String backendDirectoryPath,
      @NotNull final AndroidApplicationFilesManager localApplicationFilesManager, final boolean isBackendInstalled
  ) {
    super(id, commandProxy, backendDirectoryPath);
    this.commandProxy = commandProxy;
    this.localApplicationFilesManager = localApplicationFilesManager;
    this.isBackendInstalled = isBackendInstalled;
  }

  public RemoteMonitorableMachine(@NotNull final RemoteMonitorableMachine other) {
    super(other);
    this.commandProxy = other.commandProxy;
    this.localApplicationFilesManager = other.localApplicationFilesManager;
    this.isBackendInstalled = other.isBackendInstalled;
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  public static Subject<RemoteMonitorableMachine> createRemoteMonitorableMachineAndConnect(
      @NotNull final Observable<RemoteShellAndSFTPEnvironment> remoteEnvironmentRx,
      @NotNull final String directoryPath,
      @NotNull final AndroidApplicationFilesManager applicationFilesManager,
      @Nullable final Consumer<? super Notification<RemoteMonitorableMachine>> sideEffect
  ) {
    final val subject = BehaviorSubject
        .<RemoteMonitorableMachine>create()
        .toSerialized();

    var tempEnvironment = remoteEnvironmentRx
        .map(remoteShellAndSFTPEnvironment ->
            RemoteMonitorableMachine.builder()
                .backendDirectoryPath(directoryPath)
                .commandProxy(remoteShellAndSFTPEnvironment)
                .localApplicationFilesManager(applicationFilesManager)
                .isBackendInstalled(true).build()
        );

    if (sideEffect != null) {
      tempEnvironment = tempEnvironment.doOnEach(sideEffect);
    }

    tempEnvironment.subscribe(subject);
    return subject;
  }

  public static Observable<RemoteMonitorableMachine> checkAndInstallIfNeededThenReEmit(
      final Observable<RemoteMonitorableMachine> machineRx
  ) {
    return machineRx.flatMapSingle((RemoteMonitorableMachine originalMachine) ->
        originalMachine.initializeBackendEnvironmentIfNeeded()
            .map((RemoteMonitorableMachine maybeModifiedMachine) -> {
              if (originalMachine.isBackendInstalled() != maybeModifiedMachine.isBackendInstalled()) {
                return RemoteMonitorableMachine.IRRELEVANT;
              }
              else {
                return maybeModifiedMachine;
              }
            })
    )
        .filter((RemoteMonitorableMachine machine) -> {
          Log.d(TAG, String.format(Locale.US, "checkAndInstallIfNeededThenReEmit: id: %d - isIrrelevant: %s",
              machine.getId(), machine.equals(IRRELEVANT) + ""));
          return !machine.equals(RemoteMonitorableMachine.IRRELEVANT);
        });
  }

  //==== Object Methods ====//

  enum FileTransferStatus {
    SUCCESSFUL
  }

  private ParallelFlowable<FileTransferStatus> sendBackendRx() {
    // TODO: 7/12/18 Take "main" from a parameter or something like that.
    final val currentAssetDirectoryPath = Parameters.BACKEND_MAIN_DIRECTORY;
    final val destinationAssetDirectoryAbsolutePath = addToFilePath(getBackendFilesManager().getRoot(),
        currentAssetDirectoryPath);
    // First, make sure the target directory is created on the commandProxy using the I/O scheduler.
    Completable.fromAction(
        () -> {
          try {
            getCommandProxy().createDirectory(destinationAssetDirectoryAbsolutePath, true);
          }
          catch (IOException e) {
            e.printStackTrace();
          }
        }
    )
        .subscribeOn(Schedulers.io())
        .blockingGet();
//        .subscribe(() -> Log.d(TAG, "sendBackendRx: Directories created"),
//            throwable -> Log.d(TAG, "sendBackendRx: Not allowed to create the directories"));

    // Forced to await this way as no `.andThen()` operator works on ParallelFlowables.
    return getLocalApplicationFilesManager().copyAssetsInDirectoryToLocalDataFilesRx(currentAssetDirectoryPath)
        .filter((AndroidAssetLocalFileLink assetFileLink) -> {
          Log.d(TAG, String.format(Locale.US, "sendBackendRx: file path is %s",
              assetFileLink.getApplicationLocalFile().getPath()));
          Log.d(TAG, String.format(Locale.US, "sendBackendRx: Is file pertinent? %b",
              assetFileLink.isPertinent()));
          return assetFileLink.isPertinent();
        })
        .map((AndroidAssetLocalFileLink assetFileLink) -> {
          Threads.logCurrentThread(TAG, "sendBackendRx");
          // TODO: 7/12/18 Handle the destination directory better
          final val assetParent = new File(assetFileLink.getAssetPath()).getParent();
          try {
            getBackendFilesManager().createDirectoryAndParents(assetParent);
          }
          catch (final IOException e) {
            // TODO: 18/12/18 Check directory existence, even before trying.
            // Meanwhile previous todo is done, do nothing as it probably means the directory is there already. (SSHJ
            //  sends out a "NO_SUCH_FILE" error when it's the case, dunno why.)
            Log.d(TAG, String.format(
                Locale.US,
                "sendBackendRx: Caught an exception of type %s when trying to create directory at location %s and its" +
                    " parents. Message was: %s",
                e.getClass(),
                assetParent,
                e.getMessage()
            ));
          }
          // Send local file (copied form the asset) to the remote
          try {
            commandProxy.sendFile(
                assetFileLink.getApplicationLocalFile().getPath(),
                getBackendFilesManager().getAbsolutePathOf(assetFileLink.getAssetPath())
            );
          }
          catch (final IOException e) {
            Log.d(TAG, String.format(
                Locale.US,
                "sendBackendRx: Caught an exception of type %s when trying to send file at location %s to %s ." +
                    " Message was: %S", e.getClass(),
                assetFileLink.getApplicationLocalFile().getPath(),
                getBackendFilesManager().getAbsolutePathOf(assetFileLink.getAssetPath()),
                e.getMessage()
            ));
          }
          return FileTransferStatus.SUCCESSFUL;
        });
  }

  private void sendBackend() {
    sendBackendRx()
        .sequential()
        .blockingSubscribe(
            (FileTransferStatus fileTransferStatus) -> {
            },
            (Throwable throwable) -> {
              throw new ErrorWhileSendingBackendException(SENDING_BACKEND_ERROR_MESSAGE, throwable);
            }
        );
  }

  @SuppressLint("CheckResult")
  public Single<RemoteMonitorableMachine> initializeBackendEnvironment() {
    // Create a completable that installs the venv
    final val isVenvInstallationSuccessfulRx = Completable.fromCallable(
        () -> {
//          Threads.logCurrentThread(TAG, "initializeBackendEnvironment");
          getPythonEnvironment().installVenvUpdatePackagesAndInstallRequiredOnes(
              getBackendFilesManager().getAbsoluteVenvPath()
          );
          // If we get to this point without exception, it means it's successfully installed, return true.
          return true;
        }
    )
        .subscribeOn(Schedulers.io());

    // Create a completable that sends the backend
    final val isBackendTransferSuccessfulRx =
        Completable.fromObservable(
            sendBackendRx()
                .sequential()
                .toObservable()
        );

    // Return a completable of when both of the previous actions succeeded
    return isVenvInstallationSuccessfulRx
        .mergeWith(isBackendTransferSuccessfulRx)
        .andThen(
            // A copy was returned before, dunno why. If something bugs related to it... Well look no further.
            Single.fromCallable(() -> this)
        );
  }

  public Single<RemoteMonitorableMachine> initializeBackendEnvironmentIfNeeded() {
    if (!isBackendInstalled()) {
      return initializeBackendEnvironment();
    }
    else {
      // Just return this object if the server doesn't need to be installed
      return Single.fromCallable(() -> this);
    }
  }

  private void pulsateMachineStates(final Subscriber<? super MachineState> s) {
    try {
      final val pythonEnvironment = commandProxy.getHighestVersionPythonEnvironment();
      if (pythonEnvironment == null) {
        throw new CommandProxyHasNoPythonEnvironmentException("The command proxy doesn't seem to have a " +
            "python environment.");
      }
      while (true) {
        final val executionHandler =
            pythonEnvironment
                .executeScriptInVenv(
                    getBackendFilesManager().getAbsoluteVenvPath(),
                    getBackendFilesManager().getAbsoluteMainExecutablePath(),
                    getBackendFilesManager().getRoot()
                );

        final var machineState = MachineState.parseFromYaml(executionHandler.getOutput());
        machineState.setAssociatedMachineId(getId());
        s.onNext(machineState);
        TimeUnit.SECONDS.sleep(1);
      }
    }
    catch (final Exception e) {
      s.onError(e);
    }
  }

  @SuppressLint("CheckResult")
  public Single<Integer> killProcess(@NotNull final ProcessInfo processInfo) throws IOException {
    return Single.fromCallable(() -> executeProcessManipulationCommand("kill", processInfo))
        .subscribeOn(Schedulers.io());
  }

  public Single<Integer> terminateProcess(@NotNull final ProcessInfo processInfo) {
    return Single.fromCallable(() -> executeProcessManipulationCommand("terminate", processInfo))
        .subscribeOn(Schedulers.io());
  }

  public Single<Integer> suspendProcess(@NotNull final ProcessInfo processInfo) {
    return Single.fromCallable(() -> executeProcessManipulationCommand("suspend", processInfo))
        .subscribeOn(Schedulers.io());
  }

  @NotNull
  private Integer executeProcessManipulationCommand(@NotNull final String command,
                                                    @NotNull ProcessInfo processInfo) throws IOException {
    val pythonEnvironment = commandProxy.getHighestVersionPythonEnvironment();
    assert pythonEnvironment != null;
    return pythonEnvironment.executeScriptInVenv(
        getBackendFilesManager().getAbsoluteVenvPath(),
        // TODO: 2/04/19 Magic number alert
        getBackendFilesManager().getAbsolutePathOf("sshatellite_backend/main/" + command + ".py " +
            processInfo.getId() + " " + processInfo.getCommand()),
        getBackendFilesManager().getRoot()
    )
        .waitExecutionAndGetExitStatus();
  }
}
