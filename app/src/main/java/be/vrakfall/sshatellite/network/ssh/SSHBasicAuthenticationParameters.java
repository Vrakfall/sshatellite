package be.vrakfall.sshatellite.network.ssh;

import be.vrakfall.sshatellite.network.StandardPorts;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.jetbrains.annotations.Contract;

@ToString
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
public class SSHBasicAuthenticationParameters {
  //==== Static variables ====//

  public static final String TAG = SSHBasicAuthenticationParameters.class.getSimpleName();

  //==== Attributes ====//

  @NotNull
  private String server;

  private int port;

  @NotNull
  private String user;

  @NotNull
  private String knownHostsPath;

  @Nullable
  private String privateKeyPath;

  @Nullable
  private String publicKeyPath;

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public SSHBasicAuthenticationParameters(
      @NotNull String server, int port, @NotNull String user, @NotNull String knownHostsPath,
      @Nullable String privateKeyPath, @Nullable String publicKeyPath
  ) {
    this.server = server;

    // Set the port if it's a normal one, else set the standard port for SSH.
    if (port > StandardPorts.PORT_MIN && port < StandardPorts.PORT_MAX) {
      this.port = port;
    }
    else {
      this.port = StandardPorts.SSH;
    }

    this.user = user;
    this.knownHostsPath = knownHostsPath;
    this.privateKeyPath = privateKeyPath;
    this.publicKeyPath = publicKeyPath;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  @Contract(pure = true)
  public boolean hasPublicKeyAuthenticationParameters() {
    return privateKeyPath != null && publicKeyPath != null;
  }
}
