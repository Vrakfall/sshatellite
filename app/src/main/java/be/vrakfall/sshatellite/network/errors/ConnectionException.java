package be.vrakfall.sshatellite.network.errors;

public class ConnectionException extends Exception {
  private static final long serialVersionUID = -1361334745054846634L;
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  public ConnectionException() {
  }

  public ConnectionException(final String message) {
    super(message);
  }

  public ConnectionException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public ConnectionException(final Throwable cause) {
    super(cause);
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
