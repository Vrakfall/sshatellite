package be.vrakfall.sshatellite.network;

import org.joda.time.Duration;

import java.io.IOException;

public interface Connection {
  //==== Static variables ====//

  Duration JOIN_TIME_FOR_FINISHED_COMMAND = Duration.standardSeconds(5);

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  //====== Abstract ======//

  void disconnect() throws IOException;
}
