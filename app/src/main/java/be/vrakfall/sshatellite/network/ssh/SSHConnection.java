package be.vrakfall.sshatellite.network.ssh;

import be.vrakfall.sshatellite.network.Connection;
import be.vrakfall.sshatellite.network.RemoteShellAndSFTPEnvironment;
import be.vrakfall.sshatellite.system.commands.python.HighestVersionShellPythonEnvironment;
import be.vrakfall.sshatellite.system.commands.python.PythonEnvironment;
import org.jetbrains.annotations.Nullable;
import org.spongycastle.jce.provider.BouncyCastleProvider;

import java.io.IOException;
import java.security.Security;

public abstract class SSHConnection implements RemoteShellAndSFTPEnvironment {
  //==== Static variables ====//

  public static final String TAG = Connection.class.getSimpleName();

  //==== Attributes ====//

  private final OldSSHAuthenticationParameters authenticationParameters; // TODO: 30/06/18 Stop storing password in memory

  //==== Constructors ====//

  public SSHConnection(final OldSSHAuthenticationParameters authenticationParameters) {
    this.authenticationParameters = authenticationParameters;

    Security.removeProvider("BC");
    Security.insertProviderAt(new BouncyCastleProvider(), Security.getProviders().length + 1);
  }

  //==== Getters and Setters ====//

  protected OldSSHAuthenticationParameters getAuthenticationParameters() {
    return authenticationParameters;
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  @Override
  public boolean hasAPythonEnvironment() throws IOException {
    // TODO: 23/11/18 This
    return false;
  }

  @Override
  public @Nullable PythonEnvironment getPythonEnvironmentFrom(String path) throws IOException {
    // TODO: 23/11/18 This
    return null;
  }

  @Override
  public @Nullable PythonEnvironment getHighestVersionPythonEnvironment() {
    return HighestVersionShellPythonEnvironment.builder()
        .shellEnvironment(this)
        .build();
  }
}
