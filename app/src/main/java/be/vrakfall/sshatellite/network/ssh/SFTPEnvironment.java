package be.vrakfall.sshatellite.network.ssh;

import be.vrakfall.sshatellite.network.Connection;
import be.vrakfall.sshatellite.utils.annotations.NotNull;

import java.io.IOException;

public interface SFTPEnvironment extends Connection {
  //==== Static variables ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  void createDirectory(@NotNull final String directory, boolean createMissingParents) throws IOException;

  void createDirectory(@NotNull final String directory) throws IOException;

  boolean sendFile(String sourceFilePath, String destinationFilePath) throws IOException;
}
