package be.vrakfall.sshatellite.network;

public interface StandardPorts {
  //==== Static variables ====//

  /**
   * Minimal port you can use in networking.
   * Taken from <a href="https://stackoverflow.com/questions/25475337/maximum-tcp-port-number-constant-in-java">https://stackoverflow.com/questions/25475337/maximum-tcp-port-number-constant-in-java</a>
   */
  int PORT_MIN = 1;

  /**
   * Maximal port you can use in networking.
   * Taken from <a href="https://stackoverflow.com/questions/25475337/maximum-tcp-port-number-constant-in-java">https://stackoverflow.com/questions/25475337/maximum-tcp-port-number-constant-in-java</a>
   */
  int PORT_MAX = (1 << 16) - 1;

  int SSH = 22;

  //====== Static Methods ======//

  //====== Object Methods ======//
}
