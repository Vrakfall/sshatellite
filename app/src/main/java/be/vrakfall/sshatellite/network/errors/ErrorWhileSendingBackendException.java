package be.vrakfall.sshatellite.network.errors;

import java.io.IOException;

public class ErrorWhileSendingBackendException extends IOException {
  public ErrorWhileSendingBackendException() {
    super();
  }

  public ErrorWhileSendingBackendException(String message) {
    super(message);
  }

  public ErrorWhileSendingBackendException(String message, Throwable cause) {
    super(message, cause);
  }

  public ErrorWhileSendingBackendException(Throwable cause) {
    super(cause);
  }
}
