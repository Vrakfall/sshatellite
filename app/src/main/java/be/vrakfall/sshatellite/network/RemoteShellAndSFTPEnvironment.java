package be.vrakfall.sshatellite.network;

import be.vrakfall.sshatellite.network.ssh.SFTPEnvironment;

public interface RemoteShellAndSFTPEnvironment extends RemoteShellEnvironment, SFTPEnvironment {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
