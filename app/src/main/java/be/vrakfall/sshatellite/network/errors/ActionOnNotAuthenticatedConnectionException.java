package be.vrakfall.sshatellite.network.errors;

import java.io.IOException;

public class ActionOnNotAuthenticatedConnectionException extends IOException {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public ActionOnNotAuthenticatedConnectionException() {
  }

  public ActionOnNotAuthenticatedConnectionException(String message) {
    super(message);
  }

  public ActionOnNotAuthenticatedConnectionException(String message, Throwable cause) {
    super(message, cause);
  }

  public ActionOnNotAuthenticatedConnectionException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
