package be.vrakfall.sshatellite.network.ssh;

import android.os.Environment;
import be.vrakfall.sshatellite.network.StandardPorts;

public class OldSSHAuthenticationParameters {
  //==== Static variables ====//

  public static final String TAG = OldSSHAuthenticationParameters.class.getSimpleName();

  //==== Attributes ====//

  private String server = "";
  private int port = StandardPorts.SSH;
  private String user = "";
  private String password = "";
  private String privateKeyLocation = "";
  private String publicKeyLocation = "";
  private String knownHostsLocation = "";
  private String privateKeyPassword = "";


  //==== Getters and Setters ====//

  public String getServer() {
    return server;
  }

  public void setServer(String server) {
    this.server = server;
  }

  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    if (port > StandardPorts.PORT_MIN && port < StandardPorts.PORT_MAX) {
      this.port = port;
    }
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPrivateKeyLocation() {
    return privateKeyLocation;
  }

  public void setPrivateKeyLocation(String privateKeyLocation) {
    this.privateKeyLocation = Environment.getExternalStorageDirectory().getPath() + "/" + privateKeyLocation;
    //TODO: Check with a regex if the path is absolute and don't add the SD card's root before the file path.
  }

  public String getPublicKeyLocation() {
    return publicKeyLocation;
  }

  public void setPublicKeyLocation(String publicKeyLocation) {
    this.publicKeyLocation = Environment.getExternalStorageDirectory().getPath() + "/" + publicKeyLocation;
    //TODO: Check with a regex if the path is absolute and don't add the SD card's root before the file path.
  }

  public String getKnownHostsLocation() {
    return knownHostsLocation;
  }

  public void setKnownHostsLocation(String knownHostsLocation) {
    this.knownHostsLocation = Environment.getExternalStorageDirectory().getPath() + "/" + knownHostsLocation;
    //TODO: Check with a regex if the path is absolute and don't add the SD card's root before the file path.
  }

  public String getPrivateKeyPassword() {
    return privateKeyPassword;
  }

  public void setPrivateKeyPassword(String privateKeyPassword) {
    this.privateKeyPassword = privateKeyPassword;
  }

  //==== Constructors ====//

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

}
