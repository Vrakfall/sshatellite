package be.vrakfall.sshatellite.network.ssh.sshj;

import be.vrakfall.sshatellite.network.Connection;
import be.vrakfall.sshatellite.system.commands.Commandable;
import be.vrakfall.sshatellite.system.commands.ExecutionHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import net.schmizz.sshj.common.IOUtils;
import net.schmizz.sshj.connection.channel.direct.Session.Command;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

@Data
@EqualsAndHashCode(callSuper = true)
public class SSHJExecutionHandler extends ExecutionHandler {
  //==== Static variables ====//

  public static final String TAG = SSHJExecutionHandler.class.getSimpleName();

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Constructors ====//

  public SSHJExecutionHandler(final Commandable commandable, final Command execution) {
    super(commandable, execution);
  }

  //==== Getters and Setters ====//

  @Override
  public InputStream getOutput() {
    return ((Command) getExecution()).getInputStream();
  }

  @Override
  public InputStream getErrorOutput() {
    return ((Command) getExecution()).getErrorStream();
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @Override
  public int waitExecutionAndGetExitStatus() throws IOException {
    final val commandInstance = ((Command) getExecution());
    // Wait for the stream to return EOF. We don't need to read its output as it shouldn't have one or we just want
    // the exist status anyway.
    IOUtils.readFully(commandInstance.getInputStream());
    // Join/close the command instance.
    commandInstance.join(Connection.JOIN_TIME_FOR_FINISHED_COMMAND.getMillis(), TimeUnit.MILLISECONDS);
    return commandInstance.getExitStatus();
  }
}
