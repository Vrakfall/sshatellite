package be.vrakfall.sshatellite.network;

import be.vrakfall.sshatellite.system.commands.ShellEnvironment;

public interface RemoteShellEnvironment extends ShellEnvironment, Connection {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
