package be.vrakfall.sshatellite.ui.monitorableFragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import be.vrakfall.sshatellite.R;
import be.vrakfall.sshatellite.databinding.FragmentProcessesSummaryBinding;
import be.vrakfall.sshatellite.databinding.ProcessSummaryBinding;
import be.vrakfall.sshatellite.system.monitoring.state.MachineState;
import be.vrakfall.sshatellite.system.monitoring.state.ProcessInfo;
import be.vrakfall.sshatellite.ui.ServerActivity;
import be.vrakfall.sshatellite.ui.viewUpdaters.summary.ProcessSummary;
import be.vrakfall.sshatellite.utils.android.ui.fragments.RxRecyclerFragment;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.collections.Maps;
import com.jakewharton.rxbinding3.widget.RxTextView;
import com.minimize.android.rxrecycleradapter.RxDataSource;
import com.minimize.android.rxrecycleradapter.SimpleViewHolder;
import io.reactivex.disposables.Disposable;
import java9.util.stream.Collectors;
import java9.util.stream.StreamSupport;
import lombok.val;

import java.util.*;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProcessesSummaryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProcessesSummaryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProcessesSummaryFragment extends RxRecyclerFragment {
  //==== Static variables ====//

  public static final String TAG = ProcessesSummaryFragment.class.getSimpleName();

  private static final int DEFAULT_COLUMN_AMOUNT = 2;

  //==== > Fragment parameter names ====//

  private static final String ARG_COLUMN_AMOUNT = "columnAmount";

  //==== > Error messages ====//

  //==== Attributes ====//

  private String currentFilter = "";

  //==== > Data Binding ====//

  private List<ProcessSummary> lastProcessSummaries;

  private RxDataSource<ProcessSummaryBinding, ProcessSummary> processSummarySources;

  private Disposable viewHolderSubscription;

  //==== > Fragment specific ====//

  @Nullable
  private OnFragmentInteractionListener listener;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  public interface OnFragmentInteractionListener extends RxRecyclerFragment.OnRxRecyclerFragmentInteractionListener {
    void onProcessSuspendButtonClick(final long associatedMachineId, @NotNull final ProcessInfo processInfo);

    void onProcessKillButtonClick(final long associatedMachineId, @NotNull final ProcessInfo processInfo);

    void onProcessPauseResumeButtonClick(final long associatedMachineId, @NotNull final ProcessInfo processInfo);

    void onMoreActionsButtonClick(final long associatedMachineId, @NotNull final ProcessInfo processInfo);
  }

  //==== > Classes ====//

  //==== Constructors ====//

  public ProcessesSummaryFragment() {
    // Required empty public constructor
  }

  //==== Getters and Setters ====//

  // TODO: 27/02/19 Move the id parameter as attribute
  public void setLastProcessSummaries(final Map<Integer, ProcessInfo> processesDetails,
                                      final long associatedMachineId) {
    final val processDetailsList = new ArrayList<>(processesDetails.entrySet());
    // TODO: 27/02/19 Use the backport method for the following line when merging back with master branch
    Collections.sort(processDetailsList, Maps.comparingByValue());

    this.lastProcessSummaries = StreamSupport.parallelStream(processDetailsList)
        .map(processDetailsEntry -> new ProcessSummary(
            associatedMachineId, processDetailsEntry.getValue(), getResources().getDisplayMetrics()
        ))
        .collect(Collectors.toList());
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment ProcessesSummaryFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static ProcessesSummaryFragment newInstance() {
    return bundleInitialization(new ProcessesSummaryFragment(), 1);
  }

  //==== Object Methods ====//

  @Override
  public void onAttach(final Context context) {
    super.onAttach(context);
    if (getContext() instanceof ServerActivity) {
      this.listener = (OnFragmentInteractionListener) context;
    }
    // TODO: 3/04/19 Enforce listener implementation from the context
  }

  // TODO: 3/04/19 Add a `onDetach()` method

  @SuppressLint({"RxSubscribeOnError", "RxLeakedSubscription", "CheckResult"})
  @Override
  public View onCreateView(@NotNull final LayoutInflater inflater, final ViewGroup container,
                           final Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    final FragmentProcessesSummaryBinding binding =
        DataBindingUtil.inflate(inflater, R.layout.fragment_processes_summary, container, false);
    binding.processesSummaryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    lastProcessSummaries = Collections.emptyList();
    processSummarySources = new RxDataSource<>(R.layout.process_summary, lastProcessSummaries);

    viewHolderSubscription = processSummarySources
        .bindRecyclerView(binding.processesSummaryRecyclerView)
        .subscribe((SimpleViewHolder<ProcessSummary, ProcessSummaryBinding> viewHolder) -> {
          assert listener != null;
          RxRecyclerFragment.bindWithViewUpdater(viewHolder, listener);
        });

    RxTextView.afterTextChangeEvents(binding.processesSummaryProcessSearchText)
        .subscribe(textViewAfterTextChangeEvent ->
            {
              currentFilter = textViewAfterTextChangeEvent.getView().getText().toString();
              processSummarySources.updateDataSet(lastProcessSummaries)
                  .filter(processSummary -> processSummary.getCommand().contains(textViewAfterTextChangeEvent.getView().getText()))
                  .updateAdapter();
            }
        );

    return binding.getRoot();
  }

  public void updateWith(@NotNull final MachineState machineState) {
    // TODO: 25/01/19 Change the way the machine is associated with
    setLastProcessSummaries(machineState.getProcessesDetails(), machineState.getAssociatedMachineId());

    processSummarySources
        .updateDataSet(
            lastProcessSummaries
        )
        .filter(processSummary -> processSummary.getCommand().contains(currentFilter))
        .updateAdapter();
  }
}
