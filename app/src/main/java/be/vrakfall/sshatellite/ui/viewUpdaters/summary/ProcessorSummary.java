package be.vrakfall.sshatellite.ui.viewUpdaters.summary;

import android.annotation.SuppressLint;
import android.util.Log;
import androidx.databinding.ViewDataBinding;
import be.vrakfall.sshatellite.databinding.ProcessorSummaryBinding;
import be.vrakfall.sshatellite.network.RemoteMonitorableMachine;
import be.vrakfall.sshatellite.system.commands.top.LoadAveragesAndTimeInfo;
import be.vrakfall.sshatellite.system.monitoring.state.MachineState;
import be.vrakfall.sshatellite.ui.monitorableFragments.MonitorableSummaryFragment;
import be.vrakfall.sshatellite.utils.Reflection;
import be.vrakfall.sshatellite.utils.android.ui.DataBinding;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import com.jakewharton.rxbinding3.view.RxView;
import kotlin.Unit;
import lombok.*;
import org.joda.time.Duration;
import org.joda.time.LocalTime;

import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import static be.vrakfall.sshatellite.utils.android.ui.ViewUpdater.update;
import static be.vrakfall.sshatellite.utils.android.ui.ViewUpdater.updateAsFloat;

@Data
@EqualsAndHashCode(callSuper = true)
public class ProcessorSummary extends Summary {
  //==== Static variables ====//

  public static final String TAG = ProcessorSummary.class.getSimpleName();

  public static final int TYPE_NUMBER = getNewTypeNumber();

  public static final LoadAveragesAndTimeInfo PLACEHOLDER_LOAD_AVERAGES = new LoadAveragesAndTimeInfo(
      0.F, 0.F, 0.F, Duration.ZERO, LocalTime.MIDNIGHT
  );

  public static final ProcessorSummary PLACEHOLDER = ProcessorSummary.builder().build();

  //==== > Error messages ====//

  public static final String DATA_BINDING_IS_OF_WRONG_TYPE_ERROR_MESSAGE = String.format(Locale.US, "The databinding " +
          "object associated with this object has to be of type %s but %%s was given instead.",
      ProcessorSummaryBinding.class.getSimpleName());

  //==== Attributes ====//

  /**
   * Percentage of usage of all the processors cores.
   * Stored between 0 (0%) and 100 (100%).
   * The higher precision WHEN DISPLAYED is 4 digits.
   * Examples: 0.01%, 99.99% or 100%
   */
  // TODO: 19/01/19 Link the method that formats the number for the view in the javadoc
  private final float usagePercentage;

  /**
   * Load averages of the processor.
   */
  // TODO: 19/01/19 Separate LoadAverages and TimeInfo
  // TODO: 19/01/19 When subclassing for GPUs, move this down to CPU data only.
  @NotNull
  private final LoadAveragesAndTimeInfo loadAverages;

  @Setter(AccessLevel.NONE)
  @Getter(AccessLevel.PRIVATE)
  private final AtomicBoolean isListenerBound = new AtomicBoolean(false);

  //==== Constructors ====//

  @Builder
  private ProcessorSummary(final long associatedMachineId,
                           @Nullable final MachineState machineState
  ) {
    super(associatedMachineId);
    if (machineState == null) {
      this.usagePercentage = PLACEHOLDER_USAGE_PERCENTAGE;
      this.loadAverages = PLACEHOLDER_LOAD_AVERAGES;
    }
    else {
      this.usagePercentage = (float) machineState.cpuUsagePercentage();
      this.loadAverages = new LoadAveragesAndTimeInfo(machineState.getLoadAverage(0), machineState.getLoadAverage(1),
          machineState.getLoadAverage(2), Duration.ZERO, LocalTime.MIDNIGHT);
    }
  }

  public ProcessorSummary(final ProcessorSummary other) {
    super(other);
    this.usagePercentage = other.usagePercentage;
    this.loadAverages = other.loadAverages;
  }

  //==== Getters and Setters ====//

  public ProcessorSummary with(@NotNull final MachineState machineState) {
    return new ProcessorSummary(getAssociatedMachineId(), machineState);
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  @SuppressWarnings("ResultOfMethodCallIgnored")
  @SuppressLint({"CheckResult", "RxLeakedSubscription", "RxSubscribeOnError"})
  private void bindListener(@NotNull final ProcessorSummaryBinding binding,
                            final MonitorableSummaryFragment.OnFragmentInteractionListener listener) {
    if (this.getAssociatedMachineId() != RemoteMonitorableMachine.IRRELEVANT.getId() && !getIsListenerBound().getAndSet(true)) {
      Log.d(TAG, "bindListener: ");
      RxView
          .clicks(binding.processorSummaryTopLayout)
          .subscribe((Unit unit) -> listener.onCPUSummaryClick(getAssociatedMachineId()));
    }
  }

  //==== Object Methods ====//

  @Override
  public int getTypeNumber() {
    return TYPE_NUMBER;
  }

  protected void updateView(@NotNull final ProcessorSummaryBinding binding) {
    update(binding.processorSummaryCpuUsagePercentage, getUsagePercentage(), 2);
    updateAsFloat(binding.processorSummaryLoadAverage1m, loadAverages.loadAverages[0], 2);
    updateAsFloat(binding.processorSummaryLoadAverage5m, loadAverages.loadAverages[1], 2);
    updateAsFloat(binding.processorSummaryLoadAverage15m, loadAverages.loadAverages[2], 2);
  }

  @Override
  public void updateView(@NotNull final ViewDataBinding binding) {
    DataBinding.ensureType(ProcessorSummaryBinding.class, binding);
    updateView(((ProcessorSummaryBinding) binding));
  }

  @Override
  public void updateView(@NotNull final ViewDataBinding binding,
                         @NotNull final MonitorableSummaryFragment.OnRxRecyclerFragmentInteractionListener listener) {
    Log.d(TAG, String.format(Locale.US, "updateView: id: %d - cpu: %.2f", getAssociatedMachineId(),
        getUsagePercentage()));
    updateView(binding);
    Reflection.ensureType(MonitorableSummaryFragment.OnFragmentInteractionListener.class, listener);
    bindListener(((ProcessorSummaryBinding) binding), ((MonitorableSummaryFragment.OnFragmentInteractionListener) listener));
  }
}
