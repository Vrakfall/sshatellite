package be.vrakfall.sshatellite.ui.monitorableFragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import be.vrakfall.sshatellite.R;
import be.vrakfall.sshatellite.databinding.FragmentServerSummaryBinding;
import be.vrakfall.sshatellite.system.monitoring.state.MachineState;
import be.vrakfall.sshatellite.ui.ServerActivity;
import be.vrakfall.sshatellite.ui.viewUpdaters.summary.*;
import be.vrakfall.sshatellite.utils.android.ui.fragments.RxRecyclerFragment;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import com.minimize.android.rxrecycleradapter.OnGetItemViewType;
import com.minimize.android.rxrecycleradapter.RxDataSourceSectioned;
import com.minimize.android.rxrecycleradapter.TypesViewHolder;
import com.minimize.android.rxrecycleradapter.ViewHolderInfo;
import io.reactivex.disposables.Disposable;
import lombok.val;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MonitorableSummaryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MonitorableSummaryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MonitorableSummaryFragment extends RxRecyclerFragment {
  //==== Static variables ====//

  public static final String TAG = MonitorableSummaryFragment.class.getSimpleName();

  // TODO: 14/12/18 Use an enum
  public static final int RAM_PERCENTAGE_ALERT = 0;

  private static final List<ViewHolderInfo> viewHolderInfos =
      Arrays.asList(
          new ViewHolderInfo(R.layout.processor_summary, ProcessorSummary.TYPE_NUMBER),
          new ViewHolderInfo(R.layout.memory_summary, MemorySummary.TYPE_NUMBER),
          new ViewHolderInfo(R.layout.disks_summary, DisksSummary.TYPE_NUMBER),
          new ViewHolderInfo(R.layout.network_summary, NetworkSummary.TYPE_NUMBER)
      );

  //==== > Error messages ====//

  //==== Attributes ====//

  @Nullable
  private MachineState previousMachineState;

  //==== > Data Binding ====//

  @Nullable
  private List<Summary> lastSummaries;

  private final OnGetItemViewType viewTypeInterpreter = new OnGetItemViewType() {
    @Override
    public int getItemViewType(final int i) {
      return lastSummaries.get(i).getTypeNumber();
    }
  };

  @Nullable
  private RxDataSourceSectioned<Summary> summarySources;

  private Disposable viewHolderSubscription;

  //==== > Fragment specific ====//

  @Nullable
  private OnFragmentInteractionListener listener;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   * <p>
   * See the Android Training lesson <a href=
   * "http://developer.android.com/training/basics/fragments/communicating.html"
   * >Communicating with Other Fragments</a> for more information.
   */
  public interface OnFragmentInteractionListener extends RxRecyclerFragment.OnRxRecyclerFragmentInteractionListener {
    void onCPUSummaryClick(final long associatedMachineId);
  }

  //==== > Classes ====//

  //==== Constructors ====//

  public MonitorableSummaryFragment() {
    // Required empty public constructor
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param columnAmount
   * @return A new instance of fragment MonitorableSummaryFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static MonitorableSummaryFragment newInstance(final int columnAmount) {
    return bundleInitialization(new MonitorableSummaryFragment(), columnAmount);
  }

  //==== Object Methods ====//

  @Override
  public void onAttach(final Context context) {
    super.onAttach(context);
    if (context instanceof ServerActivity) {
      this.listener = (OnFragmentInteractionListener) context;
    }
  }

  @Override
  public View onCreateView(@NotNull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    final FragmentServerSummaryBinding binding = FragmentServerSummaryBinding.inflate(inflater, container, false);
    binding.summariesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), getColumnAmount()));

    // Initialise the summaries with placeholder data
    Log.d(TAG, "onCreateView: Placeholders");
    lastSummaries = Arrays.asList(
        ProcessorSummary.PLACEHOLDER,
        MemorySummary.PLACEHOLDER,
        DisksSummary.PLACEHOLDER,
        NetworkSummary.PLACEHOLDER
    );
    summarySources = new RxDataSourceSectioned<>(lastSummaries, viewHolderInfos, viewTypeInterpreter);

    // Bind the Recycler view with the RxDataSource and subscribe to it to update the View
    viewHolderSubscription = summarySources
        .bindRecyclerView(binding.summariesRecyclerView)
        .subscribe((TypesViewHolder<Summary> viewHolder) -> {
          assert listener != null;
          RxRecyclerFragment.bindWithViewUpdater(viewHolder, listener);
        });

    return binding.getRoot();
  }

  // TODO: 18/01/19 Rename dataSource
  public void updateWith(@NotNull final MachineState machineState) {
    Log.d(TAG, String.format(Locale.US, "updateWith: average: %.2f%%",
        machineState.diskPartitionUsageAveragePercentage()));
//    Log.d(TAG, String.format(Locale.US, "updateWith: max: %.2f%%", machineState.diskPartitionUsageMaxPercentage()));
//    Log.d(TAG, String.format(Locale.US, "updateWith: associatedId: %d", machineState.getAssociatedMachineId()));

    final val newSummaries = Arrays.asList(
        ProcessorSummary.builder()
            .machineState(machineState)
            .associatedMachineId(machineState.getAssociatedMachineId()).build(),
        MemorySummary.builder()
            .machineState(machineState)
            .associatedMachineId(machineState.getAssociatedMachineId()).build(),
        DisksSummary.builder()
            .machineState(machineState)
            .associatedMachineId(machineState.getAssociatedMachineId()).build(),
        NetworkSummary.builder()
            .associatedMachineId(machineState.getAssociatedMachineId())
            .previousMachineState(previousMachineState)
            .currentMachineState(machineState).build()
    );
    // TODO: 14/02/19 There's probably something that can be done in case summarySources is null but it shouldn't
    //  normally happen
    assert summarySources != null;
    summarySources
        .updateDataSet(newSummaries)
        .updateAdapter();

//    Log.d(TAG, String.format(
//        Locale.US, "updateWith: Recycler item count: %d", binding.summariesRecyclerView.getItemDecorationCount()
//    ));
//    Log.d(TAG, String.format(Locale.US, "updateWith: dataset: %s", summarySources.getDataSet()));

//    if (previousMachineState != null) {
//      Log.d(TAG, String.format(Locale.US, "updateWith: upload: %s",
//          machineState.uploadSpeedTotal(previousMachineState)));
//    }

    previousMachineState = machineState;
  }

//  @TargetApi(Build.VERSION_CODES.N)
//  @RequiresApi(Build.VERSION_CODES.N)
//  private void updateWith(@NotNull final MachineState machineState) {
//    final val percentage = machineState.getCpuUsages().stream().mapToDouble(CPUUsage::getTotal).average().orElse(Double.NaN);
//    binding.serverSummaryCpuUsageTextView.setText(String.format(Locale.US, "%.2f%%", percentage));
//    binding.serverSummaryCpuUsageProgressBar.setProgress(
//        // 100 doesn't come from percentage but from the range of numbers used for the progressBar.
//        Math.cropToInteger(java.lang.Math.round(percentage * 100.D))
//    );
//
//    updateLoadAverages(machineState.getLoadAverages());
//
//    binding.serverSummaryRamUsageTextView.setText(
//        String.format(Locale.US, "%.1f", machineState.getMemoryUsagePercentage())
//    );
//    if (machineState.getMemoryUsagePercentage() > DEFAULT_ALERT_PERCENTAGE) {
//      mListener.onFragmentInteraction(RAM_PERCENTAGE_ALERT, machineState.getMemoryUsagePercentage());
//    }
//    binding.serverSummaryRamUsageProgressBar.setProgress(
//        Math.cropToInteger(java.lang.Math.round(machineState.getMemoryUsagePercentage() * 100.D))
//    );
//
//    binding.serverSummarySwapUsageTextView.setText(
//        String.format(Locale.US, "%.1f", machineState.getSwapUsagePercentage())
//    );
//    binding.serverSummarySwapUsageProgressBar.setProgress(
//        Math.cropToInteger(java.lang.Math.round(machineState.getSwapUsagePercentage()))
//    );
//  }
//
//  private void updateLoadAverages(@NotNull final float[] loadAverages) {
//    if (loadAverages.length != LOAD_AVERAGE_AMOUNT) {
//      throw new IllegalArgumentException(String.format(Locale.US, "There should always be %d load averages.",
//          LOAD_AVERAGE_AMOUNT));
//    }
//
//    binding.serverSummaryLoadAverage1m.setText(String.format(Locale.US, "%.2f", loadAverages[0]));
//    binding.serverSummaryLoadAverage5m.setText(String.format(Locale.US, "%.2f", loadAverages[1]));
//    binding.serverSummaryLoadAverage15m.setText(String.format(Locale.US, "%.2f", loadAverages[2]));
//  }
}
