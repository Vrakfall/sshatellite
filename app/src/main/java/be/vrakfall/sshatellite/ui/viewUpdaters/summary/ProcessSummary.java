package be.vrakfall.sshatellite.ui.viewUpdaters.summary;

import android.annotation.SuppressLint;
import android.util.DisplayMetrics;
import androidx.databinding.ViewDataBinding;
import be.vrakfall.sshatellite.databinding.ProcessSummaryBinding;
import be.vrakfall.sshatellite.network.RemoteMonitorableMachine;
import be.vrakfall.sshatellite.system.monitoring.state.ProcessInfo;
import be.vrakfall.sshatellite.ui.monitorableFragments.ProcessesSummaryFragment;
import be.vrakfall.sshatellite.utils.Reflection;
import be.vrakfall.sshatellite.utils.android.ui.DataBinding;
import be.vrakfall.sshatellite.utils.android.ui.fragments.RxRecyclerFragment;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import com.jakewharton.rxbinding3.view.RxView;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.jetbrains.annotations.Contract;

import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

@Value
@EqualsAndHashCode(callSuper = true)
public class ProcessSummary extends Summary {
  //==== Static variables ====//

  public static final int TYPE_NUMBER = getNewTypeNumber();

  //==== > Error messages ====//

  //==== Attributes ====//

  private long id;

  @NotNull
  private String command;

  private float cpuUsagePercentage;

  private final AtomicBoolean isListenerBound = new AtomicBoolean(false);

  private final ProcessInfo processInfo;

  // TODO: 2/04/19 This might need a change in case of screen rotation or split for example <- Handle those cases
  @NotNull
  private final DisplayMetrics displayMetrics;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  public ProcessSummary(final long associatedMachineId,
                        @NotNull final ProcessInfo processInfo,
                        @NotNull final DisplayMetrics displayMetrics
  ) {
    super(associatedMachineId);
    this.processInfo = processInfo;
    this.id = processInfo.getId();
    this.command = processInfo.getCommand();
    this.cpuUsagePercentage = processInfo.getWeightedCPUUsage();
    this.displayMetrics = displayMetrics;
  }

  //==== Getters and Setters ====//

  @NotNull
  @Contract("_ -> new")
  public ProcessSummary with(@NotNull final ProcessInfo processInfo) {
    return new ProcessSummary(getAssociatedMachineId(), processInfo, getDisplayMetrics());
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  @SuppressLint({"RxLeakedSubscription", "RxSubscribeOnError", "CheckResult"})
  private void bindListener(
      @NotNull final ProcessSummaryBinding binding,
      @NotNull final ProcessesSummaryFragment.OnFragmentInteractionListener listener
  ) {
    if (this.getAssociatedMachineId() != RemoteMonitorableMachine.IRRELEVANT.getId() && !getIsListenerBound().getAndSet(true)) {
      RxView.clicks(binding.processSummarySuspendButton)
          .subscribe(unit -> listener.onProcessSuspendButtonClick(getAssociatedMachineId(), this.getProcessInfo()));
      RxView.clicks(binding.processSummaryKillButton)
          .subscribe(unit -> listener.onProcessKillButtonClick(getAssociatedMachineId(), this.getProcessInfo()));
      RxView.clicks(binding.processSummaryPauseAndResumeButton)
          .subscribe(unit -> listener.onProcessPauseResumeButtonClick(getAssociatedMachineId(), this.getProcessInfo()));
      RxView
          .clicks(binding.processSummaryMoreActionsButton)
          .subscribe(unit -> listener.onMoreActionsButtonClick(getAssociatedMachineId(), this.getProcessInfo()));
    }
  }

  //==== Object Methods ====//

  @Contract(pure = true)
  @Override
  public int getTypeNumber() {
    return TYPE_NUMBER;
  }

  @SuppressWarnings("WeakerAccess")
  protected void updateView(@NotNull final ProcessSummaryBinding binding) {
    binding.processSummaryCommand.setText(String.format(Locale.US, "%d: %s", getId(), getCommand()));
    binding.processSummaryQuickData.setText(String.format(
        Locale.US, "%.1f%%", getCpuUsagePercentage()));
  }

  @Override
  public void updateView(@NotNull final ViewDataBinding binding) {
    DataBinding.ensureType(ProcessSummaryBinding.class, binding);
    updateView(((ProcessSummaryBinding) binding));
  }

  @Override
  public void updateView(@NotNull final ViewDataBinding binding,
                         @NotNull final RxRecyclerFragment.OnRxRecyclerFragmentInteractionListener listener) {
    updateView(binding);
    Reflection.ensureType(ProcessesSummaryFragment.OnFragmentInteractionListener.class, listener);
    bindListener(((ProcessSummaryBinding) binding),
        ((ProcessesSummaryFragment.OnFragmentInteractionListener) listener));
  }
}
