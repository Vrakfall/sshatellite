package be.vrakfall.sshatellite.ui.viewUpdaters.summary;

import androidx.databinding.ViewDataBinding;
import be.vrakfall.sshatellite.databinding.MemorySummaryBinding;
import be.vrakfall.sshatellite.system.monitoring.state.MachineState;
import be.vrakfall.sshatellite.utils.android.ui.DataBinding;
import be.vrakfall.sshatellite.utils.android.ui.fragments.RxRecyclerFragment;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.val;
import org.jetbrains.annotations.Contract;

import static be.vrakfall.sshatellite.utils.android.ui.ViewUpdater.update;

@Value
@EqualsAndHashCode(callSuper = true)
public class MemorySummary extends Summary {
  //==== Static variables ====//

  public static final int TYPE_NUMBER = getNewTypeNumber();

  public static final MemorySummary PLACEHOLDER = MemorySummary.builder().build();

  //==== > Error messages ====//

  //==== Attributes ====//

  /**
   * Percentage of usage of the ram.
   * Stored between 0 (0%) and 100 (100%).
   * The higher precision WHEN DISPLAYED is 4 digits.
   * Examples: 0.01%, 99.99% or 100%
   */
  private float ramUsagePercentage;

  /**
   * Percentage of usage of the swap.
   * Stored between 0 (0%) and 100 (100%).
   * The higher precision WHEN DISPLAYED is 4 digits.
   * Examples: 0.01%, 99.99% or 100%
   */
  private float swapUsagePercentage;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  @Builder
  public MemorySummary(final long associatedMachineId,
                       @Nullable final MachineState machineState
  ) {
    super(associatedMachineId);
    if (machineState == null) {
      this.ramUsagePercentage = PLACEHOLDER_USAGE_PERCENTAGE;
      this.swapUsagePercentage = PLACEHOLDER_USAGE_PERCENTAGE;
    }
    else {
      this.ramUsagePercentage = machineState.getMemoryUsagePercentage();
      this.swapUsagePercentage = machineState.getSwapUsagePercentage();
    }
  }

  //==== Getters and Setters ====//

  @NotNull
  @Contract("_ -> new")
  public MemorySummary with(@NotNull final MachineState machineState) {
    return new MemorySummary(getAssociatedMachineId(), machineState);
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @Override
  public int getTypeNumber() {
    return TYPE_NUMBER;
  }

  @Override
  public void updateView(@NotNull final ViewDataBinding binding) {
    DataBinding.ensureType(MemorySummaryBinding.class, binding);
    final val memorySummaryBinding = ((MemorySummaryBinding) binding);
    // TODO: 21/01/19 Parametrize the precision
    update(memorySummaryBinding.memorySummaryRamUsagePercentage, getRamUsagePercentage(), 1);
    update(memorySummaryBinding.memorySummarySwapUsagePercentage, getSwapUsagePercentage(), 1);

    // Temporary fix to these text sizes
    memorySummaryBinding.memorySummaryRamUsagePercentage.percentage.setTextSize(14);
    memorySummaryBinding.memorySummarySwapUsagePercentage.percentage.setTextSize(14);
  }

  @Override
  public void updateView(@NotNull final ViewDataBinding binding, final RxRecyclerFragment.OnRxRecyclerFragmentInteractionListener listener) {
    updateView(binding);
    // This doesn't have a listener yet
  }
}
