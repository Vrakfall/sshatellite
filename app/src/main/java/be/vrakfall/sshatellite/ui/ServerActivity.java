package be.vrakfall.sshatellite.ui;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.service.notification.StatusBarNotification;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.app.NotificationCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import be.vrakfall.sshatellite.Parameters;
import be.vrakfall.sshatellite.R;
import be.vrakfall.sshatellite.databinding.ActivityServerBinding;
import be.vrakfall.sshatellite.databinding.AppBarServerBinding;
import be.vrakfall.sshatellite.network.RemoteMonitorableMachine;
import be.vrakfall.sshatellite.network.RemoteShellAndSFTPEnvironment;
import be.vrakfall.sshatellite.network.ssh.OldSSHAuthenticationParameters;
import be.vrakfall.sshatellite.network.ssh.sshj.SSHJConnection;
import be.vrakfall.sshatellite.system.monitoring.state.MachineState;
import be.vrakfall.sshatellite.system.monitoring.state.ProcessInfo;
import be.vrakfall.sshatellite.ui.monitorableFragments.MonitorableSummaryFragment;
import be.vrakfall.sshatellite.ui.monitorableFragments.ProcessDetailsFragment;
import be.vrakfall.sshatellite.ui.monitorableFragments.ProcessesSummaryFragment;
import be.vrakfall.sshatellite.utils.android.Permissions;
import be.vrakfall.sshatellite.utils.android.ui.ExtendedAppCompatActivity;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.files.AndroidApplicationFilesManager;
import com.google.android.material.navigation.NavigationView;
import io.reactivex.*;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.Subject;
import lombok.val;
import lombok.var;
import net.schmizz.sshj.common.Buffer;
import net.schmizz.sshj.common.KeyType;

import java.io.File;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.util.concurrent.CancellationException;

import static be.vrakfall.sshatellite.utils.io.ObjectMapperDispenser.YAML_OBJECT_MAPPER;

@TargetApi(Build.VERSION_CODES.O)
@RequiresApi(Build.VERSION_CODES.O)
public class ServerActivity
    extends ExtendedAppCompatActivity
    implements MonitorableSummaryFragment.OnFragmentInteractionListener,
    NavigationView.OnNavigationItemSelectedListener, ProcessesSummaryFragment.OnFragmentInteractionListener,
    ProcessDetailsFragment.OnFragmentInteractionListener {
  //==== Static variables ====//

  public static final String TAG = ServerActivity.class.getSimpleName();

  private static final int SOFIAKONJ_INDEX = 0;

  private static final String SOFIAKONJ_TITLE = "sofiakonj - Sshatellite";

  private static final int BEE_INDEX = 1;

  private static final String BEE_TITLE = "bee - Sshatellite";

  final static NotificationChannel notificationChannel = new NotificationChannel("1", "ServerAlert",
      NotificationManager.IMPORTANCE_HIGH);

  @Nullable
  private NotificationManager notificationManager;

  //==== Attributes ====//

  private ActivityServerBinding activityServerBinding;
  private AppBarServerBinding appBarBinding;
  private String defaultTitle;

  private final AndroidApplicationFilesManager localApplicationFilesManager = AndroidApplicationFilesManager.builder()
      .contextThemeWrapper(this)
      .operatingSystemName(System.getProperty("os.name"))
      .build();

  @SuppressLint("UseSparseArrays")
  private final List<Subject<RemoteMonitorableMachine>> remoteMachineRxSubjects = new ArrayList<>();
  private final List<Observable<MachineState>> machineStatesRx = new ArrayList<>();
  private final List<Single<OldSSHAuthenticationParameters>> authenticationParameters = new ArrayList<>();
  private final List<Observable<RemoteShellAndSFTPEnvironment>> connections = new ArrayList<>();

  private final Map<Long, Integer> idToIndexMap = new HashMap<>();

  private CompositeDisposable subscriptions;

  private CompositeDisposable machineSubscriptions;

  @Nullable
  private ProcessInfo processToBeMonitored;

  //==== > Fragments ====//

  private final MonitorableSummaryFragment monitorableSummaryFragment =
      MonitorableSummaryFragment.newInstance(Parameters.DEFAULT_COLUMN_AMOUNT);
  private final ProcessesSummaryFragment processesSummaryFragment = ProcessesSummaryFragment.newInstance();
  private final ProcessDetailsFragment processDetailsFragment = ProcessDetailsFragment.newInstance();

  //==== Getters and Setters ====//

  //==== Constructors ====//

  //==== Lists' CRUDs ====//

  private void addMachineSubscription(final Disposable subscription) {
    subscriptions.add(subscription);
    machineSubscriptions.add(subscription);
  }

  private long indexToId(final int index) {
    return idToIndexMap.entrySet().stream().filter(entry -> entry.getValue() == index).findFirst()
        .map(Map.Entry::getKey).orElse(-1L);
  }

  //==== Static Methods ====//

  //==== Object Methods ====//

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    activityServerBinding = DataBindingUtil.setContentView(this, R.layout.activity_server);
    appBarBinding = activityServerBinding.appBar;
    setSupportActionBar(appBarBinding.toolbar);

    subscriptions = new CompositeDisposable();
    notificationChannel.setDescription("test channel");
    notificationChannel.enableLights(true);
    notificationChannel.setLightColor(Color.RED);
    this.notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    assert notificationManager != null;
    notificationManager.createNotificationChannel(notificationChannel);

    // TODO: 19/12/18 Add an action to the button with RxBinding to add a server
//    val floatingActionButton = findViewById(R.id.floatingActionButton);
//    activityServerBinding.mainActivityLayout.addDrawerListener();

    final val actionBarDrawerToggle = new ActionBarDrawerToggle(
        this,
        activityServerBinding.mainActivityLayout,
        appBarBinding.toolbar,
        R.string.navigation_drawer_open,
        R.string.navigation_drawer_open
    );
    activityServerBinding.mainActivityLayout.addDrawerListener(actionBarDrawerToggle);
    actionBarDrawerToggle.syncState();

    activityServerBinding.navView.setNavigationItemSelectedListener(this);

    // Set the default title
    this.defaultTitle = getResources().getString(R.string.default_activity_title);

    // Register the fragments
    addFragment(monitorableSummaryFragment);
    addFragment(processesSummaryFragment);
    addFragment(processDetailsFragment);
  }

  @Override
  public void onBackPressed() {
    final val drawerLayout = activityServerBinding.mainActivityLayout;
    if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
      drawerLayout.closeDrawer(GravityCompat.START);
    }
    else {
      super.onBackPressed();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(final Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.server_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(final MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    return super.onOptionsItemSelected(item);
  }

  @Override
  public boolean onNavigationItemSelected(@NotNull final MenuItem menuItem) {
    // TODO: 20/12/18 Handle navigation view item clicks here.
    final int id = menuItem.getItemId();

    if (id == R.id.sofiakonj) {
      // TODO: 4/03/19 Add some fragment-monitorable link switch request here
      showFragment(monitorableSummaryFragment, SOFIAKONJ_TITLE);
      bindServerSummaryWith(SOFIAKONJ_INDEX);
    }
    else if (id == R.id.bee) {
      // TODO: 4/03/19 Add some fragment-monitorable link switch request here
      showFragment(monitorableSummaryFragment, BEE_TITLE);
      bindServerSummaryWith(BEE_INDEX);
    }
    else if (id == R.id.bumblebee) {

    }
    else if (id == R.id.termite) {

    }

    activityServerBinding.mainActivityLayout.closeDrawer(GravityCompat.START);
    return true;
  }

  private Single<OldSSHAuthenticationParameters> loadAuthenticationInfoRx(final int resourceId) {
    // TODO: 16/06/18 Place this in an form
    return Single.create((SingleEmitter<OldSSHAuthenticationParameters> emitter) -> {
      final OldSSHAuthenticationParameters authenticationParameters =
          YAML_OBJECT_MAPPER
              .readValue(
                  getResources().openRawResource(resourceId), OldSSHAuthenticationParameters.class
              );

//      if (Parameters.SHOW_DEBUG_INFO) {
//        Log.d(TAG, ReflectionToStringBuilder.toString(authenticationParameters, ToStringStyle.MULTI_LINE_STYLE));
//        Log.d(TAG, authenticationParameters.getKnownHostsPath());
//      }

      emitter.onSuccess(authenticationParameters);
    });
  }

  private void subscribeToAuthenticationParametersLoading() {
    for (final val authenticationParameter : authenticationParameters) {
      subscriptions.add(
          authenticationParameter.subscribe((authenticationParameters1, throwable) -> {
            if (throwable != null) {
              Log.e(TAG, "subscribeToAuthenticationParametersLoading: ", throwable);
            }
            else {
              Log.d(TAG, "subscribeToAuthenticationParametersLoading: Authentication parameters loaded.");
            }
          })
      );
    }
  }

  private void subscribeToServerConnections() {
    for (final val connectionRx : connections) {
      subscriptions.add(
          connectionRx.subscribe(remoteShellAndSFTPEnvironment ->
                  Log.d(TAG, "subscribeToServerConnections: Server connected!"),
              throwable -> Log.e(TAG, String.format(Locale.US, "onError: Error while trying to connect to the" +
                  "server.\nError message: %s", throwable.getMessage()), throwable),
              () -> Log.d(TAG, "subscribeToServerConnections: This shouldn't happen."))
      );
    }
  }

  private void throwNotification(final int alertType, final float percentage) {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
      NotificationCompat.Builder notification = new NotificationCompat.Builder(this, notificationChannel.getId())
          .setSmallIcon(R.drawable.warning)
          .setContentTitle("Problem on server sofiakonj")
          .setPriority(NotificationCompat.PRIORITY_DEFAULT);
      if (alertType == MonitorableSummaryFragment.RAM_PERCENTAGE_ALERT) {
        notification = notification.setContentText(String.format(Locale.US, "Ram is at %.2f%%!", percentage));
      }
      else {
        notification = notification.setContentText(String.format(Locale.US, "Something is at %.2f%%!", percentage));
      }
      for (final StatusBarNotification n : notificationManager.getActiveNotifications()) {
        if (n.getId() == 1) {
          return;
        }
      }
      notificationManager.notify(1, notification.build());
    }
  }

  private void throwNotification(@NotNull final String programName) {
    @NotNull final int PROGRAM_STOPPED_NOTIFICATION_ID = 2;
    var problemNotification = new NotificationCompat.Builder(this, notificationChannel.getId())
        .setSmallIcon(R.drawable.warning)
        .setContentTitle("Problem on server sofiakonj")
        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
    problemNotification = problemNotification.setContentText(String.format(Locale.US, "Process %s has been stopped!", programName));
    Arrays.stream(notificationManager.getActiveNotifications()).forEach(notification -> {
      if (notification.getId() == PROGRAM_STOPPED_NOTIFICATION_ID) {
        return;
      }
    });
    notificationManager.notify(PROGRAM_STOPPED_NOTIFICATION_ID, problemNotification.build());
  }

  private void startInitialFragment() {
    subscriptions.add(
        RemoteMonitorableMachine.checkAndInstallIfNeededThenReEmit(remoteMachineRxSubjects.get(0))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                this::bindFirstFragmentWith,
                // TODO: 11/12/18 Handle this properly
                throwable -> Log.e(TAG, "startInitialFragment:", throwable)
            )
    );
  }

  private void bindFirstFragmentWith(@NotNull final RemoteMonitorableMachine remoteMonitorableMachine) {
//     Temporarily here
    showFragment(monitorableSummaryFragment, SOFIAKONJ_TITLE);
    bindServerSummaryWith(remoteMonitorableMachine.getId());
  }

  @Override
  protected void onStart() {
    super.onStart();

//    TestDatabase.testTheDatabase(getApplicationContext());

    testSshAndGuiBinding();
  }

  @Nullable private KeyPair retrieveTestKeyPair() throws KeyStoreException, CertificateException, IOException,
      NoSuchAlgorithmException, UnrecoverableEntryException, InvalidKeySpecException {
    val androidKeyStore = KeyStore.getInstance("AndroidKeyStore");
    androidKeyStore.load(null);
    val entry = androidKeyStore.getEntry("test", null);
    if (entry instanceof KeyStore.PrivateKeyEntry) {
      val keyPair = new KeyPair(androidKeyStore.getCertificate(
          "test").getPublicKey(), ((KeyStore.PrivateKeyEntry) entry).getPrivateKey());
      Log.d(TAG, String.format(Locale.US, "retrieveTestKeyPair: pub key type: %s", KeyType.fromKey(keyPair.getPublic()).toString()));
      Log.d(TAG, String.format(Locale.US, "retrieveTestKeyPair: private key type: %s",
          KeyType.fromKey(keyPair.getPrivate()).toString()));

      val keyFactory = KeyFactory.getInstance("RSA");
//      val publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(androidKeyStore.getCertificate("test").getEncoded()));
      val publicKey = androidKeyStore.getCertificate("test").getPublicKey();
      boolean isRsa = publicKey instanceof RSAPublicKey ? true : false;
      Log.d(TAG, String.format(Locale.US, "retrieveTestKeyPair: is rsa: %b", isRsa));
      val compactData = new Buffer.PlainBuffer().putPublicKey(publicKey).getCompactData();
      Log.d(TAG, String.format(Locale.US, "retrieveTestKeyPair:  encoded: %s",
          Base64.encodeToString(androidKeyStore.getCertificate("test").getEncoded(), Base64.DEFAULT)));
      Log.d(TAG, String.format(Locale.US, "retrieveTestKeyPair: Compact data: %s", Base64.encodeToString(compactData,
          Base64.DEFAULT
      )));
      return keyPair;
    }
    Log.d(TAG, "retrieveTestKeyPair: KeyPair Not found!");
    return null;
  }

  /**
   * From https://developer.android.com/training/articles/keystore#GeneratingANewPrivateKey
   * @throws NoSuchAlgorithmException
   * @throws NoSuchProviderException
   * @throws InvalidAlgorithmParameterException
   */
  private KeyPair generateTestKeyPair() throws
      NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
    KeyPairGenerator kpg = KeyPairGenerator.getInstance(
        KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore");
    kpg.initialize(new KeyGenParameterSpec.Builder(
        "test",
        KeyProperties.PURPOSE_SIGN | KeyProperties.PURPOSE_VERIFY | KeyProperties.PURPOSE_DECRYPT |
            KeyProperties.PURPOSE_ENCRYPT
    )
        .setDigests(KeyProperties.DIGEST_SHA256,
            KeyProperties.DIGEST_SHA512)
        .setKeySize(4096)
        .build());

    val keyPair = kpg.generateKeyPair();
    Log.d(TAG, String.format(Locale.US, "generateTestKeyPair: %s", keyPair));
    Log.d(TAG, String.format(Locale.US, "generateTestKeyPair: %s", keyPair.getPublic().toString()));
    Log.d(TAG, String.format(Locale.US, "generateTestKeyPair: %s", keyPair.getPrivate()));
    return keyPair;
  }

  private void testSshAndGuiBinding() {
    final val sofiakonjRemoteDir = new File("/home/vrakfall", getApplicationName().toLowerCase(Locale.US));
    final val tozukaRemoteDir = new File("/mnt/mainVolume2.0/users/vrakfall", getApplicationName().toLowerCase(Locale.US));
    final val beeRemoteDir = new File("/home/nfs/lecocq", getApplicationName().toLowerCase(Locale.US));

    // Ask to get the required Android permissions
    Permissions.checkForPermissions(this, activityServerBinding.mainActivityLayout);

    // Load the authentication Infos (For testing purposes only).
    pipeAuthentificationInfo(R.raw.authentication_info_sofiakonj);
//    pipeAuthentificationInfo(R.raw.authentication_info_bee);

    subscribeToAuthenticationParametersLoading();

    // Connect to the servers
    pipeServerConnection(R.raw.authentication_info_sofiakonj);
//    pipeServerConnection(R.raw.authentication_info_bee);

    subscribeToServerConnections();

    pipeRemoteMachine(sofiakonjRemoteDir, 0);
//    pipeRemoteMachine(beeRemoteDir, 1);
  }

  private void pipeRemoteMachine(@NotNull final File projectRemoteDir, final int i) {
    remoteMachineRxSubjects.add(RemoteMonitorableMachine.createRemoteMonitorableMachineAndConnect(
        connections.get(i), projectRemoteDir.getAbsolutePath(), localApplicationFilesManager,
        (Notification<RemoteMonitorableMachine> notification) -> {
          final val machine = notification.getValue();
          idToIndexMap.put(machine.getId(), i);
          if (i == 0) {
            startInitialFragment();
          }
        }
    ));
  }

  private void pipeAuthentificationInfo(final int resourceId) {
    authenticationParameters.add(
        loadAuthenticationInfoRx(resourceId)
            .cache()
            .subscribeOn(Schedulers.io())
    );
  }

  private void pipeServerConnection(final int resourceId) {
    connections.add(
        SSHJConnection.createConnectionHandlerAndConnect(loadAuthenticationInfoRx(resourceId), null)
    );
  }

  @Override
  public void onRequestPermissionsResult(
      final int requestCode, @NotNull final String[] permissions, @NotNull final int[] grantResults
  ) {
    //If the `onRequestPermissionsResult from the caller's superclass has to be called, then call it.
    if (Permissions.onRequestPermissionsResult(requestCode, permissions, grantResults)) {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  @Override
  public void onCPUSummaryClick(final long machineId) {
    showFragment(processesSummaryFragment, null);
    bindProcessesSummaryWith(machineId);
  }

  private int bindServerSummaryWith(final long machineId) {
    final val i = idToIndexMap.get(machineId);
    assert i != null;
    bindServerSummaryWith(i);
    return i;
  }

  private void bindServerSummaryWith(final int index) {
    subscriptions.add(
        RemoteMonitorableMachine.checkAndInstallIfNeededThenReEmit(
            remoteMachineRxSubjects.get(index))
            // Publishers need to be turned into Observables first! They can't be Observables as is.
            .flatMap(RemoteMonitorableMachine::getMachineStateRx)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe((MachineState machineState) -> {
                  if (machineState.getMemoryUsagePercentage() > 90F) {
                    throwNotification(MonitorableSummaryFragment.RAM_PERCENTAGE_ALERT,
                        machineState.getMemoryUsagePercentage());
                  }
                  final val processDetailsList = machineState.getProcessesDetails();
                  if ((processToBeMonitored != null) &&
                      ((processDetailsList == null) ||
                          machineState.getProcessesDetails().entrySet().stream()
                              .noneMatch((Map.Entry<Integer, ProcessInfo> processDetail) ->
                              {
                                assert processToBeMonitored != null;
                                return processDetail.getValue().getId() == processToBeMonitored.getId()
                                    && processDetail.getValue().getCommand().equals(
                                    processToBeMonitored.getCommand());
                              })
                      )
                  ) {
                    throwNotification(processToBeMonitored.getId() + " " + processToBeMonitored.getCommand());
                  }
                  monitorableSummaryFragment.updateWith(machineState);
                },
                // TODO: 11/12/18 Connection error? Retry or ask the user.
                (Throwable throwable) -> {
                  if (!(throwable instanceof CancellationException)) {
                    Log.e(TAG, "bindServerSummaryWith:", throwable);
                  }
                }
            )
    );
  }

  private void bindProcessesSummaryWith(final long machineId) {
    final val i = idToIndexMap.get(machineId);
    assert i != null;
    subscriptions.add(
        RemoteMonitorableMachine.checkAndInstallIfNeededThenReEmit(
            remoteMachineRxSubjects.get(i)
        )
            .flatMap(RemoteMonitorableMachine::getMachineStateRx)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                processesSummaryFragment::updateWith, throwable -> Log.e(TAG, "bindProcessesSummaryWith: ", throwable)
            )
    );
  }

  private void bindProcessDetailsFragmentWith(final long machineId, @NotNull final ProcessInfo processInfo) {
    val index = idToIndexMap.get(machineId);
    assert index != null;
    subscriptions.add(RemoteMonitorableMachine.checkAndInstallIfNeededThenReEmit(
        remoteMachineRxSubjects.get(index)
        )
            .flatMap(RemoteMonitorableMachine::getMachineStateRx)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                machineState -> processDetailsFragment.updateWith(processInfo),
                throwable -> Log.e(TAG, "bindProcessDetailsFragmentWith: ", throwable)
            )
    );
  }

  @SuppressLint({"CheckResult", "RxLeakedSubscription", "RxSubscribeOnError"})
  @Override
  public void onProcessKillButtonClick(final long associatedMachineId, @NotNull final ProcessInfo processInfo) {
    val index = idToIndexMap.get(associatedMachineId);
    assert index != null;
    remoteMachineRxSubjects.get(index)
        .flatMapSingle(remoteMonitorableMachine -> remoteMonitorableMachine.killProcess(processInfo))
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(integer -> Log.d(TAG, String.format(Locale.US, "onProcessKillButtonClick: status code: %d", integer)));
  }

  @SuppressLint({"RxLeakedSubscription", "RxSubscribeOnError", "CheckResult"})
  @Override
  public void onProcessSuspendButtonClick(long associatedMachineId, @NotNull ProcessInfo processInfo) {
    val index = idToIndexMap.get(associatedMachineId);
    assert index != null;
    remoteMachineRxSubjects.get(index)
        .flatMapSingle(remoteMonitorableMachine -> remoteMonitorableMachine.terminateProcess(processInfo))
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(integer -> Log.d(TAG, String.format(Locale.US, "onProcessKillButtonClick: status code: %d", integer)));
  }

  @SuppressLint({"RxLeakedSubscription", "RxSubscribeOnError", "CheckResult"})
  @Override
  public void onProcessPauseResumeButtonClick(long associatedMachineId, @NotNull ProcessInfo processInfo) {
    val index = idToIndexMap.get(associatedMachineId);
    assert index != null;
    remoteMachineRxSubjects.get(index)
        .flatMapSingle(remoteMonitorableMachine -> remoteMonitorableMachine.suspendProcess(processInfo))
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(integer -> Log.d(TAG, String.format(Locale.US, "onProcessKillButtonClick: status code: %d", integer)));
  }

  @Override
  public void onMoreActionsButtonClick(long associatedMachineId, @NotNull ProcessInfo processInfo) {
    showFragment(processDetailsFragment, null);
    bindProcessDetailsFragmentWith(associatedMachineId, processInfo);
  }

  @SuppressLint({"RxLeakedSubscription", "RxSubscribeOnError"})
  public void onSelectedAlerts(final int[] indices, @NotNull final ProcessInfo processInfo) {
    for (final int i : indices) {
      if (i == 0) {
        processToBeMonitored = processInfo;
      }
      else if (i == 3) {
        connections.get(0)
            .map(remoteShellAndSFTPEnvironment -> {
              // TODO: 27/02/19 Clean this mess and put it in RemoteMonitorableMachine or somthing like that to keep that abstraction and the dynamic file pathing
              return remoteShellAndSFTPEnvironment.execute(
                  "sh -c 'PYTHONPATH=\"/home" +
                      "/vrakfall/sshatellite\" /home/vrakfall/sshatellite/venv/bin/python " +
                      "/home/vrakfall/sshatellite/sshatellite_backend/main/kill.py " + processInfo.getId() + " " +
                      processInfo.getCommand() + "'"
              )
                  .waitExecutionAndGetExitStatus();
            })
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(integer -> Log.d(TAG, String.format(Locale.US, "onSelectedAlerts: status: %d", integer)));
      }
    }
  }
}
