package be.vrakfall.sshatellite.ui.kotlinkbridges

import be.vrakfall.sshatellite.R
import be.vrakfall.sshatellite.system.monitoring.state.ProcessInfo
import be.vrakfall.sshatellite.ui.ServerActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItemsMultiChoice

class DialogBridge {
    companion object {
        fun processParametersDialog(windowContext: ServerActivity, processInfo: ProcessInfo) {
            MaterialDialog(windowContext).show {
                title(R.string.process_alert_choices_title)
                message(text = "Process: " + processInfo.command)
                listItemsMultiChoice(R.array.process_alert_choices) { dialog, indices, items ->
                    windowContext.onSelectedAlerts(indices, processInfo)
                }
                positiveButton(R.string.process_alert_choices_select)
            }
        }
    }
}
