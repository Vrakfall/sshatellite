package be.vrakfall.sshatellite.ui.errors;

import android.annotation.TargetApi;
import android.os.Build;
import androidx.annotation.RequiresApi;

public class MissingFragmentListenerException extends RuntimeException {
  public MissingFragmentListenerException() {
    super();
  }

  public MissingFragmentListenerException(String message) {
    super(message);
  }

  public MissingFragmentListenerException(String message, Throwable cause) {
    super(message, cause);
  }

  public MissingFragmentListenerException(Throwable cause) {
    super(cause);
  }

  @TargetApi(Build.VERSION_CODES.N)
  @RequiresApi(Build.VERSION_CODES.N)
  protected MissingFragmentListenerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
