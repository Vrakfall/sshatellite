package be.vrakfall.sshatellite.ui.viewUpdaters.summary;

import be.vrakfall.sshatellite.utils.android.ui.ViewUpdater;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.concurrent.atomic.AtomicInteger;

@AllArgsConstructor
public abstract class Summary implements ViewUpdater {
  //==== Static variables ====//

  public static final float PLACEHOLDER_USAGE_PERCENTAGE = 0.F;

  public static final long PLACEHOLDER_TRANSFER_SPEED = 0L;

  private static final AtomicInteger typeAmount = new AtomicInteger();

  //==== > Error messages ====//

  //==== Attributes ====//

  @Getter(AccessLevel.PACKAGE)
  private final long associatedMachineId;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  protected Summary(final Summary other) {
    this.associatedMachineId = other.associatedMachineId;
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  protected static synchronized int getNewTypeNumber() {
    return typeAmount.getAndIncrement();
  }

  //==== Object Methods ====//

  public abstract int getTypeNumber();
}
