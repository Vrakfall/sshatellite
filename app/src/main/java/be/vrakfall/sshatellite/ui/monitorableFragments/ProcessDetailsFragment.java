package be.vrakfall.sshatellite.ui.monitorableFragments;

import android.content.Context;
import android.os.Bundle;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import be.vrakfall.sshatellite.R;
import be.vrakfall.sshatellite.databinding.FragmentProcessDetailsBinding;
import be.vrakfall.sshatellite.system.monitoring.state.ProcessInfo;
import be.vrakfall.sshatellite.utils.android.ui.ViewUpdater;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import lombok.val;

import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProcessDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProcessDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProcessDetailsFragment extends Fragment {
  private OnFragmentInteractionListener mListener;

  @Nullable
  private FragmentProcessDetailsBinding binding;

  public ProcessDetailsFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment ProcessDetailsFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static ProcessDetailsFragment newInstance() {
    ProcessDetailsFragment fragment = new ProcessDetailsFragment();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    this.binding = DataBindingUtil.inflate(inflater, R.layout.fragment_process_details, container, false);
    return binding.getRoot();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    }
    else {
      throw new RuntimeException(context.toString()
          + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   * <p>
   * See the Android Training lesson <a href=
   * "http://developer.android.com/training/basics/fragments/communicating.html"
   * >Communicating with Other Fragments</a> for more information.
   */
  public interface OnFragmentInteractionListener {
  }

  public void updateWith(@NotNull final ProcessInfo processInfo) {
    binding.processDetailsMainData.setText(String.format(
        Locale.US, "Working directory: %s\n" +
        "Full command: %s\n" +
            "PID: %s - User: %s - Creation time: %s",
        processInfo.getWorkingDirectory(),
        processInfo.getFullCommand(),
        processInfo.getId(),
        processInfo.getUser(),
        processInfo.getCreationTime()
        ));
    ViewUpdater.update(binding.processDetailsCurrentPerThreadCpuUsage, processInfo.getWeightedCPUUsage(), 1);
  }
}
