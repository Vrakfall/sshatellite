package be.vrakfall.sshatellite.ui.viewUpdaters.summary;

import androidx.databinding.ViewDataBinding;
import be.vrakfall.sshatellite.databinding.NetworkSummaryBinding;
import be.vrakfall.sshatellite.system.monitoring.state.MachineState;
import be.vrakfall.sshatellite.utils.android.ui.DataBinding;
import be.vrakfall.sshatellite.utils.android.ui.fragments.RxRecyclerFragment;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.units.Bytes;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.val;
import org.jetbrains.annotations.Contract;

@Value
@EqualsAndHashCode(callSuper = true)
public class NetworkSummary extends Summary {
  //==== Static variables ====//

  public static final int TYPE_NUMBER = getNewTypeNumber();

  public static final NetworkSummary PLACEHOLDER = NetworkSummary.builder().build();

  //==== > Error messages ====//

  //==== Attributes ====//

  /**
   * Download speed expressed in bytes per second.
   */
  private long downloadSpeed;

  /**
   * Upload speed expressed in bytes per second.
   */
  private long uploadSpeed;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  @Builder
  public NetworkSummary(final long associatedMachineId,
                        @Nullable final MachineState previousMachineState, @Nullable final MachineState currentMachineState
  ) {
    super(associatedMachineId);
    if (previousMachineState == null || currentMachineState == null) {
      this.downloadSpeed = PLACEHOLDER_TRANSFER_SPEED;
      this.uploadSpeed = PLACEHOLDER_TRANSFER_SPEED;
    }
    else {
      this.downloadSpeed = currentMachineState.downloadSpeedTotal(previousMachineState);
      this.uploadSpeed = currentMachineState.uploadSpeedTotal(previousMachineState);
    }
  }

  //==== Getters and Setters ====//

  @NotNull
  @Contract("_, _ -> new")
  public NetworkSummary with(@NotNull final MachineState previousState, @NotNull final MachineState currentState) {
    return new NetworkSummary(getAssociatedMachineId(), previousState, currentState);
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @Override
  public int getTypeNumber() {
    return TYPE_NUMBER;
  }

  @Override
  public void updateView(@NotNull final ViewDataBinding binding) {
    DataBinding.ensureType(NetworkSummaryBinding.class, binding);
    final val networkSummaryBinding = ((NetworkSummaryBinding) binding);

    networkSummaryBinding.networkSummaryDownloadSpeed.setText(Bytes.readableByteAmount(getDownloadSpeed()));
    networkSummaryBinding.networkSummaryUploadSpeed.setText(Bytes.readableByteAmount(getUploadSpeed()));
  }

  @Override
  public void updateView(
      @NotNull final ViewDataBinding binding, final RxRecyclerFragment.OnRxRecyclerFragmentInteractionListener listener
  ) {
    updateView(binding);
    // This doesn't have a listener yet
  }
}
