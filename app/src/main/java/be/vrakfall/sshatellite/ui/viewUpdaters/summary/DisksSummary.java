package be.vrakfall.sshatellite.ui.viewUpdaters.summary;

import androidx.databinding.ViewDataBinding;
import be.vrakfall.sshatellite.databinding.DisksSummaryBinding;
import be.vrakfall.sshatellite.system.monitoring.state.MachineState;
import be.vrakfall.sshatellite.utils.android.ui.DataBinding;
import be.vrakfall.sshatellite.utils.android.ui.fragments.RxRecyclerFragment;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.val;
import org.jetbrains.annotations.Contract;

import java.util.Locale;

import static be.vrakfall.sshatellite.utils.android.ui.ViewUpdater.update;

@Value
@EqualsAndHashCode(callSuper = true)
public class DisksSummary extends Summary {
  //==== Static variables ====//

  public static final int TYPE_NUMBER = getNewTypeNumber();

  public static final DisksSummary PLACEHOLDER = DisksSummary.builder().build();

  //==== > Error messages ====//

  public static final String DATA_BINDING_IS_OF_WRONG_TYPE_ERROR_MESSAGE = String.format(Locale.US, "The data binding" +
          " object associated with this object has to be of type %s but %%s was given instead.",
      DisksSummary.class.getSimpleName());

  //==== Attributes ====//

  /**
   * Percentage of usage of the most used disk.
   * Stored between 0 (0%) and 100 (100%).
   * The higher precision WHEN DISPLAYED is 4 digits.
   * Examples: 0.01%, 99.99% or 100%
   */
  private float disksHighestUsagePercentage;

  /**
   * Percentage of the average usage of the disks.
   * Stored between 0 (0%) and 100 (100%).
   * The higher precision WHEN DISPLAYED is 4 digits.
   * Examples: 0.01%, 99.99% or 100%
   */
  private float disksAverageUsagePercentageTitle;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  @Builder
  public DisksSummary(final long associatedMachineId,
                      @Nullable final MachineState machineState
  ) {
    super(associatedMachineId);
    if (machineState == null) {
      this.disksHighestUsagePercentage = PLACEHOLDER_USAGE_PERCENTAGE;
      this.disksAverageUsagePercentageTitle = PLACEHOLDER_USAGE_PERCENTAGE;
    }
    else {
      this.disksHighestUsagePercentage = (float) machineState.diskPartitionUsageMaxPercentage();
      this.disksAverageUsagePercentageTitle = (float) machineState.diskPartitionUsageAveragePercentage();
    }
  }

  //==== Getters and Setters ====//

  @NotNull
  @Contract("_ -> new")
  public DisksSummary with(@NotNull final MachineState machineState) {
    return new DisksSummary(getAssociatedMachineId(), machineState);
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @Override
  public int getTypeNumber() {
    return TYPE_NUMBER;
  }

  @Override
  public void updateView(@NotNull final ViewDataBinding binding) {
    DataBinding.ensureType(DisksSummaryBinding.class, binding);
    final val memorySummaryBinding = ((DisksSummaryBinding) binding);
    // TODO: 21/01/19 Parametrize the precision
    update(memorySummaryBinding.disksSummaryHighestUsagePercentage, getDisksHighestUsagePercentage(), 1);
    update(memorySummaryBinding.disksSummaryAverageUsagePercentage, getDisksAverageUsagePercentageTitle(), 1);

    // Temporary fix to these text sizes
    memorySummaryBinding.disksSummaryHighestUsagePercentage.percentage.setTextSize(14);
    memorySummaryBinding.disksSummaryAverageUsagePercentage.percentage.setTextSize(14);
  }

  @Override
  public void updateView(@NotNull final ViewDataBinding binding, final RxRecyclerFragment.OnRxRecyclerFragmentInteractionListener listener) {
    updateView(binding);
    // This doesn't have a listener yet
  }
}
