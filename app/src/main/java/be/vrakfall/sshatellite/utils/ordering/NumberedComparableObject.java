package be.vrakfall.sshatellite.utils.ordering;


import lombok.*;
import lombok.experimental.FieldDefaults;
import org.jetbrains.annotations.NotNull;

// The following is equivalent to @Value (immutable classes) but without `final` so we can inherit from it.
@ToString
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
//@SuperBuilder
public abstract class NumberedComparableObject<T extends Comparable<? super T>> extends AbstractNumberedObject {
  //==== Static variables ====//

  public static final String TAG = NumberedComparableObject.class.getSimpleName();

  //==== > Error messages ====//

  //==== Attributes ====//

  @NotNull
  @NonNull
  private T object;

  private boolean ascendingNumberOrder;

  //==== Constructors ====//

  public NumberedComparableObject(final long number, @NotNull final T object, final boolean ascendingNumberOrder) {
    super(number);
    this.object = object;
    this.ascendingNumberOrder = ascendingNumberOrder;
  }

  public NumberedComparableObject(final long number, @NotNull final T object) {
    this(number, object, true);
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
