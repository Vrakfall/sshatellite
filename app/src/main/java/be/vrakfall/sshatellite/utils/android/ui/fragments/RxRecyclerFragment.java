package be.vrakfall.sshatellite.utils.android.ui.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import be.vrakfall.sshatellite.utils.android.ui.ViewUpdater;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import com.minimize.android.rxrecycleradapter.SimpleViewHolder;
import com.minimize.android.rxrecycleradapter.TypesViewHolder;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.val;

import java.util.MissingFormatArgumentException;
import java.util.function.BiConsumer;

public abstract class RxRecyclerFragment extends Fragment {
  //==== Static variables ====//

  protected static final int DEFAULT_COLUMN_AMOUNT = 2;

  //==== > Fragment parameter names ====//

  private static final String ARG_COLUMN_AMOUNT = "columnAmount";

  //==== > Error messages ====//

  //==== Attributes ====//

  private CompositeDisposable subscriptions;

  private boolean isListenerBound;

  //==== > Fragment specific ====//

  private OnRxRecyclerFragmentInteractionListener listener;

  //==== > > Fragment parameters ====//

  @Getter(AccessLevel.PROTECTED)
  private int columnAmount = DEFAULT_COLUMN_AMOUNT;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  public interface OnRxRecyclerFragmentInteractionListener {
  }

  //==== > Classes ====//

  //==== Constructors ====//

  //==== Getters and Setters ====//

  private void setColumnAmount(final int columnAmount) {
    if (columnAmount > 0) {
      this.columnAmount = columnAmount;
    }
  }

  //==== Lists' CRUDs ====//

  protected boolean addSubscription(@NotNull final Disposable disposable) {
    return subscriptions.add(disposable);
  }

  //==== Static Methods ====//

  protected static <T extends RxRecyclerFragment> T bundleInitialization(@NotNull final T fragment,
                                                                         final int columnAmount) {
    final Bundle args = new Bundle();
    args.putInt(ARG_COLUMN_AMOUNT, columnAmount);
    fragment.setArguments(args);
    return fragment;
  }

  @TargetApi(Build.VERSION_CODES.N)
  @RequiresApi(Build.VERSION_CODES.N)
  public static void extractViewUpdaterAndBinding(@NotNull final RecyclerView.ViewHolder viewHolder,
                                                  final BiConsumer<ViewUpdater, ViewDataBinding> consumer) {
    // This triple-if statement has to be written because the original lib's viewHolders don't extend an abstract
    // class nor implement an interface to make it possible for it to be more semantically correct.
    final ViewDataBinding binding;
    ViewUpdater viewUpdater = null;
    if (viewHolder instanceof SimpleViewHolder) {
      final val simpleViewHolder = ((SimpleViewHolder) viewHolder);
      binding = simpleViewHolder.getViewDataBinding();
      final val item = simpleViewHolder.getItem();
      if (item instanceof ViewUpdater) {
        viewUpdater = (ViewUpdater) item;
      }
    }
    else if (viewHolder instanceof TypesViewHolder) {
      final val typesViewHolder = ((TypesViewHolder) viewHolder);
      binding = typesViewHolder.getViewDataBinding();
      final val item = typesViewHolder.getItem();
      if (item instanceof ViewUpdater) {
        viewUpdater = (ViewUpdater) item;
      }
    }
    else {
      // TODO: 25/01/19 More details
      // If it happens, it's coder's fault
      throw new IllegalArgumentException();
    }
    assert binding != null;
    assert viewUpdater != null;

    consumer.accept(viewUpdater, binding);
  }

  @TargetApi(Build.VERSION_CODES.N)
  @RequiresApi(api = Build.VERSION_CODES.N)
  public static void bindWithViewUpdater(@NotNull final RecyclerView.ViewHolder viewHolder) {
    extractViewUpdaterAndBinding(viewHolder, (ViewUpdater::updateView));
  }

  @TargetApi(Build.VERSION_CODES.N)
  @RequiresApi(api = Build.VERSION_CODES.N)
  public static void bindWithViewUpdater(@NotNull final RecyclerView.ViewHolder viewHolder,
                                         @NotNull final OnRxRecyclerFragmentInteractionListener listener) {
    extractViewUpdaterAndBinding(viewHolder, (viewUpdater, binding) -> viewUpdater.updateView(binding, listener));
  }

  //==== Object Methods ====//

  @Override
  public void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Get the arguments
    if (getArguments() != null) {
      setColumnAmount(getArguments().getInt(ARG_COLUMN_AMOUNT));
    }

    // Initialize fields
    subscriptions = new CompositeDisposable();

    // The following makes the fragment being retained when the activity is re-created.
    setRetainInstance(true);
  }

  @Override
  public void onAttach(final Context context) {
    super.onAttach(context);
    if (context instanceof OnRxRecyclerFragmentInteractionListener) {
      listener = (OnRxRecyclerFragmentInteractionListener) context;
    }
    else {
      throw new MissingFormatArgumentException(context.toString()
          + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    listener = null;
  }
}
