package be.vrakfall.sshatellite.utils.io.errors;

import java.io.IOException;

public class TooManyEmptyLinesException extends IOException {
  public TooManyEmptyLinesException() {
  }

  public TooManyEmptyLinesException(String message) {
    super(message);
  }

  public TooManyEmptyLinesException(String message, Throwable cause) {
    super(message, cause);
  }

  public TooManyEmptyLinesException(Throwable cause) {
    super(cause);
  }
}
