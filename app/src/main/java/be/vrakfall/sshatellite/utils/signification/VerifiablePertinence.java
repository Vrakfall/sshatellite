package be.vrakfall.sshatellite.utils.signification;

import org.jetbrains.annotations.Contract;

public interface VerifiablePertinence {
  boolean isPertinent();
}
