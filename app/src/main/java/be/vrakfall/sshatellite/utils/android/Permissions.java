package be.vrakfall.sshatellite.utils.android;

import android.Manifest;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import be.vrakfall.sshatellite.R;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import com.google.android.material.snackbar.Snackbar;

public interface Permissions {
  //==== Static variables ====//

  String TAG = Permissions.class.getSimpleName();
  int REQUEST_WRITE_EXTERNAL_STORAGE = 0;

  //==== Static Methods ====//

  static void checkForPermissions(final AppCompatActivity activity, final View view) {
    Log.i(TAG, "Checking if the needed permissions are given.");

    // Check if READ_EXTERNAL_STORAGE permission is already granted.
    if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_CALENDAR)
        != PackageManager.PERMISSION_GRANTED) {
      // Permission is not granted
      Log.i(TAG, "Read permission is not granted yet, asking for it.");

      requestWritePermission(activity, view);
    }
    else {
      // Permission already granted
      Log.i(TAG, "Permissions are already granted.");
    }
  }

  static void requestWritePermission(final AppCompatActivity activity, final View view) {
    // To call only if the permission has not been granted yet

    Log.i(TAG, "Checking if permission rationale is needed.");
    //Check if the user previously denied the permission
    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
      Log.i(TAG, "Displaying WRITE_EXTERNAL_STORAGE rationale.");

      Snackbar.make(view, R.string.permission_write_storage_rationale, Snackbar.LENGTH_INDEFINITE)
          .setAction(R.string.ok, v -> ActivityCompat.requestPermissions(activity,
              new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE))
          .show();
    }
    else {
      Snackbar.make(view, R.string.permission_write_storage_unavailable, Snackbar.LENGTH_SHORT).show();

      ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
    }
  }

  static boolean onRequestPermissionsResult(final int requestCode, @NotNull final String[] permissions,
                                            @NotNull final int[] grantResults) {
    //TODO: Could be interesting to block the possiblity to switch activity as long as there's still a non-answered permission request.
    if (requestCode == REQUEST_WRITE_EXTERNAL_STORAGE) {
      Log.i(TAG, "Received response for reading external storage permission request.");

      // Check if the only required permission has been granted
      if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        Log.i(TAG, "WRITE_EXTERNAL_STORAGE permission has now been granted.");

//        testConnection();
      }
      else {
        Log.i(TAG, "WRITE_EXTERNAL_STORAGE permission was not granted.");
        //TODO: Do something if the permission was denied.
      }
    }
    else {
      Log.e(TAG, "Something went wrong with permissions. requestCode: " + requestCode + "\npermissions: " + permissions + "grantResults: " + grantResults);

      // If none of these were requested from us, delegate to the super-class.
//      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
      return true; //This means the `onRequestPermissionsResult from the caller's superclass has to be called.
    }
    return false;
  }

  //==== Object Methods ====//
}
