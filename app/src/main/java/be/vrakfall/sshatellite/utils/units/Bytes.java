package be.vrakfall.sshatellite.utils.units;

import java.text.DecimalFormat;

public interface Bytes {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Static Methods ====//

  /**
   * Taken from
   * <a href="https://stackoverflow.com/questions/3263892/format-file-size-as-mb-gb-etc">https://stackoverflow.com/questions/3263892/format-file-size-as-mb-gb-etc</a>
   * and changed a bit.
   * Turn an amount of bytes into a human readable string with the appropriate decimal unit.
   *
   * @param byteAmount The amount of bytes
   * @return The human readable string of the amount of bytes with a decimal unit. It can be rounded.
   */
  static String readableByteAmount(final long byteAmount) {
    if (byteAmount <= 0) {
      return "0 B/s";
    }
    final String[] units = new String[]{"B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};
    int digitGroups = (int) (Math.log10(byteAmount) / Math.log10(1000));
    if (digitGroups >= units.length) {
      digitGroups = units.length - 1;
    }
    return new DecimalFormat("#,##0.#").format(byteAmount / Math.pow(1000, digitGroups)) + " " + units[digitGroups] +
        "/s";
  }

  //==== Object Methods ====//
}
