package be.vrakfall.sshatellite.utils.io;

import java.util.regex.Pattern;

public interface UsualRegularExpressions {
  //==== Static variables ====//

  /**
   * Basic regex to find an IP address within a string.
   * Imported from previous projects: HTTPS Shop from Jérémy Lecocq & Luther Claude Dianga (projet SSL Pâques)
   * and Chat RMI August exams (Jérémy Lecocq).
   *
   * @author Jérémy Lecocq
   */
  String IP_ADDRESS = "(?!^(?:0\\.0\\.0\\.0|255\\.255\\.255\\.255)$)(^(?:(?:1?[\\d]?[\\d]|2(?:[0-4][\\d]|5[0-4]))[.]){3}(?:1?[\\d]?[\\d]|2(?:[0-5][\\d]|5[0-5]))$)";
  /**
   * Capture group to get the IP address in {@link #IP_ADDRESS}.
   * Imported from previous projects: HTTPS Shop from Jérémy Lecocq & Luther Claude Dianga (projet SSL Pâques)
   * and Chat RMI August exams (Jérémy Lecocq).
   *
   * @author Jérémy Lecocq
   */
  int IP_ADDRESS_GROUP = 1;

  /**
   * Basic regex to find an hostname within a string.
   * Imported from previous projects: HTTPS Shop from Jérémy Lecocq & Luther Claude Dianga (projet SSL Pâques)
   * and Chat RMI August exams (Jérémy Lecocq).
   *
   * @author Jérémy Lecocq
   */
  String HOSTNAME = "(?=^.{4,253}$)^((?:(?!-)[a-zA-Z0-9-]{1,63}(?<!-)\\.)+[a-zA-Z]{2,63}|localhost)$";
  /**
   * Capture group to get the hostname in {@link #HOSTNAME}.
   * Imported from previous projects: HTTPS Shop from Jérémy Lecocq & Luther Claude Dianga (projet SSL Pâques)
   * and Chat RMI August exams (Jérémy Lecocq).
   *
   * @author Jérémy Lecocq
   */
  int HOSTNAME_GROUP = 1;

  /**
   * The regex corresponding to an empty line.
   */
  String EMPTY_LINE_REGEX = "^$";

  /**
   * The pattern for the empty line regex.
   */
  Pattern EMPTY_LINE_PATTERN = Pattern.compile(EMPTY_LINE_REGEX);

  //====== Static Methods ======//

  //====== Object Methods ======//
}
