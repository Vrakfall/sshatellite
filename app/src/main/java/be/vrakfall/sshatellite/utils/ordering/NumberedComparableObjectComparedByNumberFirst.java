package be.vrakfall.sshatellite.utils.ordering;

import be.vrakfall.sshatellite.utils.annotations.NotNull;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
//@SuperBuilder
public class NumberedComparableObjectComparedByNumberFirst<T extends Comparable<? super T>> extends NumberedComparableObject<T>
    implements ExtendedComparable<NumberedComparableObjectComparedByNumberFirst<T>> {
  //==== Static variables ====//

  public static final String TAG = NumberedComparableObjectComparedByNumberFirst.class.getSimpleName();

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Constructors ====//

  @Builder
  public NumberedComparableObjectComparedByNumberFirst(final long number, @NotNull final T object, final boolean ascendingNumberOrder) {
    super(number, object, ascendingNumberOrder);
  }

  //  @Builder
  private NumberedComparableObjectComparedByNumberFirst(final long number, @NotNull final T object) {
    super(number, object);
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @Override
  public int compareTo(@NotNull final NumberedComparableObjectComparedByNumberFirst<T> o) {
    final int comparison;
    if (isAscendingNumberOrder()) {
      comparison = Long.compare(o.getNumber(), getNumber());
    }
    else {
      comparison = Long.compare(getNumber(), o.getNumber());
    }

    if (comparison == 0) {
      return getObject().compareTo(o.getObject());
    }
    return comparison;
  }
}
