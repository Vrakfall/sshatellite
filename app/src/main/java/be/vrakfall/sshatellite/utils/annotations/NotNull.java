package be.vrakfall.sshatellite.utils.annotations;

@org.jetbrains.annotations.NotNull
@lombok.NonNull
@androidx.annotation.NonNull
public @interface NotNull {
}
