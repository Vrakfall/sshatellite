package be.vrakfall.sshatellite.utils.association;

public interface NumericallyIdentifiable {
	long getId();
}
