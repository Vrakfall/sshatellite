package be.vrakfall.sshatellite.utils.files;

import android.annotation.TargetApi;
import android.os.Build;
import androidx.annotation.RequiresApi;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.files.errors.runtime.NoWriteAccessToAndroidApplicationLocalFilesException;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static be.vrakfall.sshatellite.utils.files.AndroidAssetLocalFileLink.NOT_PERTINENT_FILE_LINK;
import static be.vrakfall.sshatellite.utils.files.FileHierarchy.addToFilePath;

@ToString
@EqualsAndHashCode()
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
public class GenericApplicationFilesManager implements ApplicationFilesManager {
  //==== Static variables ====//

  protected static final String DEFAULT_ROOT = "~";

  public static final String DEFAULT_MAIN_EXECUTABLE = "main.py";

  public static final String UNKNOWN_OPERATING_SYSTEM_NAME = "unknown";

  //==== > Error messages ====//

  public static final String CANNOT_CREATE_DIRECTORY_ERROR_MESSAGE =
      "The android local data directory at location %s cannot be created.";
  public static final String CANNOT_DELETE_DIRECTORY_ERROR_MESSAGE = "The android local data file at location %s " +
      "cannot be deleted.";
  public static final String CANNOT_CREATE_FILE_ERROR_MESSAGE = "The android local data file at location %s cannot be" +
      " created.";

  //==== Attributes ====//

  @NotNull
  private String operatingSystemName;

  @NotNull
  private String root;

  @NotNull
  @Getter(value = AccessLevel.NONE)
  private String mainExecutablePath;

  @Getter(AccessLevel.PRIVATE)
  private ConcurrentMap<Integer, Object> fileLocks = new ConcurrentHashMap<>();

  //==== Constructors ====//

  protected GenericApplicationFilesManager(@Nullable final String operatingSystemName, @Nullable final String root,
                                           @Nullable final String mainExecutablePath) {
    if (operatingSystemName == null || operatingSystemName.isEmpty()) {
      val localOsName = System.getProperty("os.name");
      if (localOsName == null || localOsName.isEmpty()) {
        // Default value
        this.operatingSystemName = UNKNOWN_OPERATING_SYSTEM_NAME;
      }
      else {
        this.operatingSystemName = localOsName;
      }
    }
    else {
      this.operatingSystemName = operatingSystemName;
    }

    if (root == null || root.isEmpty()) {
      this.root = DEFAULT_ROOT;
    }
    else {
      this.root = ApplicationFilesManager.removeTrailingSlashes(root, DEFAULT_ROOT);
    }

    if (mainExecutablePath == null || mainExecutablePath.isEmpty()) {
      // Default value
      this.mainExecutablePath = DEFAULT_MAIN_EXECUTABLE;
    }
    else {
      this.mainExecutablePath = ApplicationFilesManager.cleanPathString(mainExecutablePath, getOperatingSystemName(),
          DEFAULT_MAIN_EXECUTABLE);
    }
  }

  //==== Getters and Setters ====//

  @NotNull
  @Override
  public String getRelativeMainExecutablePath() {
    return mainExecutablePath;
  }

  @NotNull
  @Override
  public String getAbsoluteMainExecutablePath() {
    // No need to clean it as it already is.
    return addToFilePath(getRoot(), mainExecutablePath);
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @NotNull
  @Override
  public String getAbsolutePathOf(@NotNull String relativeToRootPath) {
    return addToFilePath(
        getRoot(),
        ApplicationFilesManager.cleanPathString(relativeToRootPath, getOperatingSystemName())
    );
  }

  @TargetApi(Build.VERSION_CODES.N)
  @RequiresApi(Build.VERSION_CODES.N)
  Object getFileLock(File fileToLock) {
    return getFileLocks().computeIfAbsent(fileToLock.hashCode(), integer -> new Object());
  }

  /**
   * Create the specified file and its parents if they don't exist, overwrite the previously existing file if it's
   * not of the same type (directory, "normal" file, etc...). Synchronised if operating on the same file.
   *
   * The synchronisation in inspired from
   * <a href="https://stackoverflow.com/questions/27747192/synchronization-of-access-to-a-file-on-a-per-file-basis">
   *   https://stackoverflow.com/questions/27747192/synchronization-of-access-to-a-file-on-a-per-file-basis</a>
   *
   * @param fileToCreate The file to create.
   * @param isDirectory  Whether the file you want to create shall be a directory or not.
   */
  // TODO: 11/12/18 Make other natural methods for better naming
  @TargetApi(Build.VERSION_CODES.N)
  @RequiresApi(Build.VERSION_CODES.N)
  void createFileAndParentsAndOverwriteIfNeeded(File fileToCreate, boolean isDirectory) {
    synchronized (getFileLock(fileToCreate)) {
      if (fileToCreate.exists()) {
        if (fileToCreate.isDirectory() != isDirectory) {
          // If it exists but is not a directory, delete the old file and create it.
          // TODO: 11/12/18 Split implementation with nio.Files depending on android's version, at some point.
          if (fileToCreate.delete()) {
            createFileAndParents(fileToCreate, isDirectory);
          }
          else {
            throw new NoWriteAccessToAndroidApplicationLocalFilesException(
                String.format(Locale.US, CANNOT_DELETE_DIRECTORY_ERROR_MESSAGE, fileToCreate.getPath())
            );
          }
        }
      }
      else {
        createFileAndParents(fileToCreate, isDirectory);
      }
    }
  }

  /**
   * Create the specified file and its parents if they don't exist. The must NOT exist or this will throw a
   * {@link NoWriteAccessToAndroidApplicationLocalFilesException}. Not thread-safe if operating on the same file.
   *
   * @param fileToCreate The file to create.
   * @param isDirectory  Whether the file you want to create shall be a directory or not.
   */
  void createFileAndParents(File fileToCreate, boolean isDirectory) {
    if (isDirectory) {
      if (!fileToCreate.mkdirs()) {
        throw new NoWriteAccessToAndroidApplicationLocalFilesException(String.format(Locale.US,
            CANNOT_CREATE_DIRECTORY_ERROR_MESSAGE, fileToCreate.getPath()));
      }
    }
    else {
      createFileAndParentsAndOverwriteIfNeeded(fileToCreate.getParentFile(), true);
      try {
        if (!fileToCreate.createNewFile()) {
          throw new NoWriteAccessToAndroidApplicationLocalFilesException(String.format(Locale.US,
              CANNOT_CREATE_FILE_ERROR_MESSAGE, fileToCreate.getPath()));
        }
      }
      catch (IOException e) {
        throw new NoWriteAccessToAndroidApplicationLocalFilesException(String.format(Locale.US,
            CANNOT_CREATE_FILE_ERROR_MESSAGE, fileToCreate.getPath()), e);
      }
    }
  }
}
