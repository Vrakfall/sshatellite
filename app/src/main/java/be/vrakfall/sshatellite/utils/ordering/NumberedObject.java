package be.vrakfall.sshatellite.utils.ordering;

import be.vrakfall.sshatellite.utils.annotations.NotNull;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
//@SuperBuilder
public class NumberedObject<T> extends AbstractNumberedObject implements Comparable<NumberedObject> {
  //==== Static variables ====//

  public static final String TAG = NumberedObject.class.getSimpleName();

  //==== > Error messages ====//

  //==== Attributes ====//

  @NotNull
  @NonNull
  private T object;

  //==== Constructors ====//

  @Builder
  private NumberedObject(final long number, @NotNull final T object) {
    super(number);
    this.object = object;
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @Override
  public int compareTo(@NotNull final NumberedObject o) {
    return Long.compare(getNumber(), o.getNumber());
  }
}
