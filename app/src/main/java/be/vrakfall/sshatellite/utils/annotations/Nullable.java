package be.vrakfall.sshatellite.utils.annotations;

@org.jetbrains.annotations.Nullable
@androidx.annotation.Nullable
public @interface Nullable {
}
