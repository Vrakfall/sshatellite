package be.vrakfall.sshatellite.utils.android.ui;

import androidx.databinding.ViewDataBinding;

import java.util.Locale;

public interface DataBinding {
  //==== Static variables ====//

  //==== > Error messages ====//

  String DATA_BINDING_IS_OF_WRONG_TYPE_ERROR_MESSAGE = "The data binding" +
      " object associated with this object has to be of type %s but %s was given instead.";


  //==== Static Methods ====//

  static void ensureType(final Class<? extends ViewDataBinding> desiredBinding,
                         final ViewDataBinding bindingToCheck) {
    if (!(desiredBinding.isAssignableFrom(bindingToCheck.getClass()))) {
      throw new IllegalArgumentException(String.format(Locale.US, DATA_BINDING_IS_OF_WRONG_TYPE_ERROR_MESSAGE,
          desiredBinding.getName(), bindingToCheck.getClass().getName()));
    }
  }

  //==== Object Methods ====//
}
