package be.vrakfall.sshatellite.utils.files;

import be.vrakfall.sshatellite.utils.android.ui.ExtendedAppCompatActivity;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import io.reactivex.Flowable;
import io.reactivex.parallel.ParallelFlowable;
import io.reactivex.schedulers.Schedulers;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static be.vrakfall.sshatellite.utils.files.AndroidAssetLocalFileLink.NOT_PERTINENT_FILE_LINK;
import static be.vrakfall.sshatellite.utils.files.FileHierarchy.addToFilePath;

@ToString
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
public class AndroidApplicationFilesManager extends GenericApplicationFilesManager {
  //==== Static variables ====//

  public static final String TAG = AndroidApplicationFilesManager.class.getSimpleName();

  //==== > Error messages ====//

  //==== Attributes ====//

  @NotNull
  @Getter(value = AccessLevel.PROTECTED)
  private ExtendedAppCompatActivity contextThemeWrapper;

  //==== Constructors ====//

  @Builder()
  public AndroidApplicationFilesManager(@NotNull final String operatingSystemName, @NotNull final String root,
                                        @NotNull final String mainExecutablePath,
                                        @NotNull final ExtendedAppCompatActivity contextThemeWrapper) {
    super(operatingSystemName, root, mainExecutablePath);
    this.contextThemeWrapper = contextThemeWrapper;
  }

  //==== Getters and Setters ====//

  @NotNull
  public String getApplicationDataDirectory() {
    return getContextThemeWrapper().getFilesDir().getPath();
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @NotNull
  public String getLocalDataFileAbsolutePath(@NotNull final String assetRelativePath) {
    return addToFilePath(getApplicationDataDirectory(), assetRelativePath);
  }

  @NotNull
  public InputStream getAsset(@NotNull final String assetPath) throws IOException {
    return getContextThemeWrapper().getAsset(assetPath);
  }

  @NotNull
  public ParallelFlowable<AndroidAssetLocalFileLink> copyAssetsInDirectoryToLocalDataFilesRx(final String assetsDirectoryRelativePath) {
    // Initialisation
    // TODO: 7/12/18 Define a default if the cleaned path becomes empty
    final val cleanedAssetsDirectoryRelativePath = ApplicationFilesManager.cleanPathString(assetsDirectoryRelativePath,
        getOperatingSystemName());
    // Here the parent local file is used as local relative directory as the recursive look for assets returns the
    // given directory in the paths
    final val relatedLocalDirectory =
        new File(getLocalDataFileAbsolutePath(cleanedAssetsDirectoryRelativePath)).getParentFile();
    createFileAndParentsAndOverwriteIfNeeded(relatedLocalDirectory, true);

    // Parallel copy (Rx)
    return Flowable.fromIterable(getContextThemeWrapper().getAssetPathsRecursivelyIn(cleanedAssetsDirectoryRelativePath))
        .onBackpressureBuffer()
        // TODO: 7/12/18 Find out what could be the best amount of parallelism for sending files.
        .parallel(10)
        .runOn(Schedulers.io())
        .map(assetPath -> {
          // Initialisation
          final val localFile = new File(relatedLocalDirectory, assetPath);
          createFileAndParentsAndOverwriteIfNeeded(localFile, false);

          try {
            copyAssetToFile(assetPath, localFile);
          }
          catch (final IOException e) {
            // Then it's either an directory, an empty/bogus files...
            return NOT_PERTINENT_FILE_LINK;
          }
          return AndroidAssetLocalFileLink.builder()
              .assetPath(assetPath)
              .applicationLocalFile(localFile)
              .build();
        });
  }

  @SuppressWarnings("UnusedReturnValue")
  private int copyAssetToFile(final String relativeToBackendRootAssetPath, final File localFile) throws IOException {
    return IOUtils.copy(getAsset(relativeToBackendRootAssetPath),
        new FileOutputStream(localFile));
  }
}
