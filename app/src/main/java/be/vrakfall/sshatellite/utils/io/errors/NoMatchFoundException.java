package be.vrakfall.sshatellite.utils.io.errors;

public class NoMatchFoundException extends ParsingException {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public NoMatchFoundException() {
  }

  public NoMatchFoundException(String message) {
    super(message);
  }

  public NoMatchFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public NoMatchFoundException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
