package be.vrakfall.sshatellite.utils.errors;

public class APILevelTooLowException extends Exception {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public APILevelTooLowException() {
  }

  public APILevelTooLowException(String message) {
    super(message);
  }

  public APILevelTooLowException(String message, Throwable cause) {
    super(message, cause);
  }

  public APILevelTooLowException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
