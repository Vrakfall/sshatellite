package be.vrakfall.sshatellite.utils.io;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public interface ObjectMapperDispenser {
  //==== Static variables ====//

  ObjectMapper YAML_OBJECT_MAPPER = new ObjectMapper(new YAMLFactory())
      .registerModule(new JavaTimeModule());

  //==== > Error messages ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
