package be.vrakfall.sshatellite.utils.ordering;

import lombok.NonNull;
import lombok.val;
import org.jetbrains.annotations.NotNull;

public interface ExtendedComparable<T> extends Comparable<T> {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Static Methods ====//

  /**
   * Finds the maximum between two {@link Comparable} objects based on their {@link Comparable#compareTo(Object)} implementation.
   * (Inspired from
   * <a href="https://stackoverflow.com/questions/6452313/how-to-implement-a-generic-maxcomparable-a-comparable-b-function-in-java">https://stackoverflow.com/questions/6452313/how-to-implement-a-generic-maxcomparable-a-comparable-b-function-in-java</a>).
   *
   * @param a   The first operand.
   * @param b   The second operand.
   * @param <T> The type of the items to compute, must implement {@link Comparable}.
   * @return The greater between {@code a} and {@code b}, {@code a} if they are equal.
   */
  @NotNull
  @NonNull
  static <T extends Comparable<? super T>> T max(@NotNull @NonNull final T a, @NotNull @NonNull final T b) {
    final val comparison = a.compareTo(b);
    if (comparison < 0) {
      return b;
    }
    if (comparison > 0) {
      return a;
    }
    return a;
  }

  //==== Object Methods ====//
}
