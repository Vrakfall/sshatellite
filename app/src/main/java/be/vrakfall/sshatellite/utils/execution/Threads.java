package be.vrakfall.sshatellite.utils.execution;

import android.annotation.SuppressLint;
import android.os.Build;
import android.util.Log;
import be.vrakfall.sshatellite.BuildConfig;

import java.util.Locale;

public interface Threads {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  @SuppressLint("ObsoleteSdkInt")
  static void logCurrentThread(String classTag, String method) {
    if (BuildConfig.DEBUG) {
      if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.BASE)) { // True if running on Android, false if running on a normal `JVM`, e.g.: for unit tests.
        Log.d(classTag, String.format(Locale.US, "%s: Thread name: " + Thread.currentThread().getName(), method));
      }
      else {
        System.out.println(String.format(Locale.US, "%s: Thread name: " + Thread.currentThread().getName(), method));
      }
    }
  }

  // Might be needed at some point:
//  @SuppressLint("ObsoleteSdkInt")
//  public static Scheduler mainScheduler() {
//    if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.BASE)) { // True if running on Android, false if running on a normal `JVM`,
//      return AndroidSchedulers.mainThread();
//    }
//
//    // TODO: 8/10/18 Find a way to return the main thread on a normal `JVM`.
//  }

  //==== Object Methods ====//
}
