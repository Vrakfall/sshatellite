package be.vrakfall.sshatellite.utils.io;

import java.io.IOException;

@FunctionalInterface
public interface Parser<T> {
  T readNext() throws IOException;
}
