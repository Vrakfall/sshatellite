package be.vrakfall.sshatellite.utils.ordering;

import lombok.*;
import lombok.experimental.FieldDefaults;

// The following is equivalent to @Value (immutable classes) but without `final` so we can inherit from it.
@ToString
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
//@SuperBuilder
public abstract class AbstractNumberedObject {
  //==== Static variables ====//

  public static final String TAG = AbstractNumberedObject.class.getSimpleName();

  //==== > Error messages ====//

  //==== Attributes ====//

  private long number;

  //==== Constructors ====//

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
