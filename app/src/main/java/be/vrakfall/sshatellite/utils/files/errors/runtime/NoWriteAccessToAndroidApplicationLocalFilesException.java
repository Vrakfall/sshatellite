package be.vrakfall.sshatellite.utils.files.errors.runtime;

import android.annotation.TargetApi;
import android.os.Build;
import androidx.annotation.RequiresApi;

public class NoWriteAccessToAndroidApplicationLocalFilesException extends RuntimeException {
  public NoWriteAccessToAndroidApplicationLocalFilesException() {
    super();
  }

  public NoWriteAccessToAndroidApplicationLocalFilesException(String message) {
    super(message);
  }

  public NoWriteAccessToAndroidApplicationLocalFilesException(String message, Throwable cause) {
    super(message, cause);
  }

  public NoWriteAccessToAndroidApplicationLocalFilesException(Throwable cause) {
    super(cause);
  }

  @TargetApi(Build.VERSION_CODES.N)
  @RequiresApi(Build.VERSION_CODES.N)
  protected NoWriteAccessToAndroidApplicationLocalFilesException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
