package be.vrakfall.sshatellite.utils.files;

import android.os.Environment;
import android.util.Log;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import org.jetbrains.annotations.Contract;

import java.io.File;

public class FileHierarchy {
  //==== Static variables ====//

  public static final String TAG = FileHierarchy.class.getSimpleName();

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Constructors ====//

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  /**
   * List all the files and folders at a certain path in the logs, starting from the SD card's root (not the system root). Aimed for Android.
   * Inspired from <a href="https://dzone.com/articles/java-example-list-all-files">https://dzone.com/articles/java-example-list-all-files</a>.
   *
   * @param path The path where to list the files, relative to the SD card's root.
   * @see <a href="https://dzone.com/articles/java-example-list-all-files">https://dzone.com/articles/java-example-list-all-files</a>
   */
  public static void listFilesAndFolders(String path) {
    String fullPath = Environment.getExternalStorageDirectory().toString() + "/" + path;
    File directory = new File(fullPath);
    File[] files = directory.listFiles();

    if (files == null) {
      Log.d(FileHierarchy.TAG, "The path `" + path + "` probably doesn't exist!\n" +
          "Concatenated path: " + Environment.getExternalStorageDirectory().toString() + path);
    }
    else {
      for (File file :
          files) {
        Log.d(FileHierarchy.TAG, file.getName());
      }
    }
  }

  @NotNull
  @Contract(pure = true)
  public static String addToFilePath(@NotNull final String parentDirectoryPath, @NotNull final String filePath) {
    // TODO: 7/12/18 Check if it's faster to concatenate a char or a String in another String (not important).
    return parentDirectoryPath + File.separatorChar + filePath;
  }

  //==== Object Methods ====//
}
