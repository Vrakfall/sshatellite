package be.vrakfall.sshatellite.utils;

public interface Math {
  //==== Static variables ====//

  //==== Static Methods ====//

  /**
   * Casts and crops a long to an int. If it overflows an extreme value, that one is returned instead.
   * Used as a best effort replacement of {@link java.lang.Math#toIntExact(long)}
   *
   * @param l The long to be cast and cropped.
   * @return The corresponding {@code int}.
   */
  static int cropToInteger(long l) {
    if (l > Integer.MAX_VALUE) {
      return Integer.MAX_VALUE;
    }

    if (l < Integer.MIN_VALUE) {
      return Integer.MIN_VALUE;
    }

    return (int) l;
  }

  //==== Object Methods ====//
}
