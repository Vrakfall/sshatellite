package be.vrakfall.sshatellite.utils.files;

import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import lombok.val;

import java.io.File;
import java.util.regex.Pattern;

public interface ApplicationFilesManager {
  //==== Static variables ====//

  Pattern TRAILING_SLASH_PATTERN = Pattern.compile("(?!^/)[\\\\/]+$");

  Pattern UNIX_PATH_CLEANING_PATTERN = Pattern.compile("^.?/|[\\\\/]+$");

  Pattern WINDOWS_PATH_CLEANING_PATTERN = Pattern.compile("^.?/+|^.?\\\\+|[\\\\/]+$");

  //==== > Error messages ====//

  //==== Static Methods ====//

  @NotNull
  static Pattern getPathCleaningPattern(@NotNull final String operatingSystemName) {
    if (operatingSystemName.startsWith("Windows") || operatingSystemName.startsWith("windows")) {
      return WINDOWS_PATH_CLEANING_PATTERN;
    }
    return UNIX_PATH_CLEANING_PATTERN;
  }

  /**
   * Removes the trailing slashes of a {@link String}, mainly for a file path. {@code "/test/something/"} would
   * become {@code "/test/something"}.
   * @param string The string from which to remove the trailing slashes.
   * @param defaultIfResultIsEmpty The default to return if the result is empty (which shouldn't happen as {@code
   * "//"} and {@code "/"} both become {@code "/"}. Ignored if null.
   * @return The string cleaned of trailing slashes.
   */
  @NotNull
  static String removeTrailingSlashes(@NotNull final String string, @Nullable String defaultIfResultIsEmpty) {
    val result = TRAILING_SLASH_PATTERN.matcher(string).replaceAll("");
    if (result.isEmpty() && defaultIfResultIsEmpty != null) {
      return defaultIfResultIsEmpty;
    }
    return result;
  }

  /**
   * Removes the trailing slashes of a {@link String}, mainly for a file path. {@code "/test/something/"} would
   * become {@code "/test/something"}.
   * @param string The string from which to remove the trailing slashes.
   * @return The string cleaned of trailing slashes.
   */
  @NotNull
  static String removeTrailingSlashes(@NotNull final String string) {
    return removeTrailingSlashes(string, null);
  }

  @NotNull
  static String cleanPathString(@NotNull final String pathString, @NotNull final String operatingSystemName,
                                @Nullable final String defaultIfResultIsEmpty) {
    // Here, we don't care we match the FileSystem's root as this is supposedly used for relative path strings.
    val result = getPathCleaningPattern(operatingSystemName).matcher(pathString).replaceAll("");
    if (result.isEmpty() && defaultIfResultIsEmpty != null) {
      return defaultIfResultIsEmpty;
    }
    return result;
  }

  @NotNull
  static String cleanPathString(@NotNull final String pathString, @NotNull final String operatingSystemName) {
    return cleanPathString(pathString, operatingSystemName, null);
  }

  //==== Object Methods ====//

  @NotNull
  String getRoot();

  @NotNull
  String getAbsolutePathOf(@NotNull final String relativePath);

  @NotNull
  String getOperatingSystemName();

  @NotNull
  String getRelativeMainExecutablePath();

  @NotNull
  String getAbsoluteMainExecutablePath();
}
