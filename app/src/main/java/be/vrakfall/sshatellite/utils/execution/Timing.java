package be.vrakfall.sshatellite.utils.execution;

import android.os.Build;
import android.util.Log;
import androidx.annotation.RequiresApi;
import lombok.NonNull;
import lombok.Value;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public final class Timing {
  public static final String TAG = Timing.class.getSimpleName();

  /**
   * Execute a {@link Supplier} get info on the execution timing.
   * Note: The timing still contains the duration of one system call and a few variable declarations and affectations.
   * @param supplier The {@link Supplier} to execute.
   * @param <T> The return type of the {@link Supplier}.
   * @return A {@link TimedExecution} data object, containing the time the execution took and the object or value the
   * execution returned.
   */
  @NotNull
  @RequiresApi(api = Build.VERSION_CODES.N)
  public static <T> TimedExecution<T> executeAndTime(@NotNull @NonNull Supplier<T> supplier) {
    long startTime = System.nanoTime();
    val returnedObject = supplier.get();
    return new TimedExecution<T>(System.nanoTime() - startTime, returnedObject);
  }

  /**
   * Execute a {@link Supplier} log info on the execution timing.
   * Note: The timing still contains the duration of one system call and a few variable declarations and affectations.
   * @param supplier The {@link Supplier} to execute.
   * @param <T> The return type of the {@link Supplier}.
   * @return The object or value the execution returned.
   */
  @RequiresApi(api = Build.VERSION_CODES.N)
  @Nullable
  public static <T> T executeAndLogTiming(@NotNull @NonNull Supplier<T> supplier) {
    val timedExecution = executeAndTime(supplier);
    Log.i(TAG, String.format(Locale.US, "Execution took %d ns.", timedExecution.getNanoseconds()));
    return timedExecution.getReturnedObject();
  }

  /**
   * Execute a {@link Supplier} log info on the execution timing with an identification number linked to the
   * execution call.
   * Note: The timing still contains the duration of one system call and a few variable declarations and affectations.
   * @param supplier The {@link Supplier} to execute.
   * @param executionNumber The identification number linked to the execution call.
   * @param <T> The return type of the {@link Supplier}.
   * @return The object or value the execution returned.
   */
  @RequiresApi(api = Build.VERSION_CODES.N)
  @Nullable
  public static <T> T executeAndLogTiming(@NotNull @NonNull Supplier<T> supplier,
                                          long executionNumber) {
    val timedExecution = executeAndTime(supplier);
    Log.i(TAG, String.format(Locale.US, "Execution %d took %d ns.", executionNumber, timedExecution.getNanoseconds()));
    return timedExecution.getReturnedObject();
  }

  /**
   * Execute a {@link Supplier} log info on the execution timing with an identification number linked to the
   * execution call and with the desired time unit (truncated).
   * Note: The timing still contains the duration of one system call and a few variable declarations and affectations.
   * @param supplier The {@link Supplier} to execute.
   * @param executionNumber The identification number linked to the execution call.
   * @param loggingTimeUnit The time unit wanted for the log to be expressed with (truncated).
   * @param <T> The return type of the {@link Supplier}.
   * @return The object or value the execution returned.
   */
  @RequiresApi(api = Build.VERSION_CODES.N)
  @Nullable
  public static <T> T executeAndLogTimingAs(@NotNull @NonNull Supplier<T> supplier, long executionNumber,
                                            @NotNull @NonNull TimeUnit loggingTimeUnit) {
    val timedExecution = executeAndTime(supplier);
    Log.i(TAG, String.format(Locale.US, "Execution %d took %d %s.", executionNumber,
        loggingTimeUnit.convert(timedExecution.getNanoseconds(), TimeUnit.NANOSECONDS), loggingTimeUnit.toString()));
    return timedExecution.getReturnedObject();
  }

  /**
   * A data object, containing the time the execution took and the object or value the execution returned.
   * @param <T> The type of the object or value the execution returns.
   */
  @Value
  private static final class TimedExecution<T> {
    /**
     * The time in nanoseconds the execution took.
     */
    private final long nanoseconds;

    /**
     * The object or value the execution returned.
     */
    @Nullable
    private final T returnedObject;
  }

  /**
   * Sleep for a certain amount of seconds and log it in the console.
   * @param seconds The (minimum) amount of seconds to sleep.
   * @throws InterruptedException If interrupted while sleeping.
   */
  public static void sleepAndLog(long seconds) throws InterruptedException {
    sleepAndLog(seconds, TimeUnit.SECONDS);
  }

  /**
   * Sleep for a certain amount of time and log it in the console.
   * @param amount The (minimum) amount of time unit to sleep.
   * @param timeUnit The time unit for the amount of time to sleep.
   * @throws InterruptedException If interrupted while sleeping.
   */
  public static void sleepAndLog(long amount, @NotNull @NonNull TimeUnit timeUnit) throws InterruptedException {
    var stringBuilder = new StringBuilder(String.format(Locale.US, "Starting to sleep for %d %s", amount,
        timeUnit.name()));
    if (amount > 1) {
      stringBuilder.append('s');
    }
    stringBuilder.append('.');
    Log.i(TAG, stringBuilder.toString());

    timeUnit.sleep(amount);

    stringBuilder = new StringBuilder(String.format(Locale.US, "Slept %d %ss.", amount, timeUnit.name()));
    if (amount > 1) {
      stringBuilder.append('s');
    }
    stringBuilder.append('.');
    Log.i(TAG, stringBuilder.toString());
  }

  /**
   * Sleep for a certain amount of seconds and log it in the console.
   * @param seconds The (minimum) amount of seconds to sleep.
   */
  public static void sleepAndLogEverything(long seconds) {
    sleepAndLogEverything(seconds, TimeUnit.SECONDS);
  }

  /**
   * Sleep for a certain amount of time and log it in the console.
   * @param amount The (minimum) amount of time unit to sleep.
   * @param timeUnit The time unit for the amount of time to sleep.
   */
  public static void sleepAndLogEverything(long amount, @NotNull @NonNull TimeUnit timeUnit) {
    try {
      sleepAndLog(amount, timeUnit);
    }
    catch (InterruptedException e) {
      Log.e(TAG, "This thread's sleep just got interrupted.", e);
    }
  }
}
