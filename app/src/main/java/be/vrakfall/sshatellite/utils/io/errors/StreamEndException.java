package be.vrakfall.sshatellite.utils.io.errors;

import java.io.IOException;

public class StreamEndException extends IOException {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public StreamEndException() {
  }

  public StreamEndException(String message) {
    super(message);
  }

  public StreamEndException(String message, Throwable cause) {
    super(message, cause);
  }

  public StreamEndException(Throwable cause) {
    super(cause);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//
}
