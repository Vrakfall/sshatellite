package be.vrakfall.sshatellite.utils.ordering;

import be.vrakfall.sshatellite.utils.annotations.NotNull;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.val;

@Value
@EqualsAndHashCode(callSuper = true)
//@SuperBuilder
public class NumberedComparableObjectComparedByObjectFirst<T extends Comparable<? super T>>
    extends NumberedComparableObject<T>
    implements ExtendedComparable<NumberedComparableObjectComparedByObjectFirst<T>> {
  //==== Static variables ====//

  public static final String TAG = NumberedComparableObjectComparedByObjectFirst.class.getSimpleName();

  //==== > Error messages ====//

  //==== Attributes ====//

  //==== Constructors ====//

  // TODO: 11/11/18 Find a way for this builder to use the default value of `ascendingNumber` when not assigning it.
  @Builder
  public NumberedComparableObjectComparedByObjectFirst(final long number, @NotNull final T object,
                                                       final boolean ascendingNumberOrder) {
    super(number, object, ascendingNumberOrder);
  }

  //==== Getters and Setters ====//

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  @Override
  public int compareTo(@NotNull final NumberedComparableObjectComparedByObjectFirst<T> o) {
    final val comparison = getObject().compareTo(o.getObject());
    if (comparison == 0) {
      if (isAscendingNumberOrder()) {
        return Long.compare(o.getNumber(), getNumber());
      }
      else {
        return Long.compare(getNumber(), o.getNumber());
      }
    }
    return comparison;
  }
}
