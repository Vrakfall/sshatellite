package be.vrakfall.sshatellite.utils.io;

import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.io.errors.StreamEndException;
import be.vrakfall.sshatellite.utils.io.errors.TooManyEmptyLinesException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Locale;

public class ExtendedBufferedReader extends BufferedReader {
  //==== Static variables ====//

  /**
   * Tag used for debugging outputs.
   */
  public static final String TAG = ExtendedBufferedReader.class.getSimpleName();

  /**
   * Number of times a read loop can iterate (like {@link #readNextNonEmptyLineEOFAsException}). This is in order to prevent infinite
   * loops. It has a n² factor when looking for a specific line, so far.
   */
  public static final int READ_LOOP_LIMIT = 500;
  // TODO: 19/09/18 Make so it doesn't become n² for specific lines? Is it interesting?

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  public ExtendedBufferedReader(@NotNull Reader in, int sz) {
    super(in, sz);
  }

  public ExtendedBufferedReader(@NotNull Reader in) {
    super(in);
  }

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  //====== Object Methods ======//

  /**
   * Reads the next non-empty line, skipping the empty ones up to a limit set by {@link #READ_LOOP_LIMIT} to avoid
   * infinite looping.
   *
   * @return The next non-empty line if there is one.
   * @throws StreamEndException         If the end of the underlying stream has been reached. This extends {@link IOException}.
   * @throws TooManyEmptyLinesException If the loop limit set by {@link #READ_LOOP_LIMIT} has been reached. This means
   *                                    that the set amount of empty lines has been found and the loop broke because that was probably an abnormal amount
   *                                    of empty lines and an infinite loop has to be avoided.
   * @throws IOException                If an I/O error occurred while calling {@link BufferedReader#readLine()}.
   */
  @NotNull
  @Deprecated
  public String readNextNonEmptyLineEOFAsException() throws IOException {
    return readNextNonEmptyLineEOFAsException(true);
  }

  /**
   * Reads the next non-empty line, skipping the empty ones up to a limit set by {@link #READ_LOOP_LIMIT} to avoid
   * infinite looping.
   *
   * @param verboseException Whether a verbose exception containing an explanation message is wanted or not.
   * @return The next non-empty line if there is one.
   * @throws StreamEndException         If the end of the underlying stream has been reached. This extends {@link IOException}.
   * @throws TooManyEmptyLinesException If the loop limit set by {@link #READ_LOOP_LIMIT} has been reached. This means
   *                                    that the set amount of empty lines has been found and the loop broke because that was probably an abnormal amount
   *                                    of empty lines and an infinite loop has to be avoided.
   * @throws IOException                If an I/O error occurred while calling {@link BufferedReader#readLine()}.
   */
  @NotNull
  @Deprecated
  public String readNextNonEmptyLineEOFAsException(boolean verboseException) throws IOException {
    for (int i = 0; i < READ_LOOP_LIMIT; i++) {
      String line = readLine();

      if (line == null) { // If the end of the stream is reached.
        if (verboseException) {
          throw new StreamEndException("The end of the underlying stream has been reached.");
        }
        else {
          throw new StreamEndException();
        }
      }

      if (!line.isEmpty()) {
        return line;
      }
    }

    if (verboseException) {
      throw new TooManyEmptyLinesException(String.format(Locale.US, "Too many empty lines have been read, aborting the reading loop. Limit is %d.", READ_LOOP_LIMIT));
    }
    else {
      throw new TooManyEmptyLinesException();
    }
  }

  /**
   * Reads the next non-empty line, skipping the empty ones up to a limit set by {@link #READ_LOOP_LIMIT} to avoid
   * infinite looping.
   *
   * @return The next non-empty line if there is one, null if the end of the stream or the {@link #READ_LOOP_LIMIT
   * limit} of empty lines has been reached.
   * @throws IOException The I/O error that could occur when calling {@link BufferedReader#readLine()}.
   */
  @Nullable
  public String readNextNonEmptyLine() throws IOException {
    for (int i = 0; i < READ_LOOP_LIMIT; i++) {
      String line = readLine();
      if (line == null) {
        // If the end of the stream is reached, return null
        return null;
      }
      if (!line.isEmpty()) {
        // Return the just found non-empty line.
        return line;
      }
    }
    return null;
  }
}
