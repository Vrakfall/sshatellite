package be.vrakfall.sshatellite.utils.io;

import android.os.Build;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public interface StreamManipulation {
  //==== Static variables ====//

  String TAG = StreamManipulation.class.getSimpleName();

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//

  //==== Lists' CRUDs ====//

  //====== Static Methods ======//

  /**
   * Transforms an InputStream into a String using the standard UTF-8 Charset.
   * Taken from <a href="https://stackoverflow.com/questions/309424/read-convert-an-inputstream-to-a-string">https://stackoverflow.com/questions/309424/read-convert-an-inputstream-to-a-string</a>.
   *
   * @param inputStream The InputStream to transform.
   * @return The corresponding String.
   * @throws IOException In case of issues reading the the InputStream or writing the buffer OutputStream.
   * @see <a href="https://stackoverflow.com/questions/309424/read-convert-an-inputstream-to-a-string">https://stackoverflow.com/questions/309424/read-convert-an-inputstream-to-a-string</a>
   */
  static String inputStreamToString(InputStream inputStream) throws IOException {
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    byte[] buffer = new byte[1024];
    int length;

    while ((length = inputStream.read(buffer)) != -1) {
      output.write(buffer, 0, length);
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      return output.toString(StandardCharsets.UTF_8.name());
    }
    else {
      return output.toString("UTF-8");
    }
  }

  //====== Object Methods ======//

}
