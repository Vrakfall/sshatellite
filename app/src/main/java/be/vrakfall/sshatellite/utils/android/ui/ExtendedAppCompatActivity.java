package be.vrakfall.sshatellite.utils.android.ui;

import android.os.Bundle;
import android.util.TypedValue;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import be.vrakfall.sshatellite.R;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import be.vrakfall.sshatellite.utils.files.FileHierarchy;
import java9.util.stream.StreamSupport;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.val;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public abstract class ExtendedAppCompatActivity extends AppCompatActivity {
  //==== Static variables ====//

  /**
   * Placeholder title in case no default title has been set.
   */
  @NotNull
  private static final String PLACEHOLDER_TITLE = "Activity title";

  //==== > Error messages ====//

  //==== Attributes ====//

  /**
   * Default activity title for when none given during a fragment change.
   */
  @Setter(AccessLevel.PROTECTED)
  @Nullable
  private String defaultTitle;

  private final List<Fragment> fragments = new ArrayList<>();

  //==== Constructors ====//

  //==== Getters and Setters ====//

  @NotNull

  public String getDefaultTitle() {
    if (defaultTitle == null) {
      return PLACEHOLDER_TITLE;
    }
    else {
      return defaultTitle;
    }
  }

  //==== Lists' CRUDs ====//

  protected void addFragment(@NotNull final Fragment fragment) {
    fragments.add(fragment);
  }

  //==== Static Methods ====//

  //==== Object Methods ====//

  protected void onCreate(@Nullable final Bundle savedInstanceState, @NotNull final String defaultTitle) {
    super.onCreate(savedInstanceState);
    setDefaultTitle(defaultTitle);
  }

  /**
   * Get the (possibly localized) application name.
   * Taken from
   * <a href="https://stackoverflow.com/questions/11229219/android-get-application-name-not-package-name">https://stackoverflow.com/questions/11229219/android-get-application-name-not-package-name</a>.
   *
   * @return The (possibly localized) application name.
   */
  @NotNull
  public String getApplicationName() {
    final val applicationInfo = getApplicationInfo();
    final val stringId = applicationInfo.labelRes;
    if (stringId == 0) {
      // The localized name isn't available, return the non-localized one instead.
      return applicationInfo.nonLocalizedLabel.toString();
    }
    else {
      return getString(stringId);
    }
  }

  /**
   * Get the non-localized application name.
   * Taken from
   * <a href="https://stackoverflow.com/questions/11229219/android-get-application-name-not-package-name">https://stackoverflow.com/questions/11229219/android-get-application-name-not-package-name</a>.
   *
   * @return The non-localized application name.
   */
  @NotNull
  public String getNonLocalizedApplicationName() {
    return getApplicationInfo().nonLocalizedLabel.toString();
  }

  public String[] getAssetPathsIn(@NotNull final String assetsParentDirectoryPath) {
    final String[] list;
    try {
      list = getAssets().list(assetsParentDirectoryPath);
    }
    catch (final IOException e) {
      throw new IllegalArgumentException(String.format(Locale.US, "%s is not a right asset directory.",
          assetsParentDirectoryPath), e);
    }

    if (list == null) {
      return new String[0];
    }
    return list;
  }

  @NotNull
  public List<String> getAssetPathsRecursivelyIn(@NotNull final String assetsRootDirectoryPath) {
    final val pathArray = Arrays.asList(getAssetPathsIn(assetsRootDirectoryPath));
    if (pathArray.size() == 0) {
      // This is probably a file or an empty directory
      return pathArray;
    }
    final val endPointPathList = new ArrayList<String>(pathArray.size());
    for (final String path : pathArray) {
      final val fullPath = FileHierarchy.addToFilePath(assetsRootDirectoryPath, path);
      final val subList = getAssetPathsRecursivelyIn(fullPath);
      if (subList.size() == 0) {
        endPointPathList.add(fullPath);
      }
      else {
        endPointPathList.addAll(subList);
      }
    }
    return endPointPathList;
  }

  /**
   * Get an asset file as an {@link InputStream}.
   *
   * @param assetPath The path to the asset, relative to the assets' directory root.
   * @return The file as an {@link InputStream}
   * @throws IOException
   */
  @NotNull
  public InputStream getAsset(@NotNull final String assetPath) throws IOException {
    return getAssets().open(assetPath);
  }

  /**
   * Set the title or a default one if none is specified.
   *
   * @param newTitle The new title to set or null if the default one has to be set back up.
   */
  private void setTitle(@Nullable final String newTitle) {
    if (newTitle != null) {
      Objects.requireNonNull(getSupportActionBar()).setTitle(newTitle);
    }
    else {
      Objects.requireNonNull(getSupportActionBar()).setTitle(getDefaultTitle());
    }
  }

  /**
   * Show a certain fragment. This, for now, assumes each fragment type is only instantiated once and re-used.
   *
   * @param fragmentToShow The fragment to show.
   * @param newTitle       The new bar title. If none is given, the activity's default title will be shown instead.
   */
  protected void showFragment(
      @NotNull final Fragment fragmentToShow,
      @Nullable final String newTitle
  ) {
    // Begin the transaction
    final val transaction = getSupportFragmentManager().beginTransaction();
    // Add all the actions to the transaction

    // Add it if it's not already
    if (!fragmentToShow.isAdded()) {
      // TODO: 4/03/19 Add a tag back
      transaction.add(R.id.fragment_server_mainView, fragmentToShow, null);
    }
    // Show it because it could be hidden
    transaction.show(fragmentToShow);

    // Check each fragment and hide it if needed.
    // This part assumes, for now, there's only one registered fragment of each type.
    StreamSupport.stream(fragments).forEach((Fragment fragment) -> {
      if (!fragment.getClass().equals(fragmentToShow.getClass())) {
        transaction.hide(fragment);
      }
    });

    // Set the title (not in the transaction but it is set right before the commit).
    setTitle(newTitle);

    // Commit the transaction
    transaction.commit();
  }

  protected float applyDimension(final int unit, final float value) {
    return TypedValue.applyDimension(unit, value, getResources().getDisplayMetrics());
  }
}
