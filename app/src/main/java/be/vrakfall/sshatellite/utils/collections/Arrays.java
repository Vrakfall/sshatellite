package be.vrakfall.sshatellite.utils.collections;

public interface Arrays {
  //==== Static variables ====//

  //==== Static Methods ====//

  /**
   * Checks if the array contains the given element. Inspired from `org.bouncycastle.util.Arrays.contains(int[], int)`.
   *
   * @param array   The array to check.
   * @param element The element the presence of which is to be checked.
   * @return True if the array contains the element at least once.
   */
  static boolean contains(Object[] array, Object element) {
    for (Object o : array) {
      if (o.equals(element)) {
        return true;
      }
    }

    return false;
  }

  //==== Object Methods ====//
}
