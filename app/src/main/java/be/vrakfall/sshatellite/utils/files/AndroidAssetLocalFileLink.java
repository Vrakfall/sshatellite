package be.vrakfall.sshatellite.utils.files;

import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.signification.VerifiablePertinence;
import lombok.Builder;
import lombok.Value;
import org.jetbrains.annotations.Contract;

import java.io.File;

@Value
public class AndroidAssetLocalFileLink implements VerifiablePertinence {
  // This "could" actually be a real file but we'll use it as a reference for files that should be ignored in Rx
  // emissions.
  public static final AndroidAssetLocalFileLink NOT_PERTINENT_FILE_LINK = AndroidAssetLocalFileLink.builder()
      .assetPath("sầ,çp!èzrnqà16987$")
      .applicationLocalFile(new File("\\«»¢w»¢aaz=$qzefô;p"))
      .build();

  private final String assetPath;
  private final File applicationLocalFile;

  @Builder
  private AndroidAssetLocalFileLink(String assetPath, File applicationLocalFile) {
    this.assetPath = assetPath;
    this.applicationLocalFile = applicationLocalFile;
  }

  @Override
  @Contract(pure = true)
  public boolean isPertinent() {
    return !this.equals(NOT_PERTINENT_FILE_LINK);
  }
}
