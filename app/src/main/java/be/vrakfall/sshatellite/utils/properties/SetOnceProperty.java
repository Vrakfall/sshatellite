package be.vrakfall.sshatellite.utils.properties;

import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import lombok.*;

@Data
@NoArgsConstructor
public final class SetOnceProperty<T> {
  //==== Static variables ====//

  //==== > Error messages ====//

  //==== Attributes ====//

  @Nullable
  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private T value;

  //==== Inner Classes ====//

  //==== > Interfaces ====//

  //==== > Classes ====//

  //==== Constructors ====//

  public SetOnceProperty(@NotNull final T value) {
    set(value);
  }

  @Nullable
  public T get() {
    return value;
  }

  //==== Getters and Setters ====//

  @SuppressWarnings({"UnusedReturnValue", "WeakerAccess"})
  public boolean set(@NotNull final T value) {
    if (this.value == null) {
      this.value = value;
      return true;
    }
    else {
      return false;
    }
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//
}
