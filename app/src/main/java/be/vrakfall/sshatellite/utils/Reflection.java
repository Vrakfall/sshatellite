package be.vrakfall.sshatellite.utils;

import android.os.Build;
import androidx.annotation.RequiresApi;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.errors.APILevelTooLowException;

import java.lang.reflect.Constructor;
import java.util.Locale;

public interface Reflection {
  //==== Static variables ====//

  //==== > Error message ====//

  String NOT_A_SUBCLASS_ERROR_MESSAGE = "The class %s is not a subclass of %s when it was expected to be.";

  //====== Static Methods ======//

  @RequiresApi(api = Build.VERSION_CODES.O)
  static boolean objectHasAtLeastOneConstructorWithXAmountOfParameters(final Class inspectedClass, final int parameterAmount) throws APILevelTooLowException {
    for (final Constructor<?> constructor :
        inspectedClass.getDeclaredConstructors()) {
      if (constructor.getParameterCount() == parameterAmount) {
        return true;
      }
    }
    return false;
  }

  static void ensureType(@NotNull final Class desiredClass, @NotNull final Object objectToCheck) {
    if (!(desiredClass.isAssignableFrom(objectToCheck.getClass()))) {
      throw new IllegalArgumentException(String.format(Locale.US, NOT_A_SUBCLASS_ERROR_MESSAGE,
          objectToCheck.getClass().getName(), desiredClass.getName()));
    }
  }

  //====== Object Methods ======//
}
