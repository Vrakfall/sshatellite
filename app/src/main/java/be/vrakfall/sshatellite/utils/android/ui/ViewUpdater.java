package be.vrakfall.sshatellite.utils.android.ui;

import android.widget.TextView;
import androidx.databinding.ViewDataBinding;
import be.vrakfall.sshatellite.databinding.PercentageProgressCircleBinding;
import be.vrakfall.sshatellite.utils.Math;
import be.vrakfall.sshatellite.utils.android.ui.fragments.RxRecyclerFragment;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import lombok.val;

import java.util.Locale;

public interface ViewUpdater {
  //==== Static variables ====//

  //==== > Error messages ====//

  String PERCENTAGE_PRECISION_IS_NEGATIVE_ERROR_MESSAGE = "percentagePrecision cannot be negative, received %d.";

  String FLOAT_PRECISION_IS_NEGATIVE_ERROR_MESSAGE = "floatPrecision cannot be negative, received %d.";

  //==== Static Methods ====//

  /**
   * Update a progress circle and it's attached percentage text.
   *
   * @param percentageProgressCircleBinding The Data Binding object of the percentage progress circle to update.
   * @param usagePercentage                 The percentage to set to the whole element.
   */
  static void update(final PercentageProgressCircleBinding percentageProgressCircleBinding,
                     final float usagePercentage, final int percentagePrecision) {
    if (percentagePrecision < 0) {
      throw new IllegalArgumentException(
          String.format(Locale.US, PERCENTAGE_PRECISION_IS_NEGATIVE_ERROR_MESSAGE, percentagePrecision)
      );
    }
    final val format = String.format(Locale.US, "%%.%df%%%%", percentagePrecision);

    // Update the progress circle
    // 100 doesn't come from percentage but from the range of numbers used for the progressBar.
    percentageProgressCircleBinding.progressCircle.setProgress(
        // TODO: 21/01/19 Check the percentage output!
        Math.cropToInteger(java.lang.Math.round(usagePercentage * 100.D))
    );
    // Update the percentage text
    percentageProgressCircleBinding.percentage.setText(
        String.format(Locale.US, format, usagePercentage)
    );
  }

  /**
   * Update a {@link TextView} as if it was a simple float number.
   *
   * @param textView       The {@link TextView} to update.
   * @param number         The number to format and update the {@link TextView} with.
   * @param floatPrecision The float precision wanted for the format.
   */
  static void updateAsFloat(final TextView textView, final float number, final int floatPrecision) {
    if (floatPrecision < 0) {
      throw new IllegalArgumentException(
          String.format(Locale.US, FLOAT_PRECISION_IS_NEGATIVE_ERROR_MESSAGE, floatPrecision)
      );
    }
    final val format = String.format(Locale.US, "%%.%df", floatPrecision);

    textView.setText(String.format(Locale.US, format, number));
  }

  //==== Object Methods ====//

  void updateView(@NotNull final ViewDataBinding binding);

  void updateView(@NotNull final ViewDataBinding binding,
                  @NotNull final RxRecyclerFragment.OnRxRecyclerFragmentInteractionListener listener);
}
