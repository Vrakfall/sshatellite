package be.vrakfall.sshatellite.utils.files;

import be.vrakfall.sshatellite.network.ssh.SFTPEnvironment;
import be.vrakfall.sshatellite.utils.annotations.NotNull;
import be.vrakfall.sshatellite.utils.annotations.Nullable;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.File;
import java.io.IOException;

@ToString
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
public class PythonApplicationFilesManager extends GenericApplicationFilesManager {
  //==== Static variables ====//

  public static final String TAG = PythonApplicationFilesManager.class.getSimpleName();

  public static final String DEFAULT_VENV_PATH = "venv";

  //==== > Error messages ====//

  //==== Attributes ====//

  @NotNull
  @Getter(value = AccessLevel.NONE)
  private String venvPath;

  // TODO: 18/12/18 The logic behind calling this should be abstracted behind interfaces
  @Nullable
  @Getter(AccessLevel.PRIVATE)
  private SFTPEnvironment sftpProxy;

  //==== Constructors ====//

  @Builder
  protected PythonApplicationFilesManager(@Nullable final String operatingSystemName, @Nullable final String root,
                                          @Nullable final String mainExecutable, @Nullable final String venvPath,
                                          @Nullable final SFTPEnvironment sftpProxy) {
    super(operatingSystemName, root, mainExecutable);

    if (venvPath == null || venvPath.isEmpty()) {
      // Default value
      this.venvPath = DEFAULT_VENV_PATH;
    }
    else {
      this.venvPath = ApplicationFilesManager.cleanPathString(venvPath, getOperatingSystemName(), DEFAULT_VENV_PATH);
    }
    this.sftpProxy = sftpProxy;
  }

  //==== Getters and Setters ====//

  @NotNull
  public String getRelativeVenvPath() {
    return venvPath;
  }

  @NotNull
  public String getAbsoluteVenvPath() {
    return getRoot() + File.separatorChar + venvPath;
  }

  //==== Lists' CRUDs ====//

  //==== Static Methods ====//

  //==== Object Methods ====//

  public void createDirectoryAndParents(@NotNull final String directoryPathRelativeToRoot) throws IOException {
    if (sftpProxy != null) {
      sftpProxy.createDirectory(getAbsolutePathOf(directoryPathRelativeToRoot), true);
    }
    else {
      // TODO: 18/12/18 Test this, supposed to create a file locally
      createFileAndParents(new File(getAbsolutePathOf(directoryPathRelativeToRoot)), true);
    }
  }
}
