package be.vrakfall.sshatellite;

import org.joda.time.Duration;

public interface Parameters {
  int PERCENTAGE_MAX = 100;
  int LOAD_AVERAGE_AMOUNT = 3;

  int DEFAULT_COLUMN_AMOUNT = 2;

  // TODO: 10/06/18 Use the gradle variable
  boolean SHOW_DEBUG_INFO = true;
  long SLOWLY_CHANGING_VALUE_CACHE_REFRESH_TIME_IN_MILLISECONDS = Duration.standardHours(1).getMillis();
  long SLOWLY_CHANGING_VALUE_CACHE_EXPIRATION_TIME_IN_MILLISECONDS =
      SLOWLY_CHANGING_VALUE_CACHE_REFRESH_TIME_IN_MILLISECONDS * 2L;
  float DEFAULT_ALERT_PERCENTAGE = 90.F;

  // TODO: 15/11/18 Re-organize the following in a python-related file.
  String DEFAULT_VENV_PATH = "venv";
  String BIN_PATH_IN_VENV = "bin";
  String BIN_PATH_IN_VENV_FROM_PARENT = DEFAULT_VENV_PATH + "/" + BIN_PATH_IN_VENV;
  String PYTHON_BIN_PATH_IN_VENV = BIN_PATH_IN_VENV + "/python";
  String PYTHON_BIN_PATH_IN_VENV_FROM_PARENT = BIN_PATH_IN_VENV_FROM_PARENT + "/python";
  String PIP_BIN_PATH_IN_VENV = BIN_PATH_IN_VENV + "/pip";
  String PIP_BIN_PATH_IN_VENV_FROM_PARENT = BIN_PATH_IN_VENV_FROM_PARENT + "/pip";
  String BACKEND_MAIN_DIRECTORY = "sshatellite_backend";
}
