import inspect


def to_dict(obj):
    """
    Taken from
    https://stackoverflow.com/questions/7963762/what-is-the-most-economical-way-to-convert-nested-python-objects-to-dictionaries
    """
    if isinstance(obj, tuple):
        return list(obj)
    if not hasattr(obj, "__dict__") and not isinstance(obj, dict):
        # What?
        return obj
    result = {}
    if hasattr(obj, "__dict__"):
        obj = vars(obj)
    for key, val in obj.items():
        if not isinstance(obj, dict):  # todo: Check this, it has been changed too much
            if key.startswith("_"):
                continue
        element = []
        if isinstance(val, list):
            for item in val:
                element.append(to_dict(item))
        else:
            element = to_dict(val)
        result[key] = element
    return result


def to_dict_with_properties(object, show_private_members=False):
    def with_private_predicate(member):
        return not (member[0].startswith("__") & member[0].endswith("__"))

    def without_private_predicate(member):
        return not member[0].startswith("_")

    if show_private_members:
        predicate = with_private_predicate
    else:
        predicate = without_private_predicate

    return dict(
        filter(
            predicate,
            inspect.getmembers(object)
        )
    )
