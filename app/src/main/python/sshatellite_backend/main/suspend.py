import sys

import psutil

pid = int(sys.argv[1])
name = sys.argv[2]

process = psutil.Process(pid)

if process.name() == name:
    status = process.status()
    if status == "running":
        process.suspend()
    elif status == 'stopped':
        process.resume()
