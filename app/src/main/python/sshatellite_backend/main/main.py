import datetime
import psutil
import yaml
from os import getloadavg
from time import sleep

from sshatellite_backend.utils import transform_object


class CpuUsage:
    def __init__(self, number, user, nice, system, idle):
        self.number = number
        self.user = user
        self.nice = nice
        self.system = system
        self.idle = idle


class ProcessInfo:
    def __init__(self, id, workingDirectory, command, fullCommand, user, creationTime, weightedCPUUsage):
        self.id = id
        self.workingDirectory = workingDirectory
        self.command = command
        self.fullCommand = fullCommand
        self.user = user
        self.creationTime = creationTime
        self.weightedCPUUsage = weightedCPUUsage


class DiskPartition:
    def __init__(self, path, size, usedBytes, freeBytes):
        self.path = path
        self.size = size
        self.usedBytes = usedBytes
        self.freeBytes = freeBytes


class NetworkInterface:
    def __init__(self, name, sentByteAmount, receivedByteAmount):
        self.name = name
        self.sentByteAmount = sentByteAmount
        self.receivedByteAmount = receivedByteAmount


class MachineState:
    def __init__(self, cpuAmount, processesTotal, loadAverages, memoryUsagePercentage, swapUsagePercentage,
                 localDateTime, networkInterfaces=None, cpuUsages=None, processesDetails=None, diskPartitions=None):
        if networkInterfaces is None:
            networkInterfaces = {}
        if diskPartitions is None:
            diskPartitions = []
        if processesDetails is None:
            processesDetails = {}
        if cpuUsages is None:
            cpuUsages = []
        self.cpuAmount = cpuAmount
        self.processesTotal = processesTotal
        self.loadAverages = loadAverages
        self.cpuUsages = cpuUsages
        self.memoryUsagePercentage = memoryUsagePercentage
        self.processesDetails = processesDetails
        self.runningProcessesAmount = 0
        self.swapUsagePercentage = swapUsagePercentage
        self.diskPartitions = diskPartitions
        self.localDateTime = localDateTime
        self.networkInterfaces = networkInterfaces


machineState = MachineState(
    cpuAmount=psutil.cpu_count(),
    processesTotal=len(psutil.pids()),
    loadAverages=getloadavg(),
    memoryUsagePercentage=psutil.virtual_memory().used / psutil.virtual_memory().total * 100,
    swapUsagePercentage=psutil.swap_memory().percent,
    localDateTime=datetime.datetime.now().isoformat()
)

# Add cpu usages to the machineState
i = 0
# for (user, nice, system, idle, irq) in psutil.cpu_times_percent(
for cpuTime in psutil.cpu_times_percent(
        interval=1, percpu=True):
    machineState.cpuUsages.append(CpuUsage(i, cpuTime.user, cpuTime.nice, cpuTime.system, cpuTime.idle))
    i += 1

for process in psutil.process_iter():
    try:
        process.as_dict(attrs=['cpu_percent'])  # Needed in order to not get 0 at the first call
    except psutil.NoSuchProcess:
        pass

sleep(1)

# Add the list of processes to the machineState
for process in psutil.process_iter():
    try:
        details = process.as_dict(attrs=['pid', 'cwd', 'name', 'cmdline', 'username', 'create_time', 'cpu_percent'])
        # print(tuple(details))
        if process.status() == psutil.STATUS_RUNNING:
            machineState.runningProcessesAmount += 1
        machineState.processesDetails[details['pid']] =\
            ProcessInfo(details['pid'], details['cwd'], details['name'], ' '.join(details['cmdline']), details['username'],
                        creationTime=datetime.datetime.fromtimestamp(details['create_time']).isoformat(),
                        weightedCPUUsage=details['cpu_percent'])
    except psutil.NoSuchProcess:
        pass

# Add the disk partitions to the machineState
for psPartition in psutil.disk_partitions(False):
    try:
        path = psPartition.device
        # Don't add a partition already added
        if any(p for p in machineState.diskPartitions if p.path == path):
            continue
        diskUsage = psutil.disk_usage(path)
        machineState.diskPartitions.append(
            DiskPartition(psPartition.device, diskUsage.total, diskUsage.used, diskUsage.free)
        )

    except FileNotFoundError:
        pass

# Add the info about the network interfaces
interfacesStates = psutil.net_io_counters(pernic=True)
for interfaceName in interfacesStates:
    interfaceState = interfacesStates[interfaceName]
    machineState.networkInterfaces[interfaceName] = NetworkInterface(
        name=interfaceName, sentByteAmount=interfaceState.bytes_sent, receivedByteAmount=interfaceState.bytes_recv
    )

# Serialize the machineState to YAML and print it at the output
print(yaml.dump(transform_object.to_dict(machineState), default_flow_style=False))
