# Things to remember to do before running scripts on the server:

- Upgrade pip
    
    ```bash
    pip install --upgrade pip
    ```

- Install RxPY

    ```bash
    pip install rx
    ```

Wouldn't it be possible to just pre-update/install the `venv`?